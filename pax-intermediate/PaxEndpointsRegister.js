"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaxEndpointsRegister = void 0;
const _ = __importStar(require("logger"));
const PaxResponse_1 = require("./PaxResponse");
class PaxEndpointsRegister {
    constructor(application, liquid) {
        this.handle = (domain, namespace, req, res, next) => {
            _.trace(`[RF:E] Trying to handle a request for ${req.hostname} (${req.originalUrl})`);
            let found = false;
            for (const entry of this.endpoints) {
                if (entry.type === req.method && entry.query.test(req.originalUrl)) {
                    _.trace(`[RF:E] Found and entry containing ${entry.handlers.length} handlers, executing all of them`);
                    for (const handler of entry.handlers) {
                        if (res.headersSent) {
                            _.trace(`[RF:E] Skipping handler function ${handler.func.name} because headers are already sent`);
                            continue;
                        }
                        const resp = new PaxResponse_1.PaxResponse(res, this.liquid, req);
                        _.info(`${req.originalUrl} on ${req.hostname} handled by PaxEndpoint`);
                        _.trace(`[RF:E] Executing handler function ${handler.func.name}`);
                        handler.func.call(handler.thisArg, req, resp, domain, namespace);
                        found = true;
                    }
                }
            }
            if (!found) {
                _.trace(`[RF:E] Did not find any handlers for the request ${req.hostname} (${req.originalUrl}) so passing to "next"`);
                next();
            }
        };
        this.endpoints = [];
        this.application = application;
        this.liquid = liquid;
    }
    add(endpoint, type, handler, arg) {
        _.trace(`adding endpoint ${endpoint} for ${type} using a this argument of type ${typeof (arg)}`);
        const innerHandler = { func: handler, thisArg: arg };
        let found = false;
        for (const entry of this.endpoints) {
            if (entry.type === type && String(entry.query) === String(endpoint)) {
                _.trace(`Handler function set exists for ${endpoint}, pushing the new`);
                _.trace(`    Endpoint: ${endpoint}`);
                _.trace(`        Type: ${type}`);
                _.trace(`     Handler: ${handler.name}`);
                _.trace(`    Arg Type: ${typeof (arg)}`);
                _.trace(`    Existing: ${entry.handlers.length}`);
                entry.handlers.push(innerHandler);
                found = true;
            }
        }
        if (!found) {
            _.trace(`Handler function does not exist for ${endpoint}, adding a new one`);
            _.trace(`    Endpoint: ${endpoint}`);
            _.trace(`        Type: ${type}`);
            _.trace(`     Handler: ${handler.name}`);
            _.trace(`    Arg Type: ${typeof (arg)}`);
            this.endpoints.push({
                type,
                query: typeof (endpoint) === 'string' ? RegExp(`^${endpoint}$`) : endpoint,
                handlers: [innerHandler],
            });
        }
    }
    delete(endpoint, handler, arg) {
        this.add(endpoint, 'DELETE', handler, arg);
    }
    get(endpoint, handler, arg) {
        this.add(endpoint, 'GET', handler, arg);
    }
    head(endpoint, handler, arg) {
        this.add(endpoint, 'HEAD', handler, arg);
    }
    post(endpoint, handler, arg) {
        this.add(endpoint, 'POST', handler, arg);
    }
    put(endpoint, handler, arg) {
        this.add(endpoint, 'PUT', handler, arg);
    }
}
exports.PaxEndpointsRegister = PaxEndpointsRegister;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF4RW5kcG9pbnRzUmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcGF4LWludGVybWVkaWF0ZS9QYXhFbmRwb2ludHNSZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMENBQTRCO0FBSTVCLCtDQUE0QztBQUs1QyxNQUFhLG9CQUFvQjtJQUs3QixZQUFtQixXQUF3QixFQUFFLE1BQWM7UUFNcEQsV0FBTSxHQUFHLENBQUMsTUFBb0IsRUFBRSxTQUF1QixFQUFFLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBYyxFQUFFLEVBQUU7WUFDM0csQ0FBQyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUN0RixJQUFJLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbEIsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQyxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssR0FBRyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQ2hFLENBQUMsQ0FBQyxLQUFLLENBQUMscUNBQXFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxrQ0FBa0MsQ0FBQyxDQUFDO29CQUN0RyxLQUFLLE1BQU0sT0FBTyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUU7d0JBQ2xDLElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRTs0QkFDakIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLG1DQUFtQyxDQUFDLENBQUM7NEJBQ2xHLFNBQVM7eUJBQ1o7d0JBRUQsTUFBTSxJQUFJLEdBQUcsSUFBSSx5QkFBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO3dCQUNwRCxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsT0FBTyxHQUFHLENBQUMsUUFBUSx5QkFBeUIsQ0FBQyxDQUFDO3dCQUN2RSxDQUFDLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ2xFLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7d0JBQ2pFLEtBQUssR0FBRyxJQUFJLENBQUM7cUJBQ2hCO2lCQUNKO2FBQ0o7WUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNSLENBQUMsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELEdBQUcsQ0FBQyxRQUFRLEtBQUssR0FBRyxDQUFDLFdBQVcsd0JBQXdCLENBQUMsQ0FBQztnQkFDdEgsSUFBSSxFQUFFLENBQUM7YUFDVjtRQUNMLENBQUMsQ0FBQztRQTlCRSxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBNkJNLEdBQUcsQ0FBQyxRQUF5QixFQUFFLElBQVksRUFBRSxPQUFnRyxFQUFFLEdBQVE7UUFDMUosQ0FBQyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsUUFBUSxRQUFRLElBQUksa0NBQWtDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDakcsTUFBTSxZQUFZLEdBQUcsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNyRCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbEIsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2pFLENBQUMsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLFFBQVEsbUJBQW1CLENBQUMsQ0FBQztnQkFDeEUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsUUFBUSxFQUFFLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDakMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztnQkFFbEQsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDaEI7U0FDSjtRQUVELElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUixDQUFDLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxRQUFRLG9CQUFvQixDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNyQyxDQUFDLENBQUMsS0FBSyxDQUFDLGlCQUFpQixJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ2pDLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLElBQUk7Z0JBQ0osS0FBSyxFQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7Z0JBQzFFLFFBQVEsRUFBRSxDQUFDLFlBQVksQ0FBQzthQUMzQixDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsUUFBeUIsRUFBRSxPQUFnRyxFQUFFLEdBQVE7UUFDL0ksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU0sR0FBRyxDQUFDLFFBQXlCLEVBQUUsT0FBZ0csRUFBRSxHQUFRO1FBQzVJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVNLElBQUksQ0FBQyxRQUF5QixFQUFFLE9BQWdHLEVBQUUsR0FBUTtRQUM3SSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFTSxJQUFJLENBQUMsUUFBeUIsRUFBRSxPQUFnRyxFQUFFLEdBQVE7UUFDN0ksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRU0sR0FBRyxDQUFDLFFBQXlCLEVBQUUsT0FBZ0csRUFBRSxHQUFRO1FBQzVJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUNKO0FBMUZELG9EQTBGQyJ9