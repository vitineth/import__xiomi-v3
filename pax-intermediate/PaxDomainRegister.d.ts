import { Application } from 'express';
import { PaxNamespaceRegister } from './PaxNamespaceRegister';
import { Liquid } from 'liquidjs';
export declare class DomainBundle {
    private _offset;
    private _query;
    private _handler;
    constructor(offset: number, query: RegExp, handler: PaxNamespaceRegister);
    get offset(): number;
    set offset(value: number);
    get query(): RegExp;
    set query(value: RegExp);
    get handler(): PaxNamespaceRegister;
    set handler(value: PaxNamespaceRegister);
}
export declare class PaxDomainRegister {
    private liquid;
    private application;
    private domains;
    constructor(application: Application, liquid: Liquid);
    /**
     * Registers the given domain to this application handler. If the query is a string it will be converted to regex
     * like ^query$. On a second submission of the same query (NOT EQUIVALENT REGEXES), it will add the handler to the
     * set of handlers for this domain.
     * If forceQuery is false, the query and offset will become localhost when the config environment is dev.
     * @param query the query to add
     * @param offset the offset (components) from the full domain (eg xiomi.org = 2, localhost = 1)
     * @param handler the handler function to be called when this domain is requested
     * @param forceQuery if the given query should be forced to be inserted rather than using standard substitution
     */
    register(query: string | RegExp, offset: number, handler: ((instance: PaxNamespaceRegister) => void), forceQuery?: boolean): void;
    private handle;
}
