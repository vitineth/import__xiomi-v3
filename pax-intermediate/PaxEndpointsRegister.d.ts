import { Application, Request, Response } from 'express';
import { DomainBundle } from './PaxDomainRegister';
import { PrefixBundle } from './PaxNamespaceRegister';
import { PaxResponse } from './PaxResponse';
import { Liquid } from 'liquidjs';
export declare type EndpointFunction = (endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any) => void;
export declare class PaxEndpointsRegister {
    private liquid;
    private application;
    private endpoints;
    constructor(application: Application, liquid: Liquid);
    handle: (domain: DomainBundle, namespace: PrefixBundle, req: Request, res: Response, next: Function) => void;
    add(endpoint: string | RegExp, type: string, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
    delete(endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
    get(endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
    head(endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
    post(endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
    put(endpoint: string | RegExp, handler: (req: Request, res: PaxResponse, domain: DomainBundle, namepsace: PrefixBundle) => void, arg: any): void;
}
