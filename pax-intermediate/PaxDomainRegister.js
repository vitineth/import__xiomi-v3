"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaxDomainRegister = exports.DomainBundle = void 0;
const uuid_1 = require("uuid");
const PaxNamespaceRegister_1 = require("./PaxNamespaceRegister");
const _ = __importStar(require("logger"));
const config_1 = require("../config/config");
class DomainBundle {
    constructor(offset, query, handler) {
        this._offset = offset;
        this._query = query;
        this._handler = handler;
    }
    get offset() {
        return this._offset;
    }
    set offset(value) {
        this._offset = value;
    }
    get query() {
        return this._query;
    }
    set query(value) {
        this._query = value;
    }
    get handler() {
        return this._handler;
    }
    set handler(value) {
        this._handler = value;
    }
}
exports.DomainBundle = DomainBundle;
class PaxDomainRegister {
    constructor(application, liquid) {
        this.handle = (req, res, next) => {
            _.debug(`[RF:D] Trying to handle request for ${req.hostname} (${req.originalUrl})`);
            let handled = false;
            const keys = Object.keys(this.domains);
            for (const key of keys) {
                const entries = this.domains[key];
                if (entries.query.test(req.hostname)) {
                    _.debug(`[RF:D] Request for hostname ${req.hostname} (${req.originalUrl}) is handled by ${entries.query}`);
                    entries.handler.handle(entries, req, res, next);
                    handled = true;
                    break;
                }
            }
            if (!handled) {
                _.debug(`[RF:D] Could not find a domain register for ${req.hostname} (${req.originalUrl}) so passing to "next"`);
                next();
            }
        };
        this.domains = {};
        this.application = application;
        this.liquid = liquid;
        application.use(this.handle);
    }
    /**
     * Registers the given domain to this application handler. If the query is a string it will be converted to regex
     * like ^query$. On a second submission of the same query (NOT EQUIVALENT REGEXES), it will add the handler to the
     * set of handlers for this domain.
     * If forceQuery is false, the query and offset will become localhost when the config environment is dev.
     * @param query the query to add
     * @param offset the offset (components) from the full domain (eg xiomi.org = 2, localhost = 1)
     * @param handler the handler function to be called when this domain is requested
     * @param forceQuery if the given query should be forced to be inserted rather than using standard substitution
     */
    register(query, offset, handler, forceQuery = false) {
        let realQuery = query;
        let realOffset = offset;
        if (!forceQuery && config_1.has('environment') && config_1.get('environment') === 'dev') {
            realQuery = '(.*\\.)?ryan.ax370';
            realOffset = 2;
            _.debug(`A module tried to register domain ${query} with offset ${offset} but due to dev mode it has been rewritten to ${realQuery} with offset ${realOffset}`);
        }
        const keys = Object.keys(this.domains);
        for (const key of keys) {
            const entries = this.domains[key];
            // Compare it against the regex version as well, fucked this up the first time
            if (String(entries.query) === String(realQuery) || String(entries.query) === String(RegExp(`^${realQuery}$`))) {
                _.trace(`Namespace already exists for ${realQuery}, recalling the old one`);
                _.trace(`    Offset: ${realOffset}`);
                _.trace(`     Query: ${realQuery}`);
                _.trace(`        ID: ${key}`);
                handler(entries.handler);
                return;
            }
        }
        const id = uuid_1.v4();
        _.trace(`Namespace does not exist for ${realQuery}, creating a new one`);
        _.trace(`    Offset: ${realOffset}`);
        _.trace(`     Query: ${realQuery}`);
        _.trace(`        ID: ${id}`);
        const namespace = new PaxNamespaceRegister_1.PaxNamespaceRegister(this.application, this.liquid);
        this.domains[id] = new DomainBundle(realOffset, typeof (realQuery) === 'string' ? RegExp(`^${realQuery}$`) : realQuery, namespace);
        console.log(`[Application] Overwriting for ${realQuery}`);
        handler(namespace);
    }
}
exports.PaxDomainRegister = PaxDomainRegister;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF4RG9tYWluUmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcGF4LWludGVybWVkaWF0ZS9QYXhEb21haW5SZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsK0JBQTBCO0FBQzFCLGlFQUE4RDtBQUM5RCwwQ0FBNEI7QUFFNUIsNkNBQTRDO0FBRTVDLE1BQWEsWUFBWTtJQUtyQixZQUFZLE1BQWMsRUFBRSxLQUFhLEVBQUUsT0FBNkI7UUFDcEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7SUFDNUIsQ0FBQztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBSSxNQUFNLENBQUMsS0FBYTtRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBRUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLEtBQUssQ0FBQyxLQUFhO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELElBQUksT0FBTyxDQUFDLEtBQTJCO1FBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Q0FDSjtBQWxDRCxvQ0FrQ0M7QUFFRCxNQUFhLGlCQUFpQjtJQUsxQixZQUFtQixXQUF3QixFQUFFLE1BQWM7UUF5RG5ELFdBQU0sR0FBRyxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBYyxFQUFFLEVBQUU7WUFDN0QsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUVwRixJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDcEIsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkMsS0FBSyxNQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7Z0JBQ3BCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQyxDQUFDLENBQUMsS0FBSyxDQUFDLCtCQUErQixHQUFHLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxXQUFXLG1CQUFtQixPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztvQkFFM0csT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ2hELE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQ2YsTUFBTTtpQkFDVDthQUNKO1lBRUQsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixDQUFDLENBQUMsS0FBSyxDQUFDLCtDQUErQyxHQUFHLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxXQUFXLHdCQUF3QixDQUFDLENBQUM7Z0JBQ2pILElBQUksRUFBRSxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUE7UUE1RUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFckIsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7Ozs7Ozs7T0FTRztJQUNJLFFBQVEsQ0FBQyxLQUFzQixFQUFFLE1BQWMsRUFBRSxPQUFtRCxFQUFFLGFBQXNCLEtBQUs7UUFDcEksSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUV4QixJQUFJLENBQUMsVUFBVSxJQUFJLFlBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxZQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxFQUFFO1lBQ25FLFNBQVMsR0FBRyxvQkFBb0IsQ0FBQztZQUNqQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBRWYsQ0FBQyxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsS0FBSyxnQkFBZ0IsTUFBTSxpREFBaUQsU0FBUyxnQkFBZ0IsVUFBVSxFQUFFLENBQUMsQ0FBQztTQUNuSztRQUVELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZDLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO1lBQ3BCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbEMsOEVBQThFO1lBQzlFLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUMzRyxDQUFDLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxTQUFTLHlCQUF5QixDQUFDLENBQUM7Z0JBQzVFLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxVQUFVLEVBQUUsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsU0FBUyxFQUFFLENBQUMsQ0FBQztnQkFDcEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBRTlCLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3pCLE9BQU87YUFDVjtTQUNKO1FBRUQsTUFBTSxFQUFFLEdBQUcsU0FBRSxFQUFFLENBQUM7UUFFaEIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsU0FBUyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3pFLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRTdCLE1BQU0sU0FBUyxHQUFHLElBQUksMkNBQW9CLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ25JLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDMUQsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7Q0F1Qko7QUFuRkQsOENBbUZDIn0=