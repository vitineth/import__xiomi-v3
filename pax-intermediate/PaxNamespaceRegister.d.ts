import { Application, Request, Response } from 'express';
import { DomainBundle } from './PaxDomainRegister';
import { PaxEndpointsRegister } from './PaxEndpointsRegister';
import { Liquid } from 'liquidjs';
export declare class PrefixBundle {
    private _subdomains;
    private _handler;
    constructor(subdomains: RegExp, handler: PaxEndpointsRegister);
    get subdomains(): RegExp;
    set subdomains(value: RegExp);
    get handler(): PaxEndpointsRegister;
    set handler(value: PaxEndpointsRegister);
}
export declare class PaxNamespaceRegister {
    private liquid;
    private application;
    private namespaces;
    constructor(application: Application, liquid: Liquid);
    namespace(query: string | RegExp, handler: ((instance: PaxEndpointsRegister) => void)): void;
    handle: (domain: DomainBundle, req: Request, res: Response, next: Function) => void;
}
