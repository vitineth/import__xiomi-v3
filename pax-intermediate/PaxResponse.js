"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaxResponse = void 0;
const fs_1 = require("fs");
const path_1 = require("path");
const ProcessImpl_1 = require("../casvii/actions/ProcessImpl");
const _ = __importStar(require("logger"));
const config_1 = require("../config/config");
class PaxResponse {
    constructor(expressResponse, liquid, request) {
        this.expressResponse = expressResponse;
        this.liquid = liquid;
        this.request = request;
        this.listeners = {};
    }
    constructData(data) {
        const inbuiltData = {
            csrf: this.request.csrfToken(),
        };
        return Object.assign(inbuiltData, data);
    }
    get _raw() {
        return this.expressResponse;
    }
    get _request() {
        return this.request;
    }
    get app() {
        return this.expressResponse.app;
    }
    get headersSent() {
        return this.expressResponse.headersSent;
    }
    get locals() {
        return this.expressResponse.locals;
    }
    /**
     * Verifiers if the headers have already been sent. If they have it will raise an error unless
     * silent-duplicate-headers is true in the configuration. If the function returns true the headers have already
     * been sent
     * @return if the headers are already sent and the action should be cancelled
     */
    verifyHeaders() {
        if (this._raw.headersSent) {
            _.warn('Tried to send headers again!');
            if (config_1.has('silent-duplicate-headers') && config_1.get('silent-duplicate-headers') === true) {
                _.warn('Making this error silent because that is expressed in the config');
                _.warn(`I'm going to trust you but be careful`);
                return true;
            }
            throw new Error('Headers were already sent');
        }
        return false;
    }
    notify(eventName, data) {
        if (this.listeners.hasOwnProperty(eventName)) {
            for (const executor of this.listeners[eventName])
                executor(eventName, data);
        }
        if (this.listeners.hasOwnProperty('*')) {
            for (const executor of this.listeners['*'])
                executor(eventName, data);
        }
    }
    listenFor(eventName, listener) {
        if (this.listeners.hasOwnProperty(eventName)) {
            this.listeners[eventName].push(listener);
        }
        else {
            this.listeners[eventName] = [listener];
        }
    }
    append(field, value) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.append(field, value);
        this.notify('append', { field, value });
        return this;
    }
    attachment(filename) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.attachment(filename);
        this.notify('attachment', { filename });
        return this;
    }
    cookie(name, value, options) {
        if (this.verifyHeaders())
            return this;
        if (options !== undefined) {
            this.expressResponse.cookie(name, value, options);
        }
        else {
            this.expressResponse.cookie(name, value);
        }
        this.notify('cookie', { name, value, options });
        return this;
    }
    clearCookie(name, options) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.clearCookie(name, options);
        this.notify('clearCookie', { name, options });
        return this;
    }
    download(path, filename, fn) {
        if (this.verifyHeaders())
            return;
        if (filename === undefined) {
            if (fn === undefined) {
                this.expressResponse.download(path);
            }
            else {
                this.expressResponse.download(path, fn);
            }
        }
        else {
            if (fn === undefined) {
                this.expressResponse.download(path, filename);
            }
            else {
                this.expressResponse.download(path, filename, fn);
            }
        }
        this.notify('download', { path, filename, fn });
    }
    end(cb) {
        if (this.verifyHeaders())
            return;
        this.expressResponse.end(cb);
        this.notify('end', { cb });
    }
    format(object) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.format(object);
        this.notify('format', { object });
        return this;
    }
    get(field) {
        const result = this.expressResponse.get(field);
        this.notify('get', { field, result });
        return result;
    }
    header(field, value) {
        if (value === undefined) {
            return this.get(field);
        }
        if (this.verifyHeaders())
            return this;
        return this.set(field, value);
    }
    json(body) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.json(body);
        this.notify('json', { body });
        return this;
    }
    jsonp(body) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.jsonp(body);
        this.notify('jsonp', { body });
        return this;
    }
    links(links) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.links(links);
        this.notify('links', { links });
        return this;
    }
    location(path) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.location(path);
        this.notify('location', { path });
        return this;
    }
    redirect(url, status) {
        if (this.verifyHeaders())
            return;
        if (status === undefined) {
            this.expressResponse.redirect(url);
        }
        else {
            this.expressResponse.redirect(url, status);
        }
        this.notify('redirect', { url, status });
    }
    async render(target, data, options, alternateLiquid) {
        if (this.verifyHeaders())
            return this;
        const liquidInstance = alternateLiquid === undefined ? this.liquid : alternateLiquid;
        const template = await liquidInstance.parse(target);
        const realData = this.constructData(data === undefined ? {} : data);
        const html = await liquidInstance.render(template, realData, options);
        this.notify('render', { target, options, html, data: realData });
        return this.send(html);
    }
    async renderFile(file, data, alternateLiquid, options) {
        if (this.verifyHeaders())
            return this;
        const liquidInstance = alternateLiquid === undefined ? this.liquid : alternateLiquid;
        const template = await liquidInstance.parseFile(file, options);
        const realData = this.constructData(data === undefined ? {} : data);
        const html = await liquidInstance.render(template, realData, options);
        this.notify('render-file', { file, options, html, data: realData });
        return this.send(html);
    }
    send(body) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.send(body);
        this.notify('send', { body });
        return this;
    }
    sendFile(path, options, fn) {
        if (this.verifyHeaders())
            return;
        if (options === undefined) {
            if (fn === undefined) {
                this.expressResponse.sendFile(path);
            }
            else {
                this.expressResponse.sendFile(path, fn);
            }
        }
        else {
            if (fn === undefined) {
                this.expressResponse.sendFile(path, options);
            }
            else {
                this.expressResponse.sendFile(path, options, fn);
            }
        }
        this.notify('sendFile', { path, options, fn });
    }
    sendStatus(statusCode) {
        if (this.verifyHeaders())
            return this;
        this.notify('sendStatus', { statusCode });
        if (statusCode === ProcessImpl_1.HttpStatusCode.NOT_FOUND)
            return this.notFound();
        if (statusCode === ProcessImpl_1.HttpStatusCode.UNAUTHORIZED)
            return this.unauthorized();
        if (statusCode === ProcessImpl_1.HttpStatusCode.FORBIDDEN)
            return this.forbidden();
        this.expressResponse.sendStatus(statusCode);
        return this;
    }
    set(field, value) {
        if (this.verifyHeaders())
            return this;
        if (value === undefined) {
            this.expressResponse.set(field);
        }
        else {
            // This is kind of stupid but it is required for the TS typechecking to determine
            // which function to call
            if (Array.isArray(value)) {
                this.expressResponse.set(field, value);
            }
            else {
                this.expressResponse.set(field, value);
            }
        }
        this.notify('set', { field, value });
        return this;
    }
    status(code) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.status(code);
        this.notify('status', { code });
        return this;
    }
    type(type) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.type(type);
        this.notify('type', { type });
        return this;
    }
    vary(field) {
        if (this.verifyHeaders())
            return this;
        this.expressResponse.vary(field);
        this.notify('vary', { field });
        return this;
    }
    sendTemplate(code, path, fallback) {
        fs_1.readFile(path, { encoding: 'utf8' }, (err, data) => {
            if (err) {
                _.error(`Failed to render template`);
                console.error(err);
                this.status(code).send(fallback);
                return;
            }
            this.status(code).send(data);
        });
        return this;
    }
    /**
     * Renders the 401.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    unauthorized() {
        return this.sendTemplate(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED, path_1.join(__dirname, '..', '..', 'files', 'templates', '401.html'), '401 Unauthorized<br><br>Additionally, in trying to load the 401 page, an internal error occurred');
    }
    /**
     * Renders the 403.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    forbidden() {
        return this.sendTemplate(ProcessImpl_1.HttpStatusCode.FORBIDDEN, path_1.join(__dirname, '..', '..', 'files', 'templates', '403.html'), '403 Forbidden<br><br>Additionally, in trying to load the 403 page, an internal error occurred');
    }
    /**
     * Renders the 404.html template, falling back to a basic html response if the file fails to load
     * @return {PaxResponse} this response
     */
    notFound() {
        console.log(path_1.join(__dirname, '..', '..', 'files', 'templates', '404.html'));
        return this.sendTemplate(ProcessImpl_1.HttpStatusCode.NOT_FOUND, path_1.join(__dirname, '..', '..', 'files', 'templates', '404.html'), '404 Not Found<br><br>Additionally, in trying to load the 404 page, an internal error occurred');
    }
}
exports.PaxResponse = PaxResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF4UmVzcG9uc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcGF4LWludGVybWVkaWF0ZS9QYXhSZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsMkJBQThCO0FBQzlCLCtCQUE0QjtBQUc1QiwrREFBK0Q7QUFDL0QsMENBQTRCO0FBQzVCLDZDQUF5RDtBQUV6RCxNQUFhLFdBQVc7SUFPcEIsWUFBWSxlQUF5QixFQUFFLE1BQWMsRUFBRSxPQUFnQjtRQUNuRSxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztRQUN2QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRU8sYUFBYSxDQUFDLElBQVk7UUFDOUIsTUFBTSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFO1NBQ2pDLENBQUM7UUFDRixPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxJQUFJLElBQUk7UUFDSixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDaEMsQ0FBQztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBSSxHQUFHO1FBQ0gsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQztJQUNwQyxDQUFDO0lBRUQsSUFBSSxXQUFXO1FBQ1gsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQztJQUM1QyxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQztJQUN2QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSyxhQUFhO1FBQ2pCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBRXZDLElBQUksWUFBRyxDQUFDLDBCQUEwQixDQUFDLElBQUksWUFBUyxDQUFDLDBCQUEwQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUNuRixDQUFDLENBQUMsSUFBSSxDQUFDLGtFQUFrRSxDQUFDLENBQUM7Z0JBQzNFLENBQUMsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztnQkFDaEQsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztTQUNoRDtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFTyxNQUFNLENBQUMsU0FBaUIsRUFBRSxJQUE0QjtRQUMxRCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQzFDLEtBQUssTUFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7Z0JBQUUsUUFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUMvRTtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDcEMsS0FBSyxNQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztnQkFBRSxRQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3pFO0lBQ0wsQ0FBQztJQUVNLFNBQVMsQ0FBQyxTQUFpQixFQUFFLFFBQThEO1FBQzlGLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMxQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsS0FBYSxFQUFFLEtBQXlCO1FBQ2xELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRXRDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxVQUFVLENBQUMsUUFBaUI7UUFDL0IsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxNQUFNLENBQUMsSUFBWSxFQUFFLEtBQVUsRUFBRSxPQUF1QjtRQUMzRCxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUV0QyxJQUFJLE9BQU8sS0FBSyxTQUFTLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNyRDthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDaEQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLFdBQVcsQ0FBQyxJQUFZLEVBQUUsT0FBdUI7UUFDcEQsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDOUMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLFFBQVEsQ0FBQyxJQUFZLEVBQUUsUUFBaUIsRUFBRSxFQUFZO1FBQ3pELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU87UUFFakMsSUFBSSxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ3hCLElBQUksRUFBRSxLQUFLLFNBQVMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQzNDO1NBQ0o7YUFBTTtZQUNILElBQUksRUFBRSxLQUFLLFNBQVMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQ2pEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDckQ7U0FDSjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSxHQUFHLENBQUMsRUFBZTtRQUN0QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPO1FBRWpDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRU0sTUFBTSxDQUFDLE1BQVc7UUFDckIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxHQUFHLENBQUMsS0FBYTtRQUNwQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFTSxNQUFNLENBQUMsS0FBYSxFQUFFLEtBQXlCO1FBQ2xELElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtZQUNyQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDMUI7UUFFRCxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUV0QyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFTSxJQUFJLENBQUMsSUFBVTtRQUNsQixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUV0QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDOUIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFVO1FBQ25CLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRXRDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUMvQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sS0FBSyxDQUFDLEtBQVU7UUFDbkIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxRQUFRLENBQUMsSUFBWTtRQUN4QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUV0QyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDbEMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLFFBQVEsQ0FBQyxHQUFXLEVBQUUsTUFBZTtRQUN4QyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPO1FBRWpDLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUN0QixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN0QzthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRU0sS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFjLEVBQUUsSUFBYSxFQUFFLE9BQXVCLEVBQUUsZUFBd0I7UUFDaEcsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsTUFBTSxjQUFjLEdBQUcsZUFBZSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDO1FBQ3JGLE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwRCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEUsTUFBTSxJQUFJLEdBQUcsTUFBTSxjQUFjLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFFdEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNqRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVNLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBWSxFQUFFLElBQWEsRUFBRSxlQUF3QixFQUFFLE9BQXVCO1FBQ2xHLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRXRDLE1BQU0sY0FBYyxHQUFHLGVBQWUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQztRQUNyRixNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9ELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRSxNQUFNLElBQUksR0FBRyxNQUFNLGNBQWMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUV0RSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU0sSUFBSSxDQUFDLElBQVU7UUFDbEIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzlCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxRQUFRLENBQUMsSUFBWSxFQUFFLE9BQVksRUFBRSxFQUFXO1FBQ25ELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU87UUFFakMsSUFBSSxPQUFPLEtBQUssU0FBUyxFQUFFO1lBQ3ZCLElBQUksRUFBRSxLQUFLLFNBQVMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQzNDO1NBQ0o7YUFBTTtZQUNILElBQUksRUFBRSxLQUFLLFNBQVMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2hEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDcEQ7U0FDSjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFTSxVQUFVLENBQUMsVUFBa0I7UUFDaEMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBRTFDLElBQUksVUFBVSxLQUFLLDRCQUFjLENBQUMsU0FBUztZQUFFLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BFLElBQUksVUFBVSxLQUFLLDRCQUFjLENBQUMsWUFBWTtZQUFFLE9BQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzNFLElBQUksVUFBVSxLQUFLLDRCQUFjLENBQUMsU0FBUztZQUFFLE9BQU8sSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBRXJFLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxHQUFHLENBQUMsS0FBYSxFQUFFLEtBQXlCO1FBQy9DLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRXRDLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtZQUNyQixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuQzthQUFNO1lBQ0gsaUZBQWlGO1lBQ2pGLHlCQUF5QjtZQUN6QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQzthQUMxQztpQkFBTTtnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDMUM7U0FDSjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDckMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxJQUFZO1FBQ3RCLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRXRDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNoQyxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sSUFBSSxDQUFDLElBQVk7UUFDcEIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFFdEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzlCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxJQUFJLENBQUMsS0FBYTtRQUNyQixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztRQUV0QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDL0IsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVPLFlBQVksQ0FBQyxJQUFZLEVBQUUsSUFBWSxFQUFFLFFBQWdCO1FBQzdELGFBQVEsQ0FBQyxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUU7WUFDL0MsSUFBSSxHQUFHLEVBQUU7Z0JBQ0wsQ0FBQyxDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDakMsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksWUFBWTtRQUNmLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FDcEIsNEJBQWMsQ0FBQyxZQUFZLEVBQzNCLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUM3RCxrR0FBa0csQ0FDckcsQ0FBQztJQUNOLENBQUM7SUFFRDs7O09BR0c7SUFDSSxTQUFTO1FBQ1osT0FBTyxJQUFJLENBQUMsWUFBWSxDQUNwQiw0QkFBYyxDQUFDLFNBQVMsRUFDeEIsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQzdELCtGQUErRixDQUNsRyxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7T0FHRztJQUNJLFFBQVE7UUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDM0UsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUNwQiw0QkFBYyxDQUFDLFNBQVMsRUFDeEIsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQzdELCtGQUErRixDQUNsRyxDQUFDO0lBQ04sQ0FBQztDQUNKO0FBN1dELGtDQTZXQyJ9