"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaxNamespaceRegister = exports.PrefixBundle = void 0;
const uuid_1 = require("uuid");
const PaxEndpointsRegister_1 = require("./PaxEndpointsRegister");
const _ = __importStar(require("logger"));
class PrefixBundle {
    constructor(subdomains, handler) {
        this._subdomains = subdomains;
        this._handler = handler;
    }
    get subdomains() {
        return this._subdomains;
    }
    set subdomains(value) {
        this._subdomains = value;
    }
    get handler() {
        return this._handler;
    }
    set handler(value) {
        this._handler = value;
    }
}
exports.PrefixBundle = PrefixBundle;
class PaxNamespaceRegister {
    constructor(application, liquid) {
        this.handle = (domain, req, res, next) => {
            _.trace(`[RF:N] Trying to handle request for ${req.hostname} (${req.originalUrl})`);
            _.trace(`[RF:N] Testing ${req.hostname}(${req.originalUrl}) vs subdomains ${Object.values(this.namespaces).map(e => String(e.subdomains)).join(' ; ')} on domain ${domain.query}`);
            let handled = false;
            const keys = Object.keys(this.namespaces);
            for (const key of keys) {
                const entries = this.namespaces[key];
                // Extract the subdomains
                const subdomains = req.hostname.split('.').slice(0, -domain.offset).join('.');
                _.trace(`[RF:N] Testing "${entries.subdomains}" vs "${subdomains}"`);
                if (entries.subdomains.test(subdomains)) {
                    _.trace(`[RF:N] Request for hostname ${req.hostname} (${req.originalUrl}) is being handled by ${entries.subdomains}`);
                    entries.handler.handle(domain, entries, req, res, next);
                    handled = true;
                    break;
                }
            }
            if (!handled) {
                _.debug(`[RF:N] Could not find an endpoint register for ${req.hostname} (${req.originalUrl}) so passing to "next"`);
                next();
            }
        };
        this.namespaces = {};
        this.application = application;
        this.liquid = liquid;
    }
    namespace(query, handler) {
        let internalQuery = query;
        if (internalQuery === '$')
            internalQuery = RegExp('^$');
        const keys = Object.keys(this.namespaces);
        for (const key of keys) {
            const entries = this.namespaces[key];
            if (String(entries.subdomains) === String(internalQuery)) {
                _.trace(`Endpoint register already exists for ${internalQuery}, recalling it`);
                _.trace(`             Query: ${query}`);
                _.trace(`    Internal Query: ${internalQuery}`);
                _.trace(`                ID: ${key}`);
                handler(entries.handler);
                return;
            }
        }
        const id = uuid_1.v4();
        _.trace(`Endpoint register does not exist for ${internalQuery}, creating a new one`);
        _.trace(`             Query: ${query}`);
        _.trace(`    Internal Query: ${internalQuery}`);
        _.trace(`                ID: ${id}`);
        const endpoints = new PaxEndpointsRegister_1.PaxEndpointsRegister(this.application, this.liquid);
        this.namespaces[id] = new PrefixBundle(typeof (internalQuery) === 'string' ? RegExp(`^${internalQuery}$`) : internalQuery, endpoints);
        handler(endpoints);
    }
}
exports.PaxNamespaceRegister = PaxNamespaceRegister;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF4TmFtZXNwYWNlUmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcGF4LWludGVybWVkaWF0ZS9QYXhOYW1lc3BhY2VSZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsK0JBQTBCO0FBQzFCLGlFQUE4RDtBQUU5RCwwQ0FBNEI7QUFFNUIsTUFBYSxZQUFZO0lBSXJCLFlBQVksVUFBa0IsRUFBRSxPQUE2QjtRQUN6RCxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztRQUM5QixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUM1QixDQUFDO0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxJQUFJLFVBQVUsQ0FBQyxLQUFhO1FBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELElBQUksT0FBTyxDQUFDLEtBQTJCO1FBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Q0FDSjtBQXhCRCxvQ0F3QkM7QUFFRCxNQUFhLG9CQUFvQjtJQUs3QixZQUFtQixXQUF3QixFQUFFLE1BQWM7UUFtQ3BELFdBQU0sR0FBRyxDQUFDLE1BQW9CLEVBQUUsR0FBWSxFQUFFLEdBQWEsRUFBRSxJQUFjLEVBQUUsRUFBRTtZQUNsRixDQUFDLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxHQUFHLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1lBQ3BGLENBQUMsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxRQUFRLElBQUksR0FBRyxDQUFDLFdBQVcsbUJBQW1CLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDbkwsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzFDLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUNwQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQyx5QkFBeUI7Z0JBQ3pCLE1BQU0sVUFBVSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5RSxDQUFDLENBQUMsS0FBSyxDQUFDLG1CQUFtQixPQUFPLENBQUMsVUFBVSxTQUFTLFVBQVUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JFLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7b0JBQ3JDLENBQUMsQ0FBQyxLQUFLLENBQUMsK0JBQStCLEdBQUcsQ0FBQyxRQUFRLEtBQUssR0FBRyxDQUFDLFdBQVcseUJBQXlCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO29CQUN0SCxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3hELE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQ2YsTUFBTTtpQkFDVDthQUNKO1lBR0QsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixDQUFDLENBQUMsS0FBSyxDQUFDLGtEQUFrRCxHQUFHLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxXQUFXLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3BILElBQUksRUFBRSxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUE7UUF6REcsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQztJQUVNLFNBQVMsQ0FBQyxLQUFzQixFQUFFLE9BQW1EO1FBQ3hGLElBQUksYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLGFBQWEsS0FBSyxHQUFHO1lBQUUsYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQyxLQUFLLE1BQU0sR0FBRyxJQUFJLElBQUksRUFBRTtZQUNwQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0JBQ3RELENBQUMsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLGFBQWEsZ0JBQWdCLENBQUMsQ0FBQztnQkFDL0UsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDeEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsYUFBYSxFQUFFLENBQUMsQ0FBQztnQkFDaEQsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFFdEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDekIsT0FBTzthQUNWO1NBQ0o7UUFFRCxNQUFNLEVBQUUsR0FBRyxTQUFFLEVBQUUsQ0FBQztRQUVoQixDQUFDLENBQUMsS0FBSyxDQUFDLHdDQUF3QyxhQUFhLHNCQUFzQixDQUFDLENBQUM7UUFDckYsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsS0FBSyxDQUFDLHVCQUF1QixhQUFhLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFckMsTUFBTSxTQUFTLEdBQUcsSUFBSSwyQ0FBb0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN0SSxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDdkIsQ0FBQztDQTBCSjtBQWhFRCxvREFnRUMifQ==