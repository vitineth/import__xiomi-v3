"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
const path = __importStar(require("path"));
const config_1 = require("../config/config");
const fs_extra_1 = require("fs-extra");
let SqlJs;
// The goal here is to get away from completely relying on pg and pg-promise.
// Therefore we are going to base it on the configuration giving the option of either Postgres, SQL.js or a custom
// driver.
class Database {
    constructor(database) {
        this.database = database;
        this._initialised = false;
        if (!config_1.has('database'))
            throw new Error('Configuration must have a database object loaded');
        if (!config_1.has('database.type'))
            throw new Error('config: database must contain a type property');
        if (!(typeof (config_1.get('database.type')) === "string"))
            throw new Error('config: database.type must be a string');
        this._type = config_1.get('database.type').toLowerCase();
        // Check that it is of the right type
        if (this._type !== 'sqljs' && this._type !== 'pg' && this._type !== 'custom') {
            throw new Error('config: database.type must be one of sqljs, pg or custom');
        }
        if (this._type === 'custom' && !config_1.has('database.driver')) {
            throw new Error('config: database.type is custom but have not specified a driver file');
        }
    }
    verifyInitialised() {
        if (!this._initialised)
            throw new Error('You must initialise the database before calling actions on it');
    }
    async init() {
        this._fd = await fs_extra_1.open(path.join(config_1.get('log-directory'), 'queries.log'), 'w');
        if (this._type === "sqljs") {
            SqlJs = await require('sql.js')();
            await this.initSQLJs();
        }
        else {
            await this.initPg();
        }
        this._initialised = true;
    }
    async initPg() {
        const iAuth = {
            user: config_1.get('database.user'),
            host: config_1.get('database.host'),
            password: config_1.get('database.password'),
            port: config_1.get('database.port'),
            database: this.database,
        };
        this._pg = new (require('pg').Client)(iAuth);
        if (this._pg === undefined)
            throw new Error('Something went wrong with requiring pg');
        await this._pg.connect();
        await this._pg.query(`CREATE TABLE IF NOT EXISTS log_messages
                              (
                                  id            serial                  not null
                                      constraint log_messages_pkey
                                          primary key,
                                  origin        text                    not null,
                                  time          timestamp default now() not null,
                                  level         varchar(5)              not null,
                                  message       text                    not null,
                                  is_production boolean   default true  not null,
                                  line_no       integer   default 0     not null
                              );`);
        await this._pg.query(`create table if not exists log_errors
                              (
                                  id      serial                  not null
                                      constraint log_errors_pk
                                          primary key,
                                  time    timestamp default now() not null,
                                  sender  text                    not null,
                                  level   text                    not null,
                                  message text                    not null
                              );`);
        await this._pg.query(`create table if not exists request_log
                              (
                                  id            serial                  not null
                                      constraint request_log_pkey
                                          primary key,
                                  time          timestamp default now() not null,
                                  ip            text                    not null,
                                  url           text                    not null,
                                  response_code integer                 not null,
                                  is_production boolean   default true  not null,
                                  user_agent    text
                              );`);
    }
    async saveRequest(time, ip, url, responseCode, isProduction, userAgent) {
        this.verifyInitialised();
        if (this._sqljs) {
            // noinspection SqlResolve
            this.sqljsQuery(`INSERT INTO request_log (time, ip, url, response_code, is_production, user_agent)
                             VALUES (?, ?, ?, ?, ?, ?)`, [
                time.getTime(),
                ip,
                url,
                responseCode,
                isProduction,
                userAgent,
            ]);
        }
        else {
            // noinspection SqlResolve
            this.pgQuery(`INSERT INTO request_log (time, ip, url, response_code, is_production, user_agent)
                          VALUES ($1, $2, $3, $4, $5, $6)`, [
                time,
                ip,
                url,
                responseCode,
                isProduction,
                userAgent,
            ]);
        }
    }
    async initSQLJs() {
        this._sqljs = new SqlJs.Database();
        this._sqljs.exec(`CREATE TABLE IF NOT EXISTS log_errors
                          (
                              id      integer primary key,
                              time    bigint,
                              sender  text,
                              level   text,
                              message text
                          )`);
        this._sqljs.exec(`CREATE TABLE IF NOT EXISTS log_messages
                          (
                              id            integer primary key,
                              origin        text,
                              time          bigint,
                              level         text,
                              message       text,
                              is_production boolean default false,
                              line_no       int     default 0
                          );`);
        this._sqljs.exec(`CREATE TABLE IF NOT EXISTS request_log
                          (
                              id            integer primary key,
                              time          bigint,
                              ip            text,
                              url           text,
                              response_code integer,
                              is_production boolean default true,
                              user_agent    text
                          );`);
    }
    async close() {
        this.verifyInitialised();
        if (this._fd) {
            return fs_extra_1.close(this._fd);
        }
    }
    async saveQuery(query) {
        this.verifyInitialised();
        if (this._fd) {
            return fs_extra_1.writeFile(this._fd, `[${time()}]: ${query}\n`);
        }
    }
    async pgQuery(query, values) {
        this.verifyInitialised();
        await this.saveQuery(query);
        return new Promise((resolve, reject) => {
            if (this._pg === undefined) {
                reject();
                return;
            }
            this._pg.query(query, values, (err, result) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    }
    async sqljsQuery(query, values) {
        this.verifyInitialised();
        await this.saveQuery(query);
        if (this._sqljs === undefined)
            throw new Error('sql.js instance not initialised');
        return this._sqljs.run(query, values);
    }
    async saveError(time, sender, level, message) {
        this.verifyInitialised();
        if (this._sqljs) {
            // noinspection SqlResolve
            this.sqljsQuery(`INSERT INTO log_errors (time, sender, level, message)
                             VALUES (?, ?, ?, ?)`, [
                time.getTime(),
                sender,
                level,
                message,
            ]);
        }
        else {
            // noinspection SqlResolve
            this.pgQuery(`INSERT INTO log_errors (time, sender, level, message)
                          VALUES ($1, $2, $3, $4)`, [
                time,
                sender,
                level,
                message,
            ]);
        }
    }
    async saveMessage(time, sender, level, message, isProduction) {
        this.verifyInitialised();
        if (this._sqljs) {
            // noinspection SqlResolve
            this.sqljsQuery(`INSERT INTO log_messages (origin, time, level, message, is_production)
                             VALUES (?, ?, ?, ?, ?)`, [
                time.getTime(),
                sender,
                level,
                message,
                isProduction,
            ]);
        }
        else {
            // noinspection SqlResolve
            this.sqljsQuery(`INSERT INTO log_messages (origin, time, level, message, is_production)
                             VALUES ($1, $2, $3, $4, $5)`, [
                time.getTime(),
                sender,
                level,
                message,
                isProduction,
            ]);
        }
    }
}
exports.Database = Database;
/**
 * Returns the formatted time in the form "YYYY-MM-DD HH:MM:SS"
 * @return the time format
 */
function time() {
    const date = new Date();
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hour = String(date.getHours()).padStart(2, '0');
    const minute = String(date.getMinutes()).padStart(2, '0');
    const second = String(date.getSeconds()).padStart(2, '0');
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YWJhc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvZGF0YWJhc2UvZGF0YWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDJDQUE2QjtBQUU3Qiw2Q0FBNEM7QUFDNUMsdUNBQWtEO0FBS2xELElBQUksS0FBa0IsQ0FBQztBQUN2Qiw2RUFBNkU7QUFDN0Usa0hBQWtIO0FBQ2xILFVBQVU7QUFDVixNQUFhLFFBQVE7SUFRakIsWUFBb0IsUUFBaUI7UUFBakIsYUFBUSxHQUFSLFFBQVEsQ0FBUztRQUo3QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUtsQyxJQUFJLENBQUMsWUFBRyxDQUFDLFVBQVUsQ0FBQztZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztRQUMxRixJQUFJLENBQUMsWUFBRyxDQUFDLGVBQWUsQ0FBQztZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssUUFBUSxDQUFDO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1FBRTdHLElBQUksQ0FBQyxLQUFLLEdBQUksWUFBRyxDQUFDLGVBQWUsQ0FBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRTVELHFDQUFxQztRQUNyQyxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzFFLE1BQU0sSUFBSSxLQUFLLENBQUMsMERBQTBELENBQUMsQ0FBQztTQUMvRTtRQUVELElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksQ0FBQyxZQUFHLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNwRCxNQUFNLElBQUksS0FBSyxDQUFDLHNFQUFzRSxDQUFDLENBQUM7U0FDM0Y7SUFDTCxDQUFDO0lBRU8saUJBQWlCO1FBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWTtZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsK0RBQStELENBQUMsQ0FBQztJQUM3RyxDQUFDO0lBRU0sS0FBSyxDQUFDLElBQUk7UUFDYixJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sZUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBRyxDQUFDLGVBQWUsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRTNFLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxPQUFPLEVBQUU7WUFDeEIsS0FBSyxHQUFHLE1BQU0sT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7WUFDbEMsTUFBTSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDMUI7YUFBTTtZQUNILE1BQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ3ZCO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDN0IsQ0FBQztJQUVPLEtBQUssQ0FBQyxNQUFNO1FBQ2hCLE1BQU0sS0FBSyxHQUFXO1lBQ2xCLElBQUksRUFBRSxZQUFHLENBQUMsZUFBZSxDQUFDO1lBQzFCLElBQUksRUFBRSxZQUFHLENBQUMsZUFBZSxDQUFDO1lBQzFCLFFBQVEsRUFBRSxZQUFHLENBQUMsbUJBQW1CLENBQUM7WUFDbEMsSUFBSSxFQUFFLFlBQUcsQ0FBQyxlQUFlLENBQUM7WUFDMUIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQzFCLENBQUM7UUFFRixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0MsSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLFNBQVM7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7UUFDdEYsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3pCLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7Ozs7Ozs7Ozs7O2lDQVdJLENBQUMsQ0FBQztRQUMzQixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDOzs7Ozs7Ozs7aUNBU0ksQ0FBQyxDQUFDO1FBQzNCLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7Ozs7Ozs7Ozs7O2lDQVdJLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFVLEVBQUUsRUFBVSxFQUFFLEdBQVcsRUFBRSxZQUFvQixFQUFFLFlBQXFCLEVBQUUsU0FBaUI7UUFDeEgsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFekIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsMEJBQTBCO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUM7dURBQzJCLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2QsRUFBRTtnQkFDRixHQUFHO2dCQUNILFlBQVk7Z0JBQ1osWUFBWTtnQkFDWixTQUFTO2FBQ1osQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILDBCQUEwQjtZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDOzBEQUNpQyxFQUFFO2dCQUM1QyxJQUFJO2dCQUNKLEVBQUU7Z0JBQ0YsR0FBRztnQkFDSCxZQUFZO2dCQUNaLFlBQVk7Z0JBQ1osU0FBUzthQUNaLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVPLEtBQUssQ0FBQyxTQUFTO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7NEJBT0csQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7NkJBU0ksQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7Ozs7NkJBU0ksQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFTSxLQUFLLENBQUMsS0FBSztRQUNkLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNWLE9BQU8sZ0JBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDMUI7SUFDTCxDQUFDO0lBRU8sS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFhO1FBQ2pDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNWLE9BQU8sb0JBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sS0FBSyxJQUFJLENBQUMsQ0FBQztTQUN6RDtJQUNMLENBQUM7SUFFTyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQWEsRUFBRSxNQUFhO1FBQzlDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1QixPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ25DLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxTQUFTLEVBQUU7Z0JBQ3hCLE1BQU0sRUFBRSxDQUFDO2dCQUNULE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQzFDLElBQUksR0FBRyxFQUFFO29CQUNMLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDZjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ25CO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQWEsRUFBRSxNQUFhO1FBQ2pELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1QixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUztZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUNsRixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRU0sS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFVLEVBQUUsTUFBYyxFQUFFLEtBQWEsRUFBRSxPQUFlO1FBQzdFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLDBCQUEwQjtZQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDO2lEQUNxQixFQUFFO2dCQUNuQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNkLE1BQU07Z0JBQ04sS0FBSztnQkFDTCxPQUFPO2FBQ1YsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILDBCQUEwQjtZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDO2tEQUN5QixFQUFFO2dCQUNwQyxJQUFJO2dCQUNKLE1BQU07Z0JBQ04sS0FBSztnQkFDTCxPQUFPO2FBQ1YsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFVLEVBQUUsTUFBYyxFQUFFLEtBQWEsRUFBRSxPQUFlLEVBQUUsWUFBcUI7UUFDdEcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFekIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsMEJBQTBCO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUM7b0RBQ3dCLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2QsTUFBTTtnQkFDTixLQUFLO2dCQUNMLE9BQU87Z0JBQ1AsWUFBWTthQUNmLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQzt5REFDNkIsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDZCxNQUFNO2dCQUNOLEtBQUs7Z0JBQ0wsT0FBTztnQkFDUCxZQUFZO2FBQ2YsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0NBRUo7QUFwUEQsNEJBb1BDO0FBRUQ7OztHQUdHO0FBQ0gsU0FBUyxJQUFJO0lBQ1QsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztJQUV4QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDaEMsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzNELE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3BELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3RELE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzFELE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRTFELE9BQU8sR0FBRyxJQUFJLElBQUksS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLElBQUksTUFBTSxJQUFJLE1BQU0sRUFBRSxDQUFDO0FBQ2pFLENBQUMifQ==