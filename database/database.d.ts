export declare class Database {
    private database?;
    private _fd?;
    private _type;
    private _initialised;
    private _pg?;
    private _sqljs?;
    constructor(database?: string | undefined);
    private verifyInitialised;
    init(): Promise<void>;
    private initPg;
    saveRequest(time: Date, ip: string, url: string, responseCode: number, isProduction: boolean, userAgent: string): Promise<void>;
    private initSQLJs;
    close(): Promise<void>;
    private saveQuery;
    private pgQuery;
    private sqljsQuery;
    saveError(time: Date, sender: string, level: string, message: string): Promise<void>;
    saveMessage(time: Date, sender: string, level: string, message: string, isProduction: boolean): Promise<void>;
}
