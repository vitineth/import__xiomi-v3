"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = __importStar(require("logger")); // Pray for our singleton souls
const http = __importStar(require("http"));
const https = __importStar(require("https"));
const PaxDomainRegister_1 = require("./pax-intermediate/PaxDomainRegister");
const config_1 = require("./config/config");
const path_1 = require("path");
const csurf_1 = __importDefault(require("csurf"));
const _404_1 = require("./pages/404");
const _403csrf_1 = require("./pages/403csrf");
const liquidjs_1 = require("liquidjs");
const fs_extra_1 = require("fs-extra");
const casvii_1 = require("./casvii");
const permission_automation_1 = require("./casvii/automation/permission-automation");
const SequelizePermissionDAO_1 = require("./casvii/database/dao/impl/SequelizePermissionDAO");
const FileServer_1 = require("./casvii/file/FileServer");
const database_1 = require("./database/database");
const express = require("express");
const helmet = require("helmet");
const expressSession = require("express-session");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
// Port resolution determines hosting port for either production or development environments
const portResolution = require(`../config/port-resolution`);
// Logging anything
_.setMinimumLevel(_.LogLevel.BASE);
// Load the default configuration and verify it's properties. If we don't have the required keys then throw a fit
config_1.loadEasy(`default`);
if (!config_1.verifyRequired()) {
    _.error(`Failed to launch the server!`);
    _.error(`   The configuration did not contain all of the required keys for the server to launch properly`);
    _.error(`   Double check that all keys are specified and that you are pulling in all the extra configs you are meant`);
    _.error(`   to be via the [load] key, if applicable.`);
    process.exit(1);
}
const isProduction = config_1.get('environment') === 'prod';
if (config_1.get('environment') === 'dev') {
    config_1.loadEasy('dev', 10000);
    process.on('unhandledRejection', r => console.log(r));
}
else {
    if (config_1.get('log-level') !== undefined) {
        _.setMinimumLevel(_.LogLevel[config_1.get('log-level')]);
    }
    else {
        _.setMinimumLevel(_.LogLevel.INFO);
    }
}
function rejectedDB(err) {
    _.error(`Failed to connect to a pre-connect database`);
    console.error(err);
    process.exit(1);
}
// Try to connect to the databases listed in the configuration and if it fails, reject with an error. On success, add
// a log handler which logs the message to the database.
const database = new database_1.Database('ryan');
database.init().catch(rejectedDB).then(async () => {
    _.addHandler((data) => {
        if (data.level === _.LogLevel.FATAL || data.level === _.LogLevel.ERROR || data.level === _.LogLevel.WARN) {
            database.saveError(new Date(), JSON.stringify(data.caller), data.level, data.message);
        }
        database.saveMessage(new Date(), JSON.stringify(data.caller), data.level, data.message, isProduction);
        return undefined;
    });
    const liquid = new liquidjs_1.Liquid();
    liquid.registerFilter(`lpad`, (initial, size, char) => {
        let padding = ``;
        for (let i = 0; i < size - initial.length; i++)
            padding += char;
        return padding + initial;
    });
    // Create CSRF protection
    const csrfProtection = csurf_1.default({ cookie: true });
    const app = express();
    app.get('/*', (req, res, next) => {
        res.header('Content-Security-Policy', `frame-ancestors 'none'`);
        res.header('X-Frame-Options', `none`);
        res.header('Strict-Transport-Security', `max-age=86400; includeSubDomains`);
        res.header('Cache-Control', `no-cache="Set-Cookie, Set-Cookie2"`);
        next(); // http://expressjs.com/guide.html#passing-route control
    });
    app.use(helmet());
    app.use(cookieParser());
    const sessionConfiguration = config_1.get(`session`);
    sessionConfiguration['cookie']['domain'] = config_1.get('environment') === 'dev' ? 'ryan.ax370' : 'xiomi.org';
    app.use(expressSession(sessionConfiguration));
    app.use(bodyParser.json({ limit: `50mb` }));
    app.use(bodyParser.urlencoded({ limit: `50mb`, extended: true }));
    // Register it after the body parsers and cookie parser so it can read everything properly
    app.use(csrfProtection);
    // Then register an error handler
    app.use((err, req, res, next) => {
        if (err.code !== 'EBADCSRFTOKEN')
            return next(err);
        _.error('Failed to return route due to csrf error', err);
        _403csrf_1.handle(req, res);
    });
    const application = new PaxDomainRegister_1.PaxDomainRegister(app, liquid);
    // TODO: convert
    app.use(express.static(path_1.join(__dirname, `..`, `statics`)));
    app.use((req, res, next) => {
        res.on(`finish`, () => {
            _.trace(`Request from ${req.connection.remoteAddress} for ${req.originalUrl} on host ${req.get(`host`)} which returned code ${res.statusCode} (${http.STATUS_CODES[res.statusCode]})`);
            const userAgent = req.get(`User-Agent`) === undefined ? `undefined user agent` : req.get(`User-Agent`);
            database.saveRequest(new Date(), req.connection.remoteAddress === undefined ? 'undefined' : req.connection.remoteAddress, `${req.hostname} ${req.originalUrl}`, res.statusCode, config_1.get(`environment`) === `prod`, userAgent);
        });
        next();
    });
    // Then construct the cas object which will create the required processes and provide the interface to modules
    // If we are running without cas enabled we need to mock up the composite that is produced
    let casv2;
    if (!config_1.has('include-cas') || config_1.get('include-cas') === true) {
        casv2 = await casvii_1.CASVii.build();
        await casv2.register(application, csrfProtection);
        try {
            await permission_automation_1.convertPermissionsFile(new SequelizePermissionDAO_1.SequelizePermissionDAO());
        }
        catch (e) {
            _.error('failed to migrate permissions to the database');
            console.error(e);
            return;
        }
    }
    else {
        _.warn('We are skipping including CAS. This means every request will automatically be authenticated!');
        casv2 = await casvii_1.CASVii.buildFake();
    }
    const server = new FileServer_1.FileServer('/', casv2.composite);
    app.use((req, res, next) => server.handle(req, res, next));
    // And then register the pages
    // await registerCASPages(application, cas, csrfProtection);
    // Read the SSL certificates
    const sslPrivateKey = await fs_extra_1.readFile(path_1.join(__dirname, `..`, config_1.get(`server.ssl.private`)), { encoding: `utf-8` });
    const sslCertificate = await fs_extra_1.readFile(path_1.join(__dirname, `..`, config_1.get(`server.ssl.certificate`)), { encoding: `utf-8` });
    // And construct the options for the HTTPS server
    const serverOptions = {
        key: sslPrivateKey,
        cert: sslCertificate,
    };
    // Figure out what power we want to use for the https server
    const httpsPort = portResolution[config_1.get('environment')].https;
    // Then then create the server
    await createHTTPSServer(serverOptions, app, httpsPort);
    // Log that the server has started
    _.info(`HTTPS Server has started on ${httpsPort}`);
    // Load all the components that have been registered
    handleComponents(application, app, casv2.composite);
    // And then finally add the 404 listener which will be called if the components don't handle the events
    app.use(_404_1.handle);
});
function createHTTPSServer(options, app, port) {
    return new Promise((resolve, reject) => {
        https.createServer(options, app).listen(port, '0.0.0.0', () => {
            resolve();
        });
    });
}
function handleComponents(application, app, composites) {
    for (const component of config_1.get(`components`)) {
        if (typeof (component) !== `string`) {
            _.error(`Invalid component entry: ${typeof (component)} instead of string`);
            continue;
        }
        let entry = require(path_1.join(__dirname, `..`, component));
        // Support for typescript default exports
        if (typeof (entry) === `object`) {
            if (entry.hasOwnProperty(`default`))
                entry = entry[`default`];
        }
        if (!(entry instanceof Function)) {
            _.error(`Invalid component ${component}. Expected function but got ${typeof (entry)}`);
            continue;
        }
        // (application, express, logger, config, CAS)
        entry(application, app, _.Logs, { get: config_1.get, load: config_1.load, loadEasy: config_1.loadEasy }, composites);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMENBQTRCLENBQUMsK0JBQStCO0FBQzVELDJDQUE2QjtBQUM3Qiw2Q0FBK0I7QUFFL0IsNEVBQXlFO0FBQ3pFLDRDQUEyRTtBQUMzRSwrQkFBNEI7QUFDNUIsa0RBQTBCO0FBRzFCLHNDQUE2QztBQUM3Qyw4Q0FBaUQ7QUFDakQsdUNBQWtDO0FBQ2xDLHVDQUFvQztBQUNwQyxxQ0FBa0M7QUFDbEMscUZBQW1GO0FBQ25GLDhGQUEyRjtBQUUzRix5REFBc0Q7QUFDdEQsa0RBQStDO0FBQy9DLG1DQUFvQztBQUNwQyxpQ0FBa0M7QUFDbEMsa0RBQW1EO0FBQ25ELDBDQUEyQztBQUMzQyw4Q0FBK0M7QUFFL0MsNEZBQTRGO0FBQzVGLE1BQU0sY0FBYyxHQUFHLE9BQU8sQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0FBRTVELG1CQUFtQjtBQUNuQixDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7QUFFbkMsaUhBQWlIO0FBQ2pILGlCQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDcEIsSUFBSSxDQUFDLHVCQUFjLEVBQUUsRUFBRTtJQUNuQixDQUFDLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUM7SUFDeEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpR0FBaUcsQ0FBQyxDQUFDO0lBQzNHLENBQUMsQ0FBQyxLQUFLLENBQUMsNkdBQTZHLENBQUMsQ0FBQztJQUN2SCxDQUFDLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7SUFDdkQsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztDQUNuQjtBQUVELE1BQU0sWUFBWSxHQUFHLFlBQUcsQ0FBQyxhQUFhLENBQUMsS0FBSyxNQUFNLENBQUM7QUFFbkQsSUFBSSxZQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxFQUFFO0lBQzlCLGlCQUFRLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLE9BQU8sQ0FBQyxFQUFFLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Q0FDekQ7S0FBTTtJQUNILElBQUksWUFBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLFNBQVMsRUFBRTtRQUNoQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNuRDtTQUFNO1FBQ0gsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3RDO0NBQ0o7QUFFRCxTQUFTLFVBQVUsQ0FBQyxHQUFHO0lBQ25CLENBQUMsQ0FBQyxLQUFLLENBQUMsNkNBQTZDLENBQUMsQ0FBQztJQUN2RCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDcEIsQ0FBQztBQUVELHFIQUFxSDtBQUNySCx3REFBd0Q7QUFDeEQsTUFBTSxRQUFRLEdBQUcsSUFBSSxtQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3RDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFO0lBQzlDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtRQUN2QixJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFO1lBQ3RHLFFBQVEsQ0FBQyxTQUFTLENBQ2QsSUFBSSxJQUFJLEVBQUUsRUFDVixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDM0IsSUFBSSxDQUFDLEtBQUssRUFDVixJQUFJLENBQUMsT0FBTyxDQUNmLENBQUM7U0FDTDtRQUNELFFBQVEsQ0FBQyxXQUFXLENBQ2hCLElBQUksSUFBSSxFQUFFLEVBQ1YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQzNCLElBQUksQ0FBQyxLQUFLLEVBQ1YsSUFBSSxDQUFDLE9BQU8sRUFDWixZQUFZLENBQ2YsQ0FBQztRQUNGLE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBRUgsTUFBTSxNQUFNLEdBQVcsSUFBSSxpQkFBTSxFQUFFLENBQUM7SUFDcEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO1FBQ2xELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO1lBQUUsT0FBTyxJQUFJLElBQUksQ0FBQztRQUNoRSxPQUFPLE9BQU8sR0FBRyxPQUFPLENBQUM7SUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFFSCx5QkFBeUI7SUFDekIsTUFBTSxjQUFjLEdBQUcsZUFBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDL0MsTUFBTSxHQUFHLEdBQXdCLE9BQU8sRUFBRSxDQUFDO0lBRTNDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRTtRQUM3QixHQUFHLENBQUMsTUFBTSxDQUFDLHlCQUF5QixFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDaEUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN0QyxHQUFHLENBQUMsTUFBTSxDQUFDLDJCQUEyQixFQUFFLGtDQUFrQyxDQUFDLENBQUM7UUFDNUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztRQUNsRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLHdEQUF3RDtJQUNwRSxDQUFDLENBQUMsQ0FBQztJQUVILEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUNsQixHQUFHLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7SUFFeEIsTUFBTSxvQkFBb0IsR0FBRyxZQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsWUFBRyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7SUFDckcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO0lBRTlDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDNUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRWxFLDBGQUEwRjtJQUMxRixHQUFHLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBRXhCLGlDQUFpQztJQUNqQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDNUIsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLGVBQWU7WUFBRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVuRCxDQUFDLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3pELGlCQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ25CLENBQUMsQ0FBQyxDQUFDO0lBRUgsTUFBTSxXQUFXLEdBQUcsSUFBSSxxQ0FBaUIsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFFdkQsZ0JBQWdCO0lBQ2hCLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFMUQsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBSSxFQUFFLEVBQUU7UUFDMUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO1lBQ2xCLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxVQUFVLENBQUMsYUFBYSxRQUFRLEdBQUcsQ0FBQyxXQUFXLFlBQVksR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLEdBQUcsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXZMLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQVcsQ0FBQztZQUV6SCxRQUFRLENBQUMsV0FBVyxDQUNoQixJQUFJLElBQUksRUFBRSxFQUNWLEdBQUcsQ0FBQyxVQUFVLENBQUMsYUFBYSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFDdkYsR0FBRyxHQUFHLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsRUFDcEMsR0FBRyxDQUFDLFVBQVUsRUFBRSxZQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssTUFBTSxFQUM3QyxTQUFTLENBQ1osQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxFQUFFLENBQUM7SUFDWCxDQUFDLENBQUMsQ0FBQztJQUVILDhHQUE4RztJQUM5RywwRkFBMEY7SUFDMUYsSUFBSSxLQUFhLENBQUM7SUFDbEIsSUFBSSxDQUFDLFlBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxZQUFHLENBQUMsYUFBYSxDQUFDLEtBQUssSUFBSSxFQUFFO1FBQ3BELEtBQUssR0FBRyxNQUFNLGVBQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBRWxELElBQUk7WUFDQSxNQUFNLDhDQUFzQixDQUFDLElBQUksK0NBQXNCLEVBQUUsQ0FBQyxDQUFDO1NBQzlEO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixDQUFDLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7WUFDekQsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQixPQUFPO1NBQ1Y7S0FDSjtTQUFNO1FBQ0gsQ0FBQyxDQUFDLElBQUksQ0FBQyw4RkFBOEYsQ0FBQyxDQUFDO1FBQ3ZHLEtBQUssR0FBRyxNQUFNLGVBQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztLQUNwQztJQUVELE1BQU0sTUFBTSxHQUFHLElBQUksdUJBQVUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3BELEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFFM0QsOEJBQThCO0lBQzlCLDREQUE0RDtJQUU1RCw0QkFBNEI7SUFDNUIsTUFBTSxhQUFhLEdBQUcsTUFBTSxtQkFBUSxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFlBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUM5RyxNQUFNLGNBQWMsR0FBRyxNQUFNLG1CQUFRLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsWUFBRyxDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBRW5ILGlEQUFpRDtJQUNqRCxNQUFNLGFBQWEsR0FBRztRQUNsQixHQUFHLEVBQUUsYUFBYTtRQUNsQixJQUFJLEVBQUUsY0FBYztLQUN2QixDQUFDO0lBRUYsNERBQTREO0lBQzVELE1BQU0sU0FBUyxHQUFHLGNBQWMsQ0FBQyxZQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFFM0QsOEJBQThCO0lBQzlCLE1BQU0saUJBQWlCLENBQUMsYUFBYSxFQUFFLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUV2RCxrQ0FBa0M7SUFDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQywrQkFBK0IsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUVuRCxvREFBb0Q7SUFDcEQsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7SUFFcEQsdUdBQXVHO0lBQ3ZHLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBSSxDQUFDLENBQUM7QUFDbEIsQ0FBQyxDQUFDLENBQUM7QUFFSCxTQUFTLGlCQUFpQixDQUFDLE9BQTRCLEVBQUUsR0FBZ0IsRUFBRSxJQUFZO0lBQ25GLE9BQU8sSUFBSSxPQUFPLENBQU8sQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7UUFDekMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFO1lBQzFELE9BQU8sRUFBRSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRCxTQUFTLGdCQUFnQixDQUFDLFdBQThCLEVBQUUsR0FBZ0IsRUFBRSxVQUF3QjtJQUNoRyxLQUFLLE1BQU0sU0FBUyxJQUFJLFlBQUcsQ0FBQyxZQUFZLENBQUMsRUFBRTtRQUN2QyxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxRQUFRLEVBQUU7WUFDakMsQ0FBQyxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzVFLFNBQVM7U0FDWjtRQUVELElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBRXRELHlDQUF5QztRQUN6QyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLEVBQUU7WUFDN0IsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQztnQkFBRSxLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2pFO1FBRUQsSUFBSSxDQUFDLENBQUMsS0FBSyxZQUFZLFFBQVEsQ0FBQyxFQUFFO1lBQzlCLENBQUMsQ0FBQyxLQUFLLENBQUMscUJBQXFCLFNBQVMsK0JBQStCLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdkYsU0FBUztTQUNaO1FBRUQsOENBQThDO1FBQzlDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUgsWUFBRyxFQUFFLElBQUksRUFBSixhQUFJLEVBQUUsUUFBUSxFQUFSLGlCQUFRLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztLQUN4RTtBQUNMLENBQUMifQ==