"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handle = void 0;
const _ = __importStar(require("logger"));
const path_1 = require("path");
const fs_extra_1 = require("fs-extra");
function handle(req, res) {
    res.format({
        'text/plain': () => {
            res.status(403).send(`Form tampering suspected`);
        },
        'text/html': () => {
            fs_extra_1.readFile(path_1.join(__dirname, `..`, `statics`, `csrf.html`), { encoding: `utf8` }).then((data) => {
                res.status(403).send(data);
            }).catch((err) => {
                res.status(403).send(`<h1 style="font-family: sans-serif;">403 Form Tampering Suspected</h1><p>And in trying to load the error page, there was an internal error</p>`);
            });
        },
        'application/json': () => {
            res.status(403).json(`Form tampering suspected`);
        },
        default: () => {
            res.status(406).send(`Not Acceptable`);
        },
    });
    _.warn(`${req.originalUrl} on ${req.hostname} by user ${req.ip} failed due to a CSRF exception`);
}
exports.handle = handle;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNDAzY3NyZi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYWdlcy80MDNjc3JmLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBNEI7QUFDNUIsK0JBQTRCO0FBQzVCLHVDQUFvQztBQUdwQyxTQUFnQixNQUFNLENBQUMsR0FBWSxFQUFFLEdBQWE7SUFDOUMsR0FBRyxDQUFDLE1BQU0sQ0FDTjtRQUNJLFlBQVksRUFBRSxHQUFHLEVBQUU7WUFDZixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQ3JELENBQUM7UUFDRCxXQUFXLEVBQUUsR0FBRyxFQUFFO1lBQ2QsbUJBQVEsQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsV0FBVyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDeEYsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ2IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0pBQWdKLENBQUMsQ0FBQztZQUMzSyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFDRCxrQkFBa0IsRUFBRSxHQUFHLEVBQUU7WUFDckIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUNyRCxDQUFDO1FBQ0QsT0FBTyxFQUFFLEdBQUcsRUFBRTtZQUNWLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDM0MsQ0FBQztLQUNKLENBQ0osQ0FBQztJQUNGLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsV0FBVyxPQUFPLEdBQUcsQ0FBQyxRQUFRLFlBQVksR0FBRyxDQUFDLEVBQUUsaUNBQWlDLENBQUMsQ0FBQztBQUNyRyxDQUFDO0FBdEJELHdCQXNCQyJ9