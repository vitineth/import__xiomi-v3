"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handle = void 0;
const _ = __importStar(require("logger"));
const PaxResponse_1 = require("../pax-intermediate/PaxResponse");
function handle(req, res) {
    res.format({
        'text/html': () => {
            // @ts-ignore
            new PaxResponse_1.PaxResponse(res, undefined, req).notFound();
            // readFile(join(__dirname, '..', `..`, `statics`, `codes`, `404.html`), { encoding: `utf8` }).then((data) => {
            //     res.status(404).send(data);
            // }).catch((err) => {
            //     res.status(404).send(`<h1 style="font-family: sans-serif;">404 Not Found</h1><p>And in trying to load the 404 page, there was an internal error</p>`);
            // });
        },
        'application/json': () => {
            res.status(404).json(`Not Found`);
        },
        default: () => {
            res.status(406).send(`Not Acceptable`);
        },
    });
    _.info(`${req.originalUrl} on ${req.hostname} returned 404`);
}
exports.handle = handle;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNDA0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3BhZ2VzLzQwNC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMENBQTRCO0FBSTVCLGlFQUE4RDtBQUU5RCxTQUFnQixNQUFNLENBQUMsR0FBWSxFQUFFLEdBQWE7SUFDOUMsR0FBRyxDQUFDLE1BQU0sQ0FDTjtRQUNJLFdBQVcsRUFBRSxHQUFHLEVBQUU7WUFDZCxhQUFhO1lBQ2IsSUFBSSx5QkFBVyxDQUFDLEdBQUcsRUFBRSxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEQsK0dBQStHO1lBQy9HLGtDQUFrQztZQUNsQyxzQkFBc0I7WUFDdEIsNkpBQTZKO1lBQzdKLE1BQU07UUFDVixDQUFDO1FBQ0Qsa0JBQWtCLEVBQUUsR0FBRyxFQUFFO1lBQ3JCLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7UUFDRCxPQUFPLEVBQUUsR0FBRyxFQUFFO1lBQ1YsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMzQyxDQUFDO0tBQ0osQ0FDSixDQUFDO0lBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxXQUFXLE9BQU8sR0FBRyxDQUFDLFFBQVEsZUFBZSxDQUFDLENBQUM7QUFDakUsQ0FBQztBQXJCRCx3QkFxQkMifQ==