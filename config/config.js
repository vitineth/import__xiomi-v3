"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config = exports.get = exports.has = exports.load = exports.loadEasy = exports.verifyRequired = void 0;
const _ = __importStar(require("logger"));
const path_1 = require("path");
const loadedConfigurations = {};
function verifyRequired() {
    const requiredKeys = [
        'environment',
        'server',
        'session',
        'server.ssl',
        'server.ssl.private',
        'server.ssl.certificate',
        'database',
        'database.user',
        'database.host',
        'database.password',
        'database.preconnect',
        'log-directory',
    ];
    for (const key of requiredKeys) {
        if (!has(key)) {
            _.error(`missing key ${key}`);
            return false;
        }
    }
    return true;
}
exports.verifyRequired = verifyRequired;
// TODO: no protections against infinite load cycles
function loadEasy(path, priority = 10) {
    load(path_1.join(__dirname, '..', '..', 'config', `${path}.js`), priority);
}
exports.loadEasy = loadEasy;
function load(path, priority = 10) {
    _.info(`loading configuration ${path} at priority ${priority}`);
    const config = require(path);
    if (config.hasOwnProperty('load') && Array.isArray(config.load)) {
        for (const location of config.load) {
            load(path_1.join(path_1.dirname(path), location));
        }
        delete config['load'];
    }
    if (loadedConfigurations.hasOwnProperty(priority)) {
        loadedConfigurations[priority].push(config);
    }
    else {
        loadedConfigurations[priority] = [config];
    }
}
exports.load = load;
function has(path) {
    const descendingPriorities = Object.keys(loadedConfigurations).sort().reverse();
    for (const key of descendingPriorities) {
        for (const config of loadedConfigurations[key]) {
            let active = config;
            const parts = path.split('.');
            for (const part of parts) {
                if (active.hasOwnProperty(part)) {
                    active = active[part];
                }
                else {
                    active = undefined;
                    break;
                }
            }
            if (active !== undefined)
                return true;
        }
    }
    return false;
}
exports.has = has;
function get(path) {
    const descendingPriorities = Object.keys(loadedConfigurations).sort().reverse();
    for (const key of descendingPriorities) {
        for (const config of loadedConfigurations[key]) {
            let active = config;
            const parts = path.split('.');
            for (const part of parts) {
                if (active.hasOwnProperty(part)) {
                    active = active[part];
                }
                else {
                    active = undefined;
                    break;
                }
            }
            if (active !== undefined)
                return active;
        }
    }
    return undefined;
}
exports.get = get;
/**
 * A set of all externally used config functions (get, has, load) as a simple object.
 */
exports.Config = {
    get,
    has,
    load,
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDBDQUE0QjtBQUM1QiwrQkFBcUM7QUFFckMsTUFBTSxvQkFBb0IsR0FBNkIsRUFBRSxDQUFDO0FBRTFELFNBQWdCLGNBQWM7SUFDMUIsTUFBTSxZQUFZLEdBQUc7UUFDakIsYUFBYTtRQUNiLFFBQVE7UUFDUixTQUFTO1FBQ1QsWUFBWTtRQUNaLG9CQUFvQjtRQUNwQix3QkFBd0I7UUFDeEIsVUFBVTtRQUNWLGVBQWU7UUFDZixlQUFlO1FBQ2YsbUJBQW1CO1FBQ25CLHFCQUFxQjtRQUNyQixlQUFlO0tBQ2xCLENBQUM7SUFFRixLQUFLLE1BQU0sR0FBRyxJQUFJLFlBQVksRUFBRTtRQUM1QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDOUIsT0FBTyxLQUFLLENBQUM7U0FDaEI7S0FDSjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2hCLENBQUM7QUF4QkQsd0NBd0JDO0FBRUQsb0RBQW9EO0FBRXBELFNBQWdCLFFBQVEsQ0FBQyxJQUFZLEVBQUUsV0FBbUIsRUFBRTtJQUN4RCxJQUFJLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLElBQUksS0FBSyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDeEUsQ0FBQztBQUZELDRCQUVDO0FBRUQsU0FBZ0IsSUFBSSxDQUFDLElBQVksRUFBRSxXQUFtQixFQUFFO0lBQ3BELENBQUMsQ0FBQyxJQUFJLENBQUMseUJBQXlCLElBQUksZ0JBQWdCLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDaEUsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTdCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUM3RCxLQUFLLE1BQU0sUUFBUSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFdBQUksQ0FBQyxjQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUN2QztRQUNELE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ3pCO0lBRUQsSUFBSSxvQkFBb0IsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFDL0Msb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQy9DO1NBQU07UUFDSCxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQzdDO0FBQ0wsQ0FBQztBQWhCRCxvQkFnQkM7QUFFRCxTQUFnQixHQUFHLENBQUMsSUFBWTtJQUM1QixNQUFNLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoRixLQUFLLE1BQU0sR0FBRyxJQUFJLG9CQUFvQixFQUFFO1FBQ3BDLEtBQUssTUFBTSxNQUFNLElBQUksb0JBQW9CLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDNUMsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3BCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDOUIsS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLEVBQUU7Z0JBQ3RCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDN0IsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0gsTUFBTSxHQUFHLFNBQVMsQ0FBQztvQkFDbkIsTUFBTTtpQkFDVDthQUNKO1lBRUQsSUFBSSxNQUFNLEtBQUssU0FBUztnQkFBRSxPQUFPLElBQUksQ0FBQztTQUN6QztLQUNKO0lBRUQsT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQztBQXBCRCxrQkFvQkM7QUFFRCxTQUFnQixHQUFHLENBQUMsSUFBWTtJQUM1QixNQUFNLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoRixLQUFLLE1BQU0sR0FBRyxJQUFJLG9CQUFvQixFQUFFO1FBQ3BDLEtBQUssTUFBTSxNQUFNLElBQUksb0JBQW9CLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDNUMsSUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3BCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDOUIsS0FBSyxNQUFNLElBQUksSUFBSSxLQUFLLEVBQUU7Z0JBQ3RCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDN0IsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0gsTUFBTSxHQUFHLFNBQVMsQ0FBQztvQkFDbkIsTUFBTTtpQkFDVDthQUNKO1lBRUQsSUFBSSxNQUFNLEtBQUssU0FBUztnQkFBRSxPQUFPLE1BQU0sQ0FBQztTQUMzQztLQUNKO0lBRUQsT0FBTyxTQUFTLENBQUM7QUFDckIsQ0FBQztBQXBCRCxrQkFvQkM7QUFFRDs7R0FFRztBQUNVLFFBQUEsTUFBTSxHQUFHO0lBQ2xCLEdBQUc7SUFDSCxHQUFHO0lBQ0gsSUFBSTtDQUNQLENBQUMifQ==