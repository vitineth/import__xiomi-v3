export declare function verifyRequired(): boolean;
export declare function loadEasy(path: string, priority?: number): any;
export declare function load(path: string, priority?: number): any;
export declare function has(path: string): boolean;
export declare function get(path: string): any;
/**
 * A set of all externally used config functions (get, has, load) as a simple object.
 */
export declare const Config: {
    get: typeof get;
    has: typeof has;
    load: typeof load;
};
/**
 * The type of {@link Config} for using as types when passing
 */
export declare type ConfigType = typeof Config;
