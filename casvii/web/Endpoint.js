"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Endpoint = exports.Method = exports.AuthenticationRequirement = void 0;
var AuthenticationRequirement;
(function (AuthenticationRequirement) {
    AuthenticationRequirement[AuthenticationRequirement["AUTHENTICATED"] = 0] = "AUTHENTICATED";
    AuthenticationRequirement[AuthenticationRequirement["UNAUTHENTICATED"] = 1] = "UNAUTHENTICATED";
})(AuthenticationRequirement = exports.AuthenticationRequirement || (exports.AuthenticationRequirement = {}));
var Method;
(function (Method) {
    Method["DELETE"] = "delete";
    Method["GET"] = "get";
    Method["HEAD"] = "head";
    Method["POST"] = "post";
    Method["PUT"] = "put";
})(Method = exports.Method || (exports.Method = {}));
class Endpoint {
    constructor(composite, method, url, isAPI, requirement, hiddenOnUnauthenticated = false, permissions = undefined, provisional = null) {
        this._requiredProvisionalStatus = null;
        this._composite = composite;
        this._method = method;
        this._url = url;
        this._isAPI = isAPI;
        this._hiddenOnUnauthenticated = hiddenOnUnauthenticated;
        this._permissions = permissions;
        this._requirement = requirement;
        this._requiredProvisionalStatus = provisional;
    }
    get method() {
        return this._method;
    }
    get url() {
        return this._url;
    }
    get isAPI() {
        return this._isAPI;
    }
    get hiddenOnUnauthenticated() {
        return this._hiddenOnUnauthenticated;
    }
    get permissions() {
        return this._permissions;
    }
    get requirement() {
        return this._requirement;
    }
    get requiredProvisionalStatus() {
        return this._requiredProvisionalStatus;
    }
}
exports.Endpoint = Endpoint;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRW5kcG9pbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL3dlYi9FbmRwb2ludC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFPQSxJQUFZLHlCQUdYO0FBSEQsV0FBWSx5QkFBeUI7SUFDakMsMkZBQWEsQ0FBQTtJQUNiLCtGQUFlLENBQUE7QUFDbkIsQ0FBQyxFQUhXLHlCQUF5QixHQUF6QixpQ0FBeUIsS0FBekIsaUNBQXlCLFFBR3BDO0FBRUQsSUFBWSxNQU1YO0FBTkQsV0FBWSxNQUFNO0lBQ2QsMkJBQWlCLENBQUE7SUFDakIscUJBQVcsQ0FBQTtJQUNYLHVCQUFhLENBQUE7SUFDYix1QkFBYSxDQUFBO0lBQ2IscUJBQVcsQ0FBQTtBQUNmLENBQUMsRUFOVyxNQUFNLEdBQU4sY0FBTSxLQUFOLGNBQU0sUUFNakI7QUFFRCxNQUFzQixRQUFRO0lBWTFCLFlBQXNCLFNBQXVCLEVBQUUsTUFBYyxFQUFFLEdBQW9CLEVBQUUsS0FBYyxFQUFFLFdBQXNDLEVBQUUsMEJBQW1DLEtBQUssRUFBRSxjQUFvQyxTQUFTLEVBQUUsY0FBc0QsSUFBSTtRQUZ4UiwrQkFBMEIsR0FBMkMsSUFBSSxDQUFDO1FBRzlFLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBRTVCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyx3QkFBd0IsR0FBRyx1QkFBdUIsQ0FBQztRQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQztRQUNoQyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsV0FBVyxDQUFDO0lBQ2xELENBQUM7SUFJRCxJQUFJLE1BQU07UUFDTixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQUksR0FBRztRQUNILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLHVCQUF1QjtRQUN2QixPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN6QyxDQUFDO0lBRUQsSUFBSSxXQUFXO1FBQ1gsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQUkseUJBQXlCO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLDBCQUEwQixDQUFDO0lBQzNDLENBQUM7Q0FDSjtBQXJERCw0QkFxREMifQ==