"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Register = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
var Register;
(function (Register) {
    class RegisterWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/user/register', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            try {
                const questions = await this._composite.daoCollection.securityQuestionDAO.findAllQuestions();
                response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'register.html'), {
                    questions,
                }, this.renderer).catch((e) => {
                    _.error(`failed to render user registration page`, { e });
                    WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
                });
            }
            catch (e) {
                _.error(`failed to render page due to a thrown exception (likely security questions)`);
                console.error(e);
            }
        }
    }
    Register.RegisterWeb = RegisterWeb;
    class RegisterAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/user/register', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.userProcesses.registerUser(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle user registration request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    Register.RegisterAPI = RegisterAPI;
})(Register = exports.Register || (exports.Register = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvdXNlci9yZWdpc3Rlci53ZWIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDBDQUEwRTtBQU8xRSwrQkFBNEI7QUFDNUIsK0RBQWlFO0FBQ2pFLGdGQUE2RTtBQUM3RSwwQ0FBNEI7QUFFNUIsSUFBaUIsUUFBUSxDQTRDeEI7QUE1Q0QsV0FBaUIsUUFBUTtJQUVyQixNQUFhLFdBQVksU0FBUSxtQkFBUTtRQUVyQyxZQUFZLFNBQXVCLEVBQVUsUUFBZ0I7WUFDekQsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsb0NBQXlCLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRC9ELGFBQVEsR0FBUixRQUFRLENBQVE7UUFFN0QsQ0FBQztRQUVELEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBZ0IsRUFBRSxRQUFxQixFQUFFLE1BQW9CLEVBQUUsU0FBdUI7WUFDL0YsSUFBSTtnQkFDQSxNQUFNLFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzdGLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxlQUFlLENBQUMsRUFBRTtvQkFDbkcsU0FBUztpQkFDWixFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDMUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzFELGdDQUFpQixDQUFDLDhCQUE4QixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDdEUsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNSLENBQUMsQ0FBQyxLQUFLLENBQUMsNkVBQTZFLENBQUMsQ0FBQztnQkFDdkYsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwQjtRQUNMLENBQUM7S0FFSjtJQXJCWSxvQkFBVyxjQXFCdkIsQ0FBQTtJQUVELE1BQWEsV0FBWSxTQUFRLG1CQUFRO1FBRXJDLFlBQVksU0FBdUI7WUFDL0IsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLElBQUksRUFBRSxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsb0NBQXlCLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2hILENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLCtDQUErQztZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLCtCQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFDL0YsZ0NBQWlCLENBQUMsdUJBQXVCLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzdELENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUNYLENBQUMsQ0FBQyxLQUFLLENBQUMsNENBQTRDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNmLGdDQUFpQixDQUFDLDhCQUE4QixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0UsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0tBRUo7SUFqQlksb0JBQVcsY0FpQnZCLENBQUE7QUFFTCxDQUFDLEVBNUNnQixRQUFRLEdBQVIsZ0JBQVEsS0FBUixnQkFBUSxRQTRDeEIifQ==