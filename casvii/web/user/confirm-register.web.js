"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmRegister = void 0;
const Endpoint_1 = require("../Endpoint");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
const ProcessImpl_1 = require("../../actions/ProcessImpl");
const path_1 = require("path");
var ConfirmRegister;
(function (ConfirmRegister) {
    class ConfirmRegisterAPI extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/api/user/confirm.+', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            if (!request.query.hasOwnProperty(`ut`) || !request.query.hasOwnProperty(`rt`) || request.query.ut === undefined || request.query.rt === undefined) {
                response.sendStatus(ProcessImpl_1.HttpStatusCode.NOT_FOUND);
                return;
            }
            this._composite.userProcesses.registerConfirmation({
                ut: request.query.ut,
                rt: request.query.rt,
            }, new RequestSession_1.RequestSession(request)).then((cas) => {
                response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'registered.html'), {}, this.renderer).catch((e) => {
                    _.error(`failed to render registered page`);
                    WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
                });
            }).catch((e) => {
                _.error('Failed to handle confirm register request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    ConfirmRegister.ConfirmRegisterAPI = ConfirmRegisterAPI;
})(ConfirmRegister = exports.ConfirmRegister || (exports.ConfirmRegister = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1yZWdpc3Rlci53ZWIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY2FzdmlpL3dlYi91c2VyL2NvbmZpcm0tcmVnaXN0ZXIud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFNMUUsK0RBQWlFO0FBQ2pFLGdGQUE2RTtBQUM3RSwwQ0FBNEI7QUFDNUIsMkRBQTJEO0FBQzNELCtCQUE0QjtBQUc1QixJQUFpQixlQUFlLENBK0IvQjtBQS9CRCxXQUFpQixlQUFlO0lBRTVCLE1BQWEsa0JBQW1CLFNBQVEsbUJBQVE7UUFFNUMsWUFBWSxTQUF1QixFQUFVLFFBQWdCO1lBQ3pELEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxHQUFHLEVBQUUscUJBQXFCLEVBQUUsSUFBSSxFQUFFLG9DQUF5QixDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQztZQURuRSxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBRTdELENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLFNBQVMsSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7Z0JBQ2hKLFFBQVEsQ0FBQyxVQUFVLENBQUMsNEJBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDOUMsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUM7Z0JBQy9DLEVBQUUsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7YUFDdkIsRUFBRSxJQUFJLCtCQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFDekMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDcEksQ0FBQyxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO29CQUM1QyxnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3RFLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQywyQ0FBMkMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzVELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQTNCWSxrQ0FBa0IscUJBMkI5QixDQUFBO0FBRUwsQ0FBQyxFQS9CZ0IsZUFBZSxHQUFmLHVCQUFlLEtBQWYsdUJBQWUsUUErQi9CIn0=