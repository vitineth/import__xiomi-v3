"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestPasswordChange = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
var RequestPasswordChange;
(function (RequestPasswordChange) {
    class RequestPasswordChangeWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/password/request', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'change-password.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render requesting unauthenticated password change page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    RequestPasswordChange.RequestPasswordChangeWeb = RequestPasswordChangeWeb;
    class RequestPasswordChangeAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/password/request', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.passwordProcesses.changePasswordUnauthenticatedRequest(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle requesting unauthenticated password change request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    RequestPasswordChange.RequestPasswordChangeAPI = RequestPasswordChangeAPI;
})(RequestPasswordChange = exports.RequestPasswordChange || (exports.RequestPasswordChange = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWVzdC1wYXNzd29yZC1jaGFuZ2Uud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvcGFzc3dvcmQvcmVxdWVzdC1wYXNzd29yZC1jaGFuZ2Uud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFPMUUsK0JBQTRCO0FBQzVCLCtEQUFpRTtBQUNqRSxnRkFBNkU7QUFDN0UsMENBQTRCO0FBRTVCLElBQWlCLHFCQUFxQixDQW9DckM7QUFwQ0QsV0FBaUIscUJBQXFCO0lBRWxDLE1BQWEsd0JBQXlCLFNBQVEsbUJBQVE7UUFFbEQsWUFBWSxTQUF1QixFQUFVLFFBQWdCO1lBQ3pELEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxHQUFHLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLG9DQUF5QixDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQztZQURsRSxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBRTdELENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pJLENBQUMsQ0FBQyxLQUFLLENBQUMsa0VBQWtFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNuRixnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0tBRUo7SUFiWSw4Q0FBd0IsMkJBYXBDLENBQUE7SUFFRCxNQUFhLHdCQUF5QixTQUFRLG1CQUFRO1FBRWxELFlBQVksU0FBdUI7WUFDL0IsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLElBQUksRUFBRSx1QkFBdUIsRUFBRSxJQUFJLEVBQUUsb0NBQXlCLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25ILENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLCtDQUErQztZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLG9DQUFvQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSwrQkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQzNILGdDQUFpQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUM3RCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDWCxDQUFDLENBQUMsS0FBSyxDQUFDLHFFQUFxRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZixnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUVKO0lBakJZLDhDQUF3QiwyQkFpQnBDLENBQUE7QUFFTCxDQUFDLEVBcENnQixxQkFBcUIsR0FBckIsNkJBQXFCLEtBQXJCLDZCQUFxQixRQW9DckMifQ==