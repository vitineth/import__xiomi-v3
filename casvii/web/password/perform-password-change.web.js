"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PerformPasswordChange = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
var PerformPasswordChange;
(function (PerformPasswordChange) {
    class PerformPasswordChangeWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/password/perform', false, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true, ['casvii.self-serve.password']);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'change-authenticated.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render authenticated password change page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    PerformPasswordChange.PerformPasswordChangeWeb = PerformPasswordChangeWeb;
    class PerformPasswordChangeAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/password/perform', true, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true, ['casvii.self-serve.password']);
        }
        async handle(request, response, domain, namespace) {
            this._composite.passwordProcesses.changePasswordAuthenticated(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle authenticated password change request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    PerformPasswordChange.PerformPasswordChangeAPI = PerformPasswordChangeAPI;
})(PerformPasswordChange = exports.PerformPasswordChange || (exports.PerformPasswordChange = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyZm9ybS1wYXNzd29yZC1jaGFuZ2Uud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvcGFzc3dvcmQvcGVyZm9ybS1wYXNzd29yZC1jaGFuZ2Uud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFPMUUsK0JBQTRCO0FBQzVCLCtEQUFpRTtBQUNqRSxnRkFBNkU7QUFDN0UsMENBQTRCO0FBRTVCLElBQWlCLHFCQUFxQixDQW1DckM7QUFuQ0QsV0FBaUIscUJBQXFCO0lBRWxDLE1BQWEsd0JBQXlCLFNBQVEsbUJBQVE7UUFFbEQsWUFBWSxTQUF1QixFQUFVLFFBQWdCO1lBQ3pELEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxHQUFHLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLG9DQUF5QixDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7WUFEL0YsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUU3RCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRixRQUFRLENBQUMsVUFBVSxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsMkJBQTJCLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUM5SSxDQUFDLENBQUMsS0FBSyxDQUFDLHFEQUFxRCxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdEUsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUVKO0lBYlksOENBQXdCLDJCQWFwQyxDQUFBO0lBRUQsTUFBYSx3QkFBeUIsU0FBUSxtQkFBUTtRQUVsRCxZQUFZLFNBQXVCO1lBQy9CLEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxJQUFJLEVBQUUsdUJBQXVCLEVBQUUsSUFBSSxFQUFFLG9DQUF5QixDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7UUFDaEosQ0FBQztRQUVELEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBZ0IsRUFBRSxRQUFxQixFQUFFLE1BQW9CLEVBQUUsU0FBdUI7WUFDL0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksK0JBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNsSCxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyx3REFBd0QsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWhCWSw4Q0FBd0IsMkJBZ0JwQyxDQUFBO0FBRUwsQ0FBQyxFQW5DZ0IscUJBQXFCLEdBQXJCLDZCQUFxQixLQUFyQiw2QkFBcUIsUUFtQ3JDIn0=