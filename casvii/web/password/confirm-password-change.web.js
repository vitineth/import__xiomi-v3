"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfirmPasswordChange = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
const ProcessImpl_1 = require("../../actions/ProcessImpl");
var ConfirmPasswordChange;
(function (ConfirmPasswordChange) {
    class ConfirmPasswordChangeWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/password/confirm.+', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            if (!request.query.hasOwnProperty(`ut`) || !request.query.hasOwnProperty(`rt`) || request.query.ut === undefined || request.query.rt === undefined) {
                response.sendStatus(ProcessImpl_1.HttpStatusCode.NOT_FOUND);
                return;
            }
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'password-confirm.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render login page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    ConfirmPasswordChange.ConfirmPasswordChangeWeb = ConfirmPasswordChangeWeb;
    class ConfirmPasswordChangeAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/password/confirm', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            this._composite.passwordProcesses.changePasswordUnauthenticatedSubmit(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle confirm password change request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    ConfirmPasswordChange.ConfirmPasswordChangeAPI = ConfirmPasswordChangeAPI;
})(ConfirmPasswordChange = exports.ConfirmPasswordChange || (exports.ConfirmPasswordChange = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1wYXNzd29yZC1jaGFuZ2Uud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvcGFzc3dvcmQvY29uZmlybS1wYXNzd29yZC1jaGFuZ2Uud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFPMUUsK0JBQTRCO0FBQzVCLCtEQUFpRTtBQUNqRSxnRkFBNkU7QUFDN0UsMENBQTRCO0FBQzVCLDJEQUEyRDtBQUUzRCxJQUFpQixxQkFBcUIsQ0F3Q3JDO0FBeENELFdBQWlCLHFCQUFxQjtJQUVsQyxNQUFhLHdCQUF5QixTQUFRLG1CQUFRO1FBRWxELFlBQVksU0FBdUIsRUFBVSxRQUFnQjtZQUN6RCxLQUFLLENBQUMsU0FBUyxFQUFFLGlCQUFNLENBQUMsR0FBRyxFQUFFLHFCQUFxQixFQUFFLEtBQUssRUFBRSxvQ0FBeUIsQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFEcEUsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUU3RCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUFFO2dCQUNoSixRQUFRLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlDLE9BQU87YUFDVjtZQUVELFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSx1QkFBdUIsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFJLENBQUMsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM5QyxnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0tBRUo7SUFsQlksOENBQXdCLDJCQWtCcEMsQ0FBQTtJQUVELE1BQWEsd0JBQXlCLFNBQVEsbUJBQVE7UUFFbEQsWUFBWSxTQUF1QjtZQUMvQixLQUFLLENBQUMsU0FBUyxFQUFFLGlCQUFNLENBQUMsSUFBSSxFQUFFLHVCQUF1QixFQUFFLElBQUksRUFBRSxvQ0FBeUIsQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkgsQ0FBQztRQUVELEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBZ0IsRUFBRSxRQUFxQixFQUFFLE1BQW9CLEVBQUUsU0FBdUI7WUFDL0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxtQ0FBbUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksK0JBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUMxSCxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyxrREFBa0QsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWhCWSw4Q0FBd0IsMkJBZ0JwQyxDQUFBO0FBRUwsQ0FBQyxFQXhDZ0IscUJBQXFCLEdBQXJCLDZCQUFxQixLQUFyQiw2QkFBcUIsUUF3Q3JDIn0=