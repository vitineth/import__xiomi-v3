"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwoFactorSubmission = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
var TwoFactorSubmission;
(function (TwoFactorSubmission) {
    class TwoFactorSubmissionWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/auth/two-factor/submit', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'two-factor-submit.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render two factor submission page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    TwoFactorSubmission.TwoFactorSubmissionWeb = TwoFactorSubmissionWeb;
    class TwoFactorSubmissionAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/auth/two-factor/submit', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.authenticationProcesses.verifyLoginWithTwoFactor(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle two factor submission request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    TwoFactorSubmission.TwoFactorSubmissionAPI = TwoFactorSubmissionAPI;
})(TwoFactorSubmission = exports.TwoFactorSubmission || (exports.TwoFactorSubmission = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHdvLWZhY3Rvci1zdWJtaXNzaW9uLndlYi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvd2ViL2F1dGgvdHdvLWZhY3Rvci1zdWJtaXNzaW9uLndlYi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMENBQTBFO0FBTzFFLCtCQUE0QjtBQUM1QiwrREFBaUU7QUFDakUsZ0ZBQTZFO0FBQzdFLDBDQUE0QjtBQUU1QixJQUFpQixtQkFBbUIsQ0FvQ25DO0FBcENELFdBQWlCLG1CQUFtQjtJQUVoQyxNQUFhLHNCQUF1QixTQUFRLG1CQUFRO1FBRWhELFlBQVksU0FBdUIsRUFBVSxRQUFnQjtZQUN6RCxLQUFLLENBQUMsU0FBUyxFQUFFLGlCQUFNLENBQUMsR0FBRyxFQUFFLHlCQUF5QixFQUFFLEtBQUssRUFBRSxvQ0FBeUIsQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFEeEUsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUU3RCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRixRQUFRLENBQUMsVUFBVSxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsd0JBQXdCLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUMzSSxDQUFDLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUVKO0lBYlksMENBQXNCLHlCQWFsQyxDQUFBO0lBRUQsTUFBYSxzQkFBdUIsU0FBUSxtQkFBUTtRQUVoRCxZQUFZLFNBQXVCO1lBQy9CLEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxJQUFJLEVBQUUsNkJBQTZCLEVBQUUsSUFBSSxFQUFFLG9DQUF5QixDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6SCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRiwrQ0FBK0M7WUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksK0JBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNySCxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWpCWSwwQ0FBc0IseUJBaUJsQyxDQUFBO0FBRUwsQ0FBQyxFQXBDZ0IsbUJBQW1CLEdBQW5CLDJCQUFtQixLQUFuQiwyQkFBbUIsUUFvQ25DIn0=