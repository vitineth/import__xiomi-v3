"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logout = void 0;
const Endpoint_1 = require("../Endpoint");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const _ = __importStar(require("logger"));
var Logout;
(function (Logout) {
    class LogoutAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.GET, '/api/auth/logout', true, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.authenticationProcesses.logout(new RequestSession_1.RequestSession(request)).then((cas) => {
                if (cas.statusCode === 200) {
                    response.header('Location', '/auth/login');
                    response.sendStatus(302);
                    return;
                }
                response.sendStatus(cas.statusCode);
            }).catch((e) => {
                _.error('Failed to handle logout request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    Logout.LogoutAPI = LogoutAPI;
})(Logout = exports.Logout || (exports.Logout = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nb3V0LndlYi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvd2ViL2F1dGgvbG9nb3V0LndlYi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMENBQTBFO0FBTTFFLGdGQUE2RTtBQUM3RSwrREFBaUU7QUFDakUsMENBQTRCO0FBRTVCLElBQWlCLE1BQU0sQ0EwQnRCO0FBMUJELFdBQWlCLE1BQU07SUFDbkIsTUFBYSxTQUFVLFNBQVEsbUJBQVE7UUFFbkMsWUFBWSxTQUF1QjtZQUMvQixLQUFLLENBQUMsU0FBUyxFQUFFLGlCQUFNLENBQUMsR0FBRyxFQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRSxvQ0FBeUIsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUcsQ0FBQztRQUVELEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBZ0IsRUFBRSxRQUFxQixFQUFFLE1BQW9CLEVBQUUsU0FBdUI7WUFDL0YsK0NBQStDO1lBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLElBQUksK0JBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNyRixJQUFJLEdBQUcsQ0FBQyxVQUFVLEtBQUssR0FBRyxFQUFFO29CQUN4QixRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQztvQkFDM0MsUUFBUSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDekIsT0FBTztpQkFDVjtnQkFFRCxRQUFRLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDWCxDQUFDLENBQUMsS0FBSyxDQUFDLGlDQUFpQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZixnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUVKO0lBdkJZLGdCQUFTLFlBdUJyQixDQUFBO0FBRUwsQ0FBQyxFQTFCZ0IsTUFBTSxHQUFOLGNBQU0sS0FBTixjQUFNLFFBMEJ0QiJ9