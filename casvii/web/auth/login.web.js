"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Login = void 0;
const Endpoint_1 = require("../Endpoint");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const _ = __importStar(require("logger"));
const path_1 = require("path");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const ProcessImpl_1 = require("../../actions/ProcessImpl");
var Login;
(function (Login) {
    class LoginWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/auth/login', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'login.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render login page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    Login.LoginWeb = LoginWeb;
    class LoginAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/auth/login', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.authenticationProcesses.login(request.body, request.ip, new RequestSession_1.RequestSession(request)).then((cas) => {
                if (cas.statusCode === ProcessImpl_1.HttpStatusCode.REDIRECT && cas.additionalData !== undefined) {
                    response.header('Location', cas.additionalData.redirect);
                    response.json({
                        success: 'partial',
                        message: cas.externalMessage,
                        url: cas.additionalData.redirect,
                    });
                    return;
                }
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle login request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    Login.LoginAPI = LoginAPI;
})(Login = exports.Login || (exports.Login = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4ud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvYXV0aC9sb2dpbi53ZWIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDBDQUEwRTtBQU0xRSwrREFBaUU7QUFDakUsMENBQTRCO0FBQzVCLCtCQUE0QjtBQUU1QixnRkFBNkU7QUFDN0UsMkRBQTJEO0FBRTNELElBQWlCLEtBQUssQ0E2Q3JCO0FBN0NELFdBQWlCLEtBQUs7SUFFbEIsTUFBYSxRQUFTLFNBQVEsbUJBQVE7UUFFbEMsWUFBWSxTQUF1QixFQUFVLFFBQWdCO1lBQ3pELEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxHQUFHLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxvQ0FBeUIsQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFENUQsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUU3RCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRixRQUFRLENBQUMsVUFBVSxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDL0gsQ0FBQyxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzlDLGdDQUFpQixDQUFDLDhCQUE4QixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUN0RSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWJZLGNBQVEsV0FhcEIsQ0FBQTtJQUVELE1BQWEsUUFBUyxTQUFRLG1CQUFRO1FBRWxDLFlBQVksU0FBdUI7WUFDL0IsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLElBQUksRUFBRSxpQkFBaUIsRUFBRSxJQUFJLEVBQUUsb0NBQXlCLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdHLENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLCtDQUErQztZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFFLEVBQUUsSUFBSSwrQkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQzlHLElBQUksR0FBRyxDQUFDLFVBQVUsS0FBSyw0QkFBYyxDQUFDLFFBQVEsSUFBSSxHQUFHLENBQUMsY0FBYyxLQUFLLFNBQVMsRUFBRTtvQkFDaEYsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQzt3QkFDVixPQUFPLEVBQUUsU0FBUzt3QkFDbEIsT0FBTyxFQUFFLEdBQUcsQ0FBQyxlQUFlO3dCQUM1QixHQUFHLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxRQUFRO3FCQUNuQyxDQUFDLENBQUM7b0JBQ0gsT0FBTztpQkFDVjtnQkFDRCxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQTFCWSxjQUFRLFdBMEJwQixDQUFBO0FBRUwsQ0FBQyxFQTdDZ0IsS0FBSyxHQUFMLGFBQUssS0FBTCxhQUFLLFFBNkNyQiJ9