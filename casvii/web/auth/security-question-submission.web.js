"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SecurityQuestionSubmission = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
const AuthenticationProcessImpl_1 = require("../../actions/process/AuthenticationProcessImpl");
var SecurityQuestionSubmission;
(function (SecurityQuestionSubmission) {
    var requestToSession = WebUtilities_1.RequestUtilities.requestToSession;
    class SecurityQuestionSubmissionWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/auth/security-question/submit', false, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false, undefined, AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            // On each request here, we should update the security question the user is seeing and save it in the session
            // AuthenticationProcessImpl.ts checks for session key `login.securityQuestion` so we need to set that.
            try {
                const session = requestToSession(request);
                const question = await this._composite.authenticationProcesses.getRandomSecurityQuestion(session);
                session.set(`login.securityQuestion`, question.id);
                response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'security-question-submit.html'), {
                    question,
                }, this.renderer).catch((e) => {
                    _.error(`failed to render security question submission page`, { e });
                    WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
                });
            }
            catch (e) {
                console.error(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            }
        }
    }
    SecurityQuestionSubmission.SecurityQuestionSubmissionWeb = SecurityQuestionSubmissionWeb;
    class SecurityQuestionSubmissionAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/auth/security-question/submit', true, Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED, false);
        }
        async handle(request, response, domain, namespace) {
            this._composite.authenticationProcesses.verifyLoginWithSecurityQuestion(request.body, new RequestSession_1.RequestSession(request), request.ip).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle security question submission request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    SecurityQuestionSubmission.SecurityQuestionSubmissionAPI = SecurityQuestionSubmissionAPI;
})(SecurityQuestionSubmission = exports.SecurityQuestionSubmission || (exports.SecurityQuestionSubmission = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VjdXJpdHktcXVlc3Rpb24tc3VibWlzc2lvbi53ZWIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY2FzdmlpL3dlYi9hdXRoL3NlY3VyaXR5LXF1ZXN0aW9uLXN1Ym1pc3Npb24ud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFPMUUsK0JBQTRCO0FBQzVCLCtEQUFtRjtBQUNuRixnRkFBNkU7QUFDN0UsMENBQTRCO0FBQzVCLCtGQUFrRztBQUVsRyxJQUFpQiwwQkFBMEIsQ0FvRDFDO0FBcERELFdBQWlCLDBCQUEwQjtJQUV2QyxJQUFPLGdCQUFnQixHQUFHLCtCQUFnQixDQUFDLGdCQUFnQixDQUFDO0lBRTVELE1BQWEsNkJBQThCLFNBQVEsbUJBQVE7UUFFdkQsWUFBWSxTQUF1QixFQUFVLFFBQWdCO1lBQ3pELEtBQUssQ0FBQyxTQUFTLEVBQUUsaUJBQU0sQ0FBQyxHQUFHLEVBQUUsZ0NBQWdDLEVBQUUsS0FBSyxFQUFFLG9DQUF5QixDQUFDLGVBQWUsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLDJEQUErQixDQUFDLDBCQUEwQixDQUFDLENBQUM7WUFEdEosYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUU3RCxDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRiw2R0FBNkc7WUFDN0csdUdBQXVHO1lBQ3ZHLElBQUk7Z0JBQ0EsTUFBTSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBRTFDLE1BQU0sUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEcsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRW5ELFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSwrQkFBK0IsQ0FBQyxFQUFFO29CQUNuSCxRQUFRO2lCQUNYLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUMxQixDQUFDLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDckUsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN0RSxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1IsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakIsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ3JFO1FBRUwsQ0FBQztLQUVKO0lBNUJZLHdEQUE2QixnQ0E0QnpDLENBQUE7SUFFRCxNQUFhLDZCQUE4QixTQUFRLG1CQUFRO1FBRXZELFlBQVksU0FBdUI7WUFDL0IsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLElBQUksRUFBRSxvQ0FBb0MsRUFBRSxJQUFJLEVBQUUsb0NBQXlCLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2hJLENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsK0JBQStCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLCtCQUFjLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUN4SSxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyx1REFBdUQsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWhCWSx3REFBNkIsZ0NBZ0J6QyxDQUFBO0FBRUwsQ0FBQyxFQXBEZ0IsMEJBQTBCLEdBQTFCLGtDQUEwQixLQUExQixrQ0FBMEIsUUFvRDFDIn0=