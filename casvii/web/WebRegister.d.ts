import { CASComposite } from "../integration/CASComposite";
import { CASEndpointsRegister } from "../integration/CASEndpointsRegister";
import { Liquid } from "liquidjs";
export declare namespace WebRegister {
    function register(composite: CASComposite, register: CASEndpointsRegister, renderer: Liquid): void;
}
