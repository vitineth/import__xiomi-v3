"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelfAuthorize = void 0;
const Endpoint_1 = require("../Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../../utilities/WebUtilities");
const RequestSession_1 = require("../../utilities/session/impl/RequestSession");
const _ = __importStar(require("logger"));
var SelfAuthorize;
(function (SelfAuthorize) {
    class SelfAuthorizeWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/permissions/self-authorize.+', false, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true, ['casvii.permission.self-grant']);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            // If they don't provide an ID, hide it
            if (!request.query.hasOwnProperty('id') || request.query.id === undefined) {
                response.sendStatus(404);
                return;
            }
            const permissions = await this._composite.permissionProcesses.getPermissionsFromSelfGrantID(parseInt(request.query.id.toString(), 10));
            response.renderFile(path_1.join(__dirname, '..', '..', '..', '..', 'cas-data', 'templates', 'self-authorize.html'), {
                scopes: permissions,
            }, this.renderer).catch((e) => {
                _.error(`failed to render self-authorize  page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    SelfAuthorize.SelfAuthorizeWeb = SelfAuthorizeWeb;
    class SelfAuthorizeAPI extends Endpoint_1.Endpoint {
        constructor(composite) {
            super(composite, Endpoint_1.Method.POST, '/api/permissions/self-authorize', true, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true, ['casvii.permission.self-grant']);
        }
        async handle(request, response, domain, namespace) {
            // @ts-ignore - TODO: session is also undefined
            this._composite.permissionProcesses.activateSelfPermissionGrant(request.body, new RequestSession_1.RequestSession(request)).then((cas) => {
                WebUtilities_1.ResponseUtilities.sendDefaultJSONResponse(cas, response);
            }).catch((e) => {
                _.error('Failed to handle self-authorize request', { e });
                console.log(e);
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, this.isAPI);
            });
        }
    }
    SelfAuthorize.SelfAuthorizeAPI = SelfAuthorizeAPI;
})(SelfAuthorize = exports.SelfAuthorize || (exports.SelfAuthorize = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZi1hdXRob3JpemUud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvcGVybWlzc2lvbnMvc2VsZi1hdXRob3JpemUud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwQ0FBMEU7QUFPMUUsK0JBQTRCO0FBQzVCLCtEQUFpRTtBQUNqRSxnRkFBNkU7QUFDN0UsMENBQTRCO0FBRTVCLElBQWlCLGFBQWEsQ0E4QzdCO0FBOUNELFdBQWlCLGFBQWE7SUFFMUIsTUFBYSxnQkFBaUIsU0FBUSxtQkFBUTtRQUUxQyxZQUFZLFNBQXVCLEVBQVUsUUFBZ0I7WUFDekQsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLEdBQUcsRUFBRSwrQkFBK0IsRUFBRSxLQUFLLEVBQUUsb0NBQXlCLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQztZQUQ3RyxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBRTdELENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLHVDQUF1QztZQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUFFO2dCQUN2RSxRQUFRLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QixPQUFPO2FBQ1Y7WUFFRCxNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsNkJBQTZCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFdkksUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLHFCQUFxQixDQUFDLEVBQUU7Z0JBQ3pHLE1BQU0sRUFBRSxXQUFXO2FBQ3RCLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUMxQixDQUFDLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEQsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUVKO0lBdkJZLDhCQUFnQixtQkF1QjVCLENBQUE7SUFFRCxNQUFhLGdCQUFpQixTQUFRLG1CQUFRO1FBRTFDLFlBQVksU0FBdUI7WUFDL0IsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLElBQUksRUFBRSxpQ0FBaUMsRUFBRSxJQUFJLEVBQUUsb0NBQXlCLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQztRQUM1SixDQUFDO1FBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFnQixFQUFFLFFBQXFCLEVBQUUsTUFBb0IsRUFBRSxTQUF1QjtZQUMvRiwrQ0FBK0M7WUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksK0JBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNwSCxnQ0FBaUIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDN0QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ1gsQ0FBQyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsZ0NBQWlCLENBQUMsOEJBQThCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FFSjtJQWpCWSw4QkFBZ0IsbUJBaUI1QixDQUFBO0FBRUwsQ0FBQyxFQTlDZ0IsYUFBYSxHQUFiLHFCQUFhLEtBQWIscUJBQWEsUUE4QzdCIn0=