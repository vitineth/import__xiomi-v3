"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebRegister = void 0;
const Endpoint_1 = require("./Endpoint");
const login_web_1 = require("./auth/login.web");
const _ = __importStar(require("logger"));
const logout_web_1 = require("./auth/logout.web");
const security_question_submission_web_1 = require("./auth/security-question-submission.web");
const two_factor_submission_web_1 = require("./auth/two-factor-submission.web");
const confirm_password_change_web_1 = require("./password/confirm-password-change.web");
const perform_password_change_web_1 = require("./password/perform-password-change.web");
const request_password_change_web_1 = require("./password/request-password-change.web");
const self_authorize_web_1 = require("./permissions/self-authorize.web");
const confirm_register_web_1 = require("./user/confirm-register.web");
const register_web_1 = require("./user/register.web");
const index_web_1 = require("./index.web");
var WebRegister;
(function (WebRegister) {
    const endpoints = [
        login_web_1.Login.LoginAPI,
        // @ts-ignore
        login_web_1.Login.LoginWeb,
        logout_web_1.Logout.LogoutAPI,
        security_question_submission_web_1.SecurityQuestionSubmission.SecurityQuestionSubmissionAPI,
        // @ts-ignore
        security_question_submission_web_1.SecurityQuestionSubmission.SecurityQuestionSubmissionWeb,
        two_factor_submission_web_1.TwoFactorSubmission.TwoFactorSubmissionAPI,
        // @ts-ignore
        two_factor_submission_web_1.TwoFactorSubmission.TwoFactorSubmissionWeb,
        confirm_password_change_web_1.ConfirmPasswordChange.ConfirmPasswordChangeAPI,
        // @ts-ignore
        confirm_password_change_web_1.ConfirmPasswordChange.ConfirmPasswordChangeWeb,
        perform_password_change_web_1.PerformPasswordChange.PerformPasswordChangeAPI,
        // @ts-ignore
        perform_password_change_web_1.PerformPasswordChange.PerformPasswordChangeWeb,
        request_password_change_web_1.RequestPasswordChange.RequestPasswordChangeAPI,
        // @ts-ignore
        request_password_change_web_1.RequestPasswordChange.RequestPasswordChangeWeb,
        self_authorize_web_1.SelfAuthorize.SelfAuthorizeAPI,
        // @ts-ignore
        self_authorize_web_1.SelfAuthorize.SelfAuthorizeWeb,
        // @ts-ignore
        confirm_register_web_1.ConfirmRegister.ConfirmRegisterAPI,
        register_web_1.Register.RegisterAPI,
        // @ts-ignore
        register_web_1.Register.RegisterWeb,
        // @ts-ignore
        index_web_1.Index.IndexWeb,
    ];
    function register(composite, register, renderer) {
        endpoints.forEach((e) => {
            try {
                // @ts-ignore - TODO: find a nice way to do this
                const end = new e(composite, renderer);
                register[end.method.toString()](end.url, end.requirement === Endpoint_1.AuthenticationRequirement.AUTHENTICATED, end.handle, {
                    useJSON: end.isAPI,
                    hide: end.hiddenOnUnauthenticated,
                    requireUnauthenticated: end.requirement === Endpoint_1.AuthenticationRequirement.UNAUTHENTICATED,
                    requirePermissions: end.permissions === undefined ? [] : end.permissions,
                    requiredProvisionalStatus: end.requiredProvisionalStatus,
                }, end);
                _.info(`registered endpoint ${e.name} at ${end.url} using ${register[end.method.toString()].name}`);
            }
            catch (e) {
                _.error(`failed to register endpoint ${e} because it did not accept just the composite element`);
                throw e;
            }
        });
    }
    WebRegister.register = register;
})(WebRegister = exports.WebRegister || (exports.WebRegister = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2ViUmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL3dlYi9XZWJSZWdpc3Rlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EseUNBQWlFO0FBRWpFLGdEQUF5QztBQUN6QywwQ0FBNEI7QUFFNUIsa0RBQTJDO0FBQzNDLDhGQUFxRjtBQUNyRixnRkFBdUU7QUFDdkUsd0ZBQStFO0FBQy9FLHdGQUErRTtBQUMvRSx3RkFBK0U7QUFDL0UseUVBQWlFO0FBQ2pFLHNFQUE4RDtBQUM5RCxzREFBK0M7QUFDL0MsMkNBQW9DO0FBRXBDLElBQWlCLFdBQVcsQ0F5RDNCO0FBekRELFdBQWlCLFdBQVc7SUFJeEIsTUFBTSxTQUFTLEdBQXdCO1FBQ25DLGlCQUFLLENBQUMsUUFBUTtRQUNkLGFBQWE7UUFDYixpQkFBSyxDQUFDLFFBQVE7UUFDZCxtQkFBTSxDQUFDLFNBQVM7UUFDaEIsNkRBQTBCLENBQUMsNkJBQTZCO1FBQ3hELGFBQWE7UUFDYiw2REFBMEIsQ0FBQyw2QkFBNkI7UUFDeEQsK0NBQW1CLENBQUMsc0JBQXNCO1FBQzFDLGFBQWE7UUFDYiwrQ0FBbUIsQ0FBQyxzQkFBc0I7UUFDMUMsbURBQXFCLENBQUMsd0JBQXdCO1FBQzlDLGFBQWE7UUFDYixtREFBcUIsQ0FBQyx3QkFBd0I7UUFDOUMsbURBQXFCLENBQUMsd0JBQXdCO1FBQzlDLGFBQWE7UUFDYixtREFBcUIsQ0FBQyx3QkFBd0I7UUFDOUMsbURBQXFCLENBQUMsd0JBQXdCO1FBQzlDLGFBQWE7UUFDYixtREFBcUIsQ0FBQyx3QkFBd0I7UUFDOUMsa0NBQWEsQ0FBQyxnQkFBZ0I7UUFDOUIsYUFBYTtRQUNiLGtDQUFhLENBQUMsZ0JBQWdCO1FBQzlCLGFBQWE7UUFDYixzQ0FBZSxDQUFDLGtCQUFrQjtRQUNsQyx1QkFBUSxDQUFDLFdBQVc7UUFDcEIsYUFBYTtRQUNiLHVCQUFRLENBQUMsV0FBVztRQUNwQixhQUFhO1FBQ2IsaUJBQUssQ0FBQyxRQUFRO0tBQ2pCLENBQUM7SUFFRixTQUFnQixRQUFRLENBQUMsU0FBdUIsRUFBRSxRQUE4QixFQUFFLFFBQWdCO1FBQzlGLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNwQixJQUFJO2dCQUNBLGdEQUFnRDtnQkFDaEQsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUV2QyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLFdBQVcsS0FBSyxvQ0FBeUIsQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRTtvQkFDOUcsT0FBTyxFQUFFLEdBQUcsQ0FBQyxLQUFLO29CQUNsQixJQUFJLEVBQUUsR0FBRyxDQUFDLHVCQUF1QjtvQkFDakMsc0JBQXNCLEVBQUUsR0FBRyxDQUFDLFdBQVcsS0FBSyxvQ0FBeUIsQ0FBQyxlQUFlO29CQUNyRixrQkFBa0IsRUFBRSxHQUFHLENBQUMsV0FBVyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVztvQkFDeEUseUJBQXlCLEVBQUUsR0FBRyxDQUFDLHlCQUF5QjtpQkFDN0MsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLElBQUksT0FBTyxHQUFHLENBQUMsR0FBRyxVQUFVLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUN2RztZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNSLENBQUMsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsdURBQXVELENBQUMsQ0FBQztnQkFDakcsTUFBTSxDQUFDLENBQUM7YUFDWDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQW5CZSxvQkFBUSxXQW1CdkIsQ0FBQTtBQUVMLENBQUMsRUF6RGdCLFdBQVcsR0FBWCxtQkFBVyxLQUFYLG1CQUFXLFFBeUQzQiJ9