"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Index = void 0;
const Endpoint_1 = require("./Endpoint");
const path_1 = require("path");
const WebUtilities_1 = require("../utilities/WebUtilities");
const _ = __importStar(require("logger"));
var Index;
(function (Index) {
    class IndexWeb extends Endpoint_1.Endpoint {
        constructor(composite, renderer) {
            super(composite, Endpoint_1.Method.GET, '/', false, Endpoint_1.AuthenticationRequirement.AUTHENTICATED, true);
            this.renderer = renderer;
        }
        async handle(request, response, domain, namespace) {
            response.renderFile(path_1.join(__dirname, '..', '..', '..', 'cas-data', 'templates', 'index.html'), {}, this.renderer).catch((e) => {
                _.error(`failed to render index page`, { e });
                WebUtilities_1.ResponseUtilities.sendDefaultInternalServerError(response, false);
            });
        }
    }
    Index.IndexWeb = IndexWeb;
})(Index = exports.Index || (exports.Index = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXgud2ViLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS93ZWIvaW5kZXgud2ViLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx5Q0FBeUU7QUFPekUsK0JBQTRCO0FBQzVCLDREQUE4RDtBQUM5RCwwQ0FBNEI7QUFFNUIsSUFBaUIsS0FBSyxDQWdCckI7QUFoQkQsV0FBaUIsS0FBSztJQUVsQixNQUFhLFFBQVMsU0FBUSxtQkFBUTtRQUVsQyxZQUFZLFNBQXVCLEVBQVUsUUFBZ0I7WUFDekQsS0FBSyxDQUFDLFNBQVMsRUFBRSxpQkFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLG9DQUF5QixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUQvQyxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBRTdELENBQUM7UUFFRCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWdCLEVBQUUsUUFBcUIsRUFBRSxNQUFvQixFQUFFLFNBQXVCO1lBQy9GLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pILENBQUMsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM5QyxnQ0FBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0tBRUo7SUFiWSxjQUFRLFdBYXBCLENBQUE7QUFDTCxDQUFDLEVBaEJnQixLQUFLLEdBQUwsYUFBSyxLQUFMLGFBQUssUUFnQnJCIn0=