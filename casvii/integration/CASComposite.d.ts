import { IAuthenticationProcesses } from "../actions/interface/IAuthenticationProcesses";
import { IPasswordProcesses } from "../actions/interface/IPasswordProcesses";
import { IUserProcesses } from "../actions/interface/IUserProcesses";
import { ITwoFactorProcesses } from "../actions/interface/ITwoFactorProcesses";
import { IPermissionProcesses } from "../actions/interface/IPermissionProcesses";
import { DaoCollection } from "../index";
export declare class CASComposite {
    private _authenticationProcesses;
    private _passwordProcesses;
    private _permissionProcesses;
    private _twoFactorProcesses;
    private _userProcesses;
    private _daoCollection;
    constructor(_authenticationProcesses: IAuthenticationProcesses, _passwordProcesses: IPasswordProcesses, _permissionProcesses: IPermissionProcesses, _twoFactorProcesses: ITwoFactorProcesses, _userProcesses: IUserProcesses, _daoCollection: DaoCollection);
    get authenticationProcesses(): IAuthenticationProcesses;
    get passwordProcesses(): IPasswordProcesses;
    get permissionProcesses(): IPermissionProcesses;
    get twoFactorProcesses(): ITwoFactorProcesses;
    get userProcesses(): IUserProcesses;
    get daoCollection(): DaoCollection;
}
