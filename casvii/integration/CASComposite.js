"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CASComposite = void 0;
class CASComposite {
    constructor(_authenticationProcesses, _passwordProcesses, _permissionProcesses, _twoFactorProcesses, _userProcesses, _daoCollection) {
        this._authenticationProcesses = _authenticationProcesses;
        this._passwordProcesses = _passwordProcesses;
        this._permissionProcesses = _permissionProcesses;
        this._twoFactorProcesses = _twoFactorProcesses;
        this._userProcesses = _userProcesses;
        this._daoCollection = _daoCollection;
    }
    get authenticationProcesses() {
        return this._authenticationProcesses;
    }
    get passwordProcesses() {
        return this._passwordProcesses;
    }
    get permissionProcesses() {
        return this._permissionProcesses;
    }
    get twoFactorProcesses() {
        return this._twoFactorProcesses;
    }
    get userProcesses() {
        return this._userProcesses;
    }
    get daoCollection() {
        return this._daoCollection;
    }
}
exports.CASComposite = CASComposite;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ0FTQ29tcG9zaXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS9pbnRlZ3JhdGlvbi9DQVNDb21wb3NpdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBT0EsTUFBYSxZQUFZO0lBRXJCLFlBQW9CLHdCQUFrRCxFQUNsRCxrQkFBc0MsRUFDdEMsb0JBQTBDLEVBQzFDLG1CQUF3QyxFQUN4QyxjQUE4QixFQUM5QixjQUE2QjtRQUw3Qiw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBQ2xELHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtJQUNqRCxDQUFDO0lBRUQsSUFBSSx1QkFBdUI7UUFDdkIsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUM7SUFDekMsQ0FBQztJQUVELElBQUksaUJBQWlCO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ25DLENBQUM7SUFFRCxJQUFJLG1CQUFtQjtRQUNuQixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztJQUNyQyxDQUFDO0lBRUQsSUFBSSxrQkFBa0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUM7SUFDcEMsQ0FBQztJQUVELElBQUksYUFBYTtRQUNiLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQsSUFBSSxhQUFhO1FBQ2IsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9CLENBQUM7Q0FFSjtBQWxDRCxvQ0FrQ0MifQ==