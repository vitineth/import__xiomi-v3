"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CASEndpointsRegister = void 0;
const AuthenticationProcessImpl_1 = require("../actions/process/AuthenticationProcessImpl");
const WebUtilities_1 = require("../utilities/WebUtilities");
const ProcessImpl_1 = require("../actions/ProcessImpl");
const Constants_1 = require("../utilities/Constants");
const _ = __importStar(require("logger"));
var requestToSession = WebUtilities_1.RequestUtilities.requestToSession;
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
class CASEndpointsRegister {
    constructor(endpointRegister, authenticator) {
        this.endpointRegister = endpointRegister;
        this.authenticator = authenticator;
    }
    add(registerFunction, endpoint, requiresAuthentication, handler, options, arg) {
        _.trace(`CAS Endpoint Added: Serves: ${endpoint}`);
        _.trace(`           Registering via: ${registerFunction.name}`);
        _.trace(`   Required authentication: ${requiresAuthentication}`);
        _.trace(`                   Options: ${JSON.stringify(options)}`);
        _.trace(`                  Arg Type: ${typeof (arg)}`);
        const useJSON = options.useJSON || false;
        const hide = options.hide || false;
        const requireUnauthenticated = options.requireUnauthenticated || false;
        const requirePermissions = options.requirePermissions || [];
        const requireProvisional = options.requiredProvisionalStatus || null;
        if (requirePermissions.length > 0 && !requiresAuthentication) {
            throw new Error(`Cannot register endpoint because you must require authentication to require permissions`);
        }
        if (requireProvisional !== null) {
            const auth = this.authenticator;
            registerFunction.call(this.endpointRegister, endpoint, (req, res, domain, namespace) => {
                const session = requestToSession(req);
                if (auth.authenticationProcesses.getAuthenticationStatus(session) !== requireProvisional) {
                    if (hide) {
                        res.sendStatus(404);
                        return;
                    }
                    if (useJSON) {
                        res.status(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
                        res.json({
                            error: PUBLIC_RESPONSES.UNAUTHORIZED,
                            success: false,
                        });
                        return;
                    }
                    // Set the page that we are coming from before we redirect to the login page
                    _.trace(`User tried to access page https://${req.get('host')}${req.originalUrl} but was rejected so assigning this to the auth-next property`);
                    if (req.session !== undefined)
                        req.session['auth-next'] = `https://${req.get('host')}${req.originalUrl}`;
                    res.sendStatus(ProcessImpl_1.HttpStatusCode.REDIRECT);
                    res.header('Location', 'https://cas.xiomi.org/auth/login');
                    return;
                }
                if (requirePermissions.length > 0) {
                    this.authenticator.permissionProcesses.sessionHasPermissions(session, requirePermissions).then((result) => {
                        if (result) {
                            // If we ever approve a page, we need to then make sure that the session is cleared of any
                            // login redirects to be safe
                            _.trace(`Handling an authenticated response to clearing the users auth-next property`);
                            if (req.session !== undefined && req.session['auth-next'] !== undefined)
                                req.session['auth-next'] = undefined;
                            handler.call(arg, req, res, domain, namespace);
                        }
                        else {
                            if (hide) {
                                res.sendStatus(404);
                                return;
                            }
                            if (useJSON) {
                                res.status(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                                res.json({
                                    error: PUBLIC_RESPONSES.MISSING_PERMISSIONS,
                                    success: false,
                                });
                                return;
                            }
                            res.sendStatus(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                        }
                    }).catch((e) => {
                        _.error(`Failed to provide access because there was an error checking the users permissions`, { e });
                        res.sendStatus(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
                    });
                    return;
                }
                handler.call(arg, req, res, domain, namespace);
            }, arg);
        }
        // If this decorator requires authentication, then take this path
        if (requiresAuthentication || requireUnauthenticated) {
            const auth = this.authenticator;
            // Call the register function using a custom handler which will pass on the requests to the actual function
            // if the request is authenticated
            registerFunction.call(this.endpointRegister, endpoint, (req, res, domain, namepsace) => {
                const session = requestToSession(req);
                if (auth.authenticationProcesses.getAuthenticationStatus(session) === AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR) {
                    res.redirect('https://cas.xiomi.org/auth/two-factor');
                    return;
                }
                // If the request is not authenticated, either give a JSON or normal response depending on what the user
                // requested
                if (requiresAuthentication && !auth.authenticationProcesses.isAuthenticated(session)) {
                    if (useJSON) {
                        if (hide) {
                            res.sendStatus(ProcessImpl_1.HttpStatusCode.NOT_FOUND);
                            return;
                        }
                        res.status(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
                        res.json({
                            error: `This endpoint requires authentication`,
                            success: false,
                        });
                        return;
                    }
                    // Set the page that we are coming from before we redirect to the login page
                    _.trace(`User tried to access page https://${req.get('host')}${req.originalUrl} but was rejected so assigning this to the auth-next property`);
                    if (req.session !== undefined)
                        req.session['auth-next'] = `https://${req.get('host')}${req.originalUrl}`;
                    res.redirect('https://cas.xiomi.org/auth/login');
                    return;
                }
                if (requireUnauthenticated && auth.authenticationProcesses.isAuthenticated(session)) {
                    if (useJSON) {
                        if (hide) {
                            res.status(ProcessImpl_1.HttpStatusCode.NOT_FOUND);
                            return;
                        }
                        res.status(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                        res.json({
                            error: `This endpoint cannot be accessed when authenticated`,
                            success: false,
                        });
                    }
                    else {
                        res.sendStatus(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                    }
                    return;
                }
                if (requirePermissions.length > 0) {
                    this.authenticator.permissionProcesses.sessionHasPermissions(session, requirePermissions).then((result) => {
                        if (result) {
                            // If this was an authenticated request, we need to then make sure that the session is
                            // cleared of any login redirects to be safe
                            _.trace(`Handling an authenticated response to clearing the users auth-next property`);
                            if (requiresAuthentication && req.session !== undefined && req.session['auth-next'] !== undefined) {
                                req.session['auth-next'] = undefined;
                            }
                            if (res.headersSent) {
                                _.warn(`Could not pass off to handler because the headers had already been sent`);
                                return;
                            }
                            handler.call(arg, req, res, domain, namepsace);
                        }
                        else {
                            if (hide) {
                                res.sendStatus(ProcessImpl_1.HttpStatusCode.NOT_FOUND);
                                return;
                            }
                            if (useJSON) {
                                res.status(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                                res.json({
                                    error: PUBLIC_RESPONSES.MISSING_PERMISSIONS,
                                    success: false,
                                });
                                return;
                            }
                            res.sendStatus(ProcessImpl_1.HttpStatusCode.FORBIDDEN);
                        }
                    }).catch((e) => {
                        _.error(`Failed to provide access because there was an error checking the users permissions`, { e });
                        res.sendStatus(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
                    });
                    return;
                }
                // If this was an authenticated request, we need to then make sure that the session is
                // cleared of any login redirects to be safe
                _.trace(`Handling an authenticated response to clearing the users auth-next property`);
                if (requiresAuthentication && req.session !== undefined && req.session['auth-next'] !== undefined) {
                    req.session['auth-next'] = undefined;
                }
                if (res.headersSent) {
                    _.warn(`Could not pass off to handler because the headers had already been sent`);
                    return;
                }
                handler.call(arg, req, res, domain, namepsace);
            }, arg);
            return;
        }
        registerFunction.call(this.endpointRegister, endpoint, handler, arg);
    }
    delete(endpoint, requiresAuthentication, handler, options = {}, arg) {
        this.add(this.endpointRegister.delete, endpoint, requiresAuthentication, handler, options, arg);
    }
    get(endpoint, requiresAuthentication, handler, options = {}, arg) {
        this.add(this.endpointRegister.get, endpoint, requiresAuthentication, handler, options, arg);
    }
    head(endpoint, requiresAuthentication, handler, options = {}, arg) {
        this.add(this.endpointRegister.head, endpoint, requiresAuthentication, handler, options, arg);
    }
    post(endpoint, requiresAuthentication, handler, options = {}, arg) {
        this.add(this.endpointRegister.post, endpoint, requiresAuthentication, handler, options, arg);
    }
    put(endpoint, requiresAuthentication, handler, options = {}, arg) {
        this.add(this.endpointRegister.put, endpoint, requiresAuthentication, handler, options, arg);
    }
}
exports.CASEndpointsRegister = CASEndpointsRegister;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ0FTRW5kcG9pbnRzUmVnaXN0ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL2ludGVncmF0aW9uL0NBU0VuZHBvaW50c1JlZ2lzdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFPQSw0RkFBK0Y7QUFDL0YsNERBQTZEO0FBQzdELHdEQUF3RDtBQUN4RCxzREFBbUQ7QUFDbkQsMENBQTRCO0FBQzVCLElBQU8sZ0JBQWdCLEdBQUcsK0JBQWdCLENBQUMsZ0JBQWdCLENBQUM7QUFDNUQsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBV3JELE1BQWEsb0JBQW9CO0lBSzdCLFlBQVksZ0JBQXNDLEVBQUUsYUFBMkI7UUFDM0UsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFTyxHQUFHLENBQUMsZ0JBQWtDLEVBQUUsUUFBeUIsRUFBRSxzQkFBK0IsRUFBRSxPQUF3QixFQUFFLE9BQW1CLEVBQUUsR0FBUTtRQUMvSixDQUFDLENBQUMsS0FBSyxDQUFDLCtCQUErQixRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ25ELENBQUMsQ0FBQyxLQUFLLENBQUMsK0JBQStCLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFDaEUsQ0FBQyxDQUFDLEtBQUssQ0FBQywrQkFBK0Isc0JBQXNCLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsQ0FBQyxLQUFLLENBQUMsK0JBQStCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2xFLENBQUMsQ0FBQyxLQUFLLENBQUMsK0JBQStCLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdkQsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUM7UUFDekMsTUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUM7UUFDbkMsTUFBTSxzQkFBc0IsR0FBRyxPQUFPLENBQUMsc0JBQXNCLElBQUksS0FBSyxDQUFDO1FBQ3ZFLE1BQU0sa0JBQWtCLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixJQUFJLEVBQUUsQ0FBQztRQUM1RCxNQUFNLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsSUFBSSxJQUFJLENBQUM7UUFFckUsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDMUQsTUFBTSxJQUFJLEtBQUssQ0FBQyx5RkFBeUYsQ0FBQyxDQUFDO1NBQzlHO1FBRUQsSUFBSSxrQkFBa0IsS0FBSyxJQUFJLEVBQUU7WUFDN0IsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNoQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxFQUFFO2dCQUNuRixNQUFNLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdEMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLEtBQUssa0JBQWtCLEVBQUU7b0JBQ3RGLElBQUksSUFBSSxFQUFFO3dCQUNOLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3BCLE9BQU87cUJBQ1Y7b0JBRUQsSUFBSSxPQUFPLEVBQUU7d0JBQ1QsR0FBRyxDQUFDLE1BQU0sQ0FBQyw0QkFBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN4QyxHQUFHLENBQUMsSUFBSSxDQUFDOzRCQUNMLEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxZQUFZOzRCQUNwQyxPQUFPLEVBQUUsS0FBSzt5QkFDakIsQ0FBQyxDQUFDO3dCQUNILE9BQU87cUJBQ1Y7b0JBRUQsNEVBQTRFO29CQUM1RSxDQUFDLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxXQUFXLCtEQUErRCxDQUFDLENBQUM7b0JBQy9JLElBQUksR0FBRyxDQUFDLE9BQU8sS0FBSyxTQUFTO3dCQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsV0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFFekcsR0FBRyxDQUFDLFVBQVUsQ0FBQyw0QkFBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxrQ0FBa0MsQ0FBQyxDQUFDO29CQUMzRCxPQUFPO2lCQUNWO2dCQUVELElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDdEcsSUFBSSxNQUFNLEVBQUU7NEJBQ1IsMEZBQTBGOzRCQUMxRiw2QkFBNkI7NEJBQzdCLENBQUMsQ0FBQyxLQUFLLENBQUMsNkVBQTZFLENBQUMsQ0FBQzs0QkFDdkYsSUFBSSxHQUFHLENBQUMsT0FBTyxLQUFLLFNBQVMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLFNBQVM7Z0NBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUM7NEJBRTlHLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUNsRDs2QkFBTTs0QkFDSCxJQUFJLElBQUksRUFBRTtnQ0FDTixHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dDQUNwQixPQUFPOzZCQUNWOzRCQUVELElBQUksT0FBTyxFQUFFO2dDQUNULEdBQUcsQ0FBQyxNQUFNLENBQUMsNEJBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQ0FDckMsR0FBRyxDQUFDLElBQUksQ0FBQztvQ0FDTCxLQUFLLEVBQUUsZ0JBQWdCLENBQUMsbUJBQW1CO29DQUMzQyxPQUFPLEVBQUUsS0FBSztpQ0FDakIsQ0FBQyxDQUFDO2dDQUNILE9BQU87NkJBQ1Y7NEJBRUQsR0FBRyxDQUFDLFVBQVUsQ0FBQyw0QkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3lCQUM1QztvQkFDTCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTt3QkFDWCxDQUFDLENBQUMsS0FBSyxDQUFDLG9GQUFvRixFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDckcsR0FBRyxDQUFDLFVBQVUsQ0FBQyw0QkFBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQ3pELENBQUMsQ0FBQyxDQUFDO29CQUNILE9BQU87aUJBQ1Y7Z0JBRUQsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1g7UUFFRCxpRUFBaUU7UUFDakUsSUFBSSxzQkFBc0IsSUFBSSxzQkFBc0IsRUFBRTtZQUNsRCxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ2hDLDJHQUEyRztZQUMzRyxrQ0FBa0M7WUFDbEMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsRUFBRTtnQkFDbkYsTUFBTSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3RDLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxLQUFLLDJEQUErQixDQUFDLG1CQUFtQixFQUFFO29CQUN2SCxHQUFHLENBQUMsUUFBUSxDQUFDLHVDQUF1QyxDQUFDLENBQUM7b0JBQ3RELE9BQU87aUJBQ1Y7Z0JBQ0Qsd0dBQXdHO2dCQUN4RyxZQUFZO2dCQUNaLElBQUksc0JBQXNCLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNsRixJQUFJLE9BQU8sRUFBRTt3QkFDVCxJQUFJLElBQUksRUFBRTs0QkFDTixHQUFHLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ3pDLE9BQU87eUJBQ1Y7d0JBRUQsR0FBRyxDQUFDLE1BQU0sQ0FBQyw0QkFBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN4QyxHQUFHLENBQUMsSUFBSSxDQUFDOzRCQUNMLEtBQUssRUFBRSx1Q0FBdUM7NEJBQzlDLE9BQU8sRUFBRSxLQUFLO3lCQUNqQixDQUFDLENBQUM7d0JBQ0gsT0FBTztxQkFDVjtvQkFFRCw0RUFBNEU7b0JBQzVFLENBQUMsQ0FBQyxLQUFLLENBQUMscUNBQXFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsK0RBQStELENBQUMsQ0FBQztvQkFDL0ksSUFBSSxHQUFHLENBQUMsT0FBTyxLQUFLLFNBQVM7d0JBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxXQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUV6RyxHQUFHLENBQUMsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7b0JBQ2pELE9BQU87aUJBQ1Y7Z0JBRUQsSUFBSSxzQkFBc0IsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNqRixJQUFJLE9BQU8sRUFBRTt3QkFDVCxJQUFJLElBQUksRUFBRTs0QkFDTixHQUFHLENBQUMsTUFBTSxDQUFDLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ3JDLE9BQU87eUJBQ1Y7d0JBRUQsR0FBRyxDQUFDLE1BQU0sQ0FBQyw0QkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUNyQyxHQUFHLENBQUMsSUFBSSxDQUFDOzRCQUNMLEtBQUssRUFBRSxxREFBcUQ7NEJBQzVELE9BQU8sRUFBRSxLQUFLO3lCQUNqQixDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsR0FBRyxDQUFDLFVBQVUsQ0FBQyw0QkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUM1QztvQkFFRCxPQUFPO2lCQUNWO2dCQUVELElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDdEcsSUFBSSxNQUFNLEVBQUU7NEJBQ1Isc0ZBQXNGOzRCQUN0Riw0Q0FBNEM7NEJBQzVDLENBQUMsQ0FBQyxLQUFLLENBQUMsNkVBQTZFLENBQUMsQ0FBQzs0QkFDdkYsSUFBSSxzQkFBc0IsSUFBSSxHQUFHLENBQUMsT0FBTyxLQUFLLFNBQVMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLFNBQVMsRUFBRTtnQ0FDL0YsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUM7NkJBQ3hDOzRCQUVELElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRTtnQ0FDakIsQ0FBQyxDQUFDLElBQUksQ0FBQyx5RUFBeUUsQ0FBQyxDQUFDO2dDQUNsRixPQUFPOzZCQUNWOzRCQUVELE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUNsRDs2QkFBTTs0QkFDSCxJQUFJLElBQUksRUFBRTtnQ0FDTixHQUFHLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7Z0NBQ3pDLE9BQU87NkJBQ1Y7NEJBRUQsSUFBSSxPQUFPLEVBQUU7Z0NBQ1QsR0FBRyxDQUFDLE1BQU0sQ0FBQyw0QkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dDQUNyQyxHQUFHLENBQUMsSUFBSSxDQUFDO29DQUNMLEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxtQkFBbUI7b0NBQzNDLE9BQU8sRUFBRSxLQUFLO2lDQUNqQixDQUFDLENBQUM7Z0NBQ0gsT0FBTzs2QkFDVjs0QkFFRCxHQUFHLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQzVDO29CQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUNYLENBQUMsQ0FBQyxLQUFLLENBQUMsb0ZBQW9GLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNyRyxHQUFHLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDekQsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTztpQkFDVjtnQkFFRCxzRkFBc0Y7Z0JBQ3RGLDRDQUE0QztnQkFDNUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyw2RUFBNkUsQ0FBQyxDQUFDO2dCQUN2RixJQUFJLHNCQUFzQixJQUFJLEdBQUcsQ0FBQyxPQUFPLEtBQUssU0FBUyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssU0FBUyxFQUFFO29CQUMvRixHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLFNBQVMsQ0FBQztpQkFDeEM7Z0JBRUQsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFO29CQUNqQixDQUFDLENBQUMsSUFBSSxDQUFDLHlFQUF5RSxDQUFDLENBQUM7b0JBQ2xGLE9BQU87aUJBQ1Y7Z0JBRUQsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBRVIsT0FBTztTQUNWO1FBRUQsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFTSxNQUFNLENBQUMsUUFBeUIsRUFBRSxzQkFBK0IsRUFBRSxPQUF3QixFQUFFLFVBQXNCLEVBQUUsRUFBRSxHQUFRO1FBQ2xJLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsc0JBQXNCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNwRyxDQUFDO0lBRU0sR0FBRyxDQUFDLFFBQXlCLEVBQUUsc0JBQStCLEVBQUUsT0FBd0IsRUFBRSxVQUFzQixFQUFFLEVBQUUsR0FBUTtRQUMvSCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDakcsQ0FBQztJQUVNLElBQUksQ0FBQyxRQUF5QixFQUFFLHNCQUErQixFQUFFLE9BQXdCLEVBQUUsVUFBc0IsRUFBRSxFQUFFLEdBQVE7UUFDaEksSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFTSxJQUFJLENBQUMsUUFBeUIsRUFBRSxzQkFBK0IsRUFBRSxPQUF3QixFQUFFLFVBQXNCLEVBQUUsRUFBRSxHQUFRO1FBQ2hJLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsc0JBQXNCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNsRyxDQUFDO0lBRU0sR0FBRyxDQUFDLFFBQXlCLEVBQUUsc0JBQStCLEVBQUUsT0FBd0IsRUFBRSxVQUFzQixFQUFFLEVBQUUsR0FBUTtRQUMvSCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDakcsQ0FBQztDQUVKO0FBbk9ELG9EQW1PQyJ9