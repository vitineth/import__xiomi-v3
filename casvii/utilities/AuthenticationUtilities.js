"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.hashPin = exports.hashPassword = exports.testPin = exports.testPassword = exports.testPasswordHash = void 0;
const argon2 = __importStar(require("argon2"));
const Constants_1 = require("./Constants");
var ARGON2_HASHING_OPTIONS = Constants_1.Constants.ARGON2_HASHING_OPTIONS;
var CURRENT_PASSWORD_HASH_VERSION = Constants_1.Constants.CURRENT_PASSWORD_HASH_VERSION;
var CURRENT_PIN_HASH_VERSION = Constants_1.Constants.CURRENT_PIN_HASH_VERSION;
async function testPasswordHash(version, hash, password) {
    // Hash version: see constants.ts
    if (version === 0) {
        return await argon2.verify(hash, password, ARGON2_HASHING_OPTIONS);
    }
    throw new Error('Invalid hash version, cannot check');
}
exports.testPasswordHash = testPasswordHash;
/**
 * Tests the password against the users password hash determining the right hashing algorithm automatically
 * @param user the user instance to be tested
 * @param password the password to test
 */
async function testPassword(user, password) {
    return testPasswordHash(user.hashVersion, user.hash, password);
}
exports.testPassword = testPassword;
/**
 * Tests the pin against the users pin hash determining the right hashing algorithm automatically
 * @param user the user instance to be tested
 * @param pin the pin to test
 */
async function testPin(user, pin) {
    // Hash version: see constants.ts
    if (user.pinHashVersion === 0) {
        return await argon2.verify(user.pinHash, pin, ARGON2_HASHING_OPTIONS);
    }
    throw new Error('Invalid hash version, cannot check');
}
exports.testPin = testPin;
/**
 * Hashes a password using the most up to date has mapping and returns the hash version and hash
 * @param password the password
 */
async function hashPassword(password) {
    const result = await argon2.hash(password, ARGON2_HASHING_OPTIONS);
    return [CURRENT_PASSWORD_HASH_VERSION, result];
}
exports.hashPassword = hashPassword;
/**
 * Hashes a pin using the most up to date has mapping and returns the hash version and hash
 * @param pin the pin
 */
async function hashPin(pin) {
    const result = await argon2.hash(pin, ARGON2_HASHING_OPTIONS);
    return [CURRENT_PIN_HASH_VERSION, result];
}
exports.hashPin = hashPin;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aGVudGljYXRpb25VdGlsaXRpZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL3V0aWxpdGllcy9BdXRoZW50aWNhdGlvblV0aWxpdGllcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0NBQWlDO0FBRWpDLDJDQUF3QztBQUN4QyxJQUFPLHNCQUFzQixHQUFHLHFCQUFTLENBQUMsc0JBQXNCLENBQUM7QUFDakUsSUFBTyw2QkFBNkIsR0FBRyxxQkFBUyxDQUFDLDZCQUE2QixDQUFDO0FBQy9FLElBQU8sd0JBQXdCLEdBQUcscUJBQVMsQ0FBQyx3QkFBd0IsQ0FBQztBQUU5RCxLQUFLLFVBQVUsZ0JBQWdCLENBQUMsT0FBZSxFQUFFLElBQVksRUFBRSxRQUFnQjtJQUNsRixpQ0FBaUM7SUFDakMsSUFBSSxPQUFPLEtBQUssQ0FBQyxFQUFFO1FBQ2YsT0FBTyxNQUFNLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0tBQ3RFO0lBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO0FBQzFELENBQUM7QUFQRCw0Q0FPQztBQUVEOzs7O0dBSUc7QUFDSSxLQUFLLFVBQVUsWUFBWSxDQUFDLElBQVUsRUFBRSxRQUFnQjtJQUMzRCxPQUFPLGdCQUFnQixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztBQUNuRSxDQUFDO0FBRkQsb0NBRUM7QUFFRDs7OztHQUlHO0FBQ0ksS0FBSyxVQUFVLE9BQU8sQ0FBQyxJQUFVLEVBQUUsR0FBVztJQUNqRCxpQ0FBaUM7SUFFakMsSUFBSSxJQUFJLENBQUMsY0FBYyxLQUFLLENBQUMsRUFBRTtRQUMzQixPQUFPLE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0tBQ3pFO0lBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO0FBQzFELENBQUM7QUFSRCwwQkFRQztBQUVEOzs7R0FHRztBQUNJLEtBQUssVUFBVSxZQUFZLENBQUMsUUFBZ0I7SUFDL0MsTUFBTSxNQUFNLEdBQUcsTUFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0lBQ25FLE9BQU8sQ0FBQyw2QkFBNkIsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNuRCxDQUFDO0FBSEQsb0NBR0M7QUFFRDs7O0dBR0c7QUFDSSxLQUFLLFVBQVUsT0FBTyxDQUFDLEdBQVc7SUFDckMsTUFBTSxNQUFNLEdBQUcsTUFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0lBQzlELE9BQU8sQ0FBQyx3QkFBd0IsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUM5QyxDQUFDO0FBSEQsMEJBR0MifQ==