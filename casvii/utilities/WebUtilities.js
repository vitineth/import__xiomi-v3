"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseUtilities = exports.RequestUtilities = void 0;
const RequestSession_1 = require("./session/impl/RequestSession");
const ProcessImpl_1 = require("../actions/ProcessImpl");
var RequestUtilities;
(function (RequestUtilities) {
    /**
     * Tests whether the given data object contains all the keys in {@code required} and only those listed in
     * {@code required} and {@code optional}. Will return a string on fail with the name of the invalid key or the missing
     * key in the format {@code Contains invalid key []} or {@code Missing required key []}.
     * @param data the data object to test
     * @param required the keys which must be in the data object
     * @param optional the set of keys which can optionally be present
     */
    function requestContainsKeys(data, required, optional) {
        const allValidKeys = required.concat(optional);
        const containedKeys = Object.keys(data);
        // Verify that it does not contain any invalid keys
        for (const testKey of containedKeys) {
            if (!allValidKeys.includes(testKey)) {
                return `Contains invalid key ${testKey}`;
            }
        }
        // Verify all the required keys are in place
        for (const testKey of required) {
            if (!containedKeys.includes(testKey)) {
                return `Missing required key ${testKey}`;
            }
        }
        // If we've reached this point, all should be fine
        return true;
    }
    RequestUtilities.requestContainsKeys = requestContainsKeys;
    function requestToSession(req) {
        // @ts-ignore
        if (req.session === undefined) {
            // @ts-ignore - should work according to the express session api. This shouldn't really happen as the
            // session should be set at all times. TODO Verify
            req.session = {};
        }
        return new RequestSession_1.RequestSession(req);
    }
    RequestUtilities.requestToSession = requestToSession;
})(RequestUtilities = exports.RequestUtilities || (exports.RequestUtilities = {}));
var ResponseUtilities;
(function (ResponseUtilities) {
    function sendDefaultJSONResponse(response, res) {
        // Send the status code on the request
        res.status(response.statusCode);
        // Build the default response
        const jsonResponse = {
            success: response.statusCode === 200,
        };
        // If it is a failure, add the external error message and their support uuid
        if (response.statusCode !== 200) {
            jsonResponse['error'] = response.externalMessage;
            jsonResponse['support'] = response.supportUUID;
        }
        else {
            if (response.externalMessage !== undefined) {
                jsonResponse['message'] = response.externalMessage;
            }
        }
        if (response.additionalData !== undefined) {
            if (response.additionalData.hasOwnProperty('redirect') && typeof (response.additionalData.redirect) === "string") {
                jsonResponse['url'] = response.additionalData.redirect;
            }
        }
        // And finally set the response
        res.json(jsonResponse);
    }
    ResponseUtilities.sendDefaultJSONResponse = sendDefaultJSONResponse;
    /**
     * Sends the default internal server error using JSON is it is an API
     * @param response the response through which this should be written
     * @param isAPI if the response should be provided in JSON
     */
    function sendDefaultInternalServerError(response, isAPI) {
        if (isAPI) {
            response.status(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
            response.json({
                error: 'There was an internal server error by the handler performing your request',
                success: false,
            });
        }
        else {
            response.sendStatus(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    }
    ResponseUtilities.sendDefaultInternalServerError = sendDefaultInternalServerError;
})(ResponseUtilities = exports.ResponseUtilities || (exports.ResponseUtilities = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2ViVXRpbGl0aWVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS91dGlsaXRpZXMvV2ViVXRpbGl0aWVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUdBLGtFQUErRDtBQUMvRCx3REFBd0Q7QUFFeEQsSUFBaUIsZ0JBQWdCLENBOENoQztBQTlDRCxXQUFpQixnQkFBZ0I7SUFNN0I7Ozs7Ozs7T0FPRztJQUNILFNBQWdCLG1CQUFtQixDQUFDLElBQWlCLEVBQUUsUUFBa0IsRUFBRSxRQUFrQjtRQUN6RixNQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQy9DLE1BQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEMsbURBQW1EO1FBQ25ELEtBQUssTUFBTSxPQUFPLElBQUksYUFBYSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNqQyxPQUFPLHdCQUF3QixPQUFPLEVBQUUsQ0FBQzthQUM1QztTQUNKO1FBRUQsNENBQTRDO1FBQzVDLEtBQUssTUFBTSxPQUFPLElBQUksUUFBUSxFQUFFO1lBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNsQyxPQUFPLHdCQUF3QixPQUFPLEVBQUUsQ0FBQzthQUM1QztTQUNKO1FBRUQsa0RBQWtEO1FBQ2xELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFwQmUsb0NBQW1CLHNCQW9CbEMsQ0FBQTtJQUVELFNBQWdCLGdCQUFnQixDQUFDLEdBQVk7UUFDekMsYUFBYTtRQUNiLElBQUksR0FBRyxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUU7WUFDM0IscUdBQXFHO1lBQ3JHLGtEQUFrRDtZQUNsRCxHQUFHLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztTQUNwQjtRQUVELE9BQU8sSUFBSSwrQkFBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFUZSxpQ0FBZ0IsbUJBUy9CLENBQUE7QUFDTCxDQUFDLEVBOUNnQixnQkFBZ0IsR0FBaEIsd0JBQWdCLEtBQWhCLHdCQUFnQixRQThDaEM7QUFDRCxJQUFpQixpQkFBaUIsQ0ErQ2pDO0FBL0NELFdBQWlCLGlCQUFpQjtJQUU5QixTQUFnQix1QkFBdUIsQ0FBQyxRQUFxQixFQUFFLEdBQWdCO1FBQzNFLHNDQUFzQztRQUN0QyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVoQyw2QkFBNkI7UUFDN0IsTUFBTSxZQUFZLEdBQUc7WUFDakIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxVQUFVLEtBQUssR0FBRztTQUN2QyxDQUFDO1FBRUYsNEVBQTRFO1FBQzVFLElBQUksUUFBUSxDQUFDLFVBQVUsS0FBSyxHQUFHLEVBQUU7WUFDN0IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7WUFDakQsWUFBWSxDQUFDLFNBQVMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7U0FDbEQ7YUFBTTtZQUNILElBQUksUUFBUSxDQUFDLGVBQWUsS0FBSyxTQUFTLEVBQUU7Z0JBQ3hDLFlBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDO2FBQ3REO1NBQ0o7UUFFRCxJQUFJLFFBQVEsQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO1lBQ3ZDLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssUUFBUSxFQUFFO2dCQUM5RyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7YUFDMUQ7U0FDSjtRQUVELCtCQUErQjtRQUMvQixHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUEzQmUseUNBQXVCLDBCQTJCdEMsQ0FBQTtJQUVEOzs7O09BSUc7SUFDSCxTQUFnQiw4QkFBOEIsQ0FBQyxRQUFxQixFQUFFLEtBQWM7UUFDaEYsSUFBSSxLQUFLLEVBQUU7WUFDUCxRQUFRLENBQUMsTUFBTSxDQUFDLDRCQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUNWLEtBQUssRUFBRSwyRUFBMkU7Z0JBQ2xGLE9BQU8sRUFBRSxLQUFLO2FBQ2pCLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxRQUFRLENBQUMsVUFBVSxDQUFDLDRCQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUM3RDtJQUNMLENBQUM7SUFWZSxnREFBOEIsaUNBVTdDLENBQUE7QUFDTCxDQUFDLEVBL0NnQixpQkFBaUIsR0FBakIseUJBQWlCLEtBQWpCLHlCQUFpQixRQStDakMifQ==