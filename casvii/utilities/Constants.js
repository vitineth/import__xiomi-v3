"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Constants = void 0;
const argon2_1 = require("argon2");
var Constants;
(function (Constants) {
    /**
     * The current hashing code to use in the database, the mappings are shown below:
     *
     * <table>
     *  <tr>
     *   <th>Code</th>
     *   <th>Process</th>
     *  </tr>
     *  <tr>
     *   <td>0</td>
     *   <td>Argon2</td>
     *  </tr>
     * </table>
     */
    Constants.CURRENT_PASSWORD_HASH_VERSION = 0;
    /**
     * The current hashing code to use in the database, the mappings are shown below:
     *
     * <table>
     *  <tr>
     *   <th>Code</th>
     *   <th>Process</th>
     *  </tr>
     *  <tr>
     *   <td>0</td>
     *   <td>Argon2</td>
     *  </tr>
     * </table>
     */
    Constants.CURRENT_PIN_HASH_VERSION = 0;
    /**
     * Contains the default hashing options for the ARGON2(id) implementation for passes and passwords
     */
    Constants.ARGON2_HASHING_OPTIONS = {
        type: argon2_1.argon2id,
    };
    Constants.PUBLIC_RESPONSES = {
        INVALID_USERNAME_PASSWORD: 'Invalid username or password. ',
        INVALID_USERNAME_PASSWORD_PIN: 'Invalid username, password or pin. ',
        INVALID_PASSWORD_PIN: 'Invalid password or pin. ',
        INTERNAL_SERVER_ERROR: 'Internal server error!  ',
        INVALID_REQUEST: 'Something was wrong with that request! Check your details and try again or raise a problem. ',
        PASSWORD_MATCH: 'The passwords provided do not match. ',
        SECURITY_QUESTION: 'There was a problem with your security question, please check your answers and then try again. ',
        LOCK_STATUS: 'Your account has been locked temporarily. Please try again soon or contact support.',
        REQUEST_SECURITY_QUESTION: 'We don\'t recognise the location you are logging in from, before we can check your details you\'ll need to answer one of your security questions',
        NO_SECURITY_QUESTIONS: 'There is a configuration problem on your account. Please contact support.',
        EXPIRED_TOKEN: 'The request you are trying to use has already been used or is expired. Please check your email for confirmation of use or request a new one sing the standard procedures.',
        USERNAME_EMAIL_IN_USE: 'The specified username or email has already been registered in this system',
        REGISTRATION_DISABLED: 'You cannot log into your account until your registration is complete',
        INVALID_PIN_FORMAT: `Make sure your pin is of the right format`,
        SECURITY_QUESTIONS_NOT_FOUND: `There was a problem with your security questions!`,
        REQUIRE_ADDITIONAL_STEP: `You have not finished logging in! Please complete your multistep authentication before trying to perform actions`,
        AUTHORIZED: `You are logged in but this resource cannot be accessed as an authenticated user.`,
        MISSING_CODE: `Please provide a two factor authentication code`,
        NO_NEED_TWO_FACTOR: `Your account does not need two factor authentication`,
        INVALID_TWO_FACTOR: `Your two factor pin is incorrect, please try again`,
        CHECK_YOUR_RESPONSE: `Please check your response.`,
        INVALID_EMAIL: `Please check your email format`,
        UNAUTHORIZED: `You need to be logged in to access this resource`,
        MISSING_PERMISSIONS: `You are missing the required permissions to access this resource.`,
        ALREADY_GRANTED: `This user already has the selected group granted`,
        NOT_GRANTED: `This user does not have this permission group so it cannot be ungranted`,
        INVALID_AUTHORIZATION: `Something was wrong with that authorization button! Please contact the site owner!`,
        ALREADY_USED_GRANT: `You have already used this grant button or have already been given these permissions! Contact the site owner if something is not working`,
        SUCCESSFUL: feedback => `The request was successful! ${feedback}`,
        WEAK_PASSWORD: feedback => `Your password does not meet the complexity requirements! ${feedback}. `,
    };
    Constants.PUBLIC_SUBSTITUTIONS = {
        SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST: 'If your email and username were valid, you will receive an email with instructions shortly',
        LOGGED_OUT: `You have been successfully logged out`,
        LOGGED_IN: `You are now logged in!`,
        REGISTERED: `You are nearly registered, please complete your registration by clicking the link in the email sent to your account.`,
        REGISTRATION_COMPLETE: `Your registration is complete! You can now login!`,
        GRANTED: `Group has been granted successfully`,
        UNGRANTED: `Group has been ungranted successfully`,
        LOGGED_IN_WITH_CASVII: `You are now logged in with CASvii, you will automatically be logged in on next visit`,
    };
    Constants.URL_QUERY_REGEX = '(\\?[a-zA-Z0-9-._~&=]+)?';
    Constants.UUID_REGEX = /^[0-9a-z]{8}-[0-9a-z]{4}-4[0-9a-z]{3}-[ab89][0-9a-z]{3}-[0-9a-z]{12}$/;
    /**
     * Tokens will expire in 4 hours
     */
    Constants.EXPIRY_TIME_IN_MILLISECONDS = 1.44e+7;
})(Constants = exports.Constants || (exports.Constants = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS91dGlsaXRpZXMvQ29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLG1DQUFrQztBQUVsQyxJQUFpQixTQUFTLENBMkZ6QjtBQTNGRCxXQUFpQixTQUFTO0lBQ3RCOzs7Ozs7Ozs7Ozs7O09BYUc7SUFDVSx1Q0FBNkIsR0FBRyxDQUFDLENBQUM7SUFDL0M7Ozs7Ozs7Ozs7Ozs7T0FhRztJQUNVLGtDQUF3QixHQUFHLENBQUMsQ0FBQztJQUUxQzs7T0FFRztJQUNVLGdDQUFzQixHQUFHO1FBQ2xDLElBQUksRUFBRSxpQkFBUTtLQUNqQixDQUFDO0lBRVcsMEJBQWdCLEdBQUc7UUFDNUIseUJBQXlCLEVBQUUsZ0NBQWdDO1FBQzNELDZCQUE2QixFQUFFLHFDQUFxQztRQUNwRSxvQkFBb0IsRUFBRSwyQkFBMkI7UUFDakQscUJBQXFCLEVBQUUsMEJBQTBCO1FBQ2pELGVBQWUsRUFBRSw4RkFBOEY7UUFDL0csY0FBYyxFQUFFLHVDQUF1QztRQUN2RCxpQkFBaUIsRUFBRSxpR0FBaUc7UUFDcEgsV0FBVyxFQUFFLHFGQUFxRjtRQUNsRyx5QkFBeUIsRUFBRSxrSkFBa0o7UUFDN0sscUJBQXFCLEVBQUUsMkVBQTJFO1FBQ2xHLGFBQWEsRUFBRSwyS0FBMks7UUFDMUwscUJBQXFCLEVBQUUsNEVBQTRFO1FBQ25HLHFCQUFxQixFQUFFLHNFQUFzRTtRQUM3RixrQkFBa0IsRUFBRSwyQ0FBMkM7UUFDL0QsNEJBQTRCLEVBQUUsbURBQW1EO1FBQ2pGLHVCQUF1QixFQUFFLGtIQUFrSDtRQUMzSSxVQUFVLEVBQUUsa0ZBQWtGO1FBQzlGLFlBQVksRUFBRSxpREFBaUQ7UUFDL0Qsa0JBQWtCLEVBQUUsc0RBQXNEO1FBQzFFLGtCQUFrQixFQUFFLG9EQUFvRDtRQUN4RSxtQkFBbUIsRUFBRSw2QkFBNkI7UUFDbEQsYUFBYSxFQUFFLGdDQUFnQztRQUMvQyxZQUFZLEVBQUUsa0RBQWtEO1FBQ2hFLG1CQUFtQixFQUFFLG1FQUFtRTtRQUN4RixlQUFlLEVBQUUsa0RBQWtEO1FBQ25FLFdBQVcsRUFBRSx5RUFBeUU7UUFDdEYscUJBQXFCLEVBQUUsb0ZBQW9GO1FBQzNHLGtCQUFrQixFQUFFLDBJQUEwSTtRQUU5SixVQUFVLEVBQUUsUUFBUSxDQUFDLEVBQUUsQ0FBQywrQkFBK0IsUUFBUSxFQUFFO1FBQ2pFLGFBQWEsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLDREQUE0RCxRQUFRLElBQUk7S0FDdEcsQ0FBQztJQUVXLDhCQUFvQixHQUFHO1FBQ2hDLHNDQUFzQyxFQUFFLDRGQUE0RjtRQUNwSSxVQUFVLEVBQUUsdUNBQXVDO1FBQ25ELFNBQVMsRUFBRSx3QkFBd0I7UUFDbkMsVUFBVSxFQUFFLHNIQUFzSDtRQUNsSSxxQkFBcUIsRUFBRSxtREFBbUQ7UUFDMUUsT0FBTyxFQUFFLHFDQUFxQztRQUM5QyxTQUFTLEVBQUUsdUNBQXVDO1FBQ2xELHFCQUFxQixFQUFFLHNGQUFzRjtLQUNoSCxDQUFDO0lBRVcseUJBQWUsR0FBRywwQkFBMEIsQ0FBQztJQUM3QyxvQkFBVSxHQUFHLHVFQUF1RSxDQUFDO0lBRWxHOztPQUVHO0lBQ1UscUNBQTJCLEdBQVcsT0FBTyxDQUFDO0FBQy9ELENBQUMsRUEzRmdCLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBMkZ6QiJ9