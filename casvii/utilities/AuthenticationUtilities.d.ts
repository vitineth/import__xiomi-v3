import { User } from "../database/models/User.model";
export declare function testPasswordHash(version: number, hash: string, password: string): Promise<boolean>;
/**
 * Tests the password against the users password hash determining the right hashing algorithm automatically
 * @param user the user instance to be tested
 * @param password the password to test
 */
export declare function testPassword(user: User, password: string): Promise<boolean>;
/**
 * Tests the pin against the users pin hash determining the right hashing algorithm automatically
 * @param user the user instance to be tested
 * @param pin the pin to test
 */
export declare function testPin(user: User, pin: string): Promise<boolean>;
/**
 * Hashes a password using the most up to date has mapping and returns the hash version and hash
 * @param password the password
 */
export declare function hashPassword(password: string): Promise<[number, string]>;
/**
 * Hashes a pin using the most up to date has mapping and returns the hash version and hash
 * @param pin the pin
 */
export declare function hashPin(pin: string): Promise<[number, string]>;
