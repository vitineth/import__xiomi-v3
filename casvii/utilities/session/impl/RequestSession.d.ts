import { Session } from '../Session';
import { Request } from "express";
export declare class RequestSession implements Session {
    private req;
    constructor(req: Request);
    clear(key: string): void;
    get(key: string): any;
    has(key: string): boolean;
    set(key: string, value: any): void;
}
