"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestSession = void 0;
class RequestSession {
    constructor(req) {
        this.req = req;
    }
    clear(key) {
        // @ts-ignore
        if (this.req.session)
            this.req.session[key] = undefined;
    }
    get(key) {
        // @ts-ignore
        if (this.req.session)
            return this.req.session[key];
        return undefined;
    }
    has(key) {
        // @ts-ignore
        if (this.req.session)
            return this.req.session.hasOwnProperty(key);
        return false;
    }
    set(key, value) {
        // @ts-ignore
        if (this.req.session)
            this.req.session[key] = value;
    }
}
exports.RequestSession = RequestSession;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVxdWVzdFNlc3Npb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL3V0aWxpdGllcy9zZXNzaW9uL2ltcGwvUmVxdWVzdFNlc3Npb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBR0EsTUFBYSxjQUFjO0lBSXZCLFlBQVksR0FBWTtRQUNwQixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztJQUNuQixDQUFDO0lBRUQsS0FBSyxDQUFDLEdBQVc7UUFDYixhQUFhO1FBQ2IsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU87WUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLENBQUM7SUFDNUQsQ0FBQztJQUVELEdBQUcsQ0FBQyxHQUFXO1FBQ1gsYUFBYTtRQUNiLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPO1lBQUUsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRUQsR0FBRyxDQUFDLEdBQVc7UUFDWCxhQUFhO1FBQ2IsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU87WUFBRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsRSxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsR0FBRyxDQUFDLEdBQVcsRUFBRSxLQUFVO1FBQ3ZCLGFBQWE7UUFDYixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTztZQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUN4RCxDQUFDO0NBRUo7QUE5QkQsd0NBOEJDIn0=