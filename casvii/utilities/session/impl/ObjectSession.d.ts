import { Session } from '../Session';
export declare class ObjectSession implements Session {
    private store;
    constructor();
    private find;
    clear(key: string): void;
    get(key: string): any;
    has(key: string): boolean;
    set(key: string, value: any): void;
}
