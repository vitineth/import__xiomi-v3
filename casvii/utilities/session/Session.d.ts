export interface Session {
    set(key: string, value: any): void;
    get(key: string): any;
    clear(key: string): void;
    has(key: string): boolean;
}
