import { PaxResponse } from "../../pax-intermediate/PaxResponse";
import { CASResponse } from "./CASResponse";
export declare namespace ResponseUtilities {
    function sendDefaultJSONResponse(response: CASResponse, res: PaxResponse): void;
}
