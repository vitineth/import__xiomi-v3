"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CASResponse = void 0;
const uuid_1 = require("uuid");
class CASResponse {
    constructor(statusCode, internalMessage, externalMessage, additionalData = undefined) {
        /**
         * The support identifier which is logged to the database in the auth message allowing support personnel to lookup
         * the exact cause of an error beyond the generic user response
         */
        this.supportUUID = uuid_1.v4().toString();
        this.statusCode = statusCode;
        this.internalMessage = internalMessage;
        this.externalMessage = externalMessage;
        this.additionalData = additionalData;
    }
    setUser(value) {
        this.user = value;
        return this;
    }
    setAdditionalData(value) {
        this.additionalData = value;
        return this;
    }
    /**
     * Produces the authentication log message and additional data including an internal support identifier.
     */
    buildLogMessage() {
        const message = `Encountered an error: '${this.internalMessage}' and responded to the user with '${this.externalMessage}'`;
        return [
            message,
            {
                statusCode: this.statusCode,
                internalMessage: this.internalMessage,
                externalMessage: this.externalMessage,
                supportCode: this.supportUUID,
            },
        ];
    }
}
exports.CASResponse = CASResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ0FTUmVzcG9uc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL3V0aWxpdGllcy9DQVNSZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFLQSwrQkFBMEI7QUFFMUIsTUFBYSxXQUFXO0lBNkJwQixZQUFZLFVBQWtCLEVBQUUsZUFBdUIsRUFBRSxlQUF1QixFQUFFLGlCQUFxRCxTQUFTO1FBZGhKOzs7V0FHRztRQUNJLGdCQUFXLEdBQVcsU0FBRSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7UUFXekMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7UUFDdkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7UUFDdkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7SUFDekMsQ0FBQztJQUVELE9BQU8sQ0FBQyxLQUF1QjtRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNsQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBeUM7UUFDdkQsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDNUIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksZUFBZTtRQUNsQixNQUFNLE9BQU8sR0FBRywwQkFBMEIsSUFBSSxDQUFDLGVBQWUscUNBQXFDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQztRQUMzSCxPQUFPO1lBQ0gsT0FBTztZQUNQO2dCQUNJLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDM0IsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO2dCQUNyQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7Z0JBQ3JDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVzthQUNoQztTQUNKLENBQUM7SUFDTixDQUFDO0NBQ0o7QUE3REQsa0NBNkRDIn0=