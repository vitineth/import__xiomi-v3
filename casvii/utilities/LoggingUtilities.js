"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggingUtilities = void 0;
const stack_trace_1 = require("stack-trace");
const _ = __importStar(require("logger"));
var LoggingUtilities;
(function (LoggingUtilities) {
    /**
     * Logs an authentication message to the database
     * @param loggingDAO the DAO through which to access the logging records
     * @param message the message to log
     * @param additional the optional additional parameter to store in the database
     */
    LoggingUtilities.logAuthenticationMessage = async (loggingDAO, message, additional) => {
        // Get the stack trace so we can find the calling function
        const caller = stack_trace_1.get()[1];
        // Construct the message data object
        const messageData = {
            // This is versioned, each change should be logged with a new version number to allow backward compatible
            // parsing for log views
            version: 0,
            caller: {
                file: caller.getFileName(),
                method: caller.getMethodName(),
                function: caller.getFunctionName(),
                column: caller.getColumnNumber(),
                line: caller.getLineNumber(),
                trace: new Error().stack,
            },
            log: {
                message,
                additional,
            },
        };
        // Then stringify it so we can dump it into the database. We try this one so we can hand it properly if the JSON
        // stringification breaks (we can't trust the data coming in). In this event we will still log but will note
        // that the additional data could not be logged
        let stringified = '';
        try {
            stringified = JSON.stringify(messageData);
        }
        catch (ignored) {
            messageData['log']['additional'] = 'additional data could not be stringified properly';
            stringified = JSON.stringify(messageData);
        }
        // Then extract the source because we are also going to store this separately
        const source = JSON.stringify(messageData['caller']);
        _.debug(`Logging "${message}" from "${source}" with additional data: "${JSON.stringify(additional)}"`);
        // Then finally insert into the database
        return loggingDAO.saveAuthenticationMessage(source, stringified);
    };
    LoggingUtilities.logAuthenticationResponse = async (loggingDAO, response) => {
        const message = response.buildLogMessage();
        return LoggingUtilities.logAuthenticationMessage(loggingDAO, message[0], message[1]);
    };
    LoggingUtilities.logAuthenticationResponseShortcut = async (loggingDAO, response) => {
        await LoggingUtilities.logAuthenticationResponse(loggingDAO, response);
        return response;
    };
})(LoggingUtilities = exports.LoggingUtilities || (exports.LoggingUtilities = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9nZ2luZ1V0aWxpdGllcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jYXN2aWkvdXRpbGl0aWVzL0xvZ2dpbmdVdGlsaXRpZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDZDQUFrQztBQUdsQywwQ0FBNEI7QUFFNUIsSUFBaUIsZ0JBQWdCLENBNERoQztBQTVERCxXQUFpQixnQkFBZ0I7SUFDN0I7Ozs7O09BS0c7SUFDVSx5Q0FBd0IsR0FBRyxLQUFLLEVBQUUsVUFBc0IsRUFBRSxPQUFlLEVBQUUsVUFBZSxFQUFFLEVBQUU7UUFDdkcsMERBQTBEO1FBQzFELE1BQU0sTUFBTSxHQUFHLGlCQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUV4QixvQ0FBb0M7UUFDcEMsTUFBTSxXQUFXLEdBQUc7WUFDaEIseUdBQXlHO1lBQ3pHLHdCQUF3QjtZQUN4QixPQUFPLEVBQUUsQ0FBQztZQUNWLE1BQU0sRUFBRTtnQkFDSixJQUFJLEVBQUUsTUFBTSxDQUFDLFdBQVcsRUFBRTtnQkFDMUIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxhQUFhLEVBQUU7Z0JBQzlCLFFBQVEsRUFBRSxNQUFNLENBQUMsZUFBZSxFQUFFO2dCQUNsQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGVBQWUsRUFBRTtnQkFDaEMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxhQUFhLEVBQUU7Z0JBQzVCLEtBQUssRUFBRSxJQUFJLEtBQUssRUFBRSxDQUFDLEtBQUs7YUFDM0I7WUFDRCxHQUFHLEVBQUU7Z0JBQ0QsT0FBTztnQkFDUCxVQUFVO2FBQ2I7U0FDSixDQUFDO1FBRUYsZ0hBQWdIO1FBQ2hILDRHQUE0RztRQUM1RywrQ0FBK0M7UUFDL0MsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBRXJCLElBQUk7WUFDQSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUM3QztRQUFDLE9BQU8sT0FBTyxFQUFFO1lBQ2QsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLG1EQUFtRCxDQUFDO1lBQ3ZGLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzdDO1FBRUQsNkVBQTZFO1FBQzdFLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFFckQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFZLE9BQU8sV0FBVyxNQUFNLDRCQUE0QixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUV2Ryx3Q0FBd0M7UUFDeEMsT0FBTyxVQUFVLENBQUMseUJBQXlCLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ3JFLENBQUMsQ0FBQztJQUVXLDBDQUF5QixHQUFHLEtBQUssRUFBRSxVQUFzQixFQUFFLFFBQXFCLEVBQUUsRUFBRTtRQUM3RixNQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDM0MsT0FBTyxpQkFBQSx3QkFBd0IsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hFLENBQUMsQ0FBQztJQUVXLGtEQUFpQyxHQUFHLEtBQUssRUFBRSxVQUFzQixFQUFFLFFBQXFCLEVBQUUsRUFBRTtRQUNyRyxNQUFNLGlCQUFBLHlCQUF5QixDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN0RCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDLENBQUM7QUFDTixDQUFDLEVBNURnQixnQkFBZ0IsR0FBaEIsd0JBQWdCLEtBQWhCLHdCQUFnQixRQTREaEMifQ==