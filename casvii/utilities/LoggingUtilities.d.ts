import { LoggingDAO } from "../database/dao/LoggingDAO";
import { CASResponse } from "./CASResponse";
export declare namespace LoggingUtilities {
    /**
     * Logs an authentication message to the database
     * @param loggingDAO the DAO through which to access the logging records
     * @param message the message to log
     * @param additional the optional additional parameter to store in the database
     */
    const logAuthenticationMessage: (loggingDAO: LoggingDAO, message: string, additional: any) => Promise<import("../database/models/logs/LogAuth.model").LogAuth>;
    const logAuthenticationResponse: (loggingDAO: LoggingDAO, response: CASResponse) => Promise<import("../database/models/logs/LogAuth.model").LogAuth>;
    const logAuthenticationResponseShortcut: (loggingDAO: LoggingDAO, response: CASResponse) => Promise<CASResponse>;
}
