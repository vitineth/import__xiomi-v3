export declare namespace Constants {
    /**
     * The current hashing code to use in the database, the mappings are shown below:
     *
     * <table>
     *  <tr>
     *   <th>Code</th>
     *   <th>Process</th>
     *  </tr>
     *  <tr>
     *   <td>0</td>
     *   <td>Argon2</td>
     *  </tr>
     * </table>
     */
    const CURRENT_PASSWORD_HASH_VERSION = 0;
    /**
     * The current hashing code to use in the database, the mappings are shown below:
     *
     * <table>
     *  <tr>
     *   <th>Code</th>
     *   <th>Process</th>
     *  </tr>
     *  <tr>
     *   <td>0</td>
     *   <td>Argon2</td>
     *  </tr>
     * </table>
     */
    const CURRENT_PIN_HASH_VERSION = 0;
    /**
     * Contains the default hashing options for the ARGON2(id) implementation for passes and passwords
     */
    const ARGON2_HASHING_OPTIONS: {
        type: 2;
    };
    const PUBLIC_RESPONSES: {
        INVALID_USERNAME_PASSWORD: string;
        INVALID_USERNAME_PASSWORD_PIN: string;
        INVALID_PASSWORD_PIN: string;
        INTERNAL_SERVER_ERROR: string;
        INVALID_REQUEST: string;
        PASSWORD_MATCH: string;
        SECURITY_QUESTION: string;
        LOCK_STATUS: string;
        REQUEST_SECURITY_QUESTION: string;
        NO_SECURITY_QUESTIONS: string;
        EXPIRED_TOKEN: string;
        USERNAME_EMAIL_IN_USE: string;
        REGISTRATION_DISABLED: string;
        INVALID_PIN_FORMAT: string;
        SECURITY_QUESTIONS_NOT_FOUND: string;
        REQUIRE_ADDITIONAL_STEP: string;
        AUTHORIZED: string;
        MISSING_CODE: string;
        NO_NEED_TWO_FACTOR: string;
        INVALID_TWO_FACTOR: string;
        CHECK_YOUR_RESPONSE: string;
        INVALID_EMAIL: string;
        UNAUTHORIZED: string;
        MISSING_PERMISSIONS: string;
        ALREADY_GRANTED: string;
        NOT_GRANTED: string;
        INVALID_AUTHORIZATION: string;
        ALREADY_USED_GRANT: string;
        SUCCESSFUL: (feedback: any) => string;
        WEAK_PASSWORD: (feedback: any) => string;
    };
    const PUBLIC_SUBSTITUTIONS: {
        SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST: string;
        LOGGED_OUT: string;
        LOGGED_IN: string;
        REGISTERED: string;
        REGISTRATION_COMPLETE: string;
        GRANTED: string;
        UNGRANTED: string;
        LOGGED_IN_WITH_CASVII: string;
    };
    const URL_QUERY_REGEX = "(\\?[a-zA-Z0-9-._~&=]+)?";
    const UUID_REGEX: RegExp;
    /**
     * Tokens will expire in 4 hours
     */
    const EXPIRY_TIME_IN_MILLISECONDS: number;
}
