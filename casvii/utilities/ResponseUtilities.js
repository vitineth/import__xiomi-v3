"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseUtilities;
(function (ResponseUtilities) {
    function sendDefaultJSONResponse(response, res) {
        // Send the status code on the request
        res.status(response.statusCode);
        // Build the default response
        const jsonResponse = {
            success: response.statusCode === 200,
        };
        // If it is a failure, add the external error message and their support uuid
        if (response.statusCode !== 200) {
            jsonResponse['error'] = response.externalMessage;
            jsonResponse['support'] = response.supportUUID;
        }
        else {
            if (response.externalMessage !== undefined) {
                jsonResponse['message'] = response.externalMessage;
            }
        }
        // And finally set the response
        res.json(jsonResponse);
    }
    ResponseUtilities.sendDefaultJSONResponse = sendDefaultJSONResponse;
})(ResponseUtilities = exports.ResponseUtilities || (exports.ResponseUtilities = {}));
