/**
 * An internal response object containing details about where something went wrong in the system providing internal and
 * external logging messages
 */
import { User } from '../database/models/User.model';
export declare class CASResponse {
    /**
     * The HTTP status code to be returned for this request response
     */
    statusCode: number;
    /**
     * The internal logging message containing a more precise location of the error which can be logged
     */
    internalMessage: string;
    /**
     * The external message which can be displayed to the user without revealing sensitive internal information. Should
     * only be selected from {@link PUBLIC_RESPONSES}.
     */
    externalMessage: string;
    /**
     * The support identifier which is logged to the database in the auth message allowing support personnel to lookup
     * the exact cause of an error beyond the generic user response
     */
    supportUUID: string;
    /**
     * A set of additional data to be used internally to provide a suitable response to the client
     */
    additionalData: {
        [key: string]: any;
    } | undefined;
    /**
     * An optional user associated with this request
     */
    private user;
    constructor(statusCode: number, internalMessage: string, externalMessage: string, additionalData?: {
        [key: string]: any;
    } | undefined);
    setUser(value: User | undefined): CASResponse;
    setAdditionalData(value: {
        [key: string]: any;
    } | undefined): CASResponse;
    /**
     * Produces the authentication log message and additional data including an internal support identifier.
     */
    buildLogMessage(): [string, object];
}
