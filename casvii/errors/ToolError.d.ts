/**
 * Extension of error which is used in the tooling classes to provide details on specific erros and allow custom
 * responses
 */
export declare class ToolError extends Error {
    /**
     * An identifier for this exception which can be used to provide unique responses to each problem if this is being
     * echoed to the end users
     */
    private _identifier;
    /**
     * Any additional data that has been passed through with the request
     */
    private _data;
    /**
     * The exception thrown if relevant that caused this tool error
     */
    private _exception;
    constructor(message: string, identifier: string, exception?: Error | undefined, data?: {
        [key: string]: any;
    } | undefined);
    get identifier(): string;
    get data(): {
        [p: string]: any;
    } | undefined;
    get exception(): Error | undefined;
}
