import { User } from "../models/User.model";
export interface UserDAO {
    /**
     * Returns a User instance from the backing data store or rejects if one cannot be found. The username is converted
     * to lower case to ensure that it will match the record
     * @param username the username to lookup
     */
    findUserByUsername(username: string): Promise<User>;
    /**
     * Returns a user instance from the backing data store with the given ID or rejects if one cannot be found
     * @param id the user ID
     */
    findUserByID(id: number): Promise<User>;
    /**
     * Constructs a user with the properties described and returns the created instance
     * @param username the username of this user, should be converted to lower case automatically to make it all case
     * insensitive
     * @param email the email address that this user has registered with
     * @param name the full name that this user has provided and by which they should be referred
     * @param hash the hashed password to be saved with this user
     * @param hashVersion the version of hash which was used to produce this users hash
     * @param pin the hash of the users pin
     * @param pinVersion the version used to generate the hash of the pin
     * @param status the accessibility status of this model, one of 'accessible', 'auth', 'admin', 'two_factor_setup'
     */
    createUser(username: string, email: string, name: string, hash: string, hashVersion: number, pin: string, pinVersion: number, status: string): Promise<User>;
    /**
     * Constructs a user with the properties described and returns the created instance
     * @param username the username of this user, should be converted to lower case automatically to make it all case
     * insensitive
     * @param email the email address that this user has registered with
     * @param name the full name that this user has provided and by which they should be referred
     * @param password the password to be hashed and saved with this user
     * @param pin the users pin
     * @param status the accessibility status of this model, one of 'accessible', 'auth', 'admin', 'two_factor_setup'
     */
    createUserAutohash(username: string, email: string, name: string, password: string, pin: string, status: string): Promise<User>;
    /**
     * Updates the users status to be accessible
     * @param user the user to modify
     */
    makeUserAccessible(user: User): Promise<User>;
    /**
     * Tests if this username has already been used in the database. This should automatically convert the username to
     * lower case to maintain case insensitivity.
     * @param username the username to test
     */
    isUsernameAvailable(username: string): Promise<boolean>;
    /**
     * Tests if this email has already been used in the database. This should automatically convert the email to lower
     * case to maintain case insensitivity
     * @param email the email to test
     */
    isEmailAvailable(email: string): Promise<boolean>;
    /**
     * Changes a users password to the given has and version, updating the last rotation time as well
     * @param user the user to update
     * @param hash the hash they should not use
     * @param hashVersion the version of the hash used to produce the given hash
     */
    changeUserPassword(user: User, hash: string, hashVersion: number): Promise<User>;
    /**
     * Changes a users password to the given password which will be hashed, updating the last rotation time as well
     * @param user the user to update
     * @param password the new password for this user
     */
    changeUserPasswordAutohash(user: User, password: string): Promise<User>;
}
