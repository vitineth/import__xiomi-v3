import { User } from "../models/User.model";
import { TwoFactorConfiguration } from "../models/security/TwoFactorConfiguration.model";
export interface TwoFactorDAO {
    findConfigurationByUser(user: User): Promise<TwoFactorConfiguration>;
}
