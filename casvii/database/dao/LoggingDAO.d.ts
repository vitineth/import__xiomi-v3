import { User } from "../models/User.model";
import { Login } from "../models/logs/Login.model";
import { LogAuth } from "../models/logs/LogAuth.model";
export interface LoggingDAO {
    logLogin(ipAddress: string, user: User): Promise<Login>;
    fetchLoginsByUser(user: User): Promise<Login[]>;
    saveAuthenticationMessage(source: string, detail: string): Promise<LogAuth>;
}
