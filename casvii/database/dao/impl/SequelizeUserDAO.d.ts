import { User } from "../../models/User.model";
import { UserDAO } from "../UserDAO";
export declare class SequelizeUserDAO implements UserDAO {
    findUserByUsername(username: string): Promise<User>;
    createUser(username: string, email: string, name: string, hash: string, hashVersion: number, pin: string, pinVersion: number, status: string): Promise<User>;
    createUserAutohash(username: string, email: string, name: string, password: string, pin: string, status: string): Promise<User>;
    isEmailAvailable(email: string): Promise<boolean>;
    isUsernameAvailable(username: string): Promise<boolean>;
    makeUserAccessible(user: User): Promise<User>;
    changeUserPassword(user: User, hash: string, hashVersion: number): Promise<User>;
    changeUserPasswordAutohash(user: User, password: string): Promise<User>;
    findUserByID(id: number): Promise<User>;
}
