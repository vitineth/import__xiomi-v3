"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeTokenDAO = void 0;
const User_model_1 = require("../../models/User.model");
const ResetDataRequest_model_1 = require("../../models/ResetDataRequest.model");
const uuid_1 = require("uuid");
const RegistrationToken_model_1 = require("../../models/security/RegistrationToken.model");
class SequelizeTokenDAO {
    async requestPasswordChange(user) {
        return new ResetDataRequest_model_1.ResetDataRequest({
            user,
            userID: user.id,
            userToken: uuid_1.v4().toString(),
            resetToken: uuid_1.v4().toString(),
            type: 'password',
        }).save();
    }
    async findPasswordChangeRequest(userToken, token) {
        const result = await ResetDataRequest_model_1.ResetDataRequest.findOne({
            where: {
                userToken,
                type: 'password',
                resetToken: token,
                completed: false,
            },
            include: [User_model_1.User],
        });
        if (result === null)
            throw new Error('No password change request is available for that request');
        return result;
    }
    markChangeComplete(request) {
        request.completed = true;
        return request.save();
    }
    async findRegistrationToken(userToken, registrationToken) {
        const result = await RegistrationToken_model_1.RegistrationToken.findOne({
            where: {
                userToken,
                resetToken: registrationToken,
            },
            include: [User_model_1.User],
        });
        if (result === null)
            throw new Error('No valid registration token');
        return result;
    }
    markRegistrationComplete(request) {
        request.completed = true;
        return request.save();
    }
    requestRegistrationToken(user) {
        return new RegistrationToken_model_1.RegistrationToken({
            user,
            userID: user.id,
            userToken: uuid_1.v4().toString(),
            resetToken: uuid_1.v4().toString(),
        }).save();
    }
}
exports.SequelizeTokenDAO = SequelizeTokenDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplVG9rZW5EQU8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL2Rhby9pbXBsL1NlcXVlbGl6ZVRva2VuREFPLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUNBLHdEQUErQztBQUMvQyxnRkFBdUU7QUFDdkUsK0JBQTBCO0FBQzFCLDJGQUFrRjtBQUVsRixNQUFhLGlCQUFpQjtJQUUxQixLQUFLLENBQUMscUJBQXFCLENBQUMsSUFBVTtRQUNsQyxPQUFPLElBQUkseUNBQWdCLENBQUM7WUFDeEIsSUFBSTtZQUNKLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNmLFNBQVMsRUFBRSxTQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDMUIsVUFBVSxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMzQixJQUFJLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsS0FBSyxDQUFDLHlCQUF5QixDQUFDLFNBQWlCLEVBQUUsS0FBYTtRQUM1RCxNQUFNLE1BQU0sR0FBRyxNQUFNLHlDQUFnQixDQUFDLE9BQU8sQ0FBQztZQUMxQyxLQUFLLEVBQUU7Z0JBQ0gsU0FBUztnQkFDVCxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFNBQVMsRUFBRSxLQUFLO2FBQ25CO1lBQ0QsT0FBTyxFQUFFLENBQUMsaUJBQUksQ0FBQztTQUNsQixDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sS0FBSyxJQUFJO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQywwREFBMEQsQ0FBQyxDQUFDO1FBRWpHLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxPQUF5QjtRQUN4QyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN6QixPQUFPLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsS0FBSyxDQUFDLHFCQUFxQixDQUFDLFNBQWlCLEVBQUUsaUJBQXlCO1FBQ3BFLE1BQU0sTUFBTSxHQUFHLE1BQU0sMkNBQWlCLENBQUMsT0FBTyxDQUFDO1lBQzNDLEtBQUssRUFBRTtnQkFDSCxTQUFTO2dCQUNULFVBQVUsRUFBRSxpQkFBaUI7YUFDaEM7WUFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBSSxDQUFDO1NBQ2xCLENBQUMsQ0FBQztRQUVILElBQUksTUFBTSxLQUFLLElBQUk7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFFcEUsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVELHdCQUF3QixDQUFDLE9BQTBCO1FBQy9DLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLE9BQU8sT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxJQUFVO1FBQy9CLE9BQU8sSUFBSSwyQ0FBaUIsQ0FBQztZQUN6QixJQUFJO1lBQ0osTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2YsU0FBUyxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMxQixVQUFVLEVBQUUsU0FBRSxFQUFFLENBQUMsUUFBUSxFQUFFO1NBQzlCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7Q0FFSjtBQTdERCw4Q0E2REMifQ==