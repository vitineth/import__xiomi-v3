"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeLoggingDAO = void 0;
const Login_model_1 = require("../../models/logs/Login.model");
const LogAuth_model_1 = require("../../models/logs/LogAuth.model");
class SequelizeLoggingDAO {
    fetchLoginsByUser(user) {
        return Login_model_1.Login.findAll({
            where: {
                userID: user.id,
            },
        });
    }
    logLogin(ipAddress, user) {
        return new Login_model_1.Login({
            ipAddress,
            user,
            userID: user.id,
        }).save();
    }
    saveAuthenticationMessage(source, detail) {
        return new LogAuth_model_1.LogAuth({
            source,
            detail,
            logged: new Date(),
        }).save();
    }
}
exports.SequelizeLoggingDAO = SequelizeLoggingDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplTG9nZ2luZ0RBTy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvZGFvL2ltcGwvU2VxdWVsaXplTG9nZ2luZ0RBTy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFDQSwrREFBc0Q7QUFFdEQsbUVBQTBEO0FBRTFELE1BQWEsbUJBQW1CO0lBQzVCLGlCQUFpQixDQUFDLElBQVU7UUFDeEIsT0FBTyxtQkFBSyxDQUFDLE9BQU8sQ0FBQztZQUNqQixLQUFLLEVBQUU7Z0JBQ0gsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO2FBQ2xCO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFFBQVEsQ0FBQyxTQUFpQixFQUFFLElBQVU7UUFDbEMsT0FBTyxJQUFJLG1CQUFLLENBQUM7WUFDYixTQUFTO1lBQ1QsSUFBSTtZQUNKLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtTQUNsQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQseUJBQXlCLENBQUMsTUFBYyxFQUFFLE1BQWM7UUFDcEQsT0FBTyxJQUFJLHVCQUFPLENBQUM7WUFDZixNQUFNO1lBQ04sTUFBTTtZQUNOLE1BQU0sRUFBRSxJQUFJLElBQUksRUFBRTtTQUNyQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0NBRUo7QUF6QkQsa0RBeUJDIn0=