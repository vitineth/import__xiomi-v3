"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeTwoFactorDAO = void 0;
const TwoFactorConfiguration_model_1 = require("../../models/security/TwoFactorConfiguration.model");
class SequelizeTwoFactorDAO {
    async findConfigurationByUser(user) {
        const result = await TwoFactorConfiguration_model_1.TwoFactorConfiguration.findOne({
            where: {
                userID: user.id,
            },
        });
        if (result === null)
            throw new Error('That user does not have a two factor configuration');
        return result;
    }
}
exports.SequelizeTwoFactorDAO = SequelizeTwoFactorDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplVHdvRmFjdG9yREFPLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS9kYXRhYmFzZS9kYW8vaW1wbC9TZXF1ZWxpemVUd29GYWN0b3JEQU8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EscUdBQTRGO0FBRzVGLE1BQWEscUJBQXFCO0lBRTlCLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxJQUFVO1FBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0scURBQXNCLENBQUMsT0FBTyxDQUFDO1lBQ2hELEtBQUssRUFBRTtnQkFDSCxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7YUFDbEI7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sS0FBSyxJQUFJO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1FBRTNGLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7Q0FFSjtBQWRELHNEQWNDIn0=