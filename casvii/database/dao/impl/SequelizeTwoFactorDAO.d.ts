import { TwoFactorDAO } from "../TwoFactorDAO";
import { TwoFactorConfiguration } from "../../models/security/TwoFactorConfiguration.model";
import { User } from "../../models/User.model";
export declare class SequelizeTwoFactorDAO implements TwoFactorDAO {
    findConfigurationByUser(user: User): Promise<TwoFactorConfiguration>;
}
