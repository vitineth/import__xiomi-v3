"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeSecurityQuestionDAO = void 0;
const SecurityQuestion_model_1 = require("../../models/security/SecurityQuestion.model");
const SecurityQuestionMapping_model_1 = require("../../models/security/SecurityQuestionMapping.model");
const AuthenticationUtilities_1 = require("../../../utilities/AuthenticationUtilities");
class SequelizeSecurityQuestionDAO {
    async findAllQuestions() {
        return SecurityQuestion_model_1.SecurityQuestion.findAll();
    }
    async findQuestionByIDs(user, id) {
        const result = await SecurityQuestionMapping_model_1.SecurityQuestionMapping.findOne({
            where: {
                securityQuestionID: id,
                userID: user.id,
            },
        });
        if (result === null)
            throw new Error('Could not find a mapping with that combination of properties');
        return result;
    }
    async findQuestionsByUserID(userID) {
        return (await SecurityQuestionMapping_model_1.SecurityQuestionMapping.findAll({
            where: {
                userID,
            },
            include: [
                SecurityQuestion_model_1.SecurityQuestion,
            ],
        })).map(e => e.securityQuestion);
    }
    async saveSecurityAnswer(user, db, answer) {
        const hashResult = await AuthenticationUtilities_1.hashPassword(answer);
        const hash = `${hashResult[0]}:${hashResult[1]}`;
        const mapping = await (new SecurityQuestionMapping_model_1.SecurityQuestionMapping({
            user,
            userID: user.id,
            securityQuestion: db,
            securityQuestionID: db.id,
            answerHash: hash,
        }).save());
        // Ensure these values are set
        // TODO: Required?
        mapping.user = user;
        mapping.securityQuestion = db;
        return mapping;
    }
}
exports.SequelizeSecurityQuestionDAO = SequelizeSecurityQuestionDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplU2VjdXJpdHlRdWVzdGlvbkRBTy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvZGFvL2ltcGwvU2VxdWVsaXplU2VjdXJpdHlRdWVzdGlvbkRBTy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFDQSx5RkFBZ0Y7QUFFaEYsdUdBQThGO0FBQzlGLHdGQUEwRTtBQUUxRSxNQUFhLDRCQUE0QjtJQUVyQyxLQUFLLENBQUMsZ0JBQWdCO1FBQ2xCLE9BQU8seUNBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFVLEVBQUUsRUFBVTtRQUMxQyxNQUFNLE1BQU0sR0FBRyxNQUFNLHVEQUF1QixDQUFDLE9BQU8sQ0FBQztZQUNqRCxLQUFLLEVBQUU7Z0JBQ0gsa0JBQWtCLEVBQUUsRUFBRTtnQkFDdEIsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO2FBQ2xCO1NBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxNQUFNLEtBQUssSUFBSTtZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsOERBQThELENBQUMsQ0FBQztRQUVyRyxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsS0FBSyxDQUFDLHFCQUFxQixDQUFDLE1BQWM7UUFDdEMsT0FBTyxDQUFDLE1BQU0sdURBQXVCLENBQUMsT0FBTyxDQUFDO1lBQzFDLEtBQUssRUFBRTtnQkFDSCxNQUFNO2FBQ1Q7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wseUNBQWdCO2FBQ25CO1NBQ0osQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFVLEVBQUUsRUFBb0IsRUFBRSxNQUFjO1FBQ3JFLE1BQU0sVUFBVSxHQUFHLE1BQU0sc0NBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxNQUFNLElBQUksR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVqRCxNQUFNLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSx1REFBdUIsQ0FBQztZQUMvQyxJQUFJO1lBQ0osTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2YsZ0JBQWdCLEVBQUUsRUFBRTtZQUNwQixrQkFBa0IsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN6QixVQUFVLEVBQUUsSUFBSTtTQUNuQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUVYLDhCQUE4QjtRQUM5QixrQkFBa0I7UUFDbEIsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDcEIsT0FBTyxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUU5QixPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0NBRUo7QUFsREQsb0VBa0RDIn0=