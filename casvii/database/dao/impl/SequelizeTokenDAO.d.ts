import { TokenDAO } from "../TokenDAO";
import { User } from "../../models/User.model";
import { ResetDataRequest } from "../../models/ResetDataRequest.model";
import { RegistrationToken } from "../../models/security/RegistrationToken.model";
export declare class SequelizeTokenDAO implements TokenDAO {
    requestPasswordChange(user: User): Promise<ResetDataRequest>;
    findPasswordChangeRequest(userToken: string, token: string): Promise<ResetDataRequest>;
    markChangeComplete(request: ResetDataRequest): Promise<ResetDataRequest>;
    findRegistrationToken(userToken: string, registrationToken: string): Promise<RegistrationToken>;
    markRegistrationComplete(request: RegistrationToken): Promise<RegistrationToken>;
    requestRegistrationToken(user: User): Promise<RegistrationToken>;
}
