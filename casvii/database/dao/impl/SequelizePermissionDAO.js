"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizePermissionDAO = void 0;
const Permission_model_1 = require("../../models/permissions/Permission.model");
const PermissionGroup_model_1 = require("../../models/permissions/PermissionGroup.model");
const PermissionGroupInheritance_model_1 = require("../../models/permissions/PermissionGroupInheritance.model");
const PermissionGroupMapping_model_1 = require("../../models/permissions/PermissionGroupMapping.model");
const PermissionGroupUserMapping_1 = require("../../models/permissions/PermissionGroupUserMapping");
const crypto = __importStar(require("crypto"));
const SelfGrantAuthorization_model_1 = require("../../models/permissions/SelfGrantAuthorization.model");
class SequelizePermissionDAO {
    async findSelfAuthorizationGrantByID(id) {
        const mapping = await SelfGrantAuthorization_model_1.SelfGrantAuthorizationModel.findOne({
            where: {
                id,
            },
            include: [PermissionGroup_model_1.PermissionGroup],
        });
        if (mapping === null)
            throw new Error(`Could not find self grant`);
        return mapping;
    }
    async findGroupByName(group) {
        const mapping = await PermissionGroup_model_1.PermissionGroup.findOne({
            where: {
                name: group,
            },
        });
        if (mapping === null)
            throw new Error(`Could not find permission group`);
        return mapping;
    }
    async findPermissionByName(permission) {
        const mapping = await Permission_model_1.Permission.findOne({
            where: {
                name: permission,
            },
        });
        if (mapping === null)
            throw new Error(`Could not find permission group`);
        return mapping;
    }
    async findUserGroupMapping(group, user) {
        const mapping = await PermissionGroupUserMapping_1.PermissionGroupUserMapping.findOne({
            where: {
                permissionGroupID: group.id,
                grantedByUserID: user.id,
            },
        });
        if (mapping == null)
            throw new Error(`Could not find mapping`);
        return mapping;
    }
    async createGroupInheritance(target, from) {
        try {
            return await new PermissionGroupInheritance_model_1.PermissionGroupInheritance({
                targetGroup: target,
                targetGroupID: target.id,
                inheritsFrom: from,
                inheritsFromID: from.id,
            }).save();
        }
        catch (e) {
            throw e;
        }
    }
    async createGroupMapping(permission, group) {
        try {
            return await new PermissionGroupMapping_model_1.PermissionGroupMapping({
                permission,
                permissionID: permission.id,
                permissionGroup: group,
                permissionGroupID: group.id,
            }).save();
        }
        catch (e) {
            throw e;
        }
    }
    async createPermission(name, description, hash) {
        try {
            return await new Permission_model_1.Permission({
                name,
                description,
                hash,
            }).save();
        }
        catch (e) {
            throw e;
        }
    }
    createPermissionAutohash(name, description) {
        return this.createPermission(name, description, crypto.createHash('sha256').update(name).digest('hex'));
    }
    async createPermissionGroup(name, description, hash) {
        try {
            return await new PermissionGroup_model_1.PermissionGroup({
                name,
                description,
                hash,
            }).save();
        }
        catch (e) {
            throw e;
        }
    }
    createPermissionGroupAutohash(name, description) {
        return this.createPermissionGroup(name, description, crypto.createHash('sha256').update(name).digest('hex'));
    }
    async createUserGroupMapping(group, user) {
        try {
            return await new PermissionGroupUserMapping_1.PermissionGroupUserMapping({
                permissionGroup: group,
                permissionGroupID: group.id,
                grantedByUser: user,
                grantedByUserID: user.id,
            }).save();
        }
        catch (e) {
            throw e;
        }
    }
    async fetchAllPermissionGroupInheritance() {
        return PermissionGroupInheritance_model_1.PermissionGroupInheritance.findAll({
            include: [
                {
                    model: PermissionGroup_model_1.PermissionGroup,
                    as: 'targetGroup',
                },
                {
                    model: PermissionGroup_model_1.PermissionGroup,
                    as: 'inheritsFrom',
                },
            ],
        });
    }
    async fetchAllPermissionGroupMappings() {
        return PermissionGroupMapping_model_1.PermissionGroupMapping.findAll({
            include: [Permission_model_1.Permission, PermissionGroup_model_1.PermissionGroup],
        });
    }
    async fetchAllPermissionGroups() {
        return PermissionGroup_model_1.PermissionGroup.findAll();
    }
    async fetchAllPermissions() {
        return Permission_model_1.Permission.findAll();
    }
    removeGroupInheritance(inheritance) {
        return inheritance.destroy();
    }
    removeGroupMapping(mapping) {
        return mapping.destroy();
    }
    removePermission(permission) {
        return permission.destroy();
    }
    removePermissionGroup(group) {
        return group.destroy();
    }
    removeUserGroupMapping(mapping) {
        return mapping.destroy();
    }
    async resolvePermissions(user) {
        // Get every mapping and inheritance group
        const groups = await this.fetchAllPermissionGroups();
        const mappings = await this.fetchAllPermissionGroupMappings();
        const inheritance = await this.fetchAllPermissionGroupInheritance();
        // Then find all the groups that this user directly has
        const directInheritance = await PermissionGroupUserMapping_1.PermissionGroupUserMapping.findAll({
            where: {
                grantedByUserID: user.id,
            },
            include: [PermissionGroup_model_1.PermissionGroup],
        });
        // Then we want to recursively process these
        async function recurseOnInheritance(group) {
            const inheritanceGroups = inheritance.filter(e => e.targetGroupID === group.id);
            const permissions = await Promise.all(inheritanceGroups.map((e) => {
                const group = groups.find(y => y.id === e.inheritsFromID);
                if (group === undefined)
                    return [];
                return recurseOnInheritance(group);
            }));
            const directPermissions = (mappings.filter(e => e.permissionGroupID === group.id).map(e => e.permission));
            return [].concat.apply([], permissions).concat(directPermissions);
        }
        // Then finally apply this to all the direct inheritances and join them
        const allResults = await Promise.all(directInheritance.map(e => recurseOnInheritance(e.permissionGroup)));
        return [].concat.apply([], allResults);
    }
    async findPermissionByGroupID(id) {
        const result = await PermissionGroup_model_1.PermissionGroup.findOne({
            where: {
                id,
            },
            include: [
                {
                    model: PermissionGroupMapping_model_1.PermissionGroupMapping,
                    include: [Permission_model_1.Permission],
                },
            ],
        });
        if (result === null)
            throw new Error(`No group`);
        return result.permissionMappings.map(e => e.permission);
    }
}
exports.SequelizePermissionDAO = SequelizePermissionDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplUGVybWlzc2lvbkRBTy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvZGFvL2ltcGwvU2VxdWVsaXplUGVybWlzc2lvbkRBTy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0ZBQXVFO0FBQ3ZFLDBGQUFpRjtBQUNqRixnSEFBdUc7QUFDdkcsd0dBQStGO0FBQy9GLG9HQUFpRztBQUVqRywrQ0FBaUM7QUFFakMsd0dBQW9HO0FBRXBHLE1BQWEsc0JBQXNCO0lBRS9CLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxFQUFVO1FBQzNDLE1BQU0sT0FBTyxHQUFHLE1BQU0sMERBQTJCLENBQUMsT0FBTyxDQUFDO1lBQ3RELEtBQUssRUFBRTtnQkFDSCxFQUFFO2FBQ0w7WUFDRCxPQUFPLEVBQUUsQ0FBQyx1Q0FBZSxDQUFDO1NBQzdCLENBQUMsQ0FBQztRQUVILElBQUksT0FBTyxLQUFLLElBQUk7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFFbkUsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELEtBQUssQ0FBQyxlQUFlLENBQUMsS0FBYTtRQUMvQixNQUFNLE9BQU8sR0FBRyxNQUFNLHVDQUFlLENBQUMsT0FBTyxDQUFDO1lBQzFDLEtBQUssRUFBRTtnQkFDSCxJQUFJLEVBQUUsS0FBSzthQUNkO1NBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxPQUFPLEtBQUssSUFBSTtZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUV6RSxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBRUQsS0FBSyxDQUFDLG9CQUFvQixDQUFDLFVBQWtCO1FBQ3pDLE1BQU0sT0FBTyxHQUFHLE1BQU0sNkJBQVUsQ0FBQyxPQUFPLENBQUM7WUFDckMsS0FBSyxFQUFFO2dCQUNILElBQUksRUFBRSxVQUFVO2FBQ25CO1NBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxPQUFPLEtBQUssSUFBSTtZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUV6RSxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBRUQsS0FBSyxDQUFDLG9CQUFvQixDQUFDLEtBQXNCLEVBQUUsSUFBVTtRQUN6RCxNQUFNLE9BQU8sR0FBRyxNQUFNLHVEQUEwQixDQUFDLE9BQU8sQ0FBQztZQUNyRCxLQUFLLEVBQUU7Z0JBQ0gsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQzNCLGVBQWUsRUFBRSxJQUFJLENBQUMsRUFBRTthQUMzQjtTQUNKLENBQUMsQ0FBQztRQUVILElBQUksT0FBTyxJQUFJLElBQUk7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFL0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxNQUF1QixFQUFFLElBQXFCO1FBQ3ZFLElBQUk7WUFDQSxPQUFPLE1BQU0sSUFBSSw2REFBMEIsQ0FBQztnQkFDeEMsV0FBVyxFQUFFLE1BQU07Z0JBQ25CLGFBQWEsRUFBRSxNQUFNLENBQUMsRUFBRTtnQkFDeEIsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRTthQUMxQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDYjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxDQUFDLENBQUM7U0FDWDtJQUNMLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsVUFBc0IsRUFBRSxLQUFzQjtRQUNuRSxJQUFJO1lBQ0EsT0FBTyxNQUFNLElBQUkscURBQXNCLENBQUM7Z0JBQ3BDLFVBQVU7Z0JBQ1YsWUFBWSxFQUFFLFVBQVUsQ0FBQyxFQUFFO2dCQUMzQixlQUFlLEVBQUUsS0FBSztnQkFDdEIsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEVBQUU7YUFDOUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sQ0FBQyxDQUFDO1NBQ1g7SUFDTCxDQUFDO0lBRUQsS0FBSyxDQUFDLGdCQUFnQixDQUFDLElBQVksRUFBRSxXQUEwQixFQUFFLElBQVk7UUFDekUsSUFBSTtZQUNBLE9BQU8sTUFBTSxJQUFJLDZCQUFVLENBQUM7Z0JBQ3hCLElBQUk7Z0JBQ0osV0FBVztnQkFDWCxJQUFJO2FBQ1AsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sQ0FBQyxDQUFDO1NBQ1g7SUFDTCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsSUFBWSxFQUFFLFdBQW1CO1FBQ3RELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDNUcsQ0FBQztJQUVELEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxJQUFZLEVBQUUsV0FBMEIsRUFBRSxJQUFZO1FBQzlFLElBQUk7WUFDQSxPQUFPLE1BQU0sSUFBSSx1Q0FBZSxDQUFDO2dCQUM3QixJQUFJO2dCQUNKLFdBQVc7Z0JBQ1gsSUFBSTthQUNQLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNiO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLENBQUMsQ0FBQztTQUNYO0lBQ0wsQ0FBQztJQUVELDZCQUE2QixDQUFDLElBQVksRUFBRSxXQUFtQjtRQUMzRCxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2pILENBQUM7SUFFRCxLQUFLLENBQUMsc0JBQXNCLENBQUMsS0FBc0IsRUFBRSxJQUFVO1FBQzNELElBQUk7WUFDQSxPQUFPLE1BQU0sSUFBSSx1REFBMEIsQ0FBQztnQkFDeEMsZUFBZSxFQUFFLEtBQUs7Z0JBQ3RCLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUMzQixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsZUFBZSxFQUFFLElBQUksQ0FBQyxFQUFFO2FBQzNCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNiO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLENBQUMsQ0FBQztTQUNYO0lBQ0wsQ0FBQztJQUVELEtBQUssQ0FBQyxrQ0FBa0M7UUFDcEMsT0FBTyw2REFBMEIsQ0FBQyxPQUFPLENBQUM7WUFDdEMsT0FBTyxFQUFFO2dCQUNMO29CQUNJLEtBQUssRUFBRSx1Q0FBZTtvQkFDdEIsRUFBRSxFQUFFLGFBQWE7aUJBQ3BCO2dCQUNEO29CQUNJLEtBQUssRUFBRSx1Q0FBZTtvQkFDdEIsRUFBRSxFQUFFLGNBQWM7aUJBQ3JCO2FBQ0o7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsS0FBSyxDQUFDLCtCQUErQjtRQUNqQyxPQUFPLHFEQUFzQixDQUFDLE9BQU8sQ0FBQztZQUNsQyxPQUFPLEVBQUUsQ0FBQyw2QkFBVSxFQUFFLHVDQUFlLENBQUM7U0FDekMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEtBQUssQ0FBQyx3QkFBd0I7UUFDMUIsT0FBTyx1Q0FBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxLQUFLLENBQUMsbUJBQW1CO1FBQ3JCLE9BQU8sNkJBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsc0JBQXNCLENBQUMsV0FBdUM7UUFDMUQsT0FBTyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVELGtCQUFrQixDQUFDLE9BQStCO1FBQzlDLE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxVQUFzQjtRQUNuQyxPQUFPLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQscUJBQXFCLENBQUMsS0FBc0I7UUFDeEMsT0FBTyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVELHNCQUFzQixDQUFDLE9BQW1DO1FBQ3RELE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBVTtRQUMvQiwwQ0FBMEM7UUFDMUMsTUFBTSxNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNyRCxNQUFNLFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1FBQzlELE1BQU0sV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLGtDQUFrQyxFQUFFLENBQUM7UUFFcEUsdURBQXVEO1FBQ3ZELE1BQU0saUJBQWlCLEdBQUcsTUFBTSx1REFBMEIsQ0FBQyxPQUFPLENBQUM7WUFDL0QsS0FBSyxFQUFFO2dCQUNILGVBQWUsRUFBRSxJQUFJLENBQUMsRUFBRTthQUMzQjtZQUNELE9BQU8sRUFBRSxDQUFDLHVDQUFlLENBQUM7U0FDN0IsQ0FBQyxDQUFDO1FBRUgsNENBQTRDO1FBQzVDLEtBQUssVUFBVSxvQkFBb0IsQ0FBQyxLQUFzQjtZQUN0RCxNQUFNLGlCQUFpQixHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsYUFBYSxLQUFLLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVoRixNQUFNLFdBQVcsR0FBRyxNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQzlELE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxLQUFLLEtBQUssU0FBUztvQkFBRSxPQUFPLEVBQUUsQ0FBQztnQkFFbkMsT0FBTyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ0osTUFBTSxpQkFBaUIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBRTFHLE9BQVEsRUFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN4RixDQUFDO1FBRUQsdUVBQXVFO1FBQ3ZFLE1BQU0sVUFBVSxHQUFHLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFHLE9BQVEsRUFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQsS0FBSyxDQUFDLHVCQUF1QixDQUFDLEVBQVU7UUFDcEMsTUFBTSxNQUFNLEdBQUcsTUFBTSx1Q0FBZSxDQUFDLE9BQU8sQ0FBQztZQUN6QyxLQUFLLEVBQUU7Z0JBQ0gsRUFBRTthQUNMO1lBQ0QsT0FBTyxFQUFFO2dCQUNMO29CQUNJLEtBQUssRUFBRSxxREFBc0I7b0JBQzdCLE9BQU8sRUFBRSxDQUFDLDZCQUFVLENBQUM7aUJBQ3hCO2FBQ0o7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sS0FBSyxJQUFJO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVqRCxPQUFPLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDNUQsQ0FBQztDQUVKO0FBaE9ELHdEQWdPQyJ9