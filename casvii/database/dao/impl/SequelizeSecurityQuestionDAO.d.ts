import { SecurityQuestionDAO } from "../SecurityQuestionDAO";
import { SecurityQuestion } from "../../models/security/SecurityQuestion.model";
import { User } from "../../models/User.model";
import { SecurityQuestionMapping } from "../../models/security/SecurityQuestionMapping.model";
export declare class SequelizeSecurityQuestionDAO implements SecurityQuestionDAO {
    findAllQuestions(): Promise<SecurityQuestion[]>;
    findQuestionByIDs(user: User, id: number): Promise<SecurityQuestionMapping>;
    findQuestionsByUserID(userID: number): Promise<SecurityQuestion[]>;
    saveSecurityAnswer(user: User, db: SecurityQuestion, answer: string): Promise<SecurityQuestionMapping>;
}
