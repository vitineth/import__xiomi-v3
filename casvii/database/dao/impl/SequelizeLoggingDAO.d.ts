import { LoggingDAO } from "../LoggingDAO";
import { Login } from "../../models/logs/Login.model";
import { User } from "../../models/User.model";
import { LogAuth } from "../../models/logs/LogAuth.model";
export declare class SequelizeLoggingDAO implements LoggingDAO {
    fetchLoginsByUser(user: User): Promise<Login[]>;
    logLogin(ipAddress: string, user: User): Promise<Login>;
    saveAuthenticationMessage(source: string, detail: string): Promise<LogAuth>;
}
