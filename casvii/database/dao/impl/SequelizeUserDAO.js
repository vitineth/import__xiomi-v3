"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SequelizeUserDAO = void 0;
const User_model_1 = require("../../models/User.model");
const AuthenticationUtilities = __importStar(require("../../../utilities/AuthenticationUtilities"));
class SequelizeUserDAO {
    async findUserByUsername(username) {
        const result = await User_model_1.User.findOne({
            where: {
                username: username.toLowerCase(),
            },
        });
        if (result === null)
            throw new Error('Could not find a User instance with that name');
        return result;
    }
    async createUser(username, email, name, hash, hashVersion, pin, pinVersion, status) {
        const user = new User_model_1.User({
            hash,
            hashVersion,
            username,
            email,
            pinHash: pin,
            full_name: name,
            pinHashVersion: pinVersion,
            lockedStatus: 'auth',
            registered: false,
        });
        return user.save();
    }
    async createUserAutohash(username, email, name, password, pin, status) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        const [pinVersion, pinHash] = await AuthenticationUtilities.hashPin(pin);
        const user = new User_model_1.User({
            hash,
            hashVersion,
            username,
            email,
            pinHash,
            full_name: name,
            pinHashVersion: pinVersion,
            lockedStatus: 'auth',
            registered: false,
        });
        return user.save();
    }
    async isEmailAvailable(email) {
        const result = await User_model_1.User.count({
            where: {
                email: email.toLowerCase(),
            },
        });
        return result === 0;
    }
    async isUsernameAvailable(username) {
        const result = await User_model_1.User.count({
            where: {
                username: username.toLowerCase(),
            },
        });
        return result === 0;
    }
    async makeUserAccessible(user) {
        user.lockedStatus = 'accessible';
        user.registered = true;
        return user.save();
    }
    changeUserPassword(user, hash, hashVersion) {
        user.hash = hash;
        user.hashVersion = hashVersion;
        user.lastRotation = new Date();
        return user.save();
    }
    async changeUserPasswordAutohash(user, password) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        return this.changeUserPassword(user, hash, hashVersion);
    }
    async findUserByID(id) {
        const result = await User_model_1.User.findOne({
            where: {
                id,
            },
        });
        if (result === null)
            throw new Error('User with that identifier does not exist');
        return result;
    }
}
exports.SequelizeUserDAO = SequelizeUserDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VxdWVsaXplVXNlckRBTy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvZGFvL2ltcGwvU2VxdWVsaXplVXNlckRBTy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0RBQStDO0FBRS9DLG9HQUFzRjtBQUV0RixNQUFhLGdCQUFnQjtJQUVsQixLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBZ0I7UUFDNUMsTUFBTSxNQUFNLEdBQUcsTUFBTSxpQkFBSSxDQUFDLE9BQU8sQ0FBQztZQUM5QixLQUFLLEVBQUU7Z0JBQ0gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxXQUFXLEVBQUU7YUFDbkM7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sS0FBSyxJQUFJO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1FBQ3RGLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxLQUFLLENBQUMsVUFBVSxDQUFDLFFBQWdCLEVBQUUsS0FBYSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsV0FBbUIsRUFBRSxHQUFXLEVBQUUsVUFBa0IsRUFBRSxNQUFjO1FBQzlJLE1BQU0sSUFBSSxHQUFHLElBQUksaUJBQUksQ0FBQztZQUNsQixJQUFJO1lBQ0osV0FBVztZQUNYLFFBQVE7WUFDUixLQUFLO1lBQ0wsT0FBTyxFQUFFLEdBQUc7WUFDWixTQUFTLEVBQUUsSUFBSTtZQUNmLGNBQWMsRUFBRSxVQUFVO1lBQzFCLFlBQVksRUFBRSxNQUFNO1lBQ3BCLFVBQVUsRUFBRSxLQUFLO1NBQ3BCLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBZ0IsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLFFBQWdCLEVBQUUsR0FBVyxFQUFFLE1BQWM7UUFDakgsTUFBTSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsR0FBRyxNQUFNLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRixNQUFNLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxHQUFHLE1BQU0sdUJBQXVCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXpFLE1BQU0sSUFBSSxHQUFHLElBQUksaUJBQUksQ0FBQztZQUNsQixJQUFJO1lBQ0osV0FBVztZQUNYLFFBQVE7WUFDUixLQUFLO1lBQ0wsT0FBTztZQUNQLFNBQVMsRUFBRSxJQUFJO1lBQ2YsY0FBYyxFQUFFLFVBQVU7WUFDMUIsWUFBWSxFQUFFLE1BQU07WUFDcEIsVUFBVSxFQUFFLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFhO1FBQ2hDLE1BQU0sTUFBTSxHQUFHLE1BQU0saUJBQUksQ0FBQyxLQUFLLENBQUM7WUFDNUIsS0FBSyxFQUFFO2dCQUNILEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVyxFQUFFO2FBQzdCO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsT0FBTyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxLQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBZ0I7UUFDdEMsTUFBTSxNQUFNLEdBQUcsTUFBTSxpQkFBSSxDQUFDLEtBQUssQ0FBQztZQUM1QixLQUFLLEVBQUU7Z0JBQ0gsUUFBUSxFQUFFLFFBQVEsQ0FBQyxXQUFXLEVBQUU7YUFDbkM7U0FDSixDQUFDLENBQUM7UUFDSCxPQUFPLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFVO1FBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxJQUFVLEVBQUUsSUFBWSxFQUFFLFdBQW1CO1FBQzVELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUMvQixPQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsS0FBSyxDQUFDLDBCQUEwQixDQUFDLElBQVUsRUFBRSxRQUFnQjtRQUN6RCxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxHQUFHLE1BQU0sdUJBQXVCLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pGLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELEtBQUssQ0FBQyxZQUFZLENBQUMsRUFBVTtRQUN6QixNQUFNLE1BQU0sR0FBRyxNQUFNLGlCQUFJLENBQUMsT0FBTyxDQUFDO1lBQzlCLEtBQUssRUFBRTtnQkFDSCxFQUFFO2FBQ0w7U0FDSixDQUFDLENBQUM7UUFFSCxJQUFJLE1BQU0sS0FBSyxJQUFJO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1FBRWpGLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7Q0FFSjtBQTlGRCw0Q0E4RkMifQ==