import { User } from "../models/User.model";
import { ResetDataRequest } from "../models/ResetDataRequest.model";
import { RegistrationToken } from "../models/security/RegistrationToken.model";
export interface TokenDAO {
    /**
     * Creates a password reset token, automatically generating the IDs and relevant information, returning the object
     * once saved
     * @param user the user who needs their password changed
     */
    requestPasswordChange(user: User): Promise<ResetDataRequest>;
    /**
     * Creates a new registration token for this user
     * @param user the user who is trying to register
     */
    requestRegistrationToken(user: User): Promise<RegistrationToken>;
    /**
     * Returns a password reset request with the given tokens if one exists
     * @param userToken the user token associated with the request
     * @param token the token associated with this request
     */
    findPasswordChangeRequest(userToken: string, token: string): Promise<ResetDataRequest>;
    /**
     * Marks the given request as completed and saves it in the data store
     * @param request the request to update
     */
    markChangeComplete(request: ResetDataRequest): Promise<ResetDataRequest>;
    /**
     * Finds the registration token if valid rejecting if not
     * @param userToken the user token for this request
     * @param registrationToken the registration token for this request
     */
    findRegistrationToken(userToken: string, registrationToken: string): Promise<RegistrationToken>;
    /**
     * Updates the complete status of this token and saves it
     * @param request the request to update
     */
    markRegistrationComplete(request: RegistrationToken): Promise<RegistrationToken>;
}
