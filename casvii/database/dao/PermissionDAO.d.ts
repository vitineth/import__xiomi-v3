import { Permission } from "../models/permissions/Permission.model";
import { PermissionGroup } from "../models/permissions/PermissionGroup.model";
import { PermissionGroupMapping } from "../models/permissions/PermissionGroupMapping.model";
import { PermissionGroupInheritance } from "../models/permissions/PermissionGroupInheritance.model";
import { User } from "../models/User.model";
import { PermissionGroupUserMapping } from "../models/permissions/PermissionGroupUserMapping";
import { SelfGrantAuthorizationModel } from "../models/permissions/SelfGrantAuthorization.model";
export interface PermissionDAO {
    findSelfAuthorizationGrantByID(id: number): Promise<SelfGrantAuthorizationModel>;
    findUserGroupMapping(group: PermissionGroup, user: User): Promise<PermissionGroupUserMapping>;
    findGroupByName(group: string): Promise<PermissionGroup>;
    findPermissionByName(permission: string): Promise<Permission>;
    fetchAllPermissions(): Promise<Permission[]>;
    fetchAllPermissionGroups(): Promise<PermissionGroup[]>;
    fetchAllPermissionGroupMappings(): Promise<PermissionGroupMapping[]>;
    fetchAllPermissionGroupInheritance(): Promise<PermissionGroupInheritance[]>;
    createPermission(name: string, description: string | null, hash: string): Promise<Permission>;
    createPermissionAutohash(name: string, description: string | null): Promise<Permission>;
    createPermissionGroup(name: string, description: string | null, hash: string): Promise<PermissionGroup>;
    createPermissionGroupAutohash(name: string, description: string | null): Promise<PermissionGroup>;
    createGroupMapping(permission: Permission, group: PermissionGroup): Promise<PermissionGroupMapping>;
    createGroupInheritance(target: PermissionGroup, from: PermissionGroup): Promise<PermissionGroupInheritance>;
    createUserGroupMapping(group: PermissionGroup, user: User): Promise<PermissionGroupUserMapping>;
    removePermission(permission: Permission): Promise<void>;
    removePermissionGroup(group: PermissionGroup): Promise<void>;
    removeGroupMapping(mapping: PermissionGroupMapping): Promise<void>;
    removeGroupInheritance(inheritance: PermissionGroupInheritance): Promise<void>;
    removeUserGroupMapping(mapping: PermissionGroupUserMapping): Promise<void>;
    resolvePermissions(user: User): Promise<Permission[]>;
    findPermissionByGroupID(id: number): Promise<Permission[]>;
}
