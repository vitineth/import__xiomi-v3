import { User } from "../models/User.model";
import { SecurityQuestionMapping } from "../models/security/SecurityQuestionMapping.model";
import { SecurityQuestion } from "../models/security/SecurityQuestion.model";
export interface SecurityQuestionDAO {
    findQuestionByIDs(user: User, id: number): Promise<SecurityQuestionMapping>;
    findAllQuestions(): Promise<SecurityQuestion[]>;
    findQuestionsByUserID(userID: number): Promise<SecurityQuestion[]>;
    saveSecurityAnswer(user: User, db: SecurityQuestion, answer: string): Promise<SecurityQuestionMapping>;
}
