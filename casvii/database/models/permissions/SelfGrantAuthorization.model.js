"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelfGrantAuthorizationModel = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const PermissionGroup_model_1 = require("./PermissionGroup.model");
let SelfGrantAuthorizationModel = /** @class */ (() => {
    let SelfGrantAuthorizationModel = class SelfGrantAuthorizationModel extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], SelfGrantAuthorizationModel.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => PermissionGroup_model_1.PermissionGroup, {
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", PermissionGroup_model_1.PermissionGroup)
    ], SelfGrantAuthorizationModel.prototype, "group", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => PermissionGroup_model_1.PermissionGroup),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], SelfGrantAuthorizationModel.prototype, "groupID", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", String)
    ], SelfGrantAuthorizationModel.prototype, "serviceName", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", String)
    ], SelfGrantAuthorizationModel.prototype, "serviceDescription", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(true),
        sequelize_typescript_1.Column,
        __metadata("design:type", String)
    ], SelfGrantAuthorizationModel.prototype, "serviceIconURL", void 0);
    SelfGrantAuthorizationModel = __decorate([
        sequelize_typescript_1.Table
    ], SelfGrantAuthorizationModel);
    return SelfGrantAuthorizationModel;
})();
exports.SelfGrantAuthorizationModel = SelfGrantAuthorizationModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VsZkdyYW50QXV0aG9yaXphdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL3Blcm1pc3Npb25zL1NlbGZHcmFudEF1dGhvcml6YXRpb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVThCO0FBQzlCLG1FQUEwRDtBQUcxRDtJQUFBLElBQWEsMkJBQTJCLEdBQXhDLE1BQWEsMkJBQTRCLFNBQVEsNEJBQWtDO0tBK0JsRixDQUFBO0lBeEJHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07OzJEQUNZO0lBTW5CO1FBSkMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx1Q0FBZSxFQUFFO1lBQzlCLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSxTQUFTO1NBQ3RCLENBQUM7a0NBQ2EsdUNBQWU7OERBQUM7SUFJL0I7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHVDQUFlLENBQUM7UUFDakMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O2dFQUNpQjtJQUl4QjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztvRUFDcUI7SUFJNUI7UUFGQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7MkVBQzRCO0lBSW5DO1FBRkMsZ0NBQVMsQ0FBQyxJQUFJLENBQUM7UUFDZiw2QkFBTTs7dUVBQ3dCO0lBN0J0QiwyQkFBMkI7UUFEdkMsNEJBQUs7T0FDTywyQkFBMkIsQ0ErQnZDO0lBQUQsa0NBQUM7S0FBQTtBQS9CWSxrRUFBMkIifQ==