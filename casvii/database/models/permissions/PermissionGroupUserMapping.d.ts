import { Model } from 'sequelize-typescript';
import { PermissionGroup } from './PermissionGroup.model';
import { User } from '../User.model';
/**
 * Maps groups to users. This is the recommended method of granting individual permissions (create a new group that
 * inherits from their normal group and then assign that) instead as it will increase reusability.
 */
export declare class PermissionGroupUserMapping extends Model<PermissionGroupUserMapping> {
    id: number;
    permissionGroup: PermissionGroup;
    permissionGroupID: number;
    grantedByUser: User;
    grantedByUserID: number;
}
