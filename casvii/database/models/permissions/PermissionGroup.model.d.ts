import { Model } from 'sequelize-typescript';
import { PermissionGroupInheritance } from "./PermissionGroupInheritance.model";
import { PermissionGroupUserMapping } from "./PermissionGroupUserMapping";
import { PermissionGroupMapping } from "./PermissionGroupMapping.model";
/**
 * A group of permissions collected under a name and optional description. These are hierachical which means that they
 * can inherit from other groups.
 */
export declare class PermissionGroup extends Model<PermissionGroup> {
    id: number;
    name: string;
    description: string;
    hash: string;
    inheritances: PermissionGroupInheritance[];
    userMappings: PermissionGroupUserMapping[];
    permissionMappings: PermissionGroupMapping[];
}
