"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Permission = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const PermissionGroupMapping_model_1 = require("./PermissionGroupMapping.model");
const PermissionUsersMapping_model_1 = require("./PermissionUsersMapping.model");
/**
 * A single permission which grants access to something across the whole xiomi system. The description is optional and
 * can detail what the permission means. These should be auto generated from the code based and updated on program
 * launch
 */
let Permission = /** @class */ (() => {
    let Permission = class Permission extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Permission.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], Permission.prototype, "name", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], Permission.prototype, "description", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], Permission.prototype, "hash", void 0);
    __decorate([
        sequelize_typescript_1.HasMany(() => PermissionGroupMapping_model_1.PermissionGroupMapping, {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Array)
    ], Permission.prototype, "groupMappings", void 0);
    __decorate([
        sequelize_typescript_1.HasMany(() => PermissionUsersMapping_model_1.PermissionUsersMapping, {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Array)
    ], Permission.prototype, "userMappings", void 0);
    Permission = __decorate([
        sequelize_typescript_1.Table
    ], Permission);
    return Permission;
})();
exports.Permission = Permission;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL3Blcm1pc3Npb25zL1Blcm1pc3Npb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVThCO0FBQzlCLGlGQUF3RTtBQUN4RSxpRkFBd0U7QUFFeEU7Ozs7R0FJRztBQUVIO0lBQUEsSUFBYSxVQUFVLEdBQXZCLE1BQWEsVUFBVyxTQUFRLDRCQUFpQjtLQStCaEQsQ0FBQTtJQXhCRztRQUxDLG9DQUFhO1FBQ2IsNkJBQU07UUFDTixpQ0FBVTtRQUNWLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOzswQ0FDWTtJQUtuQjtRQUhDLDZCQUFNO1FBQ04sZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7NENBQ0Q7SUFHckI7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzttREFDTTtJQUc1QjtRQURDLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7OzRDQUNEO0lBTXJCO1FBSkMsOEJBQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxxREFBc0IsRUFBRTtZQUNuQyxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsU0FBUztTQUN0QixDQUFDOztxREFDOEM7SUFNaEQ7UUFKQyw4QkFBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLHFEQUFzQixFQUFFO1lBQ25DLFFBQVEsRUFBRSxTQUFTO1lBQ25CLFFBQVEsRUFBRSxTQUFTO1NBQ3RCLENBQUM7O29EQUM2QztJQTlCdEMsVUFBVTtRQUR0Qiw0QkFBSztPQUNPLFVBQVUsQ0ErQnRCO0lBQUQsaUJBQUM7S0FBQTtBQS9CWSxnQ0FBVSJ9