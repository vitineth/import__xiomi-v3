import { Model } from 'sequelize-typescript';
import { PermissionGroup } from './PermissionGroup.model';
export declare class PermissionGroupInheritance extends Model<PermissionGroupInheritance> {
    id: number;
    targetGroup: PermissionGroup;
    targetGroupID: number;
    inheritsFrom: PermissionGroup;
    inheritsFromID: number;
}
