"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionGroupUserMapping = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const PermissionGroup_model_1 = require("./PermissionGroup.model");
const User_model_1 = require("../User.model");
/**
 * Maps groups to users. This is the recommended method of granting individual permissions (create a new group that
 * inherits from their normal group and then assign that) instead as it will increase reusability.
 */
let PermissionGroupUserMapping = /** @class */ (() => {
    let PermissionGroupUserMapping = class PermissionGroupUserMapping extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupUserMapping.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => PermissionGroup_model_1.PermissionGroup),
        __metadata("design:type", PermissionGroup_model_1.PermissionGroup)
    ], PermissionGroupUserMapping.prototype, "permissionGroup", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => PermissionGroup_model_1.PermissionGroup),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupUserMapping.prototype, "permissionGroupID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], PermissionGroupUserMapping.prototype, "grantedByUser", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupUserMapping.prototype, "grantedByUserID", void 0);
    PermissionGroupUserMapping = __decorate([
        sequelize_typescript_1.Table
    ], PermissionGroupUserMapping);
    return PermissionGroupUserMapping;
})();
exports.PermissionGroupUserMapping = PermissionGroupUserMapping;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvbkdyb3VwVXNlck1hcHBpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL21vZGVscy9wZXJtaXNzaW9ucy9QZXJtaXNzaW9uR3JvdXBVc2VyTWFwcGluZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwrREFVOEI7QUFDOUIsbUVBQTBEO0FBQzFELDhDQUFxQztBQUVyQzs7O0dBR0c7QUFFSDtJQUFBLElBQWEsMEJBQTBCLEdBQXZDLE1BQWEsMEJBQTJCLFNBQVEsNEJBQWlDO0tBeUJoRixDQUFBO0lBbEJHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07OzBEQUNZO0lBR25CO1FBREMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx1Q0FBZSxDQUFDO2tDQUNSLHVDQUFlO3VFQUFDO0lBS3pDO1FBSEMsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx1Q0FBZSxDQUFDO1FBQ2pDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOzt5RUFDMkI7SUFHbEM7UUFEQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7a0NBQ0MsaUJBQUk7cUVBQUM7SUFLNUI7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7UUFDdEIsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O3VFQUN5QjtJQXZCdkIsMEJBQTBCO1FBRHRDLDRCQUFLO09BQ08sMEJBQTBCLENBeUJ0QztJQUFELGlDQUFDO0tBQUE7QUF6QlksZ0VBQTBCIn0=