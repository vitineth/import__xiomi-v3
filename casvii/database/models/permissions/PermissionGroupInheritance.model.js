"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionGroupInheritance = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const PermissionGroup_model_1 = require("./PermissionGroup.model");
let PermissionGroupInheritance = /** @class */ (() => {
    let PermissionGroupInheritance = class PermissionGroupInheritance extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupInheritance.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => PermissionGroup_model_1.PermissionGroup),
        __metadata("design:type", PermissionGroup_model_1.PermissionGroup)
    ], PermissionGroupInheritance.prototype, "targetGroup", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => PermissionGroup_model_1.PermissionGroup),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupInheritance.prototype, "targetGroupID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => PermissionGroup_model_1.PermissionGroup),
        __metadata("design:type", PermissionGroup_model_1.PermissionGroup)
    ], PermissionGroupInheritance.prototype, "inheritsFrom", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => PermissionGroup_model_1.PermissionGroup),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupInheritance.prototype, "inheritsFromID", void 0);
    PermissionGroupInheritance = __decorate([
        sequelize_typescript_1.Table
    ], PermissionGroupInheritance);
    return PermissionGroupInheritance;
})();
exports.PermissionGroupInheritance = PermissionGroupInheritance;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvbkdyb3VwSW5oZXJpdGFuY2UubW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL21vZGVscy9wZXJtaXNzaW9ucy9QZXJtaXNzaW9uR3JvdXBJbmhlcml0YW5jZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwrREFVOEI7QUFDOUIsbUVBQTBEO0FBRzFEO0lBQUEsSUFBYSwwQkFBMEIsR0FBdkMsTUFBYSwwQkFBMkIsU0FBUSw0QkFBaUM7S0F5QmhGLENBQUE7SUFsQkc7UUFMQyxvQ0FBYTtRQUNiLDZCQUFNO1FBQ04saUNBQVU7UUFDVixnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7MERBQ1k7SUFHbkI7UUFEQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLHVDQUFlLENBQUM7a0NBQ1osdUNBQWU7bUVBQUM7SUFLckM7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHVDQUFlLENBQUM7UUFDakMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O3FFQUN1QjtJQUc5QjtRQURDLGdDQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsdUNBQWUsQ0FBQztrQ0FDWCx1Q0FBZTtvRUFBQztJQUt0QztRQUhDLGlDQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsdUNBQWUsQ0FBQztRQUNqQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7c0VBQ3dCO0lBdkJ0QiwwQkFBMEI7UUFEdEMsNEJBQUs7T0FDTywwQkFBMEIsQ0F5QnRDO0lBQUQsaUNBQUM7S0FBQTtBQXpCWSxnRUFBMEIifQ==