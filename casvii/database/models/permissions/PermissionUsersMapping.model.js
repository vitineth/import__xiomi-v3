"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionUsersMapping = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Permission_model_1 = require("./Permission.model");
const User_model_1 = require("../User.model");
/**
 * Maps a user to an individual permission because while discouraged it is possible for individual grants. Time and authorizer is recorded for security purposes.
 */
let PermissionUsersMapping = /** @class */ (() => {
    let PermissionUsersMapping = class PermissionUsersMapping extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionUsersMapping.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => Permission_model_1.Permission),
        __metadata("design:type", Permission_model_1.Permission)
    ], PermissionUsersMapping.prototype, "permission", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => Permission_model_1.Permission),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionUsersMapping.prototype, "permissionID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], PermissionUsersMapping.prototype, "grantedByUser", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionUsersMapping.prototype, "grantedByUserID", void 0);
    PermissionUsersMapping = __decorate([
        sequelize_typescript_1.Table
    ], PermissionUsersMapping);
    return PermissionUsersMapping;
})();
exports.PermissionUsersMapping = PermissionUsersMapping;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvblVzZXJzTWFwcGluZy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL3Blcm1pc3Npb25zL1Blcm1pc3Npb25Vc2Vyc01hcHBpbmcubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVThCO0FBQzlCLHlEQUFnRDtBQUNoRCw4Q0FBcUM7QUFFckM7O0dBRUc7QUFFSDtJQUFBLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXVCLFNBQVEsNEJBQTZCO0tBeUJ4RSxDQUFBO0lBbEJHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O3NEQUNZO0lBR25CO1FBREMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw2QkFBVSxDQUFDO2tDQUNSLDZCQUFVOzhEQUFDO0lBSy9CO1FBSEMsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw2QkFBVSxDQUFDO1FBQzVCLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztnRUFDc0I7SUFHN0I7UUFEQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7a0NBQ0MsaUJBQUk7aUVBQUM7SUFLNUI7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7UUFDdEIsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O21FQUN5QjtJQXZCdkIsc0JBQXNCO1FBRGxDLDRCQUFLO09BQ08sc0JBQXNCLENBeUJsQztJQUFELDZCQUFDO0tBQUE7QUF6Qlksd0RBQXNCIn0=