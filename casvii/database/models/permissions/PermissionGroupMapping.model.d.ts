import { Model } from 'sequelize-typescript';
import { Permission } from './Permission.model';
import { PermissionGroup } from './PermissionGroup.model';
/**
 * Maps a permission to a permission group. Automatically duplicated from the code base.
 */
export declare class PermissionGroupMapping extends Model<PermissionGroupMapping> {
    id: number;
    permission: Permission;
    permissionID: number;
    permissionGroup: PermissionGroup;
    permissionGroupID: number;
}
