import { Model } from 'sequelize-typescript';
import { Permission } from './Permission.model';
import { User } from '../User.model';
/**
 * Maps a user to an individual permission because while discouraged it is possible for individual grants. Time and authorizer is recorded for security purposes.
 */
export declare class PermissionUsersMapping extends Model<PermissionUsersMapping> {
    id: number;
    permission: Permission;
    permissionID: number;
    grantedByUser: User;
    grantedByUserID: number;
}
