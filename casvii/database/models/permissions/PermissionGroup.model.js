"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionGroup = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const PermissionGroupInheritance_model_1 = require("./PermissionGroupInheritance.model");
const PermissionGroupUserMapping_1 = require("./PermissionGroupUserMapping");
const PermissionGroupMapping_model_1 = require("./PermissionGroupMapping.model");
/**
 * A group of permissions collected under a name and optional description. These are hierachical which means that they
 * can inherit from other groups.
 */
let PermissionGroup = /** @class */ (() => {
    let PermissionGroup = class PermissionGroup extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroup.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], PermissionGroup.prototype, "name", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], PermissionGroup.prototype, "description", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], PermissionGroup.prototype, "hash", void 0);
    __decorate([
        sequelize_typescript_1.HasMany(() => PermissionGroupInheritance_model_1.PermissionGroupInheritance, {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Array)
    ], PermissionGroup.prototype, "inheritances", void 0);
    __decorate([
        sequelize_typescript_1.HasMany(() => PermissionGroupUserMapping_1.PermissionGroupUserMapping, {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Array)
    ], PermissionGroup.prototype, "userMappings", void 0);
    __decorate([
        sequelize_typescript_1.HasMany(() => PermissionGroupMapping_model_1.PermissionGroupMapping, {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Array)
    ], PermissionGroup.prototype, "permissionMappings", void 0);
    PermissionGroup = __decorate([
        sequelize_typescript_1.Table
    ], PermissionGroup);
    return PermissionGroup;
})();
exports.PermissionGroup = PermissionGroup;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvbkdyb3VwLm1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS9kYXRhYmFzZS9tb2RlbHMvcGVybWlzc2lvbnMvUGVybWlzc2lvbkdyb3VwLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLCtEQVU4QjtBQUM5Qix5RkFBZ0Y7QUFDaEYsNkVBQTBFO0FBQzFFLGlGQUF3RTtBQUV4RTs7O0dBR0c7QUFFSDtJQUFBLElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWdCLFNBQVEsNEJBQXNCO0tBcUMxRCxDQUFBO0lBOUJHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07OytDQUNZO0lBS25CO1FBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07UUFDTiw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOztpREFDRDtJQUdyQjtRQURDLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7O3dEQUNNO0lBRzVCO1FBREMsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7aURBQ0Q7SUFNckI7UUFKQyw4QkFBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLDZEQUEwQixFQUFFO1lBQ3ZDLFFBQVEsRUFBRSxTQUFTO1lBQ25CLFFBQVEsRUFBRSxTQUFTO1NBQ3RCLENBQUM7O3lEQUNpRDtJQU1uRDtRQUpDLDhCQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsdURBQTBCLEVBQUU7WUFDdkMsUUFBUSxFQUFFLFNBQVM7WUFDbkIsUUFBUSxFQUFFLFNBQVM7U0FDdEIsQ0FBQzs7eURBQ2lEO0lBTW5EO1FBSkMsOEJBQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxxREFBc0IsRUFBRTtZQUNuQyxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsU0FBUztTQUN0QixDQUFDOzsrREFDbUQ7SUFwQzVDLGVBQWU7UUFEM0IsNEJBQUs7T0FDTyxlQUFlLENBcUMzQjtJQUFELHNCQUFDO0tBQUE7QUFyQ1ksMENBQWUifQ==