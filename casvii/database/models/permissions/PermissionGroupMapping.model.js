"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionGroupMapping = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Permission_model_1 = require("./Permission.model");
const PermissionGroup_model_1 = require("./PermissionGroup.model");
/**
 * Maps a permission to a permission group. Automatically duplicated from the code base.
 */
let PermissionGroupMapping = /** @class */ (() => {
    let PermissionGroupMapping = class PermissionGroupMapping extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupMapping.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => Permission_model_1.Permission, {
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", Permission_model_1.Permission)
    ], PermissionGroupMapping.prototype, "permission", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => Permission_model_1.Permission),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupMapping.prototype, "permissionID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => PermissionGroup_model_1.PermissionGroup, {
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        }),
        __metadata("design:type", PermissionGroup_model_1.PermissionGroup)
    ], PermissionGroupMapping.prototype, "permissionGroup", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => PermissionGroup_model_1.PermissionGroup),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], PermissionGroupMapping.prototype, "permissionGroupID", void 0);
    PermissionGroupMapping = __decorate([
        sequelize_typescript_1.Table
    ], PermissionGroupMapping);
    return PermissionGroupMapping;
})();
exports.PermissionGroupMapping = PermissionGroupMapping;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvbkdyb3VwTWFwcGluZy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL3Blcm1pc3Npb25zL1Blcm1pc3Npb25Hcm91cE1hcHBpbmcubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVThCO0FBQzlCLHlEQUFnRDtBQUNoRCxtRUFBMEQ7QUFFMUQ7O0dBRUc7QUFFSDtJQUFBLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXVCLFNBQVEsNEJBQTZCO0tBK0J4RSxDQUFBO0lBeEJHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O3NEQUNZO0lBTW5CO1FBSkMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw2QkFBVSxFQUFFO1lBQ3pCLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSxTQUFTO1NBQ3RCLENBQUM7a0NBQ2tCLDZCQUFVOzhEQUFDO0lBSy9CO1FBSEMsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw2QkFBVSxDQUFDO1FBQzVCLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztnRUFDc0I7SUFNN0I7UUFKQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLHVDQUFlLEVBQUU7WUFDOUIsUUFBUSxFQUFFLFdBQVc7WUFDckIsUUFBUSxFQUFFLFNBQVM7U0FDdEIsQ0FBQztrQ0FDdUIsdUNBQWU7bUVBQUM7SUFLekM7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHVDQUFlLENBQUM7UUFDakMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07O3FFQUMyQjtJQTdCekIsc0JBQXNCO1FBRGxDLDRCQUFLO09BQ08sc0JBQXNCLENBK0JsQztJQUFELDZCQUFDO0tBQUE7QUEvQlksd0RBQXNCIn0=