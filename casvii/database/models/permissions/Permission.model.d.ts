import { Model } from 'sequelize-typescript';
import { PermissionGroupMapping } from "./PermissionGroupMapping.model";
import { PermissionUsersMapping } from "./PermissionUsersMapping.model";
/**
 * A single permission which grants access to something across the whole xiomi system. The description is optional and
 * can detail what the permission means. These should be auto generated from the code based and updated on program
 * launch
 */
export declare class Permission extends Model<Permission> {
    id: number;
    name: string;
    description: string;
    hash: string;
    groupMappings: PermissionGroupMapping[];
    userMappings: PermissionUsersMapping[];
}
