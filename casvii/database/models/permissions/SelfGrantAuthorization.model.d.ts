import { Model } from "sequelize-typescript";
import { PermissionGroup } from "./PermissionGroup.model";
export declare class SelfGrantAuthorizationModel extends Model<SelfGrantAuthorizationModel> {
    id: number;
    group: PermissionGroup;
    groupID: number;
    serviceName: string;
    serviceDescription: string;
    serviceIconURL: string;
}
