import { Model } from 'sequelize-typescript';
import { User } from './User.model';
/**
 * Contains all reset requests for users who want to change any set of 'secure' data on their account.
 * Because the username should not be shown during this interaction we provide a unique token during
 * the process that is associated with their user. Requested allows the implementation of automatic expiry
 */
export declare class ResetDataRequest extends Model<ResetDataRequest> {
    id: number;
    requested: Date;
    userID: number;
    user: User;
    userToken: string;
    resetToken: string;
    type: string;
    completed: boolean;
    additionalData: string;
}
