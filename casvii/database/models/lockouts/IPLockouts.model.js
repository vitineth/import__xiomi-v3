"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IPLockouts = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
/**
 * IP lockouts which are distinct from the user account lockouts. These happen when a specific IP is found to be brute
 * forcing the system. These will incrementally increase with time to a code defined limit
 */
let IPLockouts = /** @class */ (() => {
    let IPLockouts = class IPLockouts extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], IPLockouts.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], IPLockouts.prototype, "ip", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], IPLockouts.prototype, "startTime", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], IPLockouts.prototype, "duration", void 0);
    IPLockouts = __decorate([
        sequelize_typescript_1.Table
    ], IPLockouts);
    return IPLockouts;
})();
exports.IPLockouts = IPLockouts;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSVBMb2Nrb3V0cy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL2xvY2tvdXRzL0lQTG9ja291dHMubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVzhCO0FBRTlCOzs7R0FHRztBQUVIO0lBQUEsSUFBYSxVQUFVLEdBQXZCLE1BQWEsVUFBVyxTQUFRLDRCQUFpQjtLQXFCaEQsQ0FBQTtJQWRHO1FBTEMsb0NBQWE7UUFDYiw2QkFBTTtRQUNOLGlDQUFVO1FBQ1YsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07OzBDQUNZO0lBSW5CO1FBRkMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7MENBQ0g7SUFLbkI7UUFIQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw4QkFBTyxDQUFDLGdDQUFTLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDL0MsNkJBQU07a0NBQ1ksSUFBSTtpREFBQztJQUl4QjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztnREFDa0I7SUFwQmhCLFVBQVU7UUFEdEIsNEJBQUs7T0FDTyxVQUFVLENBcUJ0QjtJQUFELGlCQUFDO0tBQUE7QUFyQlksZ0NBQVUifQ==