"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Lockout = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../User.model");
/**
 * All locked out users (users with the 'auth' locked state) and the justification behind the lock (to be used only
 * internally). The start time is to mark the end of the lockout to the extent of the duration (to allow to incremental
 * increases)
 */
let Lockout = /** @class */ (() => {
    let Lockout = class Lockout extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Lockout.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Lockout.prototype, "userID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], Lockout.prototype, "user", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Lockout.prototype, "duration", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], Lockout.prototype, "reason", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], Lockout.prototype, "startTime", void 0);
    Lockout = __decorate([
        sequelize_typescript_1.Table
    ], Lockout);
    return Lockout;
})();
exports.Lockout = Lockout;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9ja291dC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL2xvY2tvdXRzL0xvY2tvdXQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBYThCO0FBQzlCLDhDQUFxQztBQUVyQzs7OztHQUlHO0FBRUg7SUFBQSxJQUFhLE9BQU8sR0FBcEIsTUFBYSxPQUFRLFNBQVEsNEJBQWM7S0E2QjFDLENBQUE7SUF0Qkc7UUFMQyxvQ0FBYTtRQUNiLDZCQUFNO1FBQ04saUNBQVU7UUFDVixnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7dUNBQ1k7SUFLbkI7UUFIQyxpQ0FBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7UUFDdEIsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU07OzJDQUNnQjtJQUd2QjtRQURDLGdDQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQUksQ0FBQztrQ0FDUixpQkFBSTt5Q0FBQztJQUluQjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOzs2Q0FDa0I7SUFHekI7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzsyQ0FDQztJQUt2QjtRQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDhCQUFPLENBQUMsZ0NBQVMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQyw2QkFBTTtrQ0FDWSxJQUFJOzhDQUFDO0lBM0JmLE9BQU87UUFEbkIsNEJBQUs7T0FDTyxPQUFPLENBNkJuQjtJQUFELGNBQUM7S0FBQTtBQTdCWSwwQkFBTyJ9