import { Model } from 'sequelize-typescript';
/**
 * IP lockouts which are distinct from the user account lockouts. These happen when a specific IP is found to be brute
 * forcing the system. These will incrementally increase with time to a code defined limit
 */
export declare class IPLockouts extends Model<IPLockouts> {
    id: number;
    ip: string;
    startTime: Date;
    duration: number;
}
