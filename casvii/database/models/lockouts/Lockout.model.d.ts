import { Model } from 'sequelize-typescript';
import { User } from '../User.model';
/**
 * All locked out users (users with the 'auth' locked state) and the justification behind the lock (to be used only
 * internally). The start time is to mark the end of the lockout to the extent of the duration (to allow to incremental
 * increases)
 */
export declare class Lockout extends Model<Lockout> {
    id: number;
    userID: number;
    user: User;
    duration: number;
    reason: string;
    startTime: Date;
}
