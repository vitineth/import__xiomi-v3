import { Model } from 'sequelize-typescript';
/**
 * Contains user definitions. The basic definition of id, username, name, email and hash are self explanatory.
 */
export declare class User extends Model<User> {
    id: number;
    username: string;
    email: string;
    full_name: string;
    hash: string;
    hashVersion: number;
    pinHash: string;
    pinHashVersion: number;
    rotationRequired: boolean;
    lastRotation: Date;
    /**
     * Change log:
     * 21/01/2019: added two_factor_setup (00-add-lock-status.migration.js)
     */
    lockedStatus: any;
    /**
     * Change log:
     * 04/02/2020: added registered (01-add-registered.migration.js)
     */
    registered: boolean;
}
