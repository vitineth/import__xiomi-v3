"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResetDataRequest = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("./User.model");
/**
 * Contains all reset requests for users who want to change any set of 'secure' data on their account.
 * Because the username should not be shown during this interaction we provide a unique token during
 * the process that is associated with their user. Requested allows the implementation of automatic expiry
 */
let ResetDataRequest = /** @class */ (() => {
    let ResetDataRequest = class ResetDataRequest extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], ResetDataRequest.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], ResetDataRequest.prototype, "requested", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], ResetDataRequest.prototype, "userID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], ResetDataRequest.prototype, "user", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], ResetDataRequest.prototype, "userToken", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], ResetDataRequest.prototype, "resetToken", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.ENUM('password', 'user_pin', 'email', 'security_question')),
        __metadata("design:type", String)
    ], ResetDataRequest.prototype, "type", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Boolean)
    ], ResetDataRequest.prototype, "completed", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], ResetDataRequest.prototype, "additionalData", void 0);
    ResetDataRequest = __decorate([
        sequelize_typescript_1.Table
    ], ResetDataRequest);
    return ResetDataRequest;
})();
exports.ResetDataRequest = ResetDataRequest;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVzZXREYXRhUmVxdWVzdC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL1Jlc2V0RGF0YVJlcXVlc3QubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBYThCO0FBQzlCLDZDQUFvQztBQUVwQzs7OztHQUlHO0FBRUg7SUFBQSxJQUFhLGdCQUFnQixHQUE3QixNQUFhLGdCQUFpQixTQUFRLDRCQUF1QjtLQTBDNUQsQ0FBQTtJQW5DRztRQUxDLG9DQUFhO1FBQ2IsNkJBQU07UUFDTixpQ0FBVTtRQUNWLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztnREFDWTtJQUtuQjtRQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDhCQUFPLENBQUMsZ0NBQVMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQyw2QkFBTTtrQ0FDWSxJQUFJO3VEQUFDO0lBS3hCO1FBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBSSxDQUFDO1FBQ3RCLDZCQUFNOztvREFDZ0I7SUFHdkI7UUFEQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7a0NBQ1IsaUJBQUk7a0RBQUM7SUFJbkI7UUFGQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzt1REFDSTtJQUkxQjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7O3dEQUNLO0lBSTNCO1FBRkMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxDQUFDOztrREFDdkQ7SUFLckI7UUFIQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw4QkFBTyxDQUFDLEtBQUssQ0FBQztRQUNkLDZCQUFNOzt1REFDb0I7SUFHM0I7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzs0REFDUztJQXhDdEIsZ0JBQWdCO1FBRDVCLDRCQUFLO09BQ08sZ0JBQWdCLENBMEM1QjtJQUFELHVCQUFDO0tBQUE7QUExQ1ksNENBQWdCIn0=