"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SecurityQuestionMapping = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../User.model");
const SecurityQuestion_model_1 = require("./SecurityQuestion.model");
/**
 * Maps users to their security questions and answers. Answers for the security questions are handled like passwords
 * and hashed to be reset through the standard security reset system. Additional data holds the security question if
 * its defined as other (either as a value in security_questions or null)
 */
let SecurityQuestionMapping = /** @class */ (() => {
    let SecurityQuestionMapping = class SecurityQuestionMapping extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], SecurityQuestionMapping.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], SecurityQuestionMapping.prototype, "user", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], SecurityQuestionMapping.prototype, "userID", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.ForeignKey(() => SecurityQuestion_model_1.SecurityQuestion),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], SecurityQuestionMapping.prototype, "securityQuestionID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => SecurityQuestion_model_1.SecurityQuestion),
        __metadata("design:type", SecurityQuestion_model_1.SecurityQuestion)
    ], SecurityQuestionMapping.prototype, "securityQuestion", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], SecurityQuestionMapping.prototype, "additionalData", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], SecurityQuestionMapping.prototype, "answerHash", void 0);
    SecurityQuestionMapping = __decorate([
        sequelize_typescript_1.Table
    ], SecurityQuestionMapping);
    return SecurityQuestionMapping;
})();
exports.SecurityQuestionMapping = SecurityQuestionMapping;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VjdXJpdHlRdWVzdGlvbk1hcHBpbmcubW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL21vZGVscy9zZWN1cml0eS9TZWN1cml0eVF1ZXN0aW9uTWFwcGluZy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwrREFXOEI7QUFDOUIsOENBQXFDO0FBQ3JDLHFFQUE0RDtBQUU1RDs7OztHQUlHO0FBRUg7SUFBQSxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF3QixTQUFRLDRCQUE4QjtLQWdDMUUsQ0FBQTtJQXpCRztRQUxDLG9DQUFhO1FBQ2IsNkJBQU07UUFDTixpQ0FBVTtRQUNWLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOzt1REFDWTtJQUduQjtRQURDLGdDQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQUksQ0FBQztrQ0FDUixpQkFBSTt5REFBQztJQUtuQjtRQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLGlDQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQUksQ0FBQztRQUN0Qiw2QkFBTTs7MkRBQ2dCO0lBS3ZCO1FBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx5Q0FBZ0IsQ0FBQztRQUNsQyw2QkFBTTs7dUVBQzRCO0lBR25DO1FBREMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx5Q0FBZ0IsQ0FBQztrQ0FDUix5Q0FBZ0I7cUVBQUM7SUFHM0M7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzttRUFDUztJQUkvQjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7OytEQUNLO0lBOUJsQix1QkFBdUI7UUFEbkMsNEJBQUs7T0FDTyx1QkFBdUIsQ0FnQ25DO0lBQUQsOEJBQUM7S0FBQTtBQWhDWSwwREFBdUIifQ==