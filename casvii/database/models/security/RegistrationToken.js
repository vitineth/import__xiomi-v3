"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../User.model");
/**
 * Contains user definitions. The basic definition of id, username, name, email and hash are self explanatory.
 */
let RegistrationToken = class RegistrationToken extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.AutoIncrement,
    sequelize_typescript_1.Unique,
    sequelize_typescript_1.PrimaryKey,
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], RegistrationToken.prototype, "id", void 0);
__decorate([
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], RegistrationToken.prototype, "requested", void 0);
__decorate([
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.ForeignKey(() => User_model_1.User),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], RegistrationToken.prototype, "userID", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => User_model_1.User),
    __metadata("design:type", User_model_1.User)
], RegistrationToken.prototype, "user", void 0);
__decorate([
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
    __metadata("design:type", String)
], RegistrationToken.prototype, "userToken", void 0);
__decorate([
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
    __metadata("design:type", String)
], RegistrationToken.prototype, "resetToken", void 0);
__decorate([
    sequelize_typescript_1.AllowNull(false),
    sequelize_typescript_1.Default(false),
    sequelize_typescript_1.Column,
    __metadata("design:type", Boolean)
], RegistrationToken.prototype, "completed", void 0);
RegistrationToken = __decorate([
    sequelize_typescript_1.Table
], RegistrationToken);
exports.RegistrationToken = RegistrationToken;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmVnaXN0cmF0aW9uVG9rZW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL21vZGVscy9zZWN1cml0eS9SZWdpc3RyYXRpb25Ub2tlbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLCtEQWE4QjtBQUM5Qiw4Q0FBcUM7QUFFckM7O0dBRUc7QUFFSCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFrQixTQUFRLDRCQUF3QjtDQWtDOUQsQ0FBQTtBQTVCRztJQUxDLG9DQUFhO0lBQ2IsNkJBQU07SUFDTixpQ0FBVTtJQUNWLGdDQUFTLENBQUMsS0FBSyxDQUFDO0lBQ2hCLDZCQUFNOzs2Q0FDWTtBQUtuQjtJQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO0lBQ2hCLDhCQUFPLENBQUMsZ0NBQVMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUMvQyw2QkFBTTs4QkFDWSxJQUFJO29EQUFDO0FBS3hCO0lBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7SUFDaEIsaUNBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBSSxDQUFDO0lBQ3RCLDZCQUFNOztpREFDZ0I7QUFHdkI7SUFEQyxnQ0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLGlCQUFJLENBQUM7OEJBQ1IsaUJBQUk7K0NBQUM7QUFJbkI7SUFGQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztJQUNoQiw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOztvREFDSTtBQUkxQjtJQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO0lBQ2hCLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7O3FEQUNLO0FBSzNCO0lBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7SUFDaEIsOEJBQU8sQ0FBQyxLQUFLLENBQUM7SUFDZCw2QkFBTTs7b0RBQ29CO0FBaENsQixpQkFBaUI7SUFEN0IsNEJBQUs7R0FDTyxpQkFBaUIsQ0FrQzdCO0FBbENZLDhDQUFpQiJ9