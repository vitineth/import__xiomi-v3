import { Model } from 'sequelize-typescript';
/**
 * A set of predefined security questions which can be chosen by users rather than them writing their own.
 */
export declare class SecurityQuestion extends Model<SecurityQuestion> {
    id: number;
    question: string;
}
