import { Model } from 'sequelize-typescript';
import { User } from '../User.model';
/**
 * Contains a users two factor authentication method if it has been configured.
 */
export declare class TwoFactorConfiguration extends Model<TwoFactorConfiguration> {
    id: number;
    userID: number;
    user: User;
    secret: string;
    /**
     * Saved devices contains the ips, user agents and other identifying information for browsers and devices that they have tried to save.
     */
    savedDevices: string;
    emailOnFail: boolean;
}
