"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwoFactorConfiguration = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../User.model");
/**
 * Contains a users two factor authentication method if it has been configured.
 */
let TwoFactorConfiguration = /** @class */ (() => {
    let TwoFactorConfiguration = class TwoFactorConfiguration extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], TwoFactorConfiguration.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], TwoFactorConfiguration.prototype, "userID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], TwoFactorConfiguration.prototype, "user", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], TwoFactorConfiguration.prototype, "secret", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], TwoFactorConfiguration.prototype, "savedDevices", void 0);
    __decorate([
        sequelize_typescript_1.Default(true),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", Boolean)
    ], TwoFactorConfiguration.prototype, "emailOnFail", void 0);
    TwoFactorConfiguration = __decorate([
        sequelize_typescript_1.Table
    ], TwoFactorConfiguration);
    return TwoFactorConfiguration;
})();
exports.TwoFactorConfiguration = TwoFactorConfiguration;
//
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVHdvRmFjdG9yQ29uZmlndXJhdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL3NlY3VyaXR5L1R3b0ZhY3RvckNvbmZpZ3VyYXRpb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVzhCO0FBQzlCLDhDQUFxQztBQUVyQzs7R0FFRztBQUVIO0lBQUEsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBdUIsU0FBUSw0QkFBNkI7S0FpQ3hFLENBQUE7SUExQkc7UUFMQyxvQ0FBYTtRQUNiLDZCQUFNO1FBQ04saUNBQVU7UUFDVixnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7c0RBQ1k7SUFNbkI7UUFKQyw2QkFBTTtRQUNOLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLGlDQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQUksQ0FBQztRQUN0Qiw2QkFBTTs7MERBQ2dCO0lBR3ZCO1FBREMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBSSxDQUFDO2tDQUNSLGlCQUFJO3dEQUFDO0lBSW5CO1FBRkMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7MERBQ0M7SUFPdkI7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOztnRUFDTztJQUk3QjtRQUZDLDhCQUFPLENBQUMsSUFBSSxDQUFDO1FBQ2IsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7K0RBQ087SUEvQnBCLHNCQUFzQjtRQURsQyw0QkFBSztPQUNPLHNCQUFzQixDQWlDbEM7SUFBRCw2QkFBQztLQUFBO0FBakNZLHdEQUFzQjtBQWtDbkMsRUFBRSJ9