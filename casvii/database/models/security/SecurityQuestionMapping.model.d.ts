import { Model } from 'sequelize-typescript';
import { User } from '../User.model';
import { SecurityQuestion } from './SecurityQuestion.model';
/**
 * Maps users to their security questions and answers. Answers for the security questions are handled like passwords
 * and hashed to be reset through the standard security reset system. Additional data holds the security question if
 * its defined as other (either as a value in security_questions or null)
 */
export declare class SecurityQuestionMapping extends Model<SecurityQuestionMapping> {
    id: number;
    user: User;
    userID: number;
    securityQuestionID: number;
    securityQuestion: SecurityQuestion;
    additionalData: string;
    answerHash: string;
}
