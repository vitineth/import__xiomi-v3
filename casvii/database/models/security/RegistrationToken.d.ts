import { Model } from 'sequelize-typescript';
import { User } from '../User.model';
/**
 * Contains user definitions. The basic definition of id, username, name, email and hash are self explanatory.
 */
export declare class RegistrationToken extends Model<RegistrationToken> {
    id: number;
    requested: Date;
    userID: number;
    user: User;
    userToken: string;
    resetToken: string;
    completed: boolean;
}
