"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
/**
 * Contains user definitions. The basic definition of id, username, name, email and hash are self explanatory.
 */
let User = /** @class */ (() => {
    let User = class User extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], User.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Unique
        // @Comment('Should be lower cased, cannot currently implement the index required so should be handled manually for now')
        ,
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], User.prototype, "username", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Unique
        // @Comment('should be lower cased, see username')
        ,
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], User.prototype, "email", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], User.prototype, "full_name", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], User.prototype, "hash", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false)
        // @Comment('Hash version is an internal versioning system for password hashes which allows for easy hashing method migration while leaving backward compatibility on the system.')
        ,
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], User.prototype, "hashVersion", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false)
        // @Comment('Pin hash is another form of authentication which is a simple pin that users must enter which is used for additional verification. It is a hashed code and stored with the security of a password')
        ,
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], User.prototype, "pinHash", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], User.prototype, "pinHashVersion", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(false)
        // @Comment('Rotation required dictates whether the user needs to rotate their authentication details on the next log in. This is the same as the breached/compromised flag described in the security manual')
        ,
        sequelize_typescript_1.Column,
        __metadata("design:type", Boolean)
    ], User.prototype, "rotationRequired", void 0);
    __decorate([
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], User.prototype, "lastRotation", void 0);
    __decorate([
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.ENUM('accessible', 'auth', 'admin', 'two_factor_setup')),
        __metadata("design:type", Object)
    ], User.prototype, "lockedStatus", void 0);
    __decorate([
        sequelize_typescript_1.Column,
        __metadata("design:type", Boolean)
    ], User.prototype, "registered", void 0);
    User = __decorate([
        sequelize_typescript_1.Table
    ], User);
    return User;
})();
exports.User = User;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL1VzZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsK0RBVThCO0FBRTlCOztHQUVHO0FBRUg7SUFBQSxJQUFhLElBQUksR0FBakIsTUFBYSxJQUFLLFNBQVEsNEJBQVc7S0FtRXBDLENBQUE7SUE3REc7UUFMQyxvQ0FBYTtRQUNiLDZCQUFNO1FBQ04saUNBQVU7UUFDVixnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7b0NBQ1k7SUFNbkI7UUFKQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTtRQUNQLHlIQUF5SDs7UUFDeEgsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7MENBQ0c7SUFNekI7UUFKQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTtRQUNQLGtEQUFrRDs7UUFDakQsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7dUNBQ0E7SUFJdEI7UUFGQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzsyQ0FDSTtJQUkxQjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNLENBQUMsK0JBQVEsQ0FBQyxJQUFJLENBQUM7O3NDQUNEO0lBS3JCO1FBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDakIsbUxBQW1MOztRQUNsTCw2QkFBTTs7NkNBQ3FCO0lBSzVCO1FBSEMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDakIsK01BQStNOztRQUM5TSw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzt5Q0FDRTtJQUl4QjtRQUZDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOztnREFDd0I7SUFNL0I7UUFKQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw4QkFBTyxDQUFDLEtBQUssQ0FBQztRQUNmLDhNQUE4TTs7UUFDN00sNkJBQU07O2tEQUMyQjtJQUlsQztRQURDLDZCQUFNO2tDQUNlLElBQUk7OENBQUM7SUFRM0I7UUFEQyw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixDQUFDLENBQUM7OzhDQUNyRDtJQU9wQjtRQURDLDZCQUFNOzs0Q0FDcUI7SUFqRW5CLElBQUk7UUFEaEIsNEJBQUs7T0FDTyxJQUFJLENBbUVoQjtJQUFELFdBQUM7S0FBQTtBQW5FWSxvQkFBSSJ9