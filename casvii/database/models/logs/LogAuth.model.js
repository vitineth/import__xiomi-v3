"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogAuth = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let LogAuth = /** @class */ (() => {
    let LogAuth = class LogAuth extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], LogAuth.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], LogAuth.prototype, "source", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], LogAuth.prototype, "detail", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], LogAuth.prototype, "logged", void 0);
    LogAuth = __decorate([
        sequelize_typescript_1.Table
    ], LogAuth);
    return LogAuth;
})();
exports.LogAuth = LogAuth;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9nQXV0aC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvZGF0YWJhc2UvbW9kZWxzL2xvZ3MvTG9nQXV0aC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSwrREFXOEI7QUFHOUI7SUFBQSxJQUFhLE9BQU8sR0FBcEIsTUFBYSxPQUFRLFNBQVEsNEJBQWM7S0FvQjFDLENBQUE7SUFkRztRQUxDLG9DQUFhO1FBQ2IsNkJBQU07UUFDTixpQ0FBVTtRQUNWLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDZCQUFNOzt1Q0FDSztJQUlaO1FBRkMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7MkNBQ047SUFJaEI7UUFGQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTSxDQUFDLCtCQUFRLENBQUMsSUFBSSxDQUFDOzsyQ0FDTjtJQUtoQjtRQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLDhCQUFPLENBQUMsZ0NBQVMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQyw2QkFBTTtrQ0FDRSxJQUFJOzJDQUFDO0lBbkJMLE9BQU87UUFEbkIsNEJBQUs7T0FDTyxPQUFPLENBb0JuQjtJQUFELGNBQUM7S0FBQTtBQXBCWSwwQkFBTyJ9