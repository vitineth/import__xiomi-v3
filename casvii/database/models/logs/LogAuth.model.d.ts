import { Model } from 'sequelize-typescript';
export declare class LogAuth extends Model<LogAuth> {
    id: number;
    source: string;
    detail: string;
    logged: Date;
}
