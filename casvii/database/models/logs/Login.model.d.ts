import { Model } from 'sequelize-typescript';
import { User } from '../User.model';
export declare class Login extends Model<Login> {
    id: number;
    userID: number;
    user: User;
    ipAddress: string;
    logged: Date;
}
