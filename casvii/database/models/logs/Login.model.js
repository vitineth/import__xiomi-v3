"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Login = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../User.model");
let Login = /** @class */ (() => {
    let Login = class Login extends sequelize_typescript_1.Model {
    };
    __decorate([
        sequelize_typescript_1.AutoIncrement,
        sequelize_typescript_1.Unique,
        sequelize_typescript_1.PrimaryKey,
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Login.prototype, "id", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.ForeignKey(() => User_model_1.User),
        sequelize_typescript_1.Column,
        __metadata("design:type", Number)
    ], Login.prototype, "userID", void 0);
    __decorate([
        sequelize_typescript_1.BelongsTo(() => User_model_1.User),
        __metadata("design:type", User_model_1.User)
    ], Login.prototype, "user", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Column(sequelize_typescript_1.DataType.TEXT),
        __metadata("design:type", String)
    ], Login.prototype, "ipAddress", void 0);
    __decorate([
        sequelize_typescript_1.AllowNull(false),
        sequelize_typescript_1.Default(sequelize_typescript_1.Sequelize.literal('CURRENT_TIMESTAMP')),
        sequelize_typescript_1.Column,
        __metadata("design:type", Date)
    ], Login.prototype, "logged", void 0);
    Login = __decorate([
        sequelize_typescript_1.Table
    ], Login);
    return Login;
})();
exports.Login = Login;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9naW4ubW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2RhdGFiYXNlL21vZGVscy9sb2dzL0xvZ2luLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLCtEQWE4QjtBQUM5Qiw4Q0FBcUM7QUFHckM7SUFBQSxJQUFhLEtBQUssR0FBbEIsTUFBYSxLQUFNLFNBQVEsNEJBQVk7S0F3QnRDLENBQUE7SUFsQkc7UUFMQyxvQ0FBYTtRQUNiLDZCQUFNO1FBQ04saUNBQVU7UUFDVixnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw2QkFBTTs7cUNBQ0s7SUFLWjtRQUhDLGdDQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2hCLGlDQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsaUJBQUksQ0FBQztRQUN0Qiw2QkFBTTs7eUNBQ2dCO0lBR3ZCO1FBREMsZ0NBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBSSxDQUFDO2tDQUNSLGlCQUFJO3VDQUFDO0lBSW5CO1FBRkMsZ0NBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEIsNkJBQU0sQ0FBQywrQkFBUSxDQUFDLElBQUksQ0FBQzs7NENBQ0g7SUFLbkI7UUFIQyxnQ0FBUyxDQUFDLEtBQUssQ0FBQztRQUNoQiw4QkFBTyxDQUFDLGdDQUFTLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDL0MsNkJBQU07a0NBQ0UsSUFBSTt5Q0FBQztJQXZCTCxLQUFLO1FBRGpCLDRCQUFLO09BQ08sS0FBSyxDQXdCakI7SUFBRCxZQUFDO0tBQUE7QUF4Qlksc0JBQUsifQ==