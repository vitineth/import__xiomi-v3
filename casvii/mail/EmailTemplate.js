"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailTemplate = void 0;
const liquidjs_1 = require("liquidjs");
const fs_extra_1 = require("fs-extra");
const path = __importStar(require("path"));
/**
 * The root at which all email templates are stored
 */
const EMAIL_TEMPLATE_BASE_PATH = path.join(__dirname, '..', '..', '..', 'cas-data', 'emails');
/**
 * THe liquid instance used to render email templates
 */
const liquid = new liquidjs_1.Liquid({
    root: EMAIL_TEMPLATE_BASE_PATH,
});
var EmailTemplate;
(function (EmailTemplate) {
    async function renderTemplate(filename, data) {
        const template = await fs_extra_1.readFile(path.join(EMAIL_TEMPLATE_BASE_PATH, filename), { encoding: 'utf8' });
        return await liquid.parseAndRender(template, data);
    }
    /**
     * Renders the template for a password change request email
     * @param request
     */
    async function renderPasswordChangeRequestEmail(request) {
        return renderTemplate('unauth-change-request.html', {
            name: request.user.full_name,
            userToken: request.userToken,
            resetToken: request.resetToken,
        });
    }
    EmailTemplate.renderPasswordChangeRequestEmail = renderPasswordChangeRequestEmail;
    /**
     * Renders the template for a security warning about a changed email account
     * @param user the user who was targeted and should be sent the email
     */
    async function renderPasswordChangeSecurityWarning(user) {
        return renderTemplate('security-warning-password-change.html', {
            name: user.full_name,
        });
    }
    EmailTemplate.renderPasswordChangeSecurityWarning = renderPasswordChangeSecurityWarning;
    /**
     * Renders the template for a password change on a user account
     * @param user the user who has changed their password and should be sent the email
     */
    async function renderPasswordChangeNotice(user) {
        return renderTemplate('password-change-success.html', {
            name: user.full_name,
        });
    }
    EmailTemplate.renderPasswordChangeNotice = renderPasswordChangeNotice;
    /**
     * Renders the template for a login attempt that did not work
     * @param user the user who has tried to log in and should be sent the email
     * @param ip the ip address the login came form
     */
    async function renderSecurityWarningInvalidLogin(user, ip) {
        return renderTemplate('security-warning-invalid-login.html', {
            ip,
            name: user.full_name,
        });
    }
    EmailTemplate.renderSecurityWarningInvalidLogin = renderSecurityWarningInvalidLogin;
    async function renderRegistrationEmail(registerToken) {
        return renderTemplate('register-email.html', {
            user_token: registerToken.userToken,
            registration_token: registerToken.resetToken,
            name: registerToken.user.full_name,
        });
    }
    EmailTemplate.renderRegistrationEmail = renderRegistrationEmail;
    async function renderTwoFactorWarning(user) {
        return renderTemplate('two-factor-warning.html', {
            name: user.full_name,
        });
    }
    EmailTemplate.renderTwoFactorWarning = renderTwoFactorWarning;
    async function renderRegistrationComplete(user) {
        return renderTemplate('register-complete-email.html', {
            name: user.full_name,
        });
    }
    EmailTemplate.renderRegistrationComplete = renderRegistrationComplete;
    async function renderNewAuthorization(user, grant) {
        return renderTemplate('new-authorization.html', {
            name: user.full_name,
            service: {
                name: grant.serviceName,
                description: grant.serviceDescription,
            },
        });
    }
    EmailTemplate.renderNewAuthorization = renderNewAuthorization;
})(EmailTemplate = exports.EmailTemplate || (exports.EmailTemplate = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRW1haWxUZW1wbGF0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jYXN2aWkvbWFpbC9FbWFpbFRlbXBsYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBa0M7QUFFbEMsdUNBQW9DO0FBRXBDLDJDQUE2QjtBQU03Qjs7R0FFRztBQUNILE1BQU0sd0JBQXdCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0FBRTlGOztHQUVHO0FBQ0gsTUFBTSxNQUFNLEdBQUcsSUFBSSxpQkFBTSxDQUFDO0lBQ3RCLElBQUksRUFBRSx3QkFBd0I7Q0FDakMsQ0FBQyxDQUFDO0FBRUgsSUFBaUIsYUFBYSxDQWdGN0I7QUFoRkQsV0FBaUIsYUFBYTtJQUUxQixLQUFLLFVBQVUsY0FBYyxDQUFDLFFBQWdCLEVBQUUsSUFBNEI7UUFDeEUsTUFBTSxRQUFRLEdBQUcsTUFBTSxtQkFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNyRyxPQUFPLE1BQU0sTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7T0FHRztJQUNJLEtBQUssVUFBVSxnQ0FBZ0MsQ0FBQyxPQUF5QjtRQUM1RSxPQUFPLGNBQWMsQ0FBQyw0QkFBNEIsRUFBRTtZQUNoRCxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTO1lBQzVCLFNBQVMsRUFBRSxPQUFPLENBQUMsU0FBUztZQUM1QixVQUFVLEVBQUUsT0FBTyxDQUFDLFVBQVU7U0FDakMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQU5xQiw4Q0FBZ0MsbUNBTXJELENBQUE7SUFFRDs7O09BR0c7SUFDSSxLQUFLLFVBQVUsbUNBQW1DLENBQUMsSUFBVTtRQUNoRSxPQUFPLGNBQWMsQ0FBQyx1Q0FBdUMsRUFBRTtZQUMzRCxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDdkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUpxQixpREFBbUMsc0NBSXhELENBQUE7SUFFRDs7O09BR0c7SUFDSSxLQUFLLFVBQVUsMEJBQTBCLENBQUMsSUFBVTtRQUN2RCxPQUFPLGNBQWMsQ0FBQyw4QkFBOEIsRUFBRTtZQUNsRCxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDdkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUpxQix3Q0FBMEIsNkJBSS9DLENBQUE7SUFFRDs7OztPQUlHO0lBQ0ksS0FBSyxVQUFVLGlDQUFpQyxDQUFDLElBQVUsRUFBRSxFQUFVO1FBQzFFLE9BQU8sY0FBYyxDQUFDLHFDQUFxQyxFQUFFO1lBQ3pELEVBQUU7WUFDRixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDdkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUxxQiwrQ0FBaUMsb0NBS3RELENBQUE7SUFFTSxLQUFLLFVBQVUsdUJBQXVCLENBQUMsYUFBZ0M7UUFDMUUsT0FBTyxjQUFjLENBQUMscUJBQXFCLEVBQUU7WUFDekMsVUFBVSxFQUFFLGFBQWEsQ0FBQyxTQUFTO1lBQ25DLGtCQUFrQixFQUFFLGFBQWEsQ0FBQyxVQUFVO1lBQzVDLElBQUksRUFBRSxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVM7U0FDckMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQU5xQixxQ0FBdUIsMEJBTTVDLENBQUE7SUFFTSxLQUFLLFVBQVUsc0JBQXNCLENBQUMsSUFBVTtRQUNuRCxPQUFPLGNBQWMsQ0FBQyx5QkFBeUIsRUFBRTtZQUM3QyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDdkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUpxQixvQ0FBc0IseUJBSTNDLENBQUE7SUFFTSxLQUFLLFVBQVUsMEJBQTBCLENBQUMsSUFBVTtRQUN2RCxPQUFPLGNBQWMsQ0FBQyw4QkFBOEIsRUFBRTtZQUNsRCxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDdkIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUpxQix3Q0FBMEIsNkJBSS9DLENBQUE7SUFFTSxLQUFLLFVBQVUsc0JBQXNCLENBQUMsSUFBVSxFQUFFLEtBQWtDO1FBQ3ZGLE9BQU8sY0FBYyxDQUFDLHdCQUF3QixFQUFFO1lBQzVDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUztZQUNwQixPQUFPLEVBQUU7Z0JBQ0wsSUFBSSxFQUFFLEtBQUssQ0FBQyxXQUFXO2dCQUN2QixXQUFXLEVBQUUsS0FBSyxDQUFDLGtCQUFrQjthQUN4QztTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7SUFScUIsb0NBQXNCLHlCQVEzQyxDQUFBO0FBQ0wsQ0FBQyxFQWhGZ0IsYUFBYSxHQUFiLHFCQUFhLEtBQWIscUJBQWEsUUFnRjdCIn0=