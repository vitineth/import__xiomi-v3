"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CASEmailProvider = void 0;
const config_1 = require("../../config/config");
const nodemailer_1 = require("nodemailer");
const html_to_text_1 = require("html-to-text");
const EmailTemplate_1 = require("./EmailTemplate");
class CASEmailProvider {
    constructor(transport) {
        config_1.loadEasy('cas/default');
        this.from = config_1.get('cas.email.from');
        if (transport !== undefined)
            this.transport = transport;
        else
            this.transport = nodemailer_1.createTransport(config_1.get('cas.email.configuration'));
    }
    async sendEmail(to, subject, html) {
        return await this.transport.sendMail({
            to,
            subject,
            html,
            from: this.from,
            text: html_to_text_1.fromString(html),
        });
    }
    async sendPasswordChangeRequest(request) {
        return await this.sendEmail(request.user.email, 'Password change request', await EmailTemplate_1.EmailTemplate.renderPasswordChangeRequestEmail(request));
    }
    async sendPasswordChangeSecurityWarning(user) {
        return await this.sendEmail(user.email, 'Security warning - Password change attempt', await EmailTemplate_1.EmailTemplate.renderPasswordChangeSecurityWarning(user));
    }
    async sendPasswordChangeNotice(user) {
        return await this.sendEmail(user.email, 'Account Notice - Password change', await EmailTemplate_1.EmailTemplate.renderPasswordChangeNotice(user));
    }
    async sendSecurityWarningInvalidLogin(user, ip) {
        return await this.sendEmail(user.email, 'Security warning - Login attempt', await EmailTemplate_1.EmailTemplate.renderSecurityWarningInvalidLogin(user, ip));
    }
    async sendRegistrationEmail(registerToken) {
        return await this.sendEmail(registerToken.user.email, 'Registration Request', await EmailTemplate_1.EmailTemplate.renderRegistrationEmail(registerToken));
    }
    async sendTwoFactorWarning(user) {
        return await this.sendEmail(user.email, 'Two Factor Authentication Failed', await EmailTemplate_1.EmailTemplate.renderTwoFactorWarning(user));
    }
    async sendRegistrationComplete(user) {
        return await this.sendEmail(user.email, 'Registration Complete', await EmailTemplate_1.EmailTemplate.renderRegistrationComplete(user));
    }
    async sendAuthorizeEmail(user, grant) {
        return await this.sendEmail(user.email, 'New Account Authorization', await EmailTemplate_1.EmailTemplate.renderNewAuthorization(user, grant));
    }
}
exports.CASEmailProvider = CASEmailProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ0FTRW1haWxQcm92aWRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jYXN2aWkvbWFpbC9DQVNFbWFpbFByb3ZpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLGdEQUFvRDtBQUNwRCwyQ0FBNkM7QUFDN0MsK0NBQTBDO0FBRzFDLG1EQUFnRDtBQUtoRCxNQUFhLGdCQUFnQjtJQUt6QixZQUFZLFNBQWtCO1FBQzFCLGlCQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFeEIsSUFBSSxDQUFDLElBQUksR0FBRyxZQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUVsQyxJQUFJLFNBQVMsS0FBSyxTQUFTO1lBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7O1lBQ25ELElBQUksQ0FBQyxTQUFTLEdBQUcsNEJBQWUsQ0FBQyxZQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFTyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQVUsRUFBRSxPQUFlLEVBQUUsSUFBWTtRQUM3RCxPQUFPLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7WUFDakMsRUFBRTtZQUNGLE9BQU87WUFDUCxJQUFJO1lBQ0osSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsSUFBSSxFQUFFLHlCQUFVLENBQUMsSUFBSSxDQUFDO1NBQ3pCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxLQUFLLENBQUMseUJBQXlCLENBQUMsT0FBeUI7UUFDNUQsT0FBTyxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw2QkFBYSxDQUFDLGdDQUFnQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDOUksQ0FBQztJQUVNLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFVO1FBQ3JELE9BQU8sTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsNENBQTRDLEVBQUUsTUFBTSw2QkFBYSxDQUFDLG1DQUFtQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDekosQ0FBQztJQUVNLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxJQUFVO1FBQzVDLE9BQU8sTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsa0NBQWtDLEVBQUUsTUFBTSw2QkFBYSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDdEksQ0FBQztJQUVNLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxJQUFVLEVBQUUsRUFBVTtRQUMvRCxPQUFPLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sNkJBQWEsQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNqSixDQUFDO0lBRU0sS0FBSyxDQUFDLHFCQUFxQixDQUFDLGFBQWdDO1FBQy9ELE9BQU8sTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO0lBQzlJLENBQUM7SUFFTSxLQUFLLENBQUMsb0JBQW9CLENBQUMsSUFBVTtRQUN4QyxPQUFPLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sNkJBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2xJLENBQUM7SUFFTSxLQUFLLENBQUMsd0JBQXdCLENBQUMsSUFBVTtRQUM1QyxPQUFPLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQWEsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzNILENBQUM7SUFFTSxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBVSxFQUFFLEtBQWtDO1FBQzFFLE9BQU8sTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw2QkFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2xJLENBQUM7Q0FDSjtBQXZERCw0Q0F1REMifQ==