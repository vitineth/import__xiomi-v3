import { User } from "../database/models/User.model";
import { ResetDataRequest } from "../database/models/ResetDataRequest.model";
import { RegistrationToken } from "../database/models/security/RegistrationToken.model";
import mailer = require('nodemailer/lib/mailer');
import { SelfGrantAuthorizationModel } from "../database/models/permissions/SelfGrantAuthorization.model";
export declare class CASEmailProvider {
    private transport;
    private readonly from;
    constructor(transport?: mailer);
    private sendEmail;
    sendPasswordChangeRequest(request: ResetDataRequest): Promise<any>;
    sendPasswordChangeSecurityWarning(user: User): Promise<any>;
    sendPasswordChangeNotice(user: User): Promise<any>;
    sendSecurityWarningInvalidLogin(user: User, ip: string): Promise<any>;
    sendRegistrationEmail(registerToken: RegistrationToken): Promise<any>;
    sendTwoFactorWarning(user: User): Promise<any>;
    sendRegistrationComplete(user: User): Promise<any>;
    sendAuthorizeEmail(user: User, grant: SelfGrantAuthorizationModel): Promise<any>;
}
