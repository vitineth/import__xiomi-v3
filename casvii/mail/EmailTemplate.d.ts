import { ResetDataRequest } from "../database/models/ResetDataRequest.model";
import { User } from "../database/models/User.model";
import { RegistrationToken } from "../database/models/security/RegistrationToken.model";
import { SelfGrantAuthorizationModel } from "../database/models/permissions/SelfGrantAuthorization.model";
export declare namespace EmailTemplate {
    /**
     * Renders the template for a password change request email
     * @param request
     */
    function renderPasswordChangeRequestEmail(request: ResetDataRequest): Promise<string>;
    /**
     * Renders the template for a security warning about a changed email account
     * @param user the user who was targeted and should be sent the email
     */
    function renderPasswordChangeSecurityWarning(user: User): Promise<string>;
    /**
     * Renders the template for a password change on a user account
     * @param user the user who has changed their password and should be sent the email
     */
    function renderPasswordChangeNotice(user: User): Promise<string>;
    /**
     * Renders the template for a login attempt that did not work
     * @param user the user who has tried to log in and should be sent the email
     * @param ip the ip address the login came form
     */
    function renderSecurityWarningInvalidLogin(user: User, ip: string): Promise<string>;
    function renderRegistrationEmail(registerToken: RegistrationToken): Promise<string>;
    function renderTwoFactorWarning(user: User): Promise<string>;
    function renderRegistrationComplete(user: User): Promise<string>;
    function renderNewAuthorization(user: User, grant: SelfGrantAuthorizationModel): Promise<string>;
}
