"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CASVii = void 0;
const User_model_1 = require("./database/models/User.model");
const ResetDataRequest_model_1 = require("./database/models/ResetDataRequest.model");
const SecurityQuestion_model_1 = require("./database/models/security/SecurityQuestion.model");
const SecurityQuestionMapping_model_1 = require("./database/models/security/SecurityQuestionMapping.model");
const PermissionUsersMapping_model_1 = require("./database/models/permissions/PermissionUsersMapping.model");
const TwoFactorConfiguration_model_1 = require("./database/models/security/TwoFactorConfiguration.model");
const PermissionGroupUserMapping_1 = require("./database/models/permissions/PermissionGroupUserMapping");
const PermissionGroupMapping_model_1 = require("./database/models/permissions/PermissionGroupMapping.model");
const PermissionGroupInheritance_model_1 = require("./database/models/permissions/PermissionGroupInheritance.model");
const PermissionGroup_model_1 = require("./database/models/permissions/PermissionGroup.model");
const Permission_model_1 = require("./database/models/permissions/Permission.model");
const LogAuth_model_1 = require("./database/models/logs/LogAuth.model");
const Lockout_model_1 = require("./database/models/lockouts/Lockout.model");
const IPLockouts_model_1 = require("./database/models/lockouts/IPLockouts.model");
const Login_model_1 = require("./database/models/logs/Login.model");
const sequelize_typescript_1 = require("sequelize-typescript");
const Configuration = __importStar(require("../config/config"));
const path = __importStar(require("path"));
const _ = __importStar(require("logger"));
const CASEmailProvider_1 = require("./mail/CASEmailProvider");
const CASComposite_1 = require("./integration/CASComposite");
const SequelizeLoggingDAO_1 = require("./database/dao/impl/SequelizeLoggingDAO");
const SequelizeSecurityQuestionDAO_1 = require("./database/dao/impl/SequelizeSecurityQuestionDAO");
const SequelizeTokenDAO_1 = require("./database/dao/impl/SequelizeTokenDAO");
const SequelizeTwoFactorDAO_1 = require("./database/dao/impl/SequelizeTwoFactorDAO");
const SequelizeUserDAO_1 = require("./database/dao/impl/SequelizeUserDAO");
const AuthenticationProcessImpl_1 = require("./actions/process/AuthenticationProcessImpl");
const PasswordProcessImpl_1 = require("./actions/process/PasswordProcessImpl");
const RegistrationToken_model_1 = require("./database/models/security/RegistrationToken.model");
const SelfGrantAuthorization_model_1 = require("./database/models/permissions/SelfGrantAuthorization.model");
const WebRegister_1 = require("./web/WebRegister");
const liquidjs_1 = require("liquidjs");
const CASEndpointsRegister_1 = require("./integration/CASEndpointsRegister");
const PermissionProcessesImpl_1 = require("./actions/process/PermissionProcessesImpl");
const UserProcessesImpl_1 = require("./actions/process/UserProcessesImpl");
const SequelizePermissionDAO_1 = require("./database/dao/impl/SequelizePermissionDAO");
const in_memory_daos_1 = require("./test/in-memory-daos");
const umzug = require("umzug");
function loadConfiguration() {
    Configuration.loadEasy('cas/default');
}
function performMigrations(sequelize) {
    const um = new umzug({
        storageOptions: {
            sequelize,
        },
        storage: 'sequelize',
        upName: 'up',
        downName: 'down',
        migrations: {
            path: path.join(__dirname, '..', '..', 'cas-data', 'migrations'),
            pattern: /^\d+[\w-]+\.migration\.js$/,
            params: [
                sequelize.getQueryInterface(),
            ],
        },
        logging() {
            // @ts-ignore
            _.debug.apply(null, arguments);
        },
    });
    um.on('migrated', name => _.debug(`umzug: migrated ${name}`));
    um.on('migrating', name => _.debug(`umzug: migrating ${name}`));
    um.on('reverted', name => _.debug(`umzug: reverted ${name}`));
    um.on('reverting', name => _.debug(`umzug: reverting ${name}`));
    _.debug('umzug created and event listeners have been attached');
    return um.up();
}
function registerSequelize() {
    const sequelize = new sequelize_typescript_1.Sequelize(Object.assign({}, Configuration.get('cas.connection'), {
        logging: sql => _.debug(`Sequelize query: "${sql}"`),
    }));
    sequelize.addModels([User_model_1.User, ResetDataRequest_model_1.ResetDataRequest, TwoFactorConfiguration_model_1.TwoFactorConfiguration, SecurityQuestionMapping_model_1.SecurityQuestionMapping,
        SecurityQuestion_model_1.SecurityQuestion, PermissionUsersMapping_model_1.PermissionUsersMapping, PermissionGroupUserMapping_1.PermissionGroupUserMapping, PermissionGroupMapping_model_1.PermissionGroupMapping,
        PermissionGroupInheritance_model_1.PermissionGroupInheritance, PermissionGroup_model_1.PermissionGroup, Permission_model_1.Permission, LogAuth_model_1.LogAuth, Lockout_model_1.Lockout, IPLockouts_model_1.IPLockouts, Login_model_1.Login,
        RegistrationToken_model_1.RegistrationToken, SelfGrantAuthorization_model_1.SelfGrantAuthorizationModel]);
    return sequelize;
}
class CASVii {
    constructor(mailer, composite) {
        this.mailer = mailer;
        this.composite = composite;
        this._renderer = new liquidjs_1.Liquid({
            root: path.join(__dirname, '..', '..', 'cas-data', 'templates'),
            extname: ".html",
        });
    }
    register(app, csrf) {
        app.register('(.*\\.)?xiomi.org', 2, (handler) => {
            handler.namespace('cas', (endpoints) => {
                WebRegister_1.WebRegister.register(this.composite, new CASEndpointsRegister_1.CASEndpointsRegister(endpoints, this.composite), this._renderer);
            });
        });
    }
    static async build() {
        loadConfiguration();
        const email = new CASEmailProvider_1.CASEmailProvider();
        const sequelize = registerSequelize();
        await performMigrations(sequelize);
        await sequelize.sync();
        // Build DAO Connections
        const loggingDAO = new SequelizeLoggingDAO_1.SequelizeLoggingDAO();
        const securityQuestionDAO = new SequelizeSecurityQuestionDAO_1.SequelizeSecurityQuestionDAO();
        const tokenDAO = new SequelizeTokenDAO_1.SequelizeTokenDAO();
        const twoFactorDAO = new SequelizeTwoFactorDAO_1.SequelizeTwoFactorDAO();
        const userDAO = new SequelizeUserDAO_1.SequelizeUserDAO();
        const permissionDAO = new SequelizePermissionDAO_1.SequelizePermissionDAO();
        const daos = {
            loggingDAO,
            securityQuestionDAO,
            tokenDAO,
            twoFactorDAO,
            userDAO,
            permissionDAO,
        };
        // Then processes...
        const authenticationProcesses = AuthenticationProcessImpl_1.AuthenticationProcessImpl.construct(email, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
        const passwordProcesses = PasswordProcessImpl_1.PasswordProcessImpl.construct(email, loggingDAO, userDAO, tokenDAO);
        const permissionProcesses = PermissionProcessesImpl_1.PermissionProcessImpl.construct(email, loggingDAO, userDAO, permissionDAO);
        const userProcesses = UserProcessesImpl_1.UserProcessesImpl.construct(loggingDAO, userDAO, tokenDAO, securityQuestionDAO, permissionDAO, email);
        // Then the composites..
        const composites = new CASComposite_1.CASComposite(authenticationProcesses, passwordProcesses, permissionProcesses, 
        // @ts-ignore
        null, userProcesses, daos);
        // And finally done
        return new CASVii(email, composites);
    }
    static async buildFake() {
        loadConfiguration();
        // @ts-ignore
        const email = new CASEmailProvider_1.CASEmailProvider({
            sendMail: (options) => {
                _.warn('Tried to send email but are running in a fake cas session');
                console.log(options);
                return Promise.resolve();
            },
        });
        // Build DAO Connections
        const loggingDAO = new in_memory_daos_1.InMemoryLoggingDAO({ login: [], auth: [] });
        const securityQuestionDAO = new in_memory_daos_1.InMemorySecurityQuestionDAO({ mapping: [], questions: [] });
        const tokenDAO = new in_memory_daos_1.InMemoryTokenDAO({ registration: [], reset: [] });
        const twoFactorDAO = new in_memory_daos_1.InMemoryTwoFactorDAO([]);
        const userDAO = new in_memory_daos_1.InMemoryUserDAO([]);
        const permissionDAO = new in_memory_daos_1.InMemoryPermissionDAO({
            groups: [],
            groupMapping: [],
            grants: [],
            inheritance: [],
            permissions: [],
            users: [],
        });
        const daos = {
            loggingDAO,
            securityQuestionDAO,
            tokenDAO,
            twoFactorDAO,
            userDAO,
            permissionDAO,
        };
        // Then processes...
        const authenticationProcesses = {
            isAuthenticated: (session) => true,
            getAuthenticationStatus: (session) => AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE,
            getUserID: (session) => 0,
            // @ts-ignore
            getUser: (session) => Promise.resolve({
                registered: true,
                id: 0,
                lockedStatus: 'accessible',
                email: 'fake@example.org',
                lastRotation: new Date(),
                hash: 'fake hash',
                username: 'fake',
                pinHash: 'fake hash',
                hashVersion: -1,
                full_name: 'fake name',
                pinHashVersion: -1,
                rotationRequired: false,
            }),
        };
        const passwordProcesses = PasswordProcessImpl_1.PasswordProcessImpl.construct(email, loggingDAO, userDAO, tokenDAO);
        const permissionProcesses = {
            sessionHasPermissions: (session, permissions) => Promise.resolve(true),
            sessionHasPermission: (session, permission) => Promise.resolve(true),
            resolveAllUserPermissions: (session, user) => Promise.resolve(true),
            userHasPermission: (user, permission) => Promise.resolve(true),
        };
        const userProcesses = UserProcessesImpl_1.UserProcessesImpl.construct(loggingDAO, userDAO, tokenDAO, securityQuestionDAO, permissionDAO, email);
        // Then the composites..
        const composites = new CASComposite_1.CASComposite(authenticationProcesses, passwordProcesses, permissionProcesses, 
        // @ts-ignore
        null, userProcesses, daos);
        // And finally done
        return new CASVii(email, composites);
    }
}
exports.CASVii = CASVii;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY2FzdmlpL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw2REFBb0Q7QUFDcEQscUZBQTRFO0FBQzVFLDhGQUFxRjtBQUNyRiw0R0FBbUc7QUFDbkcsNkdBQW9HO0FBQ3BHLDBHQUFpRztBQUNqRyx5R0FBc0c7QUFDdEcsNkdBQW9HO0FBQ3BHLHFIQUE0RztBQUM1RywrRkFBc0Y7QUFDdEYscUZBQTRFO0FBQzVFLHdFQUErRDtBQUMvRCw0RUFBbUU7QUFDbkUsa0ZBQXlFO0FBQ3pFLG9FQUEyRDtBQUMzRCwrREFBaUQ7QUFDakQsZ0VBQWtEO0FBQ2xELDJDQUE2QjtBQUM3QiwwQ0FBNEI7QUFDNUIsOERBQTJEO0FBQzNELDZEQUEwRDtBQUMxRCxpRkFBOEU7QUFDOUUsbUdBQWdHO0FBQ2hHLDZFQUEwRTtBQUMxRSxxRkFBa0Y7QUFDbEYsMkVBQXdFO0FBR3hFLDJGQUdxRDtBQUNyRCwrRUFBNEU7QUFDNUUsZ0dBQXVGO0FBQ3ZGLDZHQUF5RztBQUN6RyxtREFBZ0Q7QUFDaEQsdUNBQWtDO0FBQ2xDLDZFQUEwRTtBQUMxRSx1RkFBa0Y7QUFDbEYsMkVBQXdFO0FBQ3hFLHVGQUFvRjtBQU9wRiwwREFPK0I7QUFJL0IsK0JBQWdDO0FBRWhDLFNBQVMsaUJBQWlCO0lBQ3RCLGFBQWEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDMUMsQ0FBQztBQUVELFNBQVMsaUJBQWlCLENBQUMsU0FBb0I7SUFDM0MsTUFBTSxFQUFFLEdBQWdCLElBQUksS0FBSyxDQUFDO1FBQzlCLGNBQWMsRUFBRTtZQUNaLFNBQVM7U0FDWjtRQUNELE9BQU8sRUFBRSxXQUFXO1FBQ3BCLE1BQU0sRUFBRSxJQUFJO1FBQ1osUUFBUSxFQUFFLE1BQU07UUFDaEIsVUFBVSxFQUFFO1lBQ1IsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksQ0FBQztZQUNoRSxPQUFPLEVBQUUsNEJBQTRCO1lBQ3JDLE1BQU0sRUFBRTtnQkFDSixTQUFTLENBQUMsaUJBQWlCLEVBQUU7YUFDaEM7U0FDSjtRQUNELE9BQU87WUFDSCxhQUFhO1lBQ2IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLENBQUM7S0FDSixDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5RCxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNoRSxFQUFFLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM5RCxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUVoRSxDQUFDLENBQUMsS0FBSyxDQUFDLHNEQUFzRCxDQUFDLENBQUM7SUFFaEUsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7QUFDbkIsQ0FBQztBQUVELFNBQVMsaUJBQWlCO0lBQ3RCLE1BQU0sU0FBUyxHQUFHLElBQUksZ0NBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxhQUFhLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7UUFDbkYsT0FBTyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUM7S0FDdkQsQ0FBQyxDQUFDLENBQUM7SUFDSixTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsaUJBQUksRUFBRSx5Q0FBZ0IsRUFBRSxxREFBc0IsRUFBRSx1REFBdUI7UUFDeEYseUNBQWdCLEVBQUUscURBQXNCLEVBQUUsdURBQTBCLEVBQUUscURBQXNCO1FBQzVGLDZEQUEwQixFQUFFLHVDQUFlLEVBQUUsNkJBQVUsRUFBRSx1QkFBTyxFQUFFLHVCQUFPLEVBQUUsNkJBQVUsRUFBRSxtQkFBSztRQUM1RiwyQ0FBaUIsRUFBRSwwREFBMkIsQ0FBQyxDQUFDLENBQUM7SUFDckQsT0FBTyxTQUFTLENBQUM7QUFDckIsQ0FBQztBQVdELE1BQWEsTUFBTTtJQUlmLFlBQTRCLE1BQXdCLEVBQ3pCLFNBQXVCO1FBRHRCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3pCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFDOUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGlCQUFNLENBQUM7WUFDeEIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQztZQUMvRCxPQUFPLEVBQUUsT0FBTztTQUNuQixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sUUFBUSxDQUFDLEdBQXNCLEVBQUUsSUFBb0I7UUFDeEQsR0FBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUM3QyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFO2dCQUNuQyx5QkFBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksMkNBQW9CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUcsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUs7UUFDZCxpQkFBaUIsRUFBRSxDQUFDO1FBRXBCLE1BQU0sS0FBSyxHQUFHLElBQUksbUNBQWdCLEVBQUUsQ0FBQztRQUNyQyxNQUFNLFNBQVMsR0FBRyxpQkFBaUIsRUFBRSxDQUFDO1FBRXRDLE1BQU0saUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbkMsTUFBTSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFdkIsd0JBQXdCO1FBQ3hCLE1BQU0sVUFBVSxHQUFHLElBQUkseUNBQW1CLEVBQUUsQ0FBQztRQUM3QyxNQUFNLG1CQUFtQixHQUFHLElBQUksMkRBQTRCLEVBQUUsQ0FBQztRQUMvRCxNQUFNLFFBQVEsR0FBRyxJQUFJLHFDQUFpQixFQUFFLENBQUM7UUFDekMsTUFBTSxZQUFZLEdBQUcsSUFBSSw2Q0FBcUIsRUFBRSxDQUFDO1FBQ2pELE1BQU0sT0FBTyxHQUFHLElBQUksbUNBQWdCLEVBQUUsQ0FBQztRQUN2QyxNQUFNLGFBQWEsR0FBRyxJQUFJLCtDQUFzQixFQUFFLENBQUM7UUFFbkQsTUFBTSxJQUFJLEdBQWtCO1lBQ3hCLFVBQVU7WUFDVixtQkFBbUI7WUFDbkIsUUFBUTtZQUNSLFlBQVk7WUFDWixPQUFPO1lBQ1AsYUFBYTtTQUNoQixDQUFDO1FBRUYsb0JBQW9CO1FBQ3BCLE1BQU0sdUJBQXVCLEdBQUcscURBQXlCLENBQUMsU0FBUyxDQUMvRCxLQUFLLEVBQ0wsWUFBWSxFQUNaLG1CQUFtQixFQUNuQixPQUFPLEVBQ1AsVUFBVSxDQUNiLENBQUM7UUFDRixNQUFNLGlCQUFpQixHQUFHLHlDQUFtQixDQUFDLFNBQVMsQ0FDbkQsS0FBSyxFQUNMLFVBQVUsRUFDVixPQUFPLEVBQ1AsUUFBUSxDQUNYLENBQUM7UUFDRixNQUFNLG1CQUFtQixHQUFHLCtDQUFxQixDQUFDLFNBQVMsQ0FDdkQsS0FBSyxFQUNMLFVBQVUsRUFDVixPQUFPLEVBQ1AsYUFBYSxDQUNoQixDQUFDO1FBQ0YsTUFBTSxhQUFhLEdBQUcscUNBQWlCLENBQUMsU0FBUyxDQUM3QyxVQUFVLEVBQ1YsT0FBTyxFQUNQLFFBQVEsRUFDUixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLEtBQUssQ0FDUixDQUFDO1FBRUYsd0JBQXdCO1FBQ3hCLE1BQU0sVUFBVSxHQUFHLElBQUksMkJBQVksQ0FDL0IsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLElBQUksRUFDSixhQUFhLEVBQ2IsSUFBSSxDQUNQLENBQUM7UUFFRixtQkFBbUI7UUFDbkIsT0FBTyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBUztRQUNsQixpQkFBaUIsRUFBRSxDQUFDO1FBRXBCLGFBQWE7UUFDYixNQUFNLEtBQUssR0FBRyxJQUFJLG1DQUFnQixDQUFDO1lBQy9CLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNsQixDQUFDLENBQUMsSUFBSSxDQUFDLDJEQUEyRCxDQUFDLENBQUM7Z0JBQ3BFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JCLE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzdCLENBQUM7U0FDSixDQUFDLENBQUM7UUFFSCx3QkFBd0I7UUFDeEIsTUFBTSxVQUFVLEdBQUcsSUFBSSxtQ0FBa0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbkUsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLDRDQUEyQixDQUFDLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUM1RixNQUFNLFFBQVEsR0FBRyxJQUFJLGlDQUFnQixDQUFDLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN2RSxNQUFNLFlBQVksR0FBRyxJQUFJLHFDQUFvQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELE1BQU0sT0FBTyxHQUFHLElBQUksZ0NBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4QyxNQUFNLGFBQWEsR0FBRyxJQUFJLHNDQUFxQixDQUFDO1lBQzVDLE1BQU0sRUFBRSxFQUFFO1lBQ1YsWUFBWSxFQUFFLEVBQUU7WUFDaEIsTUFBTSxFQUFFLEVBQUU7WUFDVixXQUFXLEVBQUUsRUFBRTtZQUNmLFdBQVcsRUFBRSxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUU7U0FDWixDQUFDLENBQUM7UUFFSCxNQUFNLElBQUksR0FBa0I7WUFDeEIsVUFBVTtZQUNWLG1CQUFtQjtZQUNuQixRQUFRO1lBQ1IsWUFBWTtZQUNaLE9BQU87WUFDUCxhQUFhO1NBQ2hCLENBQUM7UUFFRixvQkFBb0I7UUFDcEIsTUFBTSx1QkFBdUIsR0FBRztZQUM1QixlQUFlLEVBQUUsQ0FBQyxPQUFnQixFQUFFLEVBQUUsQ0FBQyxJQUFJO1lBQzNDLHVCQUF1QixFQUFFLENBQUMsT0FBZ0IsRUFBRSxFQUFFLENBQUMsMkRBQStCLENBQUMsTUFBTTtZQUNyRixTQUFTLEVBQUUsQ0FBQyxPQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2xDLGFBQWE7WUFDYixPQUFPLEVBQUUsQ0FBQyxPQUFnQixFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO2dCQUMzQyxVQUFVLEVBQUUsSUFBSTtnQkFDaEIsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLEtBQUssRUFBRSxrQkFBa0I7Z0JBQ3pCLFlBQVksRUFBRSxJQUFJLElBQUksRUFBRTtnQkFDeEIsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixPQUFPLEVBQUUsV0FBVztnQkFDcEIsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFDZixTQUFTLEVBQUUsV0FBVztnQkFDdEIsY0FBYyxFQUFFLENBQUMsQ0FBQztnQkFDbEIsZ0JBQWdCLEVBQUUsS0FBSzthQUMxQixDQUFDO1NBQ3VCLENBQUM7UUFDOUIsTUFBTSxpQkFBaUIsR0FBRyx5Q0FBbUIsQ0FBQyxTQUFTLENBQ25ELEtBQUssRUFDTCxVQUFVLEVBQ1YsT0FBTyxFQUNQLFFBQVEsQ0FDWCxDQUFDO1FBQ0YsTUFBTSxtQkFBbUIsR0FBRztZQUN4QixxQkFBcUIsRUFBRSxDQUFDLE9BQWdCLEVBQUUsV0FBcUIsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDekYsb0JBQW9CLEVBQUUsQ0FBQyxPQUFnQixFQUFFLFVBQWtCLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3JGLHlCQUF5QixFQUFFLENBQUMsT0FBZ0IsRUFBRSxJQUFVLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2xGLGlCQUFpQixFQUFFLENBQUMsSUFBVSxFQUFFLFVBQWtCLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQ3ZELENBQUM7UUFDMUIsTUFBTSxhQUFhLEdBQUcscUNBQWlCLENBQUMsU0FBUyxDQUM3QyxVQUFVLEVBQ1YsT0FBTyxFQUNQLFFBQVEsRUFDUixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLEtBQUssQ0FDUixDQUFDO1FBRUYsd0JBQXdCO1FBQ3hCLE1BQU0sVUFBVSxHQUFHLElBQUksMkJBQVksQ0FDL0IsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLElBQUksRUFDSixhQUFhLEVBQ2IsSUFBSSxDQUNQLENBQUM7UUFFRixtQkFBbUI7UUFDbkIsT0FBTyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekMsQ0FBQztDQUVKO0FBdkxELHdCQXVMQyJ9