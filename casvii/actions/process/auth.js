"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AuthenticationTools_1 = require("../database/AuthenticationTools");
const CASResponse_1 = require("../../utilities/CASResponse");
const constants_1 = require("../../../cas/utils/constants");
const ProcessImpl_1 = require("../ProcessImpl");
/**
 * The authentication status of a user
 */
var ProvisionalAuthenticationStatus;
(function (ProvisionalAuthenticationStatus) {
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["ACTIVE"] = 0] = "ACTIVE";
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["REQUIRES_TWO_FACTOR"] = 1] = "REQUIRES_TWO_FACTOR";
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["REQUIRES_SECURITY_QUESTION"] = 2] = "REQUIRES_SECURITY_QUESTION";
})(ProvisionalAuthenticationStatus = exports.ProvisionalAuthenticationStatus || (exports.ProvisionalAuthenticationStatus = {}));
class AuthenticationProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO) {
        super(loggingDAO);
        this.mailer = mailer;
        this.userDAO = userDAO;
        this.tools = new AuthenticationTools_1.AuthenticationTools(twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    }
    async getUser(session) {
        return session.has(`login.userID`) ? await this.userDAO.findUserByID(session.get(`login.userID`)) : null;
    }
    getUserID(session) {
        return session.has('login.userID') ? session.get('login.userID') : undefined;
    }
    getAuthenticationStatus(session) {
        if (!session.has('login.provisional'))
            return undefined;
        if (session.get('login.provisional') === ProvisionalAuthenticationStatus.ACTIVE)
            return ProvisionalAuthenticationStatus.ACTIVE;
        if (session.get('login.provisional') === ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR)
            return ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR;
        if (session.get('login.provisional') === ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION)
            return ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION;
        return undefined;
    }
    isAuthenticated(session) {
        return session.has('login.provisional') && session.get('login.provisional') === ProvisionalAuthenticationStatus.ACTIVE;
    }
    async logout(session) {
        let id = undefined;
        if (this.isAuthenticated(session)) {
            id = session.get('login.userID');
            session.clear('login.provisional');
            session.clear('login.userID');
            session.clear('login.name');
        }
        return super.passthroughLog(200, 'User successfully logged out', constants_1.PUBLIC_RESPONSES.SUCCESSFUL(''), { userID: id });
    }
    async twoFactorSubmission(data, session) {
        // Check if this session has a user entry and is still provisional
        if (!(session.has('login.provisional') && session.has('login.userID') && session.get('login.provisional') === ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR)) {
            return super.passthroughLog(400, 'User was not defined or was already authenticated', constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // Check that the user provided the code for this request
        if (!data.hasOwnProperty(`code`)) {
            return super.passthroughLog(400, `The user did not provide a code in the request`, constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // Lookup the user entry
        const user = await this.getUser(session);
        if (user === null) {
            return super.passthroughLog(500, `The user has an id in their session but could not be resolved to an actual user`, `Something went wrong with your user account`, { userID: session.get(`login.userID`) });
        }
        const action = async () => {
            await this.tools.twoFactorAuthenticate(user, data['code']);
            session.set('login.provisional', ProvisionalAuthenticationStatus.ACTIVE);
        };
        const handlers = {
            'not-found': (e) => {
                return super.passthroughLog(400, `Could not find a two factor configuration for the given user ID`, constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
            },
            // tslint:disable-next-line:object-literal-key-quotes
            'invalid': (e) => {
                return super.passthroughLog(400, 'User provided a two factor authentication code which is not valid', constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
            },
        };
        return super.runTool(action, `authenticate with two factor authentication (twoFactorAuthenticate)`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL('You are now logged in!'), { user }, handlers);
    }
    async login(data, ip, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['username', 'password'], ['sc-code', 'sc-answer']);
        if (verify)
            return verify;
        let user;
        try {
            user = await this.userDAO.findUserByUsername(data['username']);
        }
        catch (e) {
            return super.passthroughLog(500, `We found the user once but then not again, a concurrency issue in the database, who knows but it's fucked it`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { e });
        }
        const action = async () => {
            await this.tools.authenticateUser(data['username'], data['password'], ip);
            // Then actually do the successful stuff
            session.set('login.userID', user.id);
            session.set('login.name', user.full_name);
            session.set('login.provisional', ProvisionalAuthenticationStatus.ACTIVE);
        };
        const handlers = {
            'not-found': (e) => {
                return super.passthroughLog(400, `The user could not be found, they probably fucked up their username`, constants_1.PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD, { e, username: data['username'] });
            },
            'registered': (e) => {
                return super.passthroughLog(400, 'The user hasn\'t completed the registration and tried to log in! What a ponce', constants_1.PUBLIC_RESPONSES.REGISTRATION_DISABLED, { e });
            },
            'locked': (e) => {
                return super.passthroughLog(400, 'The user has their account locked, wonder what they did wrong...', constants_1.PUBLIC_RESPONSES.LOCK_STATUS, { e });
            },
            'password': (e) => {
                return super.passthroughLog(400, 'The user provided a wrong password! They fucked it up!', constants_1.PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD, { e });
            },
            'no-logins': (e) => {
                return super.passthroughLog(500, 'Something went wrong with finding their previous log ins! Something has done very wrong', constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { e });
            },
            'required-security-question': (e) => {
                // So set a provisional session so the user can access their own security questions
                session.set('login.provisional', ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION);
                session.set('login.userID', user.id);
                session.set('login.name', user.full_name);
                return super.passthroughLog(400, 'Requires a security question to be submitted', constants_1.PUBLIC_RESPONSES.INVALID_REQUEST, { e });
            },
            '2fa': (e) => {
                return super.passthroughLog(302, 'Login was successful but required a two factor confirmation', constants_1.PUBLIC_RESPONSES.SUCCESSFUL(''), { e, redirect: '/tfa/authenticate' });
            },
        };
        return super.runTool(action, `logging in with no other validation (login)`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(`You are now logged in`), { user }, handlers);
    }
    async verifyLoginWithTwoFactor(data, session) {
        return new CASResponse_1.CASResponse(0, '', '');
    }
    async verifyLoginWithSecurityQuestion(data, session) {
        return new CASResponse_1.CASResponse(0, '', '');
    }
}
var AuthenticationProcessImpl;
(function (AuthenticationProcessImpl) {
    /**
     * Constructs an {@link AuthProcesses} instance providing utility functions for authentication processes.
     * @param mailer the email connection to use for client access
     * @param twoFactorDAO DAO for accessing and manipulating two factor authentication records
     * @param securityQuestionDAO DAO for accessing and manipulating security question records
     * @param userDAO DAO for accessing and manipulating user records
     * @param loggingDAO DAO for accessing and manipulating logging records
     */
    AuthenticationProcessImpl.construct = (mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO) => {
        return new AuthenticationProcesses(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    };
})(AuthenticationProcessImpl = exports.AuthenticationProcessImpl || (exports.AuthenticationProcessImpl = {}));
