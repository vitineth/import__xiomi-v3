"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionProcessImpl = void 0;
const ProcessImpl_1 = require("../ProcessImpl");
const PermissionTools_1 = require("../database/PermissionTools");
const AuthenticationProcessImpl_1 = require("./AuthenticationProcessImpl");
const Constants_1 = require("../../utilities/Constants");
const _ = __importStar(require("logger"));
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
class PermissionProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(mailer, loggingDAO, userDAO, permissionDAO) {
        super(loggingDAO);
        this.mailer = mailer;
        this.userDAO = userDAO;
        this.tooling = new PermissionTools_1.PermissionTools(mailer, userDAO, permissionDAO);
    }
    async grantGroupToUser(data, session) {
        const verify = await super.verifyDataElements(data, ['group', 'user']);
        if (verify)
            return verify;
        // Check they are logged in!
        if (!session.has('login.userID') || !session.has('login.provisional') || session.get(`login.provisional`) !== AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED, `User was not logged in`, PUBLIC_RESPONSES.UNAUTHORIZED);
        }
        // Then check if the user has the group
        const action = async () => {
            await this.tooling.grantGroupToUser(data['user'], data['group'], session);
        };
        const handlers = {
            'no-permission': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.FORBIDDEN, `User did not have permission to change the groups`, PUBLIC_RESPONSES.MISSING_PERMISSIONS),
            'already-granted': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User already has that permission group`, PUBLIC_RESPONSES.ALREADY_GRANTED),
        };
        return super.runTool(action, `grant group to user (grantGroupToUser)`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.GRANTED), { data, session }, handlers);
    }
    async resolveAllUserPermissions(session, user) {
        try {
            const permissions = await this.tooling.resolveAllPermissions(user);
            session.set(`user.permissions`, permissions);
            session.set(`user.permission-verifications`, 0);
            return true;
        }
        catch (e) {
            return false;
        }
    }
    sessionHasPermission(session, permission) {
        return this.tooling.checkPermission(session, permission);
    }
    sessionHasPermissions(session, permissions) {
        return this.tooling.checkPermissions(session, permissions);
    }
    async ungrantGroupFromUser(data, session) {
        const verify = await super.verifyDataElements(data, ['group', 'user']);
        if (verify)
            return verify;
        // Check they are logged in!
        if (!session.has('login.userID') || !session.has('login.provisional') || session.get(`login.provisional`) !== AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED, `User was not logged in`, PUBLIC_RESPONSES.UNAUTHORIZED);
        }
        // Then check if the user has the group
        const action = async () => {
            await this.tooling.ungrantGroupToUser(data['user'], data['group'], session);
        };
        const handlers = {
            'no-permission': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.FORBIDDEN, `User did not have permission to change the groups`, PUBLIC_RESPONSES.MISSING_PERMISSIONS),
            'not-granted': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User does not have the given group`, PUBLIC_RESPONSES.NOT_GRANTED),
        };
        return super.runTool(action, `ungrant group from user (ungrantGroupFromUser)`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.UNGRANTED), { data, session }, handlers);
    }
    async userHasPermission(user, permission) {
        try {
            const permissions = await this.tooling.resolveAllPermissions(user);
            return permissions.find(e => e.name === permission) !== undefined;
        }
        catch (e) {
            _.error(`failed to load permissions for this user`, { e, user });
            return false;
        }
    }
    async activateSelfPermissionGrant(data, session) {
        const verify = await super.verifyDataElements(data, ['authorizationID']);
        if (verify)
            return verify;
        // Check they are logged in!
        if (!session.has('login.userID') || !session.has('login.provisional') || session.get(`login.provisional`) !== AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED, `User was not logged in`, PUBLIC_RESPONSES.UNAUTHORIZED);
        }
        // Then check if the user has the group
        const action = async () => {
            await this.tooling.activateSelfPermissionGrant(data['authorizationID'], session);
        };
        const handlers = {
            'no-permission': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.FORBIDDEN, `User did not have permission to change the groups`, PUBLIC_RESPONSES.MISSING_PERMISSIONS),
            'already-granted': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User already has that permission group`, PUBLIC_RESPONSES.ALREADY_USED_GRANT),
            'invalid-grant': (e) => super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User provided an invalid authorization code`, PUBLIC_RESPONSES.INVALID_AUTHORIZATION),
        };
        return super.runTool(action, `self grant to user (activateSelfPermissionGrant)`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN_WITH_CASVII), { data, session }, handlers);
    }
    async getPermissionsFromSelfGrantID(id) {
        return this.tooling.getPermissionsFromSelfGrantID(id);
    }
}
var PermissionProcessImpl;
(function (PermissionProcessImpl) {
    PermissionProcessImpl.construct = (mailer, loggingDAO, userDAO, permissionDAO) => {
        return new PermissionProcesses(mailer, loggingDAO, userDAO, permissionDAO);
    };
})(PermissionProcessImpl = exports.PermissionProcessImpl || (exports.PermissionProcessImpl = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvblByb2Nlc3Nlc0ltcGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2FjdGlvbnMvcHJvY2Vzcy9QZXJtaXNzaW9uUHJvY2Vzc2VzSW1wbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsZ0RBQTZEO0FBVTdELGlFQUE4RDtBQUM5RCwyRUFBOEU7QUFDOUUseURBQXNEO0FBRXRELDBDQUE0QjtBQUU1QixJQUFPLGdCQUFnQixHQUFHLHFCQUFTLENBQUMsZ0JBQWdCLENBQUM7QUFDckQsSUFBTyxvQkFBb0IsR0FBRyxxQkFBUyxDQUFDLG9CQUFvQixDQUFDO0FBRTdELE1BQU0sbUJBQW9CLFNBQVEseUJBQVc7SUFLekMsWUFBb0IsTUFBd0IsRUFBRSxVQUFzQixFQUFFLE9BQWdCLEVBQUUsYUFBNEI7UUFDaEgsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBREYsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFFeEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlDQUFlLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsS0FBSyxDQUFDLGdCQUFnQixDQUFDLElBQWtDLEVBQUUsT0FBZ0I7UUFDdkUsTUFBTSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxNQUFNO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFFMUIsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsS0FBSywyREFBK0IsQ0FBQyxNQUFNLEVBQUU7WUFDbEosT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFlBQVksRUFDM0Isd0JBQXdCLEVBQ3hCLGdCQUFnQixDQUFDLFlBQVksQ0FDaEMsQ0FBQztTQUNMO1FBRUQsdUNBQXVDO1FBQ3ZDLE1BQU0sTUFBTSxHQUFHLEtBQUssSUFBSSxFQUFFO1lBQ3RCLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQztRQUVGLE1BQU0sUUFBUSxHQUFHO1lBQ2IsZUFBZSxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUNuRCw0QkFBYyxDQUFDLFNBQVMsRUFDeEIsbURBQW1ELEVBQ25ELGdCQUFnQixDQUFDLG1CQUFtQixDQUN2QztZQUNELGlCQUFpQixFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUNyRCw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsd0NBQXdDLEVBQ3hDLGdCQUFnQixDQUFDLGVBQWUsQ0FDbkM7U0FDSixDQUFDO1FBRUYsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUNoQixNQUFNLEVBQ04sd0NBQXdDLEVBQ3hDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsRUFDekQsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQ2pCLFFBQVEsQ0FDWCxDQUFDO0lBQ04sQ0FBQztJQUVELEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxPQUFnQixFQUFFLElBQVU7UUFDeEQsSUFBSTtZQUNBLE1BQU0sV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVuRSxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFaEQsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsT0FBZ0IsRUFBRSxVQUFrQjtRQUNyRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBZ0IsRUFBRSxXQUFxQjtRQUN6RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCxLQUFLLENBQUMsb0JBQW9CLENBQUMsSUFBa0MsRUFBRSxPQUFnQjtRQUMzRSxNQUFNLE1BQU0sR0FBRyxNQUFNLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLE1BQU07WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUUxQiw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLDJEQUErQixDQUFDLE1BQU0sRUFBRTtZQUNsSixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsWUFBWSxFQUMzQix3QkFBd0IsRUFDeEIsZ0JBQWdCLENBQUMsWUFBWSxDQUNoQyxDQUFDO1NBQ0w7UUFFRCx1Q0FBdUM7UUFDdkMsTUFBTSxNQUFNLEdBQUcsS0FBSyxJQUFJLEVBQUU7WUFDdEIsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFDO1FBRUYsTUFBTSxRQUFRLEdBQUc7WUFDYixlQUFlLEVBQUUsQ0FBQyxDQUFZLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQ25ELDRCQUFjLENBQUMsU0FBUyxFQUN4QixtREFBbUQsRUFDbkQsZ0JBQWdCLENBQUMsbUJBQW1CLENBQ3ZDO1lBQ0QsYUFBYSxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUNqRCw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsb0NBQW9DLEVBQ3BDLGdCQUFnQixDQUFDLFdBQVcsQ0FDL0I7U0FDSixDQUFDO1FBRUYsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUNoQixNQUFNLEVBQ04sZ0RBQWdELEVBQ2hELGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsRUFDM0QsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQ2pCLFFBQVEsQ0FDWCxDQUFDO0lBQ04sQ0FBQztJQUVELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFVLEVBQUUsVUFBa0I7UUFDbEQsSUFBSTtZQUNBLE1BQU0sV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRSxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxLQUFLLFNBQVMsQ0FBQztTQUNyRTtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsQ0FBQyxDQUFDLEtBQUssQ0FBQywwQ0FBMEMsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ2pFLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQztJQUVELEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxJQUFrQyxFQUFFLE9BQWdCO1FBQ2xGLE1BQU0sTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLE1BQU07WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUUxQiw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLDJEQUErQixDQUFDLE1BQU0sRUFBRTtZQUNsSixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsWUFBWSxFQUMzQix3QkFBd0IsRUFDeEIsZ0JBQWdCLENBQUMsWUFBWSxDQUNoQyxDQUFDO1NBQ0w7UUFFRCx1Q0FBdUM7UUFDdkMsTUFBTSxNQUFNLEdBQUcsS0FBSyxJQUFJLEVBQUU7WUFDdEIsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3JGLENBQUMsQ0FBQztRQUVGLE1BQU0sUUFBUSxHQUFHO1lBQ2IsZUFBZSxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUNuRCw0QkFBYyxDQUFDLFNBQVMsRUFDeEIsbURBQW1ELEVBQ25ELGdCQUFnQixDQUFDLG1CQUFtQixDQUN2QztZQUNELGlCQUFpQixFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUNyRCw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsd0NBQXdDLEVBQ3hDLGdCQUFnQixDQUFDLGtCQUFrQixDQUN0QztZQUNELGVBQWUsRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FDbkQsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLDZDQUE2QyxFQUM3QyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FDekM7U0FDSixDQUFDO1FBRUYsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUNoQixNQUFNLEVBQ04sa0RBQWtELEVBQ2xELGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxFQUN2RSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFDakIsUUFBUSxDQUNYLENBQUM7SUFDTixDQUFDO0lBRUQsS0FBSyxDQUFDLDZCQUE2QixDQUFDLEVBQVU7UUFDMUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzFELENBQUM7Q0FFSjtBQUVELElBQWlCLHFCQUFxQixDQUtyQztBQUxELFdBQWlCLHFCQUFxQjtJQUVyQiwrQkFBUyxHQUFHLENBQUMsTUFBd0IsRUFBRSxVQUFzQixFQUFFLE9BQWdCLEVBQUUsYUFBNEIsRUFBd0IsRUFBRTtRQUNoSixPQUFPLElBQUksbUJBQW1CLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDL0UsQ0FBQyxDQUFDO0FBQ04sQ0FBQyxFQUxnQixxQkFBcUIsR0FBckIsNkJBQXFCLEtBQXJCLDZCQUFxQixRQUtyQyJ9