import { IUserProcesses } from "../interface/IUserProcesses";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
import { UserDAO } from "../../database/dao/UserDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { SecurityQuestionDAO } from "../../database/dao/SecurityQuestionDAO";
import { PermissionDAO } from "../../database/dao/PermissionDAO";
export declare namespace UserProcessesImpl {
    const construct: (loggingDAO: LoggingDAO, userDAO: UserDAO, tokenDAO: TokenDAO, securityQuestionDAO: SecurityQuestionDAO, permissionDAO: PermissionDAO, mailer: CASEmailProvider) => IUserProcesses;
}
