"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordProcessImpl = void 0;
const ProcessImpl_1 = require("../ProcessImpl");
const PasswordTools_1 = require("../database/PasswordTools");
const ToolError_1 = require("../../errors/ToolError");
const Constants_1 = require("../../utilities/Constants");
const AuthenticationProcessImpl_1 = require("./AuthenticationProcessImpl");
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
class PasswordProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(mailer, loggingDAO, userDAO, tokenDAO) {
        super(loggingDAO);
        this.tooling = new PasswordTools_1.PasswordTools(mailer, userDAO, tokenDAO);
    }
    /**
     * Performs the first step of an unauthenticated password change - the request phase. Verifies the provided username
     * and email and will then generate a range request and email the user with it. In the event that the username and
     * email are not the same, it will provide a positive response to the user but will not create a change request or
     * email the user. Only provides a cas response with public comments.
     * @param data the data request containing the username and email
     * @param session the session from the user who sent the request
     */
    async changePasswordUnauthenticatedRequest(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['username', 'email']);
        if (verify)
            return verify;
        if (session.has('login.userID')) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user was logged in but that is not allowed`, PUBLIC_RESPONSES.AUTHORIZED);
        }
        try {
            await this.tooling.unauthenticatedChangeRequest(data['username'], data['email']);
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The user successfully requested that their password be changed`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `The password tool threw an unexpected exception`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                case `not-found`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `Could not find the username they provided but we are still providing a positive response to the user`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST), { error: e });
                case `invalid-email`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The email they provided did not match the one associated with their username but we still provide a positive response in this case`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST), { error: e });
                case `failed-save-request`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Something went wrong when they tried to save the user request! Aaaahhh`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                case `failed-email`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to send them an email! The change request was submitted but we got some error from our system.`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                default:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `We got an unexpected error identifier from the password tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
        }
    }
    /**
     * Requests data containing an old password, two new passwords and a pin. A user should be stored in the session
     * using standard locations. This verifies the passwords, password complexity, verifies pin and then updates the
     * database.
     * On error it will return an {@link CASResponse} which will contain the public response and the HTTP status
     * code to use
     * @param data the data required to perform this operation (required: old, new1, new2, pin)
     * @param session the session from which to fetch the user
     * @return the response the server should provide or true if the password was updated successfully
     */
    async changePasswordAuthenticated(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['old', 'new1', 'new2', 'pin']);
        if (verify)
            return verify;
        // Test the user is fully logged in
        if (!session.has('login.provisional') || session.get('login.provisional') !== AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE) {
            return super.passthroughLog(302, `The users provisional status is not active, it is: ${session.get('login.provisional')}`, PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP, {
                'Location': '/casvii/login/flow/security',
            });
        }
        // Test if passwords match
        if (data['new1'] !== data['new2']) {
            return super.passthroughLog(400, 'Passwords provided by the user do not match', PUBLIC_RESPONSES.PASSWORD_MATCH);
        }
        // Swap to the new system!
        try {
            await this.tooling.changePassword(session.get('login.userID'), data['old'], data['new1'], data['pin']);
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The user successfully changed their password and provided additional authentication through thier pin code`, PUBLIC_RESPONSES.SUCCESSFUL(''));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to log in the user because the authentication tool threw an unexpected exception`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                default:
                    return super.passthroughLog(500, `We got an unexpected error identifier from the authenticator tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                case `too-weak`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided a password that was too weak! It had a score of ${e.data === undefined ? '[score was not provided]' : e.data['score']}`, PUBLIC_RESPONSES.WEAK_PASSWORD(e.data === undefined ? '' : e.data['feedback']), { error: e });
                case `wrong-old`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided an invalid old password`, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN, { error: e });
                case `invalid-pin`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided an invalid pin`, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN, { error: e });
                case `failed-save`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `The database save failed to happen for some reason. Start screaming`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                case `not-found`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not identify the user from their userID!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
        }
    }
    async changePasswordUnauthenticatedSubmit(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['ut', 'rt', 'pin', 'new1', 'new2']);
        if (verify)
            return verify;
        // Test if passwords match
        if (data['new1'] !== data['new2']) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `Passwords did not match`, PUBLIC_RESPONSES.PASSWORD_MATCH);
        }
        try {
            await this.tooling.unauthenticatedChangeSubmission(data['ut'], data['rt'], data['pin'], data['new1']);
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The user successfully changed their password as an unauthenticated user providing valid tokens and pins`, PUBLIC_RESPONSES.SUCCESSFUL(''));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to log in the user because the authentication tool threw an unexpected exception`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                case `no-request`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `No request could be found using the provided user and reset tokens`, PUBLIC_RESPONSES.INVALID_REQUEST, { error: e });
                case `already-used`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `Token has already been marked as used, repeat attack?`, PUBLIC_RESPONSES.EXPIRED_TOKEN, { error: e });
                case `expired`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The reset token is out of date`, PUBLIC_RESPONSES.EXPIRED_TOKEN, { error: e });
                case `too-weak`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided a password that was too weak! It had a score of ${e.data === undefined ? '[score was not provided]' : e.data['score']}`, PUBLIC_RESPONSES.WEAK_PASSWORD(e.data === undefined ? '' : e.data['feedback']), { error: e });
                case `invalid-pin`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The pin provided by the user did not match, could be a targeted attack?`, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN, { error: e });
                case `cannot-save`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not save the users request, nothing has happened!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                default:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `We got an unexpected error identifier from the authenticator tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
        }
    }
}
var PasswordProcessImpl;
(function (PasswordProcessImpl) {
    PasswordProcessImpl.construct = (mailer, loggingDAO, userDAO, tokenDAO) => {
        return new PasswordProcesses(mailer, loggingDAO, userDAO, tokenDAO);
    };
})(PasswordProcessImpl = exports.PasswordProcessImpl || (exports.PasswordProcessImpl = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGFzc3dvcmRQcm9jZXNzSW1wbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvYWN0aW9ucy9wcm9jZXNzL1Bhc3N3b3JkUHJvY2Vzc0ltcGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsZ0RBQTZEO0FBRTdELDZEQUEwRDtBQUcxRCxzREFBbUQ7QUFLbkQseURBQXNEO0FBQ3RELDJFQUE4RTtBQUM5RSxJQUFPLG9CQUFvQixHQUFHLHFCQUFTLENBQUMsb0JBQW9CLENBQUM7QUFDN0QsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBRXJELE1BQU0saUJBQWtCLFNBQVEseUJBQVc7SUFJdkMsWUFBWSxNQUF3QixFQUFFLFVBQXNCLEVBQUUsT0FBZ0IsRUFBRSxRQUFrQjtRQUM5RixLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLDZCQUFhLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNILEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxJQUEwQixFQUFFLE9BQWdCO1FBQ25GLG1IQUFtSDtRQUNuSCxNQUFNLE1BQU0sR0FBRyxNQUFNLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUMzRSxJQUFJLE1BQU07WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUUxQixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsZ0RBQWdELEVBQ2hELGdCQUFnQixDQUFDLFVBQVUsQ0FDOUIsQ0FBQztTQUNMO1FBRUQsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDakYsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLE9BQU8sRUFDdEIsZ0VBQWdFLEVBQ2hFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUMzRixDQUFDO1NBQ0w7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLGlDQUFpQztZQUVqQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVkscUJBQVMsQ0FBQyxFQUFFO2dCQUMzQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLGlEQUFpRCxFQUNqRCxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQzthQUNMO1lBRUQseUNBQXlDO1lBQ3pDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsRUFBRTtnQkFDbEIsS0FBSyxXQUFXO29CQUNaLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxPQUFPLEVBQ3RCLHNHQUFzRyxFQUN0RyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsc0NBQXNDLENBQUMsRUFDeEYsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLGVBQWU7b0JBQ2hCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxPQUFPLEVBQ3RCLG9JQUFvSSxFQUNwSSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsc0NBQXNDLENBQUMsRUFDeEYsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLHFCQUFxQjtvQkFDdEIsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLHFCQUFxQixFQUNwQyx3RUFBd0UsRUFDeEUsZ0JBQWdCLENBQUMscUJBQXFCLEVBQ3RDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7Z0JBQ04sS0FBSyxjQUFjO29CQUNmLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMsdUdBQXVHLEVBQ3ZHLGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2dCQUNOO29CQUNJLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMsOERBQThELEVBQzlELGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2FBQ1Q7U0FDSjtJQUNMLENBQUM7SUFFRDs7Ozs7Ozs7O09BU0c7SUFDSCxLQUFLLENBQUMsMkJBQTJCLENBQUMsSUFBMEIsRUFBRSxPQUFnQjtRQUMxRSxtSEFBbUg7UUFDbkgsTUFBTSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNwRixJQUFJLE1BQU07WUFBRSxPQUFPLE1BQU0sQ0FBQztRQUUxQixtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEtBQUssMkRBQStCLENBQUMsTUFBTSxFQUFFO1lBQ2xILE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILHNEQUFzRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsRUFDeEYsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQ3hDO2dCQUNJLFVBQVUsRUFBRSw2QkFBNkI7YUFDNUMsQ0FDSixDQUFDO1NBQ0w7UUFFRCwwQkFBMEI7UUFDMUIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQy9CLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILDZDQUE2QyxFQUM3QyxnQkFBZ0IsQ0FBQyxjQUFjLENBQ2xDLENBQUM7U0FDTDtRQUVELDBCQUEwQjtRQUUxQixJQUFJO1lBQ0EsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDdkcsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLE9BQU8sRUFDdEIsNEdBQTRHLEVBQzVHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FDbEMsQ0FBQztTQUNMO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixpQ0FBaUM7WUFFakMsSUFBSSxDQUFDLENBQUMsQ0FBQyxZQUFZLHFCQUFTLENBQUMsRUFBRTtnQkFDM0IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLHFCQUFxQixFQUNwQyx5RkFBeUYsRUFDekYsZ0JBQWdCLENBQUMscUJBQXFCLEVBQ3RDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7YUFDTDtZQUVELHlDQUF5QztZQUN6QyxRQUFRLENBQUMsQ0FBQyxVQUFVLEVBQUU7Z0JBQ2xCO29CQUNJLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILG1FQUFtRSxFQUNuRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLFVBQVU7b0JBQ1gsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIscUVBQXFFLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUMxSSxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUM5RSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2dCQUNOLEtBQUssV0FBVztvQkFDWixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQiwyQ0FBMkMsRUFDM0MsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQ3JDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7Z0JBQ04sS0FBSyxhQUFhO29CQUNkLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLGtDQUFrQyxFQUNsQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsRUFDckMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLGFBQWE7b0JBQ2QsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLHFCQUFxQixFQUNwQyxxRUFBcUUsRUFDckUsZ0JBQWdCLENBQUMscUJBQXFCLEVBQ3RDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7Z0JBQ04sS0FBSyxXQUFXO29CQUNaLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMsZ0RBQWdELEVBQ2hELGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2FBQ1Q7U0FDSjtJQUNMLENBQUM7SUFFRCxLQUFLLENBQUMsbUNBQW1DLENBQUMsSUFBMEIsRUFBRSxPQUFnQjtRQUNsRixtSEFBbUg7UUFDbkgsTUFBTSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDekYsSUFBSSxNQUFNO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFFMUIsMEJBQTBCO1FBQzFCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUMvQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQix5QkFBeUIsRUFDekIsZ0JBQWdCLENBQUMsY0FBYyxDQUNsQyxDQUFDO1NBQ0w7UUFFRCxJQUFJO1lBQ0EsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3RHLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxPQUFPLEVBQ3RCLHlHQUF5RyxFQUN6RyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQ2xDLENBQUM7U0FDTDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsaUNBQWlDO1lBRWpDLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxxQkFBUyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMseUZBQXlGLEVBQ3pGLGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2FBQ0w7WUFFRCx5Q0FBeUM7WUFDekMsUUFBUSxDQUFDLENBQUMsVUFBVSxFQUFFO2dCQUNsQixLQUFLLFlBQVk7b0JBQ2IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsb0VBQW9FLEVBQ3BFLGdCQUFnQixDQUFDLGVBQWUsRUFDaEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLGNBQWM7b0JBQ2YsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsdURBQXVELEVBQ3ZELGdCQUFnQixDQUFDLGFBQWEsRUFDOUIsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLFNBQVM7b0JBQ1YsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsZ0NBQWdDLEVBQ2hDLGdCQUFnQixDQUFDLGFBQWEsRUFDOUIsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLFVBQVU7b0JBQ1gsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIscUVBQXFFLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUMxSSxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUM5RSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2dCQUNOLEtBQUssYUFBYTtvQkFDZCxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQix5RUFBeUUsRUFDekUsZ0JBQWdCLENBQUMsb0JBQW9CLEVBQ3JDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7Z0JBQ04sS0FBSyxhQUFhO29CQUNkLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMseURBQXlELEVBQ3pELGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2dCQUNOO29CQUNJLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMsbUVBQW1FLEVBQ25FLGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO2FBQ1Q7U0FDSjtJQUNMLENBQUM7Q0FFSjtBQUVELElBQWlCLG1CQUFtQixDQUtuQztBQUxELFdBQWlCLG1CQUFtQjtJQUVuQiw2QkFBUyxHQUFHLENBQUMsTUFBd0IsRUFBRSxVQUFzQixFQUFFLE9BQWdCLEVBQUUsUUFBa0IsRUFBc0IsRUFBRTtRQUNwSSxPQUFPLElBQUksaUJBQWlCLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDeEUsQ0FBQyxDQUFDO0FBQ04sQ0FBQyxFQUxnQixtQkFBbUIsR0FBbkIsMkJBQW1CLEtBQW5CLDJCQUFtQixRQUtuQyJ9