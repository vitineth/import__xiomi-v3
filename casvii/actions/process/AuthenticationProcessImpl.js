"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationProcessImpl = exports.ProvisionalAuthenticationStatus = void 0;
const AuthenticationTools_1 = require("../database/AuthenticationTools");
const ProcessImpl_1 = require("../ProcessImpl");
const Constants_1 = require("../../utilities/Constants");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
/**
 * The authentication status of a user
 */
var ProvisionalAuthenticationStatus;
(function (ProvisionalAuthenticationStatus) {
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["ACTIVE"] = 0] = "ACTIVE";
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["REQUIRES_TWO_FACTOR"] = 1] = "REQUIRES_TWO_FACTOR";
    ProvisionalAuthenticationStatus[ProvisionalAuthenticationStatus["REQUIRES_SECURITY_QUESTION"] = 2] = "REQUIRES_SECURITY_QUESTION";
})(ProvisionalAuthenticationStatus = exports.ProvisionalAuthenticationStatus || (exports.ProvisionalAuthenticationStatus = {}));
class AuthenticationProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO) {
        super(loggingDAO);
        this.mailer = mailer;
        this.securityQuestionDAO = securityQuestionDAO;
        this.userDAO = userDAO;
        this.tools = new AuthenticationTools_1.AuthenticationTools(twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    }
    async getUser(session) {
        if (session === null || session === undefined)
            return null;
        if (!session.has(`login.userID`))
            return null;
        try {
            return await this.userDAO.findUserByID(session.get(`login.userID`));
        }
        catch (e) {
            return null;
        }
    }
    getUserID(session) {
        if (session === null || session === undefined)
            return null;
        return session.has('login.userID') ? session.get('login.userID') : null;
    }
    getAuthenticationStatus(session) {
        if (session === null || session === undefined)
            return null;
        if (!session.has('login.provisional'))
            return null;
        if (session.get('login.provisional') in ProvisionalAuthenticationStatus)
            return session.get('login.provisional');
        return null;
    }
    isAuthenticated(session) {
        if (session === null || session === undefined)
            return false;
        return session.has('login.provisional') && session.get('login.provisional') === ProvisionalAuthenticationStatus.ACTIVE;
    }
    async logout(session) {
        if (session === null || session === undefined) {
            return super.passthroughLog(200, `Had an invalid session so can't log out but this doesn't change the result`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_OUT));
        }
        const id = session.get('login.userID');
        session.clear('login.provisional');
        session.clear('login.userID');
        session.clear('login.name');
        return super.passthroughLog(200, 'User successfully logged out', PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_OUT), { userID: id });
    }
    async verifyLoginWithTwoFactor(data, session) {
        if (session === null || session === undefined) {
            return super.passthroughLog(401, `Had an invalid session so cant verify the log in, this shouldn't happen unless something really weird has happened`, PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // Check if this session has a user entry and is still provisional
        if (!(session.has('login.provisional') && session.has('login.userID') && session.get('login.provisional') === ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR)) {
            return super.passthroughLog(400, 'User was not defined or was already authenticated', PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // Check that the user provided the code for this request
        if (!data.hasOwnProperty(`code`)) {
            return super.passthroughLog(400, `The user did not provide a code in the request`, PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // Lookup the user entry
        const user = await this.getUser(session);
        if (user === null) {
            return super.passthroughLog(500, `The user has an id in their session but could not be resolved to an actual user`, `Something went wrong with your user account`, { userID: session.get(`login.userID`) });
        }
        const action = async () => {
            await this.tools.twoFactorAuthenticate(user, data['code']);
            session.set('login.provisional', ProvisionalAuthenticationStatus.ACTIVE);
        };
        const handlers = {
            'not-found': (e) => {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.REDIRECT, `Could not find a two factor configuration for the given user ID`, PUBLIC_RESPONSES.NO_NEED_TWO_FACTOR, {
                    // TODO: next location?
                    location: '/',
                });
            },
            // tslint:disable-next-line:object-literal-key-quotes
            'invalid': (e) => {
                return super.passthroughLog(400, 'User provided a two factor authentication code which is not valid', PUBLIC_RESPONSES.INVALID_TWO_FACTOR);
            },
            'invalid-with-email': async (e) => {
                await this.mailer.sendTwoFactorWarning(user);
                return super.passthroughLog(400, 'User provided a two factor authentication code which is not valid', PUBLIC_RESPONSES.INVALID_TWO_FACTOR);
            },
        };
        // If they were going to another page, resolve it
        let redirect = '/';
        if (session.has('auth-next')) {
            redirect = session.get('auth-next');
        }
        return super.runTool(action, `authenticate with two factor authentication (twoFactorAuthenticate)`, PUBLIC_RESPONSES.SUCCESSFUL('You are now logged in!'), { user, redirect }, handlers);
    }
    async login(data, ip, session) {
        if (this.isAuthenticated(session)) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User tried to log in but was already authenticated`, PUBLIC_RESPONSES.AUTHORIZED, { session });
        }
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['username', 'password'], ['sc-code', 'sc-answer']);
        if (verify)
            return verify;
        let user;
        try {
            user = await this.userDAO.findUserByUsername(data['username']);
        }
        catch (e) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `Username could not be found in the database!`, PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD, { e });
        }
        const action = async () => {
            await this.tools.authenticateUser(data['username'], data['password'], ip);
            // Then actually do the successful stuff
            session.set('login.userID', user.id);
            session.set('login.name', user.full_name);
            session.set('login.provisional', ProvisionalAuthenticationStatus.ACTIVE);
        };
        const handlers = {
            'not-found': (e) => {
                return super.passthroughLog(400, `The user could not be found, they probably fucked up their username`, PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD, { e, username: data['username'] });
            },
            'registered': (e) => {
                return super.passthroughLog(400, 'The user hasn\'t completed the registration and tried to log in! What a ponce', PUBLIC_RESPONSES.REGISTRATION_DISABLED, { e });
            },
            'locked': (e) => {
                return super.passthroughLog(400, 'The user has their account locked, wonder what they did wrong...', PUBLIC_RESPONSES.LOCK_STATUS, { e });
            },
            'password': (e) => {
                return super.passthroughLog(400, 'The user provided a wrong password! They fucked it up!', PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD, { e });
            },
            'no-logins': (e) => {
                return super.passthroughLog(500, 'Something went wrong with finding their previous log ins! Something has done very wrong', PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { e });
            },
            'requires-security-question': (e) => {
                // So set a provisional session so the user can access their own security questions
                session.set('login.provisional', ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION);
                session.set('login.userID', user.id);
                session.set('login.name', user.full_name);
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.REDIRECT, 'Requires a security question to be submitted', PUBLIC_RESPONSES.REQUEST_SECURITY_QUESTION, {
                    e,
                    // TODO: actual locations?
                    redirect: '/auth/security-question/submit',
                });
            },
            '2fa': (e) => {
                // So set a provisional session so the user can access the two factor page
                session.set('login.provisional', ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
                session.set('login.userID', user.id);
                session.set('login.name', user.full_name);
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.REDIRECT, 'Login was successful but required a two factor confirmation', PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP, { e, redirect: '/tfa/authenticate' });
            },
        };
        // If they were going to another page, resolve it
        let redirect = '/';
        if (session !== null && session.has('auth-next')) {
            redirect = session.get('auth-next');
        }
        return super.runTool(action, `logging in with no other validation (login)`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN), { user, redirect }, handlers);
    }
    async verifyLoginWithSecurityQuestion(data, session, ip) {
        // But make sure they are logged in enough
        if (!session.has('login.userID') || !session.has('login.provisional')) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User tried to verify with security question while not being logged in at all`, PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        // If they were going to another page, resolve it
        let redirect = '/';
        if (session.has('auth-next')) {
            redirect = session.get('auth-next');
        }
        // And that they need security questions
        if (session.get('login.provisional') !== ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.REDIRECT, `User did not require security questions`, PUBLIC_RESPONSES.NO_NEED_TWO_FACTOR, {
                redirect,
            });
        }
        // And that they have a security question
        if (!session.has('login.securityQuestion')) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `User session did not contain a security question`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
        }
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['answer']);
        if (verify)
            return verify;
        let user;
        try {
            user = await this.userDAO.findUserByID(session.get('login.userID'));
        }
        catch (e) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Username could not be found in the database!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { e });
        }
        const action = async () => {
            await this.tools.finishSecurityQuestionAuthentication(user, session.get('login.securityQuestion'), data['answer'], ip);
            // Then actually do the successful stuff
            session.set('login.userID', user.id);
            session.set('login.name', user.full_name);
            session.set('login.provisional', ProvisionalAuthenticationStatus.ACTIVE);
        };
        const handlers = {
            'not-found': (e) => {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not find the security question mapping for this user`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { e, username: data['username'] });
            },
            'invalid': async (e) => {
                await this.mailer.sendSecurityWarningInvalidLogin(user, ip);
                return super.passthroughLog(400, 'The user provided an invalid answer', PUBLIC_RESPONSES.CHECK_YOUR_RESPONSE, { e });
            },
            '2fa': (e) => {
                // So set a provisional session so the user can access the two factor page
                session.set('login.provisional', ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
                session.set('login.userID', user.id);
                session.set('login.name', user.full_name);
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.REDIRECT, 'Login was successful but required a two factor confirmation', PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP, { e, redirect: '/tfa/authenticate' });
            },
        };
        return super.runTool(action, `logging in with security question validation (verifyLoginWithSecurityQuestion)`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN), { user, redirect }, handlers);
    }
    async getRandomSecurityQuestion(request) {
        if (!request.has(`login.userID`))
            throw new Error(`User is not authenticated`);
        const questions = await this.securityQuestionDAO.findQuestionsByUserID(request.get(`login.userID`));
        if (questions.length === 0)
            throw new Error(`Not enough security questions!?`);
        return questions[Math.floor(Math.random() * questions.length)];
    }
}
var AuthenticationProcessImpl;
(function (AuthenticationProcessImpl) {
    /**
     * Constructs an {@link AuthProcesses} instance providing utility functions for authentication processes.
     * @param mailer the email connection to use for client access
     * @param twoFactorDAO DAO for accessing and manipulating two factor authentication records
     * @param securityQuestionDAO DAO for accessing and manipulating security question records
     * @param userDAO DAO for accessing and manipulating user records
     * @param loggingDAO DAO for accessing and manipulating logging records
     */
    AuthenticationProcessImpl.construct = (mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO) => {
        return new AuthenticationProcesses(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    };
})(AuthenticationProcessImpl = exports.AuthenticationProcessImpl || (exports.AuthenticationProcessImpl = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aGVudGljYXRpb25Qcm9jZXNzSW1wbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvYWN0aW9ucy9wcm9jZXNzL0F1dGhlbnRpY2F0aW9uUHJvY2Vzc0ltcGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EseUVBQXNFO0FBU3RFLGdEQUE2RDtBQUc3RCx5REFBc0Q7QUFHdEQsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBQ3JELElBQU8sb0JBQW9CLEdBQUcscUJBQVMsQ0FBQyxvQkFBb0IsQ0FBQztBQUU3RDs7R0FFRztBQUNILElBQVksK0JBSVg7QUFKRCxXQUFZLCtCQUErQjtJQUN2Qyx5RkFBTSxDQUFBO0lBQ04sbUhBQW1CLENBQUE7SUFDbkIsaUlBQTBCLENBQUE7QUFDOUIsQ0FBQyxFQUpXLCtCQUErQixHQUEvQix1Q0FBK0IsS0FBL0IsdUNBQStCLFFBSTFDO0FBRUQsTUFBTSx1QkFBd0IsU0FBUSx5QkFBVztJQUk3QyxZQUFvQixNQUF3QixFQUNoQyxZQUEwQixFQUNsQixtQkFBd0MsRUFDeEMsT0FBZ0IsRUFDeEIsVUFBc0I7UUFDOUIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBTEYsV0FBTSxHQUFOLE1BQU0sQ0FBa0I7UUFFeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxZQUFPLEdBQVAsT0FBTyxDQUFTO1FBR2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSx5Q0FBbUIsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQWdCO1FBQzFCLElBQUksT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLEtBQUssU0FBUztZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQztZQUFFLE9BQU8sSUFBSSxDQUFDO1FBRTlDLElBQUk7WUFDQSxPQUFPLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQ3ZFO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELFNBQVMsQ0FBQyxPQUFnQjtRQUN0QixJQUFJLE9BQU8sS0FBSyxJQUFJLElBQUksT0FBTyxLQUFLLFNBQVM7WUFBRSxPQUFPLElBQUksQ0FBQztRQUMzRCxPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUM1RSxDQUFDO0lBRUQsdUJBQXVCLENBQUMsT0FBZ0I7UUFDcEMsSUFBSSxPQUFPLEtBQUssSUFBSSxJQUFJLE9BQU8sS0FBSyxTQUFTO1lBQUUsT0FBTyxJQUFJLENBQUM7UUFDM0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUM7WUFBRSxPQUFPLElBQUksQ0FBQztRQUVuRCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsSUFBSSwrQkFBK0I7WUFBRSxPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUVqSCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsZUFBZSxDQUFDLE9BQWdCO1FBQzVCLElBQUksT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLEtBQUssU0FBUztZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQzVELE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsS0FBSywrQkFBK0IsQ0FBQyxNQUFNLENBQUM7SUFDM0gsQ0FBQztJQUVELEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBZ0I7UUFDekIsSUFBSSxPQUFPLEtBQUssSUFBSSxJQUFJLE9BQU8sS0FBSyxTQUFTLEVBQUU7WUFDM0MsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2QixHQUFHLEVBQ0gsNEVBQTRFLEVBQzVFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FDL0QsQ0FBQztTQUNMO1FBRUQsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUV2QyxPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDbkMsT0FBTyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM5QixPQUFPLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRTVCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILDhCQUE4QixFQUM5QixnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLEVBQzVELEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxDQUNqQixDQUFDO0lBQ04sQ0FBQztJQUVELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxJQUFpQixFQUFFLE9BQWdCO1FBQzlELElBQUksT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLEtBQUssU0FBUyxFQUFFO1lBQzNDLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILG9IQUFvSCxFQUNwSCxnQkFBZ0IsQ0FBQyxlQUFlLENBQ25DLENBQUM7U0FDTDtRQUVELGtFQUFrRTtRQUNsRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEtBQUssK0JBQStCLENBQUMsbUJBQW1CLENBQUMsRUFBRTtZQUNoSyxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLEdBQUcsRUFDSCxtREFBbUQsRUFDbkQsZ0JBQWdCLENBQUMsZUFBZSxDQUNuQyxDQUFDO1NBQ0w7UUFFRCx5REFBeUQ7UUFFekQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDOUIsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2QixHQUFHLEVBQ0gsZ0RBQWdELEVBQ2hELGdCQUFnQixDQUFDLGVBQWUsQ0FDbkMsQ0FBQztTQUNMO1FBRUQsd0JBQXdCO1FBRXhCLE1BQU0sSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV6QyxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUU7WUFDZixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLEdBQUcsRUFDSCxpRkFBaUYsRUFDakYsNkNBQTZDLEVBQzdDLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FDMUMsQ0FBQztTQUNMO1FBRUQsTUFBTSxNQUFNLEdBQUcsS0FBSyxJQUFJLEVBQUU7WUFDdEIsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLCtCQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdFLENBQUMsQ0FBQztRQUVGLE1BQU0sUUFBUSxHQUFHO1lBQ2IsV0FBVyxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUU7Z0JBQzFCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxRQUFRLEVBQ3ZCLGlFQUFpRSxFQUNqRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFDbkM7b0JBQ0ksdUJBQXVCO29CQUN2QixRQUFRLEVBQUUsR0FBRztpQkFDaEIsQ0FDSixDQUFDO1lBQ04sQ0FBQztZQUNELHFEQUFxRDtZQUNyRCxTQUFTLEVBQUUsQ0FBQyxDQUFZLEVBQUUsRUFBRTtnQkFDeEIsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2QixHQUFHLEVBQ0gsbUVBQW1FLEVBQ25FLGdCQUFnQixDQUFDLGtCQUFrQixDQUN0QyxDQUFDO1lBQ04sQ0FBQztZQUNELG9CQUFvQixFQUFFLEtBQUssRUFBRSxDQUFZLEVBQUUsRUFBRTtnQkFDekMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM3QyxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLEdBQUcsRUFDSCxtRUFBbUUsRUFDbkUsZ0JBQWdCLENBQUMsa0JBQWtCLENBQ3RDLENBQUM7WUFDTixDQUFDO1NBQ0osQ0FBQztRQUVGLGlEQUFpRDtRQUNqRCxJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDbkIsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQzFCLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUNoQixNQUFNLEVBQ04scUVBQXFFLEVBQ3JFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxFQUNyRCxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsRUFDbEIsUUFBUSxDQUNYLENBQUM7SUFDTixDQUFDO0lBRUQsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFpQixFQUFFLEVBQVUsRUFBRSxPQUFnQjtRQUN2RCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDL0IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsb0RBQW9ELEVBQ3BELGdCQUFnQixDQUFDLFVBQVUsRUFDM0IsRUFBRSxPQUFPLEVBQUUsQ0FDZCxDQUFDO1NBQ0w7UUFFRCxtSEFBbUg7UUFDbkgsTUFBTSxNQUFNLEdBQUcsTUFBTSxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDeEcsSUFBSSxNQUFNO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUM7UUFDVCxJQUFJO1lBQ0EsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUNsRTtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsOENBQThDLEVBQzlDLGdCQUFnQixDQUFDLHlCQUF5QixFQUMxQyxFQUFFLENBQUMsRUFBRSxDQUNSLENBQUM7U0FDTDtRQUVELE1BQU0sTUFBTSxHQUFHLEtBQUssSUFBSSxFQUFFO1lBQ3RCLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRTFFLHdDQUF3QztZQUV4QyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsK0JBQStCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDN0UsQ0FBQyxDQUFDO1FBRUYsTUFBTSxRQUFRLEdBQUc7WUFDYixXQUFXLEVBQUUsQ0FBQyxDQUFZLEVBQUUsRUFBRTtnQkFDMUIsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2QixHQUFHLEVBQ0gscUVBQXFFLEVBQ3JFLGdCQUFnQixDQUFDLHlCQUF5QixFQUMxQyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQ3BDLENBQUM7WUFDTixDQUFDO1lBQ0QsWUFBWSxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUU7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILCtFQUErRSxFQUMvRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxDQUFDLEVBQUUsQ0FDUixDQUFDO1lBQ04sQ0FBQztZQUNELFFBQVEsRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFO2dCQUN2QixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLEdBQUcsRUFDSCxrRUFBa0UsRUFDbEUsZ0JBQWdCLENBQUMsV0FBVyxFQUM1QixFQUFFLENBQUMsRUFBRSxDQUNSLENBQUM7WUFDTixDQUFDO1lBQ0QsVUFBVSxFQUFFLENBQUMsQ0FBWSxFQUFFLEVBQUU7Z0JBQ3pCLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILHdEQUF3RCxFQUN4RCxnQkFBZ0IsQ0FBQyx5QkFBeUIsRUFDMUMsRUFBRSxDQUFDLEVBQUUsQ0FDUixDQUFDO1lBQ04sQ0FBQztZQUNELFdBQVcsRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFO2dCQUMxQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLEdBQUcsRUFDSCx5RkFBeUYsRUFDekYsZ0JBQWdCLENBQUMscUJBQXFCLEVBQ3RDLEVBQUUsQ0FBQyxFQUFFLENBQ1IsQ0FBQztZQUNOLENBQUM7WUFDRCw0QkFBNEIsRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFO2dCQUMzQyxtRkFBbUY7Z0JBQ25GLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsK0JBQStCLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDN0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRTFDLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxRQUFRLEVBQ3ZCLDhDQUE4QyxFQUM5QyxnQkFBZ0IsQ0FBQyx5QkFBeUIsRUFDMUM7b0JBQ0ksQ0FBQztvQkFDRCwwQkFBMEI7b0JBQzFCLFFBQVEsRUFBRSxnQ0FBZ0M7aUJBQzdDLENBQ0osQ0FBQztZQUNOLENBQUM7WUFDRCxLQUFLLEVBQUUsQ0FBQyxDQUFZLEVBQUUsRUFBRTtnQkFDcEIsMEVBQTBFO2dCQUMxRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLCtCQUErQixDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ3RGLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUUxQyxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsUUFBUSxFQUN2Qiw2REFBNkQsRUFDN0QsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQ3hDLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSxDQUN2QyxDQUFDO1lBRU4sQ0FBQztTQUNKLENBQUM7UUFFRixpREFBaUQ7UUFDakQsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ25CLElBQUksT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQzlDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUNoQixNQUFNLEVBQ04sNkNBQTZDLEVBQzdDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsRUFDM0QsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQ2xCLFFBQVEsQ0FDWCxDQUFDO0lBQ04sQ0FBQztJQUVELEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxJQUFpQixFQUFFLE9BQWdCLEVBQUUsRUFBVTtRQUNqRiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7WUFDbkUsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsOEVBQThFLEVBQzlFLGdCQUFnQixDQUFDLGVBQWUsQ0FDbkMsQ0FBQztTQUNMO1FBRUQsaURBQWlEO1FBQ2pELElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUNuQixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDMUIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDdkM7UUFFRCx3Q0FBd0M7UUFDeEMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEtBQUssK0JBQStCLENBQUMsMEJBQTBCLEVBQUU7WUFDakcsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFFBQVEsRUFDdkIseUNBQXlDLEVBQ3pDLGdCQUFnQixDQUFDLGtCQUFrQixFQUNuQztnQkFDSSxRQUFRO2FBQ1gsQ0FDSixDQUFDO1NBQ0w7UUFFRCx5Q0FBeUM7UUFDekMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsRUFBRTtZQUN4QyxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLGtEQUFrRCxFQUNsRCxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FDekMsQ0FBQztTQUNMO1FBRUQsbUhBQW1IO1FBQ25ILE1BQU0sTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxNQUFNO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUM7UUFDVCxJQUFJO1lBQ0EsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1NBQ3ZFO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLDhDQUE4QyxFQUM5QyxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxDQUFDLEVBQUUsQ0FDUixDQUFDO1NBQ0w7UUFFRCxNQUFNLE1BQU0sR0FBRyxLQUFLLElBQUksRUFBRTtZQUN0QixNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFFdkgsd0NBQXdDO1lBRXhDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSwrQkFBK0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3RSxDQUFDLENBQUM7UUFFRixNQUFNLFFBQVEsR0FBRztZQUNiLFdBQVcsRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFO2dCQUMxQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLDREQUE0RCxFQUM1RCxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUNwQyxDQUFDO1lBQ04sQ0FBQztZQUNELFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBWSxFQUFFLEVBQUU7Z0JBQzlCLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzVELE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsR0FBRyxFQUNILHFDQUFxQyxFQUNyQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFDcEMsRUFBRSxDQUFDLEVBQUUsQ0FDUixDQUFDO1lBQ04sQ0FBQztZQUNELEtBQUssRUFBRSxDQUFDLENBQVksRUFBRSxFQUFFO2dCQUNwQiwwRUFBMEU7Z0JBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsK0JBQStCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDdEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRTFDLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxRQUFRLEVBQ3ZCLDZEQUE2RCxFQUM3RCxnQkFBZ0IsQ0FBQyx1QkFBdUIsRUFDeEMsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFLENBQ3ZDLENBQUM7WUFFTixDQUFDO1NBQ0osQ0FBQztRQUVGLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FDaEIsTUFBTSxFQUNOLGdGQUFnRixFQUNoRixnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLEVBQzNELEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUNsQixRQUFRLENBQ1gsQ0FBQztJQUNOLENBQUM7SUFFRCxLQUFLLENBQUMseUJBQXlCLENBQUMsT0FBZ0I7UUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQy9FLE1BQU0sU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNwRyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQztZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUMvRSxPQUFPLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNuRSxDQUFDO0NBRUo7QUFFRCxJQUFpQix5QkFBeUIsQ0FpQnpDO0FBakJELFdBQWlCLHlCQUF5QjtJQUV0Qzs7Ozs7OztPQU9HO0lBQ1UsbUNBQVMsR0FBRyxDQUFDLE1BQXdCLEVBQ3hCLFlBQTBCLEVBQzFCLG1CQUF3QyxFQUN4QyxPQUFnQixFQUNoQixVQUFzQixFQUE0QixFQUFFO1FBQzFFLE9BQU8sSUFBSSx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsWUFBWSxFQUFFLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztJQUN2RyxDQUFDLENBQUM7QUFDTixDQUFDLEVBakJnQix5QkFBeUIsR0FBekIsaUNBQXlCLEtBQXpCLGlDQUF5QixRQWlCekMifQ==