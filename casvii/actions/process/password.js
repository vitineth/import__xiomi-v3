"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ProcessImpl_1 = require("../ProcessImpl");
const PasswordTools_1 = require("../database/PasswordTools");
const constants_1 = require("../../../cas/utils/constants");
const ToolError_1 = require("../../errors/ToolError");
var HttpStatusCode = ProcessImpl_1.ProcessImpl.HttpStatusCode;
class PasswordProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(mailer, loggingDAO, userDAO, tokenDAO) {
        super(loggingDAO);
        this.tooling = new PasswordTools_1.PasswordTools(mailer, userDAO, tokenDAO);
    }
    /**
     * Performs the first step of an unauthenticated password change - the request phase. Verifies the provided username
     * and email and will then generate a range request and email the user with it. In the event that the username and
     * email are not the same, it will provide a positive response to the user but will not create a change request or
     * email the user. Only provides a cas response with public comments.
     * @param data the data request containing the username and email
     */
    async changePasswordUnauthenticatedRequest(data) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['username', 'email']);
        if (verify)
            return verify;
        try {
            await this.tooling.unauthenticatedChangeRequest(data['username'], data['email']);
            return super.passthroughLog(HttpStatusCode.SUCCESS, `The user successfully requested that their password be changed`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(constants_1.PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `The password tool threw an unexpected exception`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                case `not-found`:
                    return super.passthroughLog(HttpStatusCode.SUCCESS, `Could not find the username they provided but we are still providing a positive response to the user`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(constants_1.PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
                case `invalid-email`:
                    return super.passthroughLog(HttpStatusCode.SUCCESS, `The email they provided did not match the one associated with their username but we still provide a positive response in this case`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(constants_1.PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
                case `failed-save-request`:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Something went wrong when they tried to save the user request! Aaaahhh`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                case `failed-email`:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to send them an email! The change request was submitted but we got some error from our system.`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                default:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `We got an unexpected error identifier from the password tool`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            }
        }
    }
    /**
     * Requests data containing an old password, two new passwords and a pin. A user should be stored in the session
     * using standard locations. This verifies the passwords, password complexity, verifies pin and then updates the
     * database.
     * On error it will return an {@link CASResponse} which will contain the public response and the HTTP status
     * code to use
     * @param data the data required to perform this operation (required: old, new1, new2, pin)
     * @param session the session from which to fetch the user
     * @return the response the server should provide or true if the password was updated successfully
     */
    async changePasswordAuthenticated(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['old', 'new1', 'new2', 'pin']);
        if (verify)
            return verify;
        // Test if passwords match
        if (data['new1'] !== data['new2']) {
            return super.passthroughLog(400, 'Passwords provided by the user do not match', constants_1.PUBLIC_RESPONSES.PASSWORD_MATCH);
        }
        // Swap to the new system!
        try {
            await this.tooling.changePassword(session.get('login.userID'), data['old'], data['new1'], data['pin']);
            return super.passthroughLog(HttpStatusCode.SUCCESS, `The user successfully changed their password and provided additional authentication through thier pin code`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(''));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to log in the user because the authentication tool threw an unexpected exception`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                default:
                    return super.passthroughLog(500, `We got an unexpected error identifier from the authenticator tool`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `too-weak`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The user provided a password that was too weak! It had a score of ${e.data === undefined ? '[score was not provided]' : e.data['score']}`, constants_1.PUBLIC_RESPONSES.WEAK_PASSWORD(e.data === undefined ? '' : e.data['feedback']));
                case `wrong-old`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The user provided an invalid old password`, constants_1.PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                case `invalid-pin`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The user provided an invalid pin`, constants_1.PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                case `failed-save`:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `The database save failed to happen for some reason. Start screaming`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `not-found`:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not identify the user from their userID!`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            }
        }
    }
    async changePasswordUnauthenticatedSubmit(data) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['ut', 'rt', 'pin', 'new1', 'new2']);
        if (verify)
            return verify;
        // Test if passwords match
        if (data['new1'] !== data['new2']) {
            return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `Passwords did not match`, constants_1.PUBLIC_RESPONSES.PASSWORD_MATCH);
        }
        try {
            await this.tooling.unauthenticatedChangeSubmission(data['ut'], data['rt'], data['pin'], data['new1']);
            return super.passthroughLog(HttpStatusCode.SUCCESS, `The user successfully changed their password as an unauthenticated user providing valid tokens and pins`, constants_1.PUBLIC_RESPONSES.SUCCESSFUL(''));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to log in the user because the authentication tool threw an unexpected exception`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                case `no-request`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `No request could be found using the provided user and reset tokens`, constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
                case `already-used`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `Token has already been marked as used, repeat attack?`, constants_1.PUBLIC_RESPONSES.EXPIRED_TOKEN);
                case `expired`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The reset token is out of date`, constants_1.PUBLIC_RESPONSES.EXPIRED_TOKEN);
                case `too-weak`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The user provided a password that was too weak! It had a score of ${e.data === undefined ? '[score was not provided]' : e.data['score']}`, constants_1.PUBLIC_RESPONSES.WEAK_PASSWORD(e.data === undefined ? '' : e.data['feedback']));
                case `invalid-pin`:
                    return super.passthroughLog(HttpStatusCode.BAD_REQUEST, `The pin provided by the user did not match, could be a targeted attack?`, constants_1.PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                case `cannot-save`:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not save the users request, nothing has happened!`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                default:
                    return super.passthroughLog(HttpStatusCode.INTERNAL_SERVER_ERROR, `We got an unexpected error identifier from the authenticator tool`, constants_1.PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
exports.construct = (mailer, loggingDAO, userDAO, tokenDAO) => {
    return new PasswordProcesses(mailer, loggingDAO, userDAO, tokenDAO);
};
