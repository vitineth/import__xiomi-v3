"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProcessesImpl = void 0;
const ProcessImpl_1 = require("../ProcessImpl");
const ToolError_1 = require("../../errors/ToolError");
const UserTools_1 = require("../database/UserTools");
const Constants_1 = require("../../utilities/Constants");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
/**
 * Regex for validating emails. Abandon all hope ye who enter here. In all seriouesness, its sourced from here:
 *      https://stackoverflow.com/a/201378
 * And considering they drew a finite state machine for this, I'm pretty sure this is valid. Is it over 6000 characters
 * long, yes. Is this horrific overkill, also yes. But where's the fun if we don't.
 */
const emailRegex = /(?:(?:\r\n)?[ \t])*(?:(?:(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*|(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)*\<(?:(?:\r\n)?[ \t])*(?:@(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*(?:,@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*)*:(?:(?:\r\n)?[ \t])*)?(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*\>(?:(?:\r\n)?[ \t])*)|(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)*:(?:(?:\r\n)?[ \t])*(?:(?:(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*|(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)*\<(?:(?:\r\n)?[ \t])*(?:@(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*(?:,@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*)*:(?:(?:\r\n)?[ \t])*)?(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*\>(?:(?:\r\n)?[ \t])*)(?:,\s*(?:(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*|(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)*\<(?:(?:\r\n)?[ \t])*(?:@(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*(?:,@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*)*:(?:(?:\r\n)?[ \t])*)?(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|"(?:[^\"\r\\]|\\.|(?:(?:\r\n)?[ \t]))*"(?:(?:\r\n)?[ \t])*))*@(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*)(?:\.(?:(?:\r\n)?[ \t])*(?:[^()<>@,;:\\".\[\] \000-\031]+(?:(?:(?:\r\n)?[ \t])+|\Z|(?=[\["()<>@,;:\\".\[\]]))|\[([^\[\]\r\\]|\\.)*\](?:(?:\r\n)?[ \t])*))*\>(?:(?:\r\n)?[ \t])*))*)?;\s*)/;
class UserProcesses extends ProcessImpl_1.ProcessImpl {
    constructor(loggingDAO, userDAO, tokenDAO, securityQuestionDAO, permissionDAO, mailer) {
        super(loggingDAO);
        this.tools = new UserTools_1.UserTools(mailer, userDAO, tokenDAO, securityQuestionDAO, permissionDAO);
    }
    async registerConfirmation(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['ut', 'rt']);
        if (verify)
            return verify;
        // Check they are not logged in!
        if (session.has('login.userID') && session.has('login.provisional')) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User was logged in when trying to register`, PUBLIC_RESPONSES.AUTHORIZED);
        }
        try {
            await this.tools.completeRegister(data['ut'], data['rt']);
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The user successfully completed their registation`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.REGISTRATION_COMPLETE));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to confirm registration because the tool threw an unexpected exception`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                default:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `We got an unexpected error identifier from the tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `not-found`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `Could not find the codes provided by the user!`, PUBLIC_RESPONSES.INVALID_REQUEST);
                case `failed-save`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Could not save the user or the registration token`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
                case `expired`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided a token that was already used or has expired`, PUBLIC_RESPONSES.EXPIRED_TOKEN, { e });
            }
        }
    }
    async registerUser(data, session) {
        // Verify keys, if not, this automatically logs it before returning the response so we can just return the response
        const verify = await super.verifyDataElements(data, ['username', 'email', 'name', 'password', 'confirmation', 'pin', 'sc1', 'sc2', 'sc3', 'sc1a', 'sc2a', 'sc3a'], ['tfa']);
        if (verify)
            return verify;
        // Check they are not logged in!
        if (session.has('login.userID') && session.has('login.provisional')) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User was logged in when trying to register`, PUBLIC_RESPONSES.AUTHORIZED);
        }
        // Check that their email is valid
        if (emailRegex.exec(data['email']) == null) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `User provided an invalid email address`, PUBLIC_RESPONSES.INVALID_EMAIL, { data });
        }
        const security = [
            { question: data['sc1'], answer: data['sc1a'] },
            { question: data['sc2'], answer: data['sc2a'] },
            { question: data['sc3'], answer: data['sc3a'] },
        ];
        if (data['password'] !== data['confirmation']) {
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, 'Passwords provided by the user do not match', PUBLIC_RESPONSES.PASSWORD_MATCH);
        }
        try {
            await this.tools.register(data['username'], data['email'], data['name'], data['password'], data['pin'], security);
            return super.passthroughLog(ProcessImpl_1.HttpStatusCode.SUCCESS, `The user has successfully registered and it awaiting confirmation`, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.REGISTERED));
        }
        catch (e) {
            // If not, get the reason for why
            if (!(e instanceof ToolError_1.ToolError)) {
                return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to register the user because the user tool threw an unexpected exception`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            // e should be a type error by this point
            switch (e.identifier) {
                default:
                    return super.passthroughLog(500, `We got an unexpected error identifier from the user tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `wrong-number`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Invalid number of security questions was provided, should not be possible`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `already-used`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The username or email is already used in the database`, PUBLIC_RESPONSES.USERNAME_EMAIL_IN_USE);
                case `too-weak`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided a password that was too weak! It had a score of ${e.data === undefined ? '[score was not provided]' : e.data['score']}`, PUBLIC_RESPONSES.WEAK_PASSWORD(e.data === undefined ? '' : e.data['feedback']));
                case `invalid-pin`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user provided a pin which did not match the regex`, PUBLIC_RESPONSES.INVALID_PIN_FORMAT);
                case `invalid-questions`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `Questions not found in the database!`, PUBLIC_RESPONSES.SECURITY_QUESTIONS_NOT_FOUND);
                case `cannot-save-user`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to save the user object!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `cannot-save-registration`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to save the registration object!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `cannot-email`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR, `Failed to email the user with their details!`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
                case `duplicate-questions`:
                    return super.passthroughLog(ProcessImpl_1.HttpStatusCode.BAD_REQUEST, `The user picked duplicate security questions or answers`, PUBLIC_RESPONSES.SECURITY_QUESTIONS_NOT_FOUND);
            }
        }
    }
}
var UserProcessesImpl;
(function (UserProcessesImpl) {
    UserProcessesImpl.construct = (loggingDAO, userDAO, tokenDAO, securityQuestionDAO, permissionDAO, mailer) => {
        return new UserProcesses(loggingDAO, userDAO, tokenDAO, securityQuestionDAO, permissionDAO, mailer);
    };
})(UserProcessesImpl = exports.UserProcessesImpl || (exports.UserProcessesImpl = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlclByb2Nlc3Nlc0ltcGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY2FzdmlpL2FjdGlvbnMvcHJvY2Vzcy9Vc2VyUHJvY2Vzc2VzSW1wbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxnREFBNkQ7QUFDN0Qsc0RBQW1EO0FBS25ELHFEQUFrRDtBQUdsRCx5REFBc0Q7QUFLdEQsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBQ3JELElBQU8sb0JBQW9CLEdBQUcscUJBQVMsQ0FBQyxvQkFBb0IsQ0FBQztBQUc3RDs7Ozs7R0FLRztBQUNILE1BQU0sVUFBVSxHQUFHLHlzTUFBeXNNLENBQUM7QUFFN3RNLE1BQU0sYUFBYyxTQUFRLHlCQUFXO0lBSW5DLFlBQVksVUFBc0IsRUFDdEIsT0FBZ0IsRUFDaEIsUUFBa0IsRUFDbEIsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLE1BQXdCO1FBQ2hDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUkscUJBQVMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSxhQUFhLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsS0FBSyxDQUFDLG9CQUFvQixDQUFDLElBQWlCLEVBQUUsT0FBZ0I7UUFDMUQsbUhBQW1IO1FBQ25ILE1BQU0sTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksTUFBTTtZQUFFLE9BQU8sTUFBTSxDQUFDO1FBRTFCLGdDQUFnQztRQUNoQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO1lBQ2pFLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLDRDQUE0QyxFQUM1QyxnQkFBZ0IsQ0FBQyxVQUFVLENBQzlCLENBQUM7U0FDTDtRQUVELElBQUk7WUFDQSxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFELE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxPQUFPLEVBQ3RCLG1EQUFtRCxFQUNuRCxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsQ0FDMUUsQ0FBQztTQUNMO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixpQ0FBaUM7WUFFakMsSUFBSSxDQUFDLENBQUMsQ0FBQyxZQUFZLHFCQUFTLENBQUMsRUFBRTtnQkFDM0IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLHFCQUFxQixFQUNwQywrRUFBK0UsRUFDL0UsZ0JBQWdCLENBQUMscUJBQXFCLEVBQ3RDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUNmLENBQUM7YUFDTDtZQUVELHlDQUF5QztZQUN6QyxRQUFRLENBQUMsQ0FBQyxVQUFVLEVBQUU7Z0JBQ2xCO29CQUNJLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMscURBQXFELEVBQ3JELGdCQUFnQixDQUFDLHFCQUFxQixDQUN6QyxDQUFDO2dCQUNOLEtBQUssV0FBVztvQkFDWixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQixnREFBZ0QsRUFDaEQsZ0JBQWdCLENBQUMsZUFBZSxDQUNuQyxDQUFDO2dCQUNOLEtBQUssYUFBYTtvQkFDZCxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLG1EQUFtRCxFQUNuRCxnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQztnQkFDTixLQUFLLFNBQVM7b0JBQ1YsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsZ0VBQWdFLEVBQ2hFLGdCQUFnQixDQUFDLGFBQWEsRUFDOUIsRUFBRSxDQUFDLEVBQUUsQ0FDUixDQUFDO2FBQ1Q7U0FDSjtJQUVMLENBQUM7SUFFRCxLQUFLLENBQUMsWUFBWSxDQUFDLElBQWlCLEVBQUUsT0FBZ0I7UUFDbEQsbUhBQW1IO1FBQ25ILE1BQU0sTUFBTSxHQUFHLE1BQU0sS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzVLLElBQUksTUFBTTtZQUFFLE9BQU8sTUFBTSxDQUFDO1FBRTFCLGdDQUFnQztRQUNoQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO1lBQ2pFLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLDRDQUE0QyxFQUM1QyxnQkFBZ0IsQ0FBQyxVQUFVLENBQzlCLENBQUM7U0FDTDtRQUVELGtDQUFrQztRQUNsQyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFO1lBQ3hDLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLHdDQUF3QyxFQUN4QyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQzlCLEVBQUUsSUFBSSxFQUFFLENBQ1gsQ0FBQztTQUNMO1FBRUQsTUFBTSxRQUFRLEdBQUc7WUFDYixFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUMvQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUMvQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtTQUNsRCxDQUFDO1FBRUYsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQzNDLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLDZDQUE2QyxFQUM3QyxnQkFBZ0IsQ0FBQyxjQUFjLENBQ2xDLENBQUM7U0FDTDtRQUVELElBQUk7WUFDQSxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLEVBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsRUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUNoQixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQ1gsUUFBUSxDQUNYLENBQUM7WUFDRixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsT0FBTyxFQUN0QixtRUFBbUUsRUFDbkUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUMvRCxDQUFDO1NBQ0w7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLGlDQUFpQztZQUVqQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVkscUJBQVMsQ0FBQyxFQUFFO2dCQUMzQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLGlGQUFpRixFQUNqRixnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQzthQUNMO1lBRUQseUNBQXlDO1lBQ3pDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsRUFBRTtnQkFDbEI7b0JBQ0ksT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2QixHQUFHLEVBQ0gsMERBQTBELEVBQzFELGdCQUFnQixDQUFDLHFCQUFxQixDQUN6QyxDQUFDO2dCQUNOLEtBQUssY0FBYztvQkFDZixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLDJFQUEyRSxFQUMzRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FDekMsQ0FBQztnQkFDTixLQUFLLGNBQWM7b0JBQ2YsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsdURBQXVELEVBQ3ZELGdCQUFnQixDQUFDLHFCQUFxQixDQUN6QyxDQUFDO2dCQUNOLEtBQUssVUFBVTtvQkFDWCxPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQixxRUFBcUUsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQzFJLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQ2pGLENBQUM7Z0JBQ04sS0FBSyxhQUFhO29CQUNkLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxXQUFXLEVBQzFCLHVEQUF1RCxFQUN2RCxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FDdEMsQ0FBQztnQkFDTixLQUFLLG1CQUFtQjtvQkFDcEIsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLFdBQVcsRUFDMUIsc0NBQXNDLEVBQ3RDLGdCQUFnQixDQUFDLDRCQUE0QixDQUNoRCxDQUFDO2dCQUNOLEtBQUssa0JBQWtCO29CQUNuQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMscUJBQXFCLEVBQ3BDLGlDQUFpQyxFQUNqQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FDekMsQ0FBQztnQkFDTixLQUFLLDBCQUEwQjtvQkFDM0IsT0FBTyxLQUFLLENBQUMsY0FBYyxDQUN2Qiw0QkFBYyxDQUFDLHFCQUFxQixFQUNwQyx5Q0FBeUMsRUFDekMsZ0JBQWdCLENBQUMscUJBQXFCLENBQ3pDLENBQUM7Z0JBQ04sS0FBSyxjQUFjO29CQUNmLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FDdkIsNEJBQWMsQ0FBQyxxQkFBcUIsRUFDcEMsOENBQThDLEVBQzlDLGdCQUFnQixDQUFDLHFCQUFxQixDQUN6QyxDQUFDO2dCQUNOLEtBQUsscUJBQXFCO29CQUN0QixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQ3ZCLDRCQUFjLENBQUMsV0FBVyxFQUMxQix5REFBeUQsRUFDekQsZ0JBQWdCLENBQUMsNEJBQTRCLENBQ2hELENBQUM7YUFDVDtTQUNKO0lBQ0wsQ0FBQztDQUVKO0FBRUQsSUFBaUIsaUJBQWlCLENBS2pDO0FBTEQsV0FBaUIsaUJBQWlCO0lBQ2pCLDJCQUFTLEdBQUcsQ0FBQyxVQUFzQixFQUFFLE9BQWdCLEVBQUUsUUFBa0IsRUFDNUQsbUJBQXdDLEVBQUUsYUFBNEIsRUFBRSxNQUF3QixFQUFrQixFQUFFO1FBQzFJLE9BQU8sSUFBSSxhQUFhLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3hHLENBQUMsQ0FBQztBQUNOLENBQUMsRUFMZ0IsaUJBQWlCLEdBQWpCLHlCQUFpQixLQUFqQix5QkFBaUIsUUFLakMifQ==