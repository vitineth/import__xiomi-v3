import { CASEmailProvider } from "../../mail/CASEmailProvider";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
import { UserDAO } from "../../database/dao/UserDAO";
import { IPermissionProcesses } from "../interface/IPermissionProcesses";
import { PermissionDAO } from "../../database/dao/PermissionDAO";
export declare namespace PermissionProcessImpl {
    const construct: (mailer: CASEmailProvider, loggingDAO: LoggingDAO, userDAO: UserDAO, permissionDAO: PermissionDAO) => IPermissionProcesses;
}
