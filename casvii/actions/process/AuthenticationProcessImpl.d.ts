import { IAuthenticationProcesses } from "../interface/IAuthenticationProcesses";
import { TwoFactorDAO } from "../../database/dao/TwoFactorDAO";
import { SecurityQuestionDAO } from "../../database/dao/SecurityQuestionDAO";
import { UserDAO } from "../../database/dao/UserDAO";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
/**
 * The authentication status of a user
 */
export declare enum ProvisionalAuthenticationStatus {
    ACTIVE = 0,
    REQUIRES_TWO_FACTOR = 1,
    REQUIRES_SECURITY_QUESTION = 2
}
export declare namespace AuthenticationProcessImpl {
    /**
     * Constructs an {@link AuthProcesses} instance providing utility functions for authentication processes.
     * @param mailer the email connection to use for client access
     * @param twoFactorDAO DAO for accessing and manipulating two factor authentication records
     * @param securityQuestionDAO DAO for accessing and manipulating security question records
     * @param userDAO DAO for accessing and manipulating user records
     * @param loggingDAO DAO for accessing and manipulating logging records
     */
    const construct: (mailer: CASEmailProvider, twoFactorDAO: TwoFactorDAO, securityQuestionDAO: SecurityQuestionDAO, userDAO: UserDAO, loggingDAO: LoggingDAO) => IAuthenticationProcesses;
}
