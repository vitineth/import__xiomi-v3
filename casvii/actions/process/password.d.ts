import { IPasswordProcesses } from "../interface/IPasswordProcesses";
import { CASEmailProvider } from "../../../cas/email/emailer";
import { UserDAO } from "../../database/dao/UserDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
export declare const construct: (mailer: CASEmailProvider, loggingDAO: LoggingDAO, userDAO: UserDAO, tokenDAO: TokenDAO) => IPasswordProcesses;
