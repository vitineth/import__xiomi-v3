import { Session } from "../../utilities/session/Session";
import { CASResponse } from "../../utilities/CASResponse";
import { RequestUtilities } from "../../utilities/WebUtilities";
import { ProvisionalAuthenticationStatus } from "../process/AuthenticationProcessImpl";
import { User } from "../../database/models/User.model";
import RequestBody = RequestUtilities.RequestBody;
import { SecurityQuestion } from "../../database/models/security/SecurityQuestion.model";
export interface IAuthenticationProcesses {
    /**
     * Attempts to find the given user in the session and return its representation from the database, returning null if
     * it is not found for any reason
     * @param session the session interface for this request
     */
    getUser(session: Session): Promise<User | null>;
    /**
     * Attempts to fetch the user ID contained within the users session. If the user is not authenticated or the
     * session is corrupted, it will return undefined.
     *
     * @param session the session interface for this request
     */
    getUserID(session: Session): number | null;
    /**
     * Finds the current authentication status of the user associated with the session. If the user is not authenticated
     * or the session is corrupted the function will return undefined. Otherwise it tests the provisional value and
     * returns the associated value from {@link ProvisionalAuthenticationStatus}.
     *
     * @param session the session interface for this request
     */
    getAuthenticationStatus(session: Session): ProvisionalAuthenticationStatus | null;
    /**
     * Returns if the user is authenticated, verifying if the session contains a provisional status and that it is
     * active. Will not verify the other values as they should always be set if this value is present.
     *
     * @param session the session interface for this request
     */
    isAuthenticated(session: Session): boolean;
    /**
     * Invalidates the session for this user if it is present.
     *
     * @param session the session interface for this request
     */
    logout(session: Session): Promise<CASResponse>;
    /**
     * Tests the request to the server for two factor status. This requires the users provisional status to
     * require a two factor authentication confirmation. The request, user configuration and code will be
     * checked to see if the code is valid and an appropriate CAS response will be returned for the web response.
     * This function will automatically update the provisional status if the code is correct.
     *
     * @param data the body data from the request to the server
     * @param session the session interface for this request
     */
    verifyLoginWithTwoFactor(data: RequestBody, session: Session): Promise<CASResponse>;
    /**
     * Confirms the security question requested from the user in a login workflow which requires them to answer a
     * security question (ie due to logging in from an unrecognised location). The sent security question ID must be
     * provided in the users session and the users provisional status to be set to requiring a security question. On
     * successful answer, the user will be tested for an active two factor authentication configuration and the
     * provisional status code updated appropriately to the response
     *
     * @param data the body data from the request to the server
     * @param session the session interface for this request
     * @param ip the ip from which this request came for logging and email
     */
    verifyLoginWithSecurityQuestion(data: RequestBody, session: Session, ip: string): Promise<CASResponse>;
    /**
     * Attempts to log in the user using the request data. Will verify the username, password, login ip address and
     * then set the user state. If the user requires two factor authentication or a security question (logging in
     * from an unknown IP), it will set the user session and then return.<br><br>
     *
     * Verify all responses with {@link CASResponse#statusCode} of 200 because this does not always mean a
     * successful login. On security questions being requires, the additional data will contain errCode: 'sc'.
     *
     * @param data the body data from the request to the server
     * @param ip the ip address of the user accessing the server to be logged in the database
     * @param session the session interface for this request
     */
    login(data: RequestBody, ip: string, session: Session): Promise<CASResponse>;
    /**
     * Returns a random security question instance that the user has configured. This should be used to present the user
     * with a random security question when required.
     * @param request the session from which the user is requesting this question
     */
    getRandomSecurityQuestion(request: Session): Promise<SecurityQuestion>;
}
