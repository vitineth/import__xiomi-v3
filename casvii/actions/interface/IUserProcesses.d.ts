import { Session } from "../../utilities/session/Session";
import { CASResponse } from "../../utilities/CASResponse";
import { RequestUtilities } from "../../utilities/WebUtilities";
import RequestBody = RequestUtilities.RequestBody;
export interface IUserProcesses {
    /**
     * Registers a user from the given data. This will verify all data in the request and set the session if valid and
     * will return a response suitable for the user. Required in data: username, email, password, password confirmation,
     * pin code, security question x3 and optionally a two factor setting.
     * @param data request data
     * @param session abstraction to the request session
     */
    registerUser(data: RequestBody, session: Session): Promise<CASResponse>;
    /**
     * Completes a user registration when they come from the email link of their registration token. Requires the
     * request to contain a user token and a registration token in the keys ut and rt.
     * @param data request data
     * @param session abstraction to the request session
     */
    registerConfirmation(data: RequestBody, session: Session): Promise<CASResponse>;
}
