import { RequestUtilities } from "../../utilities/WebUtilities";
import { Session } from "../../utilities/session/Session";
import { CASResponse } from "../../utilities/CASResponse";
import { User } from "../../database/models/User.model";
import RequestBody = RequestUtilities.RequestBody;
import { Permission } from "../../database/models/permissions/Permission.model";
export interface IPermissionProcesses {
    /**
     * Request which will grant the permission in the request to the user specified in the request
     * assuming the person sending the request (session) has the correct permissions.
     * @param data the data included in the request
     * @param session the session of the user sending the request
     */
    grantGroupToUser(data: RequestBody, session: Session): Promise<CASResponse>;
    /**
     * Request which will ungrant the permission in the request to the user specified in the request
     * assuming the person sending the request (session) has the correct permissions.
     * @param data the data included in the request
     * @param session the session of the user sending the request
     */
    ungrantGroupFromUser(data: RequestBody, session: Session): Promise<CASResponse>;
    /**
     * Returns if the given user has the given permission. This will require us to query the database
     * for every group that this user is apart of, including those they inherit.
     * @param user the user to be tested
     * @param permission the permission to test
     */
    userHasPermission(user: User, permission: string): Promise<boolean>;
    /**
     * Returns if the session contains the given permission. If the session contains `user.permissions`
     * it will check that (providing `user.permission-verifications` is < 10) it is contained. If the verifications
     * number is too high, it will requery the database to get an updated list of permissions to ensure that it works
     * @param session
     * @param permission
     */
    sessionHasPermission(session: Session, permission: string): Promise<boolean>;
    /**
     * Returns if the session contains the given permissions. If the session contains `user.permissions`
     * it will check that (providing `user.permission-verifications` is < 10) it is contained. If the verifications
     * number is too high, it will requery the database to get an updated list of permissions to ensure that it works
     * @param session the session in which this should be tested
     * @param permissions the permissions which should be checked
     */
    sessionHasPermissions(session: Session, permissions: string[]): Promise<boolean>;
    /**
     * Resolves the full set of permissions associated with this user and then saves it in the session provided
     * @param session the session into which the permission list should be saved
     * @param user the user for which permissions should be retrieved
     */
    resolveAllUserPermissions(session: Session, user: User): Promise<boolean>;
    /**
     * Attempts to let the user activate a set of permissions on thier account through a {@link SelfGrantAuthorization}
     * @param data the content sent with the request
     * @param session the session of the user requesting the data
     */
    activateSelfPermissionGrant(data: RequestBody, session: Session): Promise<CASResponse>;
    /**
     * Return the list of permissions from a self grant ID which can be displayed to the user. This should only provide
     * directly obtained permissions to stop them from getting the entire tree of shit
     * @param id the id of the self grant
     */
    getPermissionsFromSelfGrantID(id: number): Promise<Permission[]>;
}
