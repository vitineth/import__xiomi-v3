import { Session } from "../../utilities/session/Session";
import { CASResponse } from "../../utilities/CASResponse";
export interface IPasswordProcesses {
    /**
     * Requests data containing an old password, two new passwords and a pin. A user should be stored in the session
     * using standard locations. This verifies the passwords, password complexity, verifies pin and then updates the
     * database.
     * On error it will return an {@link CASResponse} which will contain the public response and the HTTP status
     * code to use
     * @param data the data required to perform this operation (required: old, new1, new2, pin)
     * @param session the session from which to fetch the user
     * @return the response the server should provide or true if the password was updated successfully
     */
    changePasswordAuthenticated(data: {
        [key: string]: any;
    }, session: Session): Promise<CASResponse>;
    /**
     * Performs the first step of an unauthenticated password change - the request phase. Verifies the provided username
     * and email and will then generate a range request and email the user with it. In the event that the username and
     * email are not the same, it will provide a positive response to the user but will not create a change request or
     * email the user. Only provides a cas response with public comments.
     * @param data the data request containing the username and email
     * @param session the session from the user who sent the request
     */
    changePasswordUnauthenticatedRequest(data: {
        [key: string]: any;
    }, session: Session): Promise<CASResponse>;
    changePasswordUnauthenticatedSubmit(data: {
        [key: string]: any;
    }, session: Session): Promise<CASResponse>;
}
