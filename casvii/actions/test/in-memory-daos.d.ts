import { LoggingDAO } from "../../database/dao/LoggingDAO";
import { SecurityQuestionDAO } from "../../database/dao/SecurityQuestionDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { TwoFactorDAO } from "../../database/dao/TwoFactorDAO";
import { UserDAO } from "../../database/dao/UserDAO";
import { User } from "../../database/models/User.model";
import { Login } from "../../database/models/logs/Login.model";
import { SecurityQuestion } from "../../database/models/security/SecurityQuestion.model";
import { SecurityQuestionMapping } from "../../database/models/security/SecurityQuestionMapping.model";
import { ResetDataRequest } from "../../database/models/ResetDataRequest.model";
import { RegistrationToken } from "../../database/models/security/RegistrationToken";
import { TwoFactorConfiguration } from "../../database/models/security/TwoFactorConfiguration.model";
import { LogAuth } from "../../database/models/logs/LogAuth.model";
export declare class InMemoryLoggingDAO implements LoggingDAO {
    private data;
    constructor(data: {
        login: Login[];
        auth: LogAuth[];
    });
    fetchLoginsByUser(user: User): Promise<Login[]>;
    logLogin(ipAddress: string, user: User): Promise<Login>;
    saveAuthenticationMessage(source: string, detail: string): Promise<LogAuth>;
}
export declare class InMemorySecurityQuestionDAO implements SecurityQuestionDAO {
    private data;
    constructor(data: {
        questions: SecurityQuestion[];
        mapping: SecurityQuestionMapping[];
    });
    findAllQuestions(): Promise<SecurityQuestion[]>;
    findQuestionByIDs(user: User, id: number): Promise<SecurityQuestionMapping>;
}
export declare class InMemoryTokenDAO implements TokenDAO {
    private data;
    constructor(data: {
        reset: ResetDataRequest[];
        registration: RegistrationToken[];
    });
    findPasswordChangeRequest(userToken: string, token: string): Promise<ResetDataRequest>;
    findRegistrationToken(userToken: string, registrationToken: string): Promise<RegistrationToken>;
    markChangeComplete(request: ResetDataRequest): Promise<ResetDataRequest>;
    markRegistrationComplete(request: RegistrationToken): Promise<RegistrationToken>;
    requestPasswordChange(user: User): Promise<ResetDataRequest>;
    requestRegistrationToken(user: User): Promise<RegistrationToken>;
}
export declare class InMemoryTwoFactorDAO implements TwoFactorDAO {
    private data;
    constructor(data: TwoFactorConfiguration[]);
    findConfigurationByUser(user: User): Promise<TwoFactorConfiguration>;
}
export declare class InMemoryUserDAO implements UserDAO {
    private data;
    constructor(data: User[]);
    changeUserPassword(user: User, hash: string, hashVersion: number): Promise<User>;
    changeUserPasswordAutohash(user: User, password: string): Promise<User>;
    createUser(username: string, email: string, name: string, hash: string, hashVersion: number, pin: string, pinVersion: number, status: string): Promise<User>;
    createUserAutohash(username: string, email: string, name: string, password: string, pin: string, status: string): Promise<User>;
    findUserByID(id: number): Promise<User>;
    findUserByUsername(username: string): Promise<User>;
    isEmailAvailable(email: string): Promise<boolean>;
    isUsernameAvailable(username: string): Promise<boolean>;
    makeUserAccessible(user: User): Promise<User>;
}
