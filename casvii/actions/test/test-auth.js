"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const TestingDAO = __importStar(require("./in-memory-daos"));
const auth_1 = require("../process/auth");
const password_1 = require("../process/password");
const emailer_1 = require("../../../cas/email/emailer");
const ObjectSession_1 = require("../../../cas/integration/session/impl/ObjectSession");
const chai_1 = require("chai");
const constants_1 = require("../../../cas/utils/constants");
let authenticator;
let password;
let loggingDAO;
let securityQuestionDAO;
let tokenDAO;
let twoFactorDAO;
let userDAO;
// Setup the mocking for node mailer
before(async function () {
    this.timeout(10000);
    const transport = {
        sendMail: async (data) => {
            return {};
        },
    };
    loggingDAO = new TestingDAO.InMemoryLoggingDAO({ auth: [], login: [] });
    securityQuestionDAO = new TestingDAO.InMemorySecurityQuestionDAO({ questions: [], mapping: [] });
    tokenDAO = new TestingDAO.InMemoryTokenDAO({ reset: [], registration: [] });
    twoFactorDAO = new TestingDAO.InMemoryTwoFactorDAO([]);
    userDAO = new TestingDAO.InMemoryUserDAO([]);
    // @ts-ignore - this is valid because we are mocking it, I know it only calls sendMail
    const mailer = new emailer_1.CASEmailProvider(transport);
    authenticator = auth_1.construct(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    password = password_1.construct(mailer, loggingDAO, userDAO, tokenDAO);
});
describe('actions', () => {
    describe('password', () => {
        describe('change when authenticated', () => {
            it('should return a valid response when provided no user authentication information', async () => {
                const response = await password.changePasswordAuthenticated({}, new ObjectSession_1.ObjectSession());
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(response.externalMessage, constants_1.PUBLIC_RESPONSES.INVALID_REQUEST);
            });
        });
    });
});
