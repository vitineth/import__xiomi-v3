"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = __importDefault(require("uuid"));
const AuthenticationUtilities = __importStar(require("../../utilities/AuthenticationUtilities"));
function nextID(data) {
    return Math.max.apply(null, data.map(d => d.id)) + 1;
}
class InMemoryLoggingDAO {
    constructor(data) {
        this.data = data;
    }
    async fetchLoginsByUser(user) {
        const result = [];
        for (const entry of this.data.login) {
            if (entry.userID === user.id)
                result.push(entry);
        }
        return result;
    }
    async logLogin(ipAddress, user) {
        const entry = {
            user,
            ipAddress,
            id: nextID(this.data.login),
            userID: user.id,
            logged: new Date(),
        };
        // @ts-ignore
        this.data.login.push(entry);
        // @ts-ignore
        return entry;
    }
    async saveAuthenticationMessage(source, detail) {
        const entry = {
            source,
            detail,
            id: nextID(this.data.auth),
            logged: new Date(),
        };
        // @ts-ignore
        this.data.auth.push(entry);
        // @ts-ignore
        return entry;
    }
}
exports.InMemoryLoggingDAO = InMemoryLoggingDAO;
class InMemorySecurityQuestionDAO {
    constructor(data) {
        this.data = data;
    }
    async findAllQuestions() {
        // @ts-ignore
        return this.data.questions;
    }
    async findQuestionByIDs(user, id) {
        for (const entry of this.data.mapping) {
            if (entry.userID === user.id && entry.id === id)
                return entry;
        }
        throw new Error('');
    }
}
exports.InMemorySecurityQuestionDAO = InMemorySecurityQuestionDAO;
class InMemoryTokenDAO {
    constructor(data) {
        this.data = data;
    }
    async findPasswordChangeRequest(userToken, token) {
        for (const entry of this.data.reset) {
            if (entry.type === 'password' && entry.userToken === userToken && entry.resetToken === token)
                return entry;
        }
        throw new Error('');
    }
    async findRegistrationToken(userToken, registrationToken) {
        for (const entry of this.data.registration) {
            if (entry.userToken === userToken && entry.resetToken === registrationToken)
                return entry;
        }
        throw new Error('');
    }
    async markChangeComplete(request) {
        for (const entry of this.data.reset) {
            if (entry.id === request.id) {
                entry.completed = true;
                return entry;
            }
        }
        throw new Error('');
    }
    async markRegistrationComplete(request) {
        for (const entry of this.data.registration) {
            if (entry.id === request.id) {
                entry.completed = true;
                return entry;
            }
        }
        throw new Error('');
    }
    async requestPasswordChange(user) {
        const entry = {
            user,
            id: nextID(this.data.reset),
            requested: new Date(),
            userID: user.id,
            userToken: uuid_1.default.v4(),
            resetToken: uuid_1.default.v4(),
            type: 'password',
            completed: false,
            additionalData: '',
        };
        // @ts-ignore
        this.data.reset.push(entry);
        // @ts-ignore
        return entry;
    }
    requestRegistrationToken(user) {
        const entry = {
            user,
            id: nextID(this.data.reset),
            requested: new Date(),
            userID: user.id,
            userToken: uuid_1.default.v4(),
            resetToken: uuid_1.default.v4(),
            completed: false,
        };
        // @ts-ignore
        this.data.registration.push(entry);
        // @ts-ignore
        return entry;
    }
}
exports.InMemoryTokenDAO = InMemoryTokenDAO;
class InMemoryTwoFactorDAO {
    constructor(data) {
        this.data = data;
    }
    async findConfigurationByUser(user) {
        for (const entry of this.data) {
            if (entry.userID === user.id)
                return entry;
        }
        throw new Error('');
    }
}
exports.InMemoryTwoFactorDAO = InMemoryTwoFactorDAO;
class InMemoryUserDAO {
    constructor(data) {
        this.data = data;
    }
    async changeUserPassword(user, hash, hashVersion) {
        for (const entry of this.data) {
            if (entry.id === user.id) {
                entry.hash = hash;
                entry.hashVersion = hashVersion;
                entry.lastRotation = new Date();
                return entry;
            }
        }
        throw new Error('');
    }
    async changeUserPasswordAutohash(user, password) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        return this.changeUserPassword(user, hash, hashVersion);
    }
    async createUser(username, email, name, hash, hashVersion, pin, pinVersion, status) {
        const entry = {
            hash,
            hashVersion,
            pin,
            username: username.toLowerCase(),
            email: email.toLowerCase(),
            full_name: name,
            pinHashVersion: pinVersion,
            lockedStatus: 'auth',
            registered: false,
        };
        // @ts-ignore
        this.data.push(entry);
        // @ts-ignore
        return entry;
    }
    async createUserAutohash(username, email, name, password, pin, status) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        const [pinVersion, pinHash] = await AuthenticationUtilities.hashPin(pin);
        return this.createUser(username, email, name, hash, hashVersion, pinHash, pinVersion, status);
    }
    async findUserByID(id) {
        for (const entry of this.data) {
            if (entry.id === id)
                return entry;
        }
        throw new Error('');
    }
    async findUserByUsername(username) {
        for (const entry of this.data) {
            if (entry.username === username.toLowerCase())
                return entry;
        }
        throw new Error('');
    }
    async isEmailAvailable(email) {
        for (const entry of this.data) {
            if (entry.email === email.toLowerCase())
                return false;
        }
        return true;
    }
    async isUsernameAvailable(username) {
        for (const entry of this.data) {
            if (entry.username === username.toLowerCase())
                return false;
        }
        return true;
    }
    async makeUserAccessible(user) {
        for (const entry of this.data) {
            if (entry.id === user.id) {
                entry.lockedStatus = 'accessible';
                return entry;
            }
        }
        throw new Error('');
    }
}
exports.InMemoryUserDAO = InMemoryUserDAO;
