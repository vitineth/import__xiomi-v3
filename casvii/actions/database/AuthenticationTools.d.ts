import { User } from "../../database/models/User.model";
import { TwoFactorDAO } from "../../database/dao/TwoFactorDAO";
import { SecurityQuestionDAO } from "../../database/dao/SecurityQuestionDAO";
import { UserDAO } from "../../database/dao/UserDAO";
import { LoggingDAO } from "../../database/dao/LoggingDAO";
/**
 * Provides abstraction to the database for authentication utilities. Functions will throw ToolError with a suitable
 * identifier for the problem that occurred.
 */
export declare class AuthenticationTools {
    private twoFactorDAO;
    private securityQuestionDAO;
    private userDAO;
    private loggingDAO;
    constructor(twoFactorDAO: TwoFactorDAO, securityQuestionDAO: SecurityQuestionDAO, userDAO: UserDAO, loggingDAO: LoggingDAO);
    /**
     * Checks if this code works for the given user and their configuration. Will throw an exception with an internal
     * message if this fails.
     *
     * The tool can reject with the following identifiers:
     *
     * - not-found
     * - invalid
     *
     * @param user the user who is trying to authenticate
     * @param code the code the user has provided
     */
    twoFactorAuthenticate(user: User, code: string): Promise<boolean>;
    /**
     * Finishes the authentication when the user has been pushed to use security questions. It will just verify the
     * security question and answer given by the user and returns the result
     *
     * This tool can reject with the following messages:
     *
     * - not-found
     * - invalid
     *
     * @param user the user which is being authenticated
     * @param securityQuestionID the id of the security question which should be tested
     * @param answer the answer the user provided
     * @param ip the ip from which this request originated
     */
    finishSecurityQuestionAuthentication(user: User, securityQuestionID: number, answer: string, ip: string): Promise<boolean>;
    /**
     * Tries to log in the given user with the username and password from a given ip address. This checks the previous
     * log in locations to see if a security question is required and the two factor configuration to see whether a code
     * is required from the user.
     *
     * This tool can reject with the following messages:
     *
     * - not-found
     * - registered
     * - locked
     * - password
     * - no-logins
     * - requires-security-question
     * - 2fa
     *
     * @param username the username of the user to test
     * @param password the password to try against the username
     * @param ipAddress the ip address this user is logging in from
     */
    authenticateUser(username: string, password: string, ipAddress: string): Promise<boolean>;
}
