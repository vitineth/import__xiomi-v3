import { UserDAO } from "../../database/dao/UserDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
export declare class PasswordTools {
    private mailer;
    private userDAO;
    private tokenDAO;
    constructor(mailer: CASEmailProvider, userDAO: UserDAO, tokenDAO: TokenDAO);
    unauthenticatedChangeSubmission(userToken: string, resetToken: string, pin: string, password: string): Promise<void>;
    unauthenticatedChangeRequest(username: string, email: string): Promise<void>;
    changePassword(userID: number, oldPassword: string, newPassword: string, pinCode: string): Promise<void>;
}
