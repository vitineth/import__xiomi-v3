"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTools = void 0;
const ToolError_1 = require("../../errors/ToolError");
const Constants_1 = require("../../utilities/Constants");
const zxcvbn = require("zxcvbn");
class UserTools {
    constructor(mailer, userDAO, tokenDAO, securityQuestionDAO, permissionDAO) {
        this.mailer = mailer;
        this.userDAO = userDAO;
        this.tokenDAO = tokenDAO;
        this.securityQuestionDAO = securityQuestionDAO;
        this.permissionDAO = permissionDAO;
    }
    async completeRegister(userToken, registrationToken) {
        let registration;
        try {
            registration = await this.tokenDAO.findRegistrationToken(userToken, registrationToken);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Registration token could not be found`, `not-found`, e);
        }
        if (registration.completed) {
            throw new ToolError_1.ToolError(`the registration has already been completed`, `expired`);
        }
        if (new Date().getTime() - registration.requested.getTime() > Constants_1.Constants.EXPIRY_TIME_IN_MILLISECONDS) {
            throw new ToolError_1.ToolError(`The request has expired, they should request a new one!`, `expired`);
        }
        try {
            await this.userDAO.makeUserAccessible(registration.user);
            // Make sure they have the default set of permissions!
            await this.permissionDAO.createUserGroupMapping(await this.permissionDAO.findGroupByName(`default`), registration.user);
            await this.tokenDAO.markRegistrationComplete(registration);
            await this.mailer.sendRegistrationComplete(registration.user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to save registration changes`, `failed-save`, e);
        }
    }
    async register(username, email, name, password, pin, security) {
        const iUsername = username.toLowerCase();
        const iEmail = email.toLowerCase();
        if (security.length !== 3) {
            throw new ToolError_1.ToolError(`Need to provide three security questions`, `wrong-number`);
        }
        const usernameAvailable = await this.userDAO.isUsernameAvailable(iUsername);
        const emailAvailable = await this.userDAO.isEmailAvailable(iEmail);
        console.log(`checking ${iUsername} and ${iEmail} produced results ${usernameAvailable} and ${emailAvailable}`);
        if (!usernameAvailable || !emailAvailable) {
            throw new ToolError_1.ToolError(`Username or email is already in use`, `already-used`);
        }
        const scoring = zxcvbn(password);
        if (scoring.score <= 2) {
            throw new ToolError_1.ToolError(`The user did not provide a strong enough password`, `too-weak`, undefined, {
                score: scoring.score.toString(),
                feedback: `${scoring.feedback.warning}, how about: ${scoring.feedback.suggestions.join(', ')}`,
            });
        }
        // Check that the pin is valid
        if (!/^[0-9]+$/.test(pin)) {
            throw new ToolError_1.ToolError(`The pin was not a sequence of digits`, `invalid-pin`);
        }
        // Check that the security questions exist within the database
        const dbQuestions = await this.securityQuestionDAO.findAllQuestions();
        const ids = dbQuestions.map(a => a.id);
        if (!ids.includes(security[0].question) || !ids.includes(security[1].question) || !ids.includes(security[2].question)) {
            throw new ToolError_1.ToolError(`The specified security questions are not in the database!`, `invalid-questions`);
        }
        if (security[0].question === security[1].question || security[0].question === security[2].question || security[1].question === security[2].question) {
            throw new ToolError_1.ToolError(`The picked two of the same security question.`, `duplicate-questions`);
        }
        if (security[0].answer === security[1].answer || security[0].answer === security[2].answer || security[1].answer === security[2].answer) {
            throw new ToolError_1.ToolError(`The picked two of the same security question answers.`, `duplicate-questions`);
        }
        // Then convert them into the ones found in the database
        const mappings = [];
        security.forEach((question) => {
            mappings.push({
                db: dbQuestions.find(e => e.id === question.question),
                answer: question.answer,
            });
        });
        // Then create the user provisionally
        let user;
        try {
            user = await this.userDAO.createUserAutohash(username, email, name, password, pin, 'auth');
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to save the user provisionally!`, `cannot-save-user`, e);
        }
        // Then try and save the questions
        try {
            mappings.filter(e => e.db !== undefined).forEach(e => this.securityQuestionDAO.saveSecurityAnswer(user, e.db, e.answer));
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to save the security questions`, `cannot-save-sq`, e);
        }
        // Then create the registration token
        let token;
        try {
            token = await this.tokenDAO.requestRegistrationToken(user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to save the registration token!`, `cannot-save-registration`, e);
        }
        // TODO: this should not be necessary
        token.user = user;
        try {
            await this.mailer.sendRegistrationEmail(token);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to send the email`, `cannot-email`, e);
        }
    }
}
exports.UserTools = UserTools;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlclRvb2xzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS9hY3Rpb25zL2RhdGFiYXNlL1VzZXJUb29scy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFDQSxzREFBbUQ7QUFPbkQseURBQXNEO0FBR3RELGlDQUFrQztBQUVsQyxNQUFhLFNBQVM7SUFFbEIsWUFBb0IsTUFBd0IsRUFDeEIsT0FBZ0IsRUFDaEIsUUFBa0IsRUFDbEIsbUJBQXdDLEVBQ3hDLGFBQTRCO1FBSjVCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3hCLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDaEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO0lBQ2hELENBQUM7SUFFRCxLQUFLLENBQUMsZ0JBQWdCLENBQUMsU0FBaUIsRUFBRSxpQkFBeUI7UUFDL0QsSUFBSSxZQUErQixDQUFDO1FBQ3BDLElBQUk7WUFDQSxZQUFZLEdBQUcsTUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1NBQzFGO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLElBQUkscUJBQVMsQ0FBQyx1Q0FBdUMsRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEY7UUFFRCxJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7WUFDeEIsTUFBTSxJQUFJLHFCQUFTLENBQUMsNkNBQTZDLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDakY7UUFFRCxJQUFJLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxxQkFBUyxDQUFDLDJCQUEyQixFQUFFO1lBQ2pHLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHlEQUF5RCxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQzdGO1FBRUQsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekQsc0RBQXNEO1lBQ3RELE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4SCxNQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsd0JBQXdCLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0QsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNqRTtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMscUNBQXFDLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hGO0lBQ0wsQ0FBQztJQUVELEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBZ0IsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLFFBQWdCLEVBQUUsR0FBVyxFQUFFLFFBQWdEO1FBQ3pJLE1BQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN6QyxNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFbkMsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN2QixNQUFNLElBQUkscUJBQVMsQ0FBQywwQ0FBMEMsRUFBRSxjQUFjLENBQUMsQ0FBQztTQUNuRjtRQUVELE1BQU0saUJBQWlCLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVFLE1BQU0sY0FBYyxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVuRSxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksU0FBUyxRQUFRLE1BQU0scUJBQXFCLGlCQUFpQixRQUFRLGNBQWMsRUFBRSxDQUFDLENBQUM7UUFFL0csSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZDLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHFDQUFxQyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1NBQzlFO1FBRUQsTUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pDLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDcEIsTUFBTSxJQUFJLHFCQUFTLENBQUMsbURBQW1ELEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRTtnQkFDNUYsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUMvQixRQUFRLEVBQUUsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sZ0JBQWdCLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTthQUNqRyxDQUFDLENBQUM7U0FDTjtRQUVELDhCQUE4QjtRQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QixNQUFNLElBQUkscUJBQVMsQ0FBQyxzQ0FBc0MsRUFBRSxhQUFhLENBQUMsQ0FBQztTQUM5RTtRQUVELDhEQUE4RDtRQUM5RCxNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3RFLE1BQU0sR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNuSCxNQUFNLElBQUkscUJBQVMsQ0FBQywyREFBMkQsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1NBQ3pHO1FBRUQsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUNqSixNQUFNLElBQUkscUJBQVMsQ0FBQywrQ0FBK0MsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1NBQy9GO1FBRUQsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRTtZQUNySSxNQUFNLElBQUkscUJBQVMsQ0FBQyx1REFBdUQsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1NBQ3ZHO1FBRUQsd0RBQXdEO1FBQ3hELE1BQU0sUUFBUSxHQUEyRCxFQUFFLENBQUM7UUFDNUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQ1YsRUFBRSxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0JBQ3JELE1BQU0sRUFBRSxRQUFRLENBQUMsTUFBTTthQUMxQixDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUVILHFDQUFxQztRQUNyQyxJQUFJLElBQVUsQ0FBQztRQUNmLElBQUk7WUFDQSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUN4QyxRQUFRLEVBQ1IsS0FBSyxFQUNMLElBQUksRUFDSixRQUFRLEVBQ1IsR0FBRyxFQUNILE1BQU0sQ0FDVCxDQUFDO1NBQ0w7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdDQUF3QyxFQUFFLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hGO1FBRUQsa0NBQWtDO1FBQ2xDLElBQUk7WUFDQSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxFQUFzQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ2hKO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLElBQUkscUJBQVMsQ0FBQyx1Q0FBdUMsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNyRjtRQUVELHFDQUFxQztRQUNyQyxJQUFJLEtBQXdCLENBQUM7UUFDN0IsSUFBSTtZQUNBLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDOUQ7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdDQUF3QyxFQUFFLDBCQUEwQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hHO1FBRUQscUNBQXFDO1FBQ3JDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBRWxCLElBQUk7WUFDQSxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEQ7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLDBCQUEwQixFQUFFLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN0RTtJQUNMLENBQUM7Q0FFSjtBQW5JRCw4QkFtSUMifQ==