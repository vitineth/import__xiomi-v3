import { UserDAO } from "../../database/dao/UserDAO";
import { PermissionDAO } from "../../database/dao/PermissionDAO";
import { PermissionGroup } from "../../database/models/permissions/PermissionGroup.model";
import { User } from "../../database/models/User.model";
import { Permission } from "../../database/models/permissions/Permission.model";
import { Session } from "../../utilities/session/Session";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
export declare class PermissionTools {
    private mailer;
    private userDAO;
    private permissionDAO;
    constructor(mailer: CASEmailProvider, userDAO: UserDAO, permissionDAO: PermissionDAO);
    /**
     * Checks the permission given in the given session, refreshing the permissions if they haven't been loaded yet or
     * if they haven't been refreshed in 10 requests
     * @param session the session in which we should be searching
     * @param permission the permission to find
     */
    checkPermission(session: Session, permission: string): Promise<boolean>;
    /**
     * Checks the permissions given in the given session, refreshing the permissions if they haven't been loaded yet or
     * if they haven't been refreshed in 10 requests. This will only count as one test
     * @param session the session in which we should be searching
     * @param permissions the permissions to find
     */
    checkPermissions(session: Session, permissions: string[]): Promise<boolean>;
    /**
     * Attempts to grant the given permission to the given user by creating a new {@link PermissionGroupUserMapping}
     * returning the new instance if valid
     * @param userID the user who is receiving the group
     * @param group the group to which this user should belong
     * @param session the session of the user requesting the permission to be added
     */
    grantGroupToUser(userID: number, group: string, session: Session): Promise<void>;
    /**
     * Attempts to ungrant the given permission to the given user by removing the given mapping
     * @param userID the user ID from which the group should be revoked
     * @param group the group which should be removed from this user
     * @param session the session of the user requesting this change
     */
    ungrantGroupToUser(userID: number, group: string, session: Session): Promise<void>;
    /**
     * Removes the given permission group and user pairing from the database if it exists. If the mapping
     * does not exist, the promise will reject
     * @param group the group that this user inherits permissions from
     * @param user the user in this mapping
     */
    ungrantPermissionGroupFromUser(group: PermissionGroup, user: User): Promise<void>;
    /**
     * Resolves the full list of permissions for the given user returned as an array of permissions
     * @param user the user to search
     */
    resolveAllPermissions(user: User): Promise<Permission[]>;
    /**
     * Shorthand for {@link resolveAllPermissions} which dispatches a call to {@link UserDAO.findUserByID} to resolve
     * the user from ID
     * @param userID the ID to lookup to find a user
     */
    resolveAllUserIDPermissions(userID: number): Promise<Permission[]>;
    activateSelfPermissionGrant(grantID: number, session: Session): Promise<void>;
    /**
     * Return the list of permissions from a self grant ID which can be displayed to the user. This should only provide
     * directly obtained permissions to stop them from getting the entire tree of shit
     * @param id the id of the self grant
     */
    getPermissionsFromSelfGrantID(id: number): Promise<Permission[]>;
}
