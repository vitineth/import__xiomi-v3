"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationTools = void 0;
const otplib_1 = require("otplib");
const AuthenticationUtilities = __importStar(require("../../utilities/AuthenticationUtilities"));
const AuthenticationUtilities_1 = require("../../utilities/AuthenticationUtilities");
const ToolError_1 = require("../../errors/ToolError");
/**
 * Provides abstraction to the database for authentication utilities. Functions will throw ToolError with a suitable
 * identifier for the problem that occurred.
 */
class AuthenticationTools {
    constructor(twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO) {
        this.twoFactorDAO = twoFactorDAO;
        this.securityQuestionDAO = securityQuestionDAO;
        this.userDAO = userDAO;
        this.loggingDAO = loggingDAO;
    }
    /**
     * Checks if this code works for the given user and their configuration. Will throw an exception with an internal
     * message if this fails.
     *
     * The tool can reject with the following identifiers:
     *
     * - not-found
     * - invalid
     *
     * @param user the user who is trying to authenticate
     * @param code the code the user has provided
     */
    async twoFactorAuthenticate(user, code) {
        let twoFactorConfiguration;
        try {
            twoFactorConfiguration = await this.twoFactorDAO.findConfigurationByUser(user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to find a two factor configuration for this user`, `not-found`, e);
        }
        if (!otplib_1.authenticator.check(code, twoFactorConfiguration.secret)) {
            if (twoFactorConfiguration.emailOnFail) {
                throw new ToolError_1.ToolError(`The user provided an invalid code and requested an email!`, `invalid-with-email`);
            }
            throw new ToolError_1.ToolError(`The user provided an invalid code!`, `invalid`);
        }
        return true;
    }
    /**
     * Finishes the authentication when the user has been pushed to use security questions. It will just verify the
     * security question and answer given by the user and returns the result
     *
     * This tool can reject with the following messages:
     *
     * - not-found
     * - invalid
     *
     * @param user the user which is being authenticated
     * @param securityQuestionID the id of the security question which should be tested
     * @param answer the answer the user provided
     * @param ip the ip from which this request originated
     */
    async finishSecurityQuestionAuthentication(user, securityQuestionID, answer, ip) {
        let securityQuestion;
        try {
            securityQuestion = await this.securityQuestionDAO.findQuestionByIDs(user, securityQuestionID);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to find the security question for this user, wrong combination`, `not-found`, e);
        }
        // We do this the same way as passwords we just need to separate out the components
        const index = securityQuestion.answerHash.indexOf(':');
        const version = parseInt(securityQuestion.answerHash.substr(0, index), 10);
        const hash = securityQuestion.answerHash.substr(index + 1);
        if (!(await AuthenticationUtilities_1.testPasswordHash(version, hash, answer))) {
            throw new ToolError_1.ToolError(`Failed to verify the users response! They probably fucked up the answer.`, `invalid`);
        }
        try {
            await this.twoFactorDAO.findConfigurationByUser(user);
        }
        catch (e) {
            await this.loggingDAO.logLogin(ip, user);
            return true;
        }
        throw new ToolError_1.ToolError(`The user authenticated successfully but needs to log in with two factor authentication`, `2fa`);
    }
    /**
     * Tries to log in the given user with the username and password from a given ip address. This checks the previous
     * log in locations to see if a security question is required and the two factor configuration to see whether a code
     * is required from the user.
     *
     * This tool can reject with the following messages:
     *
     * - not-found
     * - registered
     * - locked
     * - password
     * - no-logins
     * - requires-security-question
     * - 2fa
     *
     * @param username the username of the user to test
     * @param password the password to try against the username
     * @param ipAddress the ip address this user is logging in from
     */
    async authenticateUser(username, password, ipAddress) {
        let user;
        try {
            user = await this.userDAO.findUserByUsername(username);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`The requested user does not exist`, `not-found`, e);
        }
        if (user.registered !== true) {
            throw new ToolError_1.ToolError(`The user has not completed their registration.`, `registered`);
        }
        if (user.lockedStatus !== `accessible`) {
            throw new ToolError_1.ToolError(`The user has had their account locked for some reason`, `locked`);
        }
        if (!await AuthenticationUtilities.testPassword(user, password)) {
            throw new ToolError_1.ToolError(`The user provided the wrong password`, `password`);
        }
        // Lookup the logins that this user has done
        let loginList;
        try {
            loginList = await this.loggingDAO.fetchLoginsByUser(user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to lookup any logins that this user has previously done`, `no-logins`, e);
        }
        // Then convert this to a list of IP addresses so we can see if this is new login
        // or if this is somewhere we are used to
        const previousLoginIPs = loginList.map(value => value.ipAddress);
        if (!previousLoginIPs.includes(ipAddress)) {
            throw new ToolError_1.ToolError(`This is a new login that required a security question to be answered`, `requires-security-question`);
        }
        try {
            await this.twoFactorDAO.findConfigurationByUser(user);
        }
        catch (e) {
            await this.loggingDAO.logLogin(ipAddress, user);
            return true;
        }
        throw new ToolError_1.ToolError(`The user authenticated successfully but needs to log in with two factor authentication`, `2fa`);
    }
}
exports.AuthenticationTools = AuthenticationTools;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aGVudGljYXRpb25Ub29scy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvYWN0aW9ucy9kYXRhYmFzZS9BdXRoZW50aWNhdGlvblRvb2xzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxtQ0FBdUM7QUFDdkMsaUdBQW1GO0FBQ25GLHFGQUEyRTtBQUMzRSxzREFBbUQ7QUFPbkQ7OztHQUdHO0FBQ0gsTUFBYSxtQkFBbUI7SUFFNUIsWUFBb0IsWUFBMEIsRUFDMUIsbUJBQXdDLEVBQ3hDLE9BQWdCLEVBQ2hCLFVBQXNCO1FBSHRCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsWUFBTyxHQUFQLE9BQU8sQ0FBUztRQUNoQixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzFDLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7T0FXRztJQUNILEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxJQUFVLEVBQUUsSUFBWTtRQUNoRCxJQUFJLHNCQUE4QyxDQUFDO1FBRW5ELElBQUk7WUFDQSxzQkFBc0IsR0FBRyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEY7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHlEQUF5RCxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNsRztRQUVELElBQUksQ0FBQyxzQkFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsc0JBQXNCLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDM0QsSUFBSSxzQkFBc0IsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3BDLE1BQU0sSUFBSSxxQkFBUyxDQUFDLDJEQUEyRCxFQUFFLG9CQUFvQixDQUFDLENBQUM7YUFDMUc7WUFFRCxNQUFNLElBQUkscUJBQVMsQ0FBQyxvQ0FBb0MsRUFBRSxTQUFTLENBQUMsQ0FBQztTQUN4RTtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7OztPQWFHO0lBQ0gsS0FBSyxDQUFDLG9DQUFvQyxDQUFDLElBQVUsRUFBRSxrQkFBMEIsRUFBRSxNQUFjLEVBQUUsRUFBVTtRQUN6RyxJQUFJLGdCQUFnQixDQUFDO1FBRXJCLElBQUk7WUFDQSxnQkFBZ0IsR0FBRyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztTQUNqRztRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMsdUVBQXVFLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hIO1FBRUQsbUZBQW1GO1FBQ25GLE1BQU0sS0FBSyxHQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkQsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNFLE1BQU0sSUFBSSxHQUFHLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBRTNELElBQUksQ0FBQyxDQUFDLE1BQU0sMENBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFFO1lBQ2xELE1BQU0sSUFBSSxxQkFBUyxDQUFDLDBFQUEwRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1NBQzlHO1FBRUQsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6RDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFekMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdGQUF3RixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3pILENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O09Ba0JHO0lBQ0gsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxTQUFpQjtRQUN4RSxJQUFJLElBQUksQ0FBQztRQUVULElBQUk7WUFDQSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzFEO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLElBQUkscUJBQVMsQ0FBQyxtQ0FBbUMsRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDNUU7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO1lBQzFCLE1BQU0sSUFBSSxxQkFBUyxDQUFDLGdEQUFnRCxFQUFFLFlBQVksQ0FBQyxDQUFDO1NBQ3ZGO1FBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLFlBQVksRUFBRTtZQUNwQyxNQUFNLElBQUkscUJBQVMsQ0FBQyx1REFBdUQsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUMxRjtRQUVELElBQUksQ0FBQyxNQUFNLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLEVBQUU7WUFDN0QsTUFBTSxJQUFJLHFCQUFTLENBQUMsc0NBQXNDLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDM0U7UUFFRCw0Q0FBNEM7UUFFNUMsSUFBSSxTQUFTLENBQUM7UUFFZCxJQUFJO1lBQ0EsU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3RDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMsZ0VBQWdFLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3pHO1FBRUQsaUZBQWlGO1FBQ2pGLHlDQUF5QztRQUV6QyxNQUFNLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFakUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN2QyxNQUFNLElBQUkscUJBQVMsQ0FBQyxzRUFBc0UsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO1NBQzdIO1FBRUQsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6RDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFaEQsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdGQUF3RixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3pILENBQUM7Q0FFSjtBQXpKRCxrREF5SkMifQ==