"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionTools = void 0;
const ToolError_1 = require("../../errors/ToolError");
const _ = __importStar(require("logger"));
const AuthenticationProcessImpl_1 = require("../process/AuthenticationProcessImpl");
class PermissionTools {
    constructor(mailer, userDAO, permissionDAO) {
        this.mailer = mailer;
        this.userDAO = userDAO;
        this.permissionDAO = permissionDAO;
    }
    /**
     * Checks the permission given in the given session, refreshing the permissions if they haven't been loaded yet or
     * if they haven't been refreshed in 10 requests
     * @param session the session in which we should be searching
     * @param permission the permission to find
     */
    checkPermission(session, permission) {
        return this.checkPermissions(session, [permission]);
    }
    /**
     * Checks the permissions given in the given session, refreshing the permissions if they haven't been loaded yet or
     * if they haven't been refreshed in 10 requests. This will only count as one test
     * @param session the session in which we should be searching
     * @param permissions the permissions to find
     */
    async checkPermissions(session, permissions) {
        if (!session.has(`login.userID`))
            return false;
        if (!session.has(`login.provisional`))
            return false;
        if (session.get(`login.provisional`) !== AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE)
            return false;
        // Increment their permission count because we are checking one
        if (!session.has(`user.permission-verifications`)) {
            session.set(`user.permission-verifications`, 1);
        }
        else {
            session.set(`user.permission-verifications`, session.get(`user.permission-verifications`) + 1);
        }
        try {
            // If their permissions aren't in the session or they need to refresh them, then set them in the session
            if (!session.has(`user.permissions`) || session.get(`user.permission-verifications`) >= 10) {
                session.set(`user.permissions`, await this.resolveAllUserIDPermissions(session.get(`login.userID`)));
                session.set(`user.permission-verifications`, 0);
            }
        }
        catch (e) {
            _.error(`failed to load the permissions for this user`, { e });
            console.error(e);
            return false;
        }
        const userPermissions = session.get(`user.permissions`);
        const userPermissionNames = userPermissions.map(e => e.name);
        const result = !permissions.map(e => userPermissionNames.includes(e)).includes(false);
        if (result) {
            _.debug(`looked for permissions ${permissions} in session for ID ${session.get(`login.userID`)} and found it`);
        }
        else {
            _.debug(`looked for permissions ${permissions} in session for ID ${session.get(`login.userID`)} and did not find it`);
        }
        return result;
    }
    /**
     * Attempts to grant the given permission to the given user by creating a new {@link PermissionGroupUserMapping}
     * returning the new instance if valid
     * @param userID the user who is receiving the group
     * @param group the group to which this user should belong
     * @param session the session of the user requesting the permission to be added
     */
    async grantGroupToUser(userID, group, session) {
        if (session.get(`login.userID`) === userID) {
            if (!await this.checkPermission(session, `casvii.permission.grant.self`)) {
                throw new ToolError_1.ToolError(`User does not have the necessary permissions to change their own positions`, `no-permission`);
            }
        }
        else {
            if (!await this.checkPermission(session, `casvii.permission.grant.other`)) {
                throw new ToolError_1.ToolError(`User does not have the necessary permissions to change other users positions`, `no-permission`);
            }
        }
        const user = await this.userDAO.findUserByID(userID);
        const groupInstance = await this.permissionDAO.findGroupByName(group);
        try {
            await this.permissionDAO.findUserGroupMapping(groupInstance, user);
            // noinspection ExceptionCaughtLocallyJS
            throw new ToolError_1.ToolError(`User already has the group requested`, `already-granted`);
        }
        catch (e) {
            // If it threw a tool error we want to rethrow it, otherwise we want to continue because it means that it
            // wasn't found
            if (e instanceof ToolError_1.ToolError)
                throw e;
        }
        await this.permissionDAO.createUserGroupMapping(groupInstance, user);
    }
    /**
     * Attempts to ungrant the given permission to the given user by removing the given mapping
     * @param userID the user ID from which the group should be revoked
     * @param group the group which should be removed from this user
     * @param session the session of the user requesting this change
     */
    async ungrantGroupToUser(userID, group, session) {
        if (session.get(`login.userID`) === userID) {
            if (!await this.checkPermission(session, `casvii.permission.ungrant.self`)) {
                throw new ToolError_1.ToolError(`User does not have the necessary permissions to change their own positions`, `no-permission`);
            }
        }
        else {
            if (!await this.checkPermission(session, `casvii.permission.ungrant.other`)) {
                throw new ToolError_1.ToolError(`User does not have the necessary permissions to change other users positions`, `no-permission`);
            }
        }
        const user = await this.userDAO.findUserByID(userID);
        const groupInstance = await this.permissionDAO.findGroupByName(group);
        try {
            const instance = await this.permissionDAO.findUserGroupMapping(groupInstance, user);
            await this.permissionDAO.removeUserGroupMapping(instance);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`User does not have the group requested`, `not-granted`, e);
        }
    }
    /**
     * Removes the given permission group and user pairing from the database if it exists. If the mapping
     * does not exist, the promise will reject
     * @param group the group that this user inherits permissions from
     * @param user the user in this mapping
     */
    async ungrantPermissionGroupFromUser(group, user) {
        const mapping = await this.permissionDAO.findUserGroupMapping(group, user);
        return mapping.destroy();
    }
    /**
     * Resolves the full list of permissions for the given user returned as an array of permissions
     * @param user the user to search
     */
    async resolveAllPermissions(user) {
        return await this.permissionDAO.resolvePermissions(user);
    }
    /**
     * Shorthand for {@link resolveAllPermissions} which dispatches a call to {@link UserDAO.findUserByID} to resolve
     * the user from ID
     * @param userID the ID to lookup to find a user
     */
    async resolveAllUserIDPermissions(userID) {
        return this.resolveAllPermissions(await this.userDAO.findUserByID(userID));
    }
    async activateSelfPermissionGrant(grantID, session) {
        if (!await this.checkPermission(session, `casvii.permission.self-grant`)) {
            throw new ToolError_1.ToolError(`User does not have the self-grant permission so cannot use these buttons`, `no-permission`);
        }
        const user = await this.userDAO.findUserByID(session.get('login.userID'));
        let grant;
        try {
            grant = await this.permissionDAO.findSelfAuthorizationGrantByID(grantID);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`The requested self grant doesn't exist`, `invalid-grant`, e);
        }
        try {
            await this.permissionDAO.findUserGroupMapping(grant.group, user);
            // noinspection ExceptionCaughtLocallyJS
            throw new ToolError_1.ToolError(`User already has the group requested`, `already-granted`);
        }
        catch (e) {
            // If it threw a tool error we want to rethrow it, otherwise we want to continue because it means that it
            // wasn't found
            if (e instanceof ToolError_1.ToolError)
                throw e;
        }
        await this.mailer.sendAuthorizeEmail(user, grant);
        await this.permissionDAO.createUserGroupMapping(grant.group, user);
    }
    /**
     * Return the list of permissions from a self grant ID which can be displayed to the user. This should only provide
     * directly obtained permissions to stop them from getting the entire tree of shit
     * @param id the id of the self grant
     */
    async getPermissionsFromSelfGrantID(id) {
        const grant = await this.permissionDAO.findSelfAuthorizationGrantByID(id);
        const permissions = await this.permissionDAO.findPermissionByGroupID(grant.groupID);
        return permissions;
    }
}
exports.PermissionTools = PermissionTools;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGVybWlzc2lvblRvb2xzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS9hY3Rpb25zL2RhdGFiYXNlL1Blcm1pc3Npb25Ub29scy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBT0Esc0RBQW1EO0FBQ25ELDBDQUE0QjtBQUM1QixvRkFBdUY7QUFHdkYsTUFBYSxlQUFlO0lBRXhCLFlBQW9CLE1BQXdCLEVBQ3hCLE9BQWdCLEVBQ2hCLGFBQTRCO1FBRjVCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3hCLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDaEIsa0JBQWEsR0FBYixhQUFhLENBQWU7SUFDaEQsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsZUFBZSxDQUFDLE9BQWdCLEVBQUUsVUFBa0I7UUFDaEQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxLQUFLLENBQUMsZ0JBQWdCLENBQUMsT0FBZ0IsRUFBRSxXQUFxQjtRQUMxRCxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUM7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUMvQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQztZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQ3BELElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLDJEQUErQixDQUFDLE1BQU07WUFBRSxPQUFPLEtBQUssQ0FBQztRQUU5RiwrREFBK0Q7UUFDL0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsRUFBRTtZQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25EO2FBQU07WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNsRztRQUVELElBQUk7WUFDQSx3R0FBd0c7WUFDeEcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLElBQUksRUFBRSxFQUFFO2dCQUN4RixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sSUFBSSxDQUFDLDJCQUEyQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRyxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ25EO1NBQ0o7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLENBQUMsQ0FBQyxLQUFLLENBQUMsOENBQThDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9ELE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxNQUFNLGVBQWUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFpQixDQUFDO1FBQ3hFLE1BQU0sbUJBQW1CLEdBQUcsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3RCxNQUFNLE1BQU0sR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdEYsSUFBSSxNQUFNLEVBQUU7WUFDUixDQUFDLENBQUMsS0FBSyxDQUFDLDBCQUEwQixXQUFXLHNCQUFzQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNsSDthQUFNO1lBQ0gsQ0FBQyxDQUFDLEtBQUssQ0FBQywwQkFBMEIsV0FBVyxzQkFBc0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUN6SDtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSCxLQUFLLENBQUMsZ0JBQWdCLENBQUMsTUFBYyxFQUFFLEtBQWEsRUFBRSxPQUFnQjtRQUNsRSxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEtBQUssTUFBTSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLDhCQUE4QixDQUFDLEVBQUU7Z0JBQ3RFLE1BQU0sSUFBSSxxQkFBUyxDQUFDLDRFQUE0RSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2FBQ3RIO1NBQ0o7YUFBTTtZQUNILElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLCtCQUErQixDQUFDLEVBQUU7Z0JBQ3ZFLE1BQU0sSUFBSSxxQkFBUyxDQUFDLDhFQUE4RSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2FBQ3hIO1NBQ0o7UUFFRCxNQUFNLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JELE1BQU0sYUFBYSxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdEUsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFbkUsd0NBQXdDO1lBQ3hDLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHNDQUFzQyxFQUFFLGlCQUFpQixDQUFDLENBQUM7U0FDbEY7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLHlHQUF5RztZQUN6RyxlQUFlO1lBQ2YsSUFBSSxDQUFDLFlBQVkscUJBQVM7Z0JBQUUsTUFBTSxDQUFDLENBQUM7U0FDdkM7UUFFRCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxNQUFjLEVBQUUsS0FBYSxFQUFFLE9BQWdCO1FBQ3BFLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsS0FBSyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsZ0NBQWdDLENBQUMsRUFBRTtnQkFDeEUsTUFBTSxJQUFJLHFCQUFTLENBQUMsNEVBQTRFLEVBQUUsZUFBZSxDQUFDLENBQUM7YUFDdEg7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsaUNBQWlDLENBQUMsRUFBRTtnQkFDekUsTUFBTSxJQUFJLHFCQUFTLENBQUMsOEVBQThFLEVBQUUsZUFBZSxDQUFDLENBQUM7YUFDeEg7U0FDSjtRQUVELE1BQU0sSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckQsTUFBTSxhQUFhLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUV0RSxJQUFJO1lBQ0EsTUFBTSxRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNwRixNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDN0Q7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdDQUF3QyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNuRjtJQUNMLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxLQUFzQixFQUFFLElBQVU7UUFDbkUsTUFBTSxPQUFPLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzRSxPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsS0FBSyxDQUFDLHFCQUFxQixDQUFDLElBQVU7UUFDbEMsT0FBTyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxLQUFLLENBQUMsMkJBQTJCLENBQUMsTUFBYztRQUM1QyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVELEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxPQUFlLEVBQUUsT0FBZ0I7UUFDL0QsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsOEJBQThCLENBQUMsRUFBRTtZQUN0RSxNQUFNLElBQUkscUJBQVMsQ0FBQywwRUFBMEUsRUFBRSxlQUFlLENBQUMsQ0FBQztTQUNwSDtRQUVELE1BQU0sSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBRTFFLElBQUksS0FBSyxDQUFDO1FBRVYsSUFBSTtZQUNBLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsOEJBQThCLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDNUU7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHdDQUF3QyxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNyRjtRQUVELElBQUk7WUFDQSxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztZQUVqRSx3Q0FBd0M7WUFDeEMsTUFBTSxJQUFJLHFCQUFTLENBQUMsc0NBQXNDLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztTQUNsRjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IseUdBQXlHO1lBQ3pHLGVBQWU7WUFDZixJQUFJLENBQUMsWUFBWSxxQkFBUztnQkFBRSxNQUFNLENBQUMsQ0FBQztTQUN2QztRQUVELE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEQsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxLQUFLLENBQUMsNkJBQTZCLENBQUMsRUFBVTtRQUMxQyxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsOEJBQThCLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDMUUsTUFBTSxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVwRixPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDO0NBRUo7QUFqTUQsMENBaU1DIn0=