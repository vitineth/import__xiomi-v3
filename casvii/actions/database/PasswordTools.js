"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordTools = void 0;
const ToolError_1 = require("../../errors/ToolError");
const AuthenticationUtilities = __importStar(require("../../utilities/AuthenticationUtilities"));
const zxcvbn = require("zxcvbn");
const Constants_1 = require("../../utilities/Constants");
class PasswordTools {
    constructor(mailer, userDAO, tokenDAO) {
        this.mailer = mailer;
        this.userDAO = userDAO;
        this.tokenDAO = tokenDAO;
    }
    async unauthenticatedChangeSubmission(userToken, resetToken, pin, password) {
        let reset;
        try {
            // Fetch the reset request for the user using the two provided pieces of data
            reset = await this.tokenDAO.findPasswordChangeRequest(userToken, resetToken);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to find the reset data request for this token`, `no-request`, e);
        }
        if (reset.completed) {
            throw new ToolError_1.ToolError(`The request has already been used!`, `already-used`);
        }
        if (new Date().getTime() - reset.requested.getTime() > Constants_1.Constants.EXPIRY_TIME_IN_MILLISECONDS) {
            throw new ToolError_1.ToolError(`The request has expired, they should request a new one!`, `expired`);
        }
        const scoring = zxcvbn(password);
        if (scoring.score <= 2) {
            throw new ToolError_1.ToolError(`The user did not provide a strong enough password`, `too-weak`, undefined, {
                score: scoring.score,
                feedback: scoring.feedback,
            });
        }
        const user = reset.user;
        if (!await AuthenticationUtilities.testPin(user, pin)) {
            await this.mailer.sendPasswordChangeSecurityWarning(user);
            throw new ToolError_1.ToolError(`The user fucked up their pin`, `invalid-pin`);
        }
        try {
            await this.tokenDAO.markChangeComplete(reset);
            await this.userDAO.changeUserPasswordAutohash(user, password);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Could not save the user or the reset token!`, `cannot-save`, e);
        }
        // Email them!
        await this.mailer.sendPasswordChangeNotice(user);
    }
    async unauthenticatedChangeRequest(username, email) {
        let user;
        try {
            user = await this.userDAO.findUserByUsername(username);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`The requested user does not exist`, `not-found`, e);
        }
        if (user.email.toLowerCase() !== email.toLowerCase()) {
            throw new ToolError_1.ToolError(`The user provided an email which did not match the email on the account`, `invalid-email`);
        }
        let request;
        try {
            request = await this.tokenDAO.requestPasswordChange(user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to create a reset data request for this user and their password`, `failed-save-request`, e);
        }
        // TODO<vitineth@gmail.com> This should be sorted above but idk whats happening
        request.user = user;
        // Try to send the email
        const mailStatus = await this.mailer.sendPasswordChangeRequest(request);
        // And check that it was sent correctly
        if (!mailStatus) {
            throw new ToolError_1.ToolError(`Change request was submitted and saved for the user ${user.username} but could not email them!`, `failed-email`);
        }
    }
    async changePassword(userID, oldPassword, newPassword, pinCode) {
        let user;
        try {
            user = await this.userDAO.findUserByID(userID);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`The requested user does not exist`, `not-found`, e);
        }
        const scoring = zxcvbn(newPassword);
        if (scoring.score <= 2) {
            throw new ToolError_1.ToolError(`The user did not provide a strong enough password`, `too-weak`, undefined, {
                score: scoring.score,
                feedback: scoring.feedback,
            });
        }
        if (!await AuthenticationUtilities.testPassword(user, oldPassword)) {
            await this.mailer.sendPasswordChangeSecurityWarning(user);
            throw new ToolError_1.ToolError(`The user fucked up and provided an invalid password`, `wrong-old`);
        }
        if (!await AuthenticationUtilities.testPin(user, pinCode)) {
            await this.mailer.sendPasswordChangeSecurityWarning(user);
            throw new ToolError_1.ToolError(`The user fucked up their pin`, `invalid-pin`);
        }
        try {
            await this.userDAO.changeUserPasswordAutohash(user, newPassword);
            await this.mailer.sendPasswordChangeNotice(user);
        }
        catch (e) {
            throw new ToolError_1.ToolError(`Failed to save the user with updated credentials`, `failed-save`, e);
        }
    }
}
exports.PasswordTools = PasswordTools;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGFzc3dvcmRUb29scy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvYWN0aW9ucy9kYXRhYmFzZS9QYXNzd29yZFRvb2xzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxzREFBbUQ7QUFJbkQsaUdBQW1GO0FBR25GLGlDQUFrQztBQUNsQyx5REFBc0Q7QUFFdEQsTUFBYSxhQUFhO0lBRXRCLFlBQW9CLE1BQXdCLEVBQ3hCLE9BQWdCLEVBQ2hCLFFBQWtCO1FBRmxCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3hCLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDaEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtJQUN0QyxDQUFDO0lBRUQsS0FBSyxDQUFDLCtCQUErQixDQUFDLFNBQWlCLEVBQUUsVUFBa0IsRUFBRSxHQUFXLEVBQUUsUUFBZ0I7UUFDdEcsSUFBSSxLQUF1QixDQUFDO1FBQzVCLElBQUk7WUFDQSw2RUFBNkU7WUFDN0UsS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDaEY7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHNEQUFzRCxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNoRztRQUVELElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtZQUNqQixNQUFNLElBQUkscUJBQVMsQ0FBQyxvQ0FBb0MsRUFBRSxjQUFjLENBQUMsQ0FBQztTQUM3RTtRQUVELElBQUksSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxHQUFHLHFCQUFTLENBQUMsMkJBQTJCLEVBQUU7WUFDMUYsTUFBTSxJQUFJLHFCQUFTLENBQUMseURBQXlELEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDN0Y7UUFFRCxNQUFNLE9BQU8sR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakMsSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNwQixNQUFNLElBQUkscUJBQVMsQ0FBQyxtREFBbUQsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFO2dCQUM1RixLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUs7Z0JBQ3BCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUTthQUM3QixDQUFDLENBQUM7U0FDTjtRQUVELE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLE1BQU0sdUJBQXVCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRTtZQUNuRCxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsaUNBQWlDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUQsTUFBTSxJQUFJLHFCQUFTLENBQUMsOEJBQThCLEVBQUUsYUFBYSxDQUFDLENBQUM7U0FDdEU7UUFFRCxJQUFJO1lBQ0EsTUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDakU7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE1BQU0sSUFBSSxxQkFBUyxDQUFDLDZDQUE2QyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4RjtRQUVELGNBQWM7UUFDZCxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxRQUFnQixFQUFFLEtBQWE7UUFDOUQsSUFBSSxJQUFJLENBQUM7UUFFVCxJQUFJO1lBQ0EsSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMxRDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMsbUNBQW1DLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzVFO1FBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxLQUFLLEtBQUssQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUNsRCxNQUFNLElBQUkscUJBQVMsQ0FBQyx5RUFBeUUsRUFBRSxlQUFlLENBQUMsQ0FBQztTQUNuSDtRQUVELElBQUksT0FBeUIsQ0FBQztRQUU5QixJQUFJO1lBQ0EsT0FBTyxHQUFHLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3RDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMsd0VBQXdFLEVBQUUscUJBQXFCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDM0g7UUFFRCwrRUFBK0U7UUFDL0UsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFFcEIsd0JBQXdCO1FBQ3hCLE1BQU0sVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV4RSx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNiLE1BQU0sSUFBSSxxQkFBUyxDQUFDLHVEQUF1RCxJQUFJLENBQUMsUUFBUSw0QkFBNEIsRUFBRSxjQUFjLENBQUMsQ0FBQztTQUN6STtJQUNMLENBQUM7SUFFRCxLQUFLLENBQUMsY0FBYyxDQUFDLE1BQWMsRUFBRSxXQUFtQixFQUFFLFdBQW1CLEVBQUUsT0FBZTtRQUMxRixJQUFJLElBQUksQ0FBQztRQUVULElBQUk7WUFDQSxJQUFJLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNsRDtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsTUFBTSxJQUFJLHFCQUFTLENBQUMsbUNBQW1DLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzVFO1FBRUQsTUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BDLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDcEIsTUFBTSxJQUFJLHFCQUFTLENBQUMsbURBQW1ELEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRTtnQkFDNUYsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLO2dCQUNwQixRQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVE7YUFDN0IsQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLENBQUMsTUFBTSx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxFQUFFO1lBQ2hFLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxRCxNQUFNLElBQUkscUJBQVMsQ0FBQyxxREFBcUQsRUFBRSxXQUFXLENBQUMsQ0FBQztTQUMzRjtRQUVELElBQUksQ0FBQyxNQUFNLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDdkQsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGlDQUFpQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFELE1BQU0sSUFBSSxxQkFBUyxDQUFDLDhCQUE4QixFQUFFLGFBQWEsQ0FBQyxDQUFDO1NBQ3RFO1FBRUQsSUFBSTtZQUNBLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDakUsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BEO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDUixNQUFNLElBQUkscUJBQVMsQ0FBQyxrREFBa0QsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDN0Y7SUFDTCxDQUFDO0NBRUo7QUFySEQsc0NBcUhDIn0=