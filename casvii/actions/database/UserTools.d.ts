import { UserDAO } from "../../database/dao/UserDAO";
import { TokenDAO } from "../../database/dao/TokenDAO";
import { SecurityQuestionDAO } from "../../database/dao/SecurityQuestionDAO";
import { CASEmailProvider } from "../../mail/CASEmailProvider";
import { PermissionDAO } from "../../database/dao/PermissionDAO";
export declare class UserTools {
    private mailer;
    private userDAO;
    private tokenDAO;
    private securityQuestionDAO;
    private permissionDAO;
    constructor(mailer: CASEmailProvider, userDAO: UserDAO, tokenDAO: TokenDAO, securityQuestionDAO: SecurityQuestionDAO, permissionDAO: PermissionDAO);
    completeRegister(userToken: string, registrationToken: string): Promise<void>;
    register(username: string, email: string, name: string, password: string, pin: string, security: {
        question: number;
        answer: string;
    }[]): Promise<void>;
}
