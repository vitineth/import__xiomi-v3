"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpStatusCode = exports.ProcessImpl = void 0;
const ToolError_1 = require("../errors/ToolError");
const CASResponse_1 = require("../utilities/CASResponse");
const LoggingUtilities_1 = require("../utilities/LoggingUtilities");
const WebUtilities_1 = require("../utilities/WebUtilities");
const Constants_1 = require("../utilities/Constants");
var logAuthenticationResponseShortcut = LoggingUtilities_1.LoggingUtilities.logAuthenticationResponseShortcut;
var requestContainsKeys = WebUtilities_1.RequestUtilities.requestContainsKeys;
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
class ProcessImpl {
    constructor(logging) {
        this.logging = logging;
    }
    async passthroughLog(code, internal, external, data) {
        return logAuthenticationResponseShortcut(this.logging, new CASResponse_1.CASResponse(code, internal, external, data));
    }
    async verifyDataElements(data, requiredKeys, optionalKeys = []) {
        const containsRequired = requestContainsKeys(data, requiredKeys, optionalKeys);
        if (typeof (containsRequired) === 'string') {
            return this.passthroughLog(400, containsRequired, PUBLIC_RESPONSES.INVALID_REQUEST);
        }
        return undefined;
    }
    /**
     * Runs the given tool (awaiting it) through the action function. On success it will return a passthrough log
     * returning 200 and logging that the action name was done successfully, logging an additional data. On error, it
     * automatically log any non-tool errors but will dispatch to the handlers if one is found for the error identifier.
     * @param action the action to execute (expected to be an async function as it is awaited) with the result discarded
     * @param actionName the name of the tool that is being run in a human readable format
     * @param publicSuccess the message to log on success
     * @param additional any additional data to log when performing any log actions
     * @param handlers an object of handlers for tool errors
     */
    async runTool(action, actionName, publicSuccess, additional, handlers) {
        try {
            await action();
            return this.passthroughLog(200, `Successfully performed ${actionName}`, publicSuccess, additional);
        }
        catch (e) {
            // If it's not a tool error, we need to raise why
            if (!(e instanceof ToolError_1.ToolError)) {
                console.log(e);
                return this.passthroughLog(500, `Failed to execute ${actionName} tool due to an error that was not a tool error`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
            }
            if (handlers.hasOwnProperty(e.identifier)) {
                return handlers[e.identifier](e);
            }
            return this.passthroughLog(500, `Received an unexpected error identifier '${e.identifier}' from the ${actionName} tool`, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR, { error: e });
        }
    }
}
exports.ProcessImpl = ProcessImpl;
let HttpStatusCode = /** @class */ (() => {
    class HttpStatusCode {
    }
    HttpStatusCode.SUCCESS = 200;
    HttpStatusCode.REDIRECT = 302;
    HttpStatusCode.BAD_REQUEST = 400;
    HttpStatusCode.UNAUTHORIZED = 401;
    HttpStatusCode.FORBIDDEN = 403;
    HttpStatusCode.NOT_FOUND = 404;
    HttpStatusCode.INTERNAL_SERVER_ERROR = 500;
    HttpStatusCode.SERVICE_UNAVAILABLE = 503;
    return HttpStatusCode;
})();
exports.HttpStatusCode = HttpStatusCode;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJvY2Vzc0ltcGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL2FjdGlvbnMvUHJvY2Vzc0ltcGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsbURBQWdEO0FBRWhELDBEQUF1RDtBQUN2RCxvRUFBaUU7QUFDakUsNERBQTZEO0FBQzdELHNEQUFtRDtBQUNuRCxJQUFPLGlDQUFpQyxHQUFHLG1DQUFnQixDQUFDLGlDQUFpQyxDQUFDO0FBQzlGLElBQU8sbUJBQW1CLEdBQUcsK0JBQWdCLENBQUMsbUJBQW1CLENBQUM7QUFDbEUsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBRXJELE1BQWEsV0FBVztJQUVwQixZQUFvQixPQUFtQjtRQUFuQixZQUFPLEdBQVAsT0FBTyxDQUFZO0lBQ3ZDLENBQUM7SUFFRCxLQUFLLENBQUMsY0FBYyxDQUFDLElBQVksRUFBRSxRQUFnQixFQUFFLFFBQWdCLEVBQUUsSUFBeUM7UUFDNUcsT0FBTyxpQ0FBaUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUkseUJBQVcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzVHLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBNEIsRUFBRSxZQUFzQixFQUFFLGVBQXlCLEVBQUU7UUFDdEcsTUFBTSxnQkFBZ0IsR0FBRyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQy9FLElBQUksT0FBTyxDQUFDLGdCQUFnQixDQUFDLEtBQUssUUFBUSxFQUFFO1lBQ3hDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FDdEIsR0FBRyxFQUNILGdCQUFnQixFQUNoQixnQkFBZ0IsQ0FBQyxlQUFlLENBQ25DLENBQUM7U0FDTDtRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7Ozs7Ozs7O09BU0c7SUFDSCxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQWtCLEVBQUUsVUFBa0IsRUFBRSxhQUFxQixFQUFFLFVBQWUsRUFBRSxRQUFtRTtRQUM3SixJQUFJO1lBQ0EsTUFBTSxNQUFNLEVBQUUsQ0FBQztZQUVmLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FDdEIsR0FBRyxFQUNILDBCQUEwQixVQUFVLEVBQUUsRUFDdEMsYUFBYSxFQUNiLFVBQVUsQ0FDYixDQUFDO1NBQ0w7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUVSLGlEQUFpRDtZQUVqRCxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVkscUJBQVMsQ0FBQyxFQUFFO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNmLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FDdEIsR0FBRyxFQUNILHFCQUFxQixVQUFVLGlEQUFpRCxFQUNoRixnQkFBZ0IsQ0FBQyxxQkFBcUIsRUFDdEMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQ2YsQ0FBQzthQUNMO1lBRUQsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDdkMsT0FBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3BDO1lBRUQsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUN0QixHQUFHLEVBQ0gsNENBQTRDLENBQUMsQ0FBQyxVQUFVLGNBQWMsVUFBVSxPQUFPLEVBQ3ZGLGdCQUFnQixDQUFDLHFCQUFxQixFQUN0QyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FDZixDQUFDO1NBQ0w7SUFDTCxDQUFDO0NBRUo7QUFyRUQsa0NBcUVDO0FBRUQ7SUFBQSxNQUFhLGNBQWM7O0lBQ1Asc0JBQU8sR0FBVyxHQUFHLENBQUM7SUFFdEIsdUJBQVEsR0FBVyxHQUFHLENBQUM7SUFFdkIsMEJBQVcsR0FBVyxHQUFHLENBQUM7SUFDMUIsMkJBQVksR0FBVyxHQUFHLENBQUM7SUFDM0Isd0JBQVMsR0FBVyxHQUFHLENBQUM7SUFDeEIsd0JBQVMsR0FBVyxHQUFHLENBQUM7SUFFeEIsb0NBQXFCLEdBQVcsR0FBRyxDQUFDO0lBQ3BDLGtDQUFtQixHQUFXLEdBQUcsQ0FBQztJQUV0RCxxQkFBQztLQUFBO0FBYlksd0NBQWMifQ==