"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileServer = void 0;
const Configuration = __importStar(require("../../config/config"));
const config_1 = require("../../config/config");
const _ = __importStar(require("logger"));
const WebUtilities_1 = require("../utilities/WebUtilities");
const path_1 = require("path");
const ProcessImpl_1 = require("../actions/ProcessImpl");
const express = require("express");
var requestToSession = WebUtilities_1.RequestUtilities.requestToSession;
var AuthenticationMode;
(function (AuthenticationMode) {
    AuthenticationMode[AuthenticationMode["AUTHENTICATED"] = 0] = "AUTHENTICATED";
    AuthenticationMode[AuthenticationMode["UNAUTHENTICATED"] = 1] = "UNAUTHENTICATED";
    AuthenticationMode[AuthenticationMode["ANY"] = 2] = "ANY";
})(AuthenticationMode || (AuthenticationMode = {}));
class EndpointRecord {
    constructor(_path, _authentication, _subdomain, _domain, _fileRoot) {
        this._path = _path;
        this._authentication = _authentication;
        this._subdomain = _subdomain;
        this._domain = _domain;
        this._fileRoot = _fileRoot;
    }
    get path() {
        return this._path;
    }
    get authentication() {
        return this._authentication;
    }
    get subdomain() {
        return this._subdomain;
    }
    get domain() {
        return this._domain;
    }
    get fileRoot() {
        return this._fileRoot;
    }
}
class FileServer {
    constructor(basePath, composite, _express = express) {
        this.basePath = basePath;
        this.composite = composite;
        this._express = _express;
        // Load the files configuration
        Configuration.loadEasy('cas/files');
        if (!Configuration.has('file-servers')) {
            throw new Error('Configuration does not contain a "file-servers" entry, please check config/cas/files.js for a valid configuration');
        }
        this.records = [];
        const servers = Configuration.get('file-servers');
        for (const index in servers) {
            if (!servers.hasOwnProperty(index))
                continue;
            const entry = servers[index];
            if (!entry.hasOwnProperty('path'))
                throw new Error(`Index ${index} of the file configuration does not contain a path`);
            if (!entry.hasOwnProperty('authentication'))
                throw new Error(`Index ${index} of the file configuration does not contain a authentication`);
            if (!entry.hasOwnProperty('subdomain'))
                throw new Error(`Index ${index} of the file configuration does not contain a subdomain`);
            if (!entry.hasOwnProperty('domain'))
                throw new Error(`Index ${index} of the file configuration does not contain a domain`);
            if (!entry.hasOwnProperty('fileRoot'))
                throw new Error(`Index ${index} of the file configuration does not contain a fileRoot`);
            if (typeof (entry.path) !== "string")
                throw new Error(`Index ${index} of the file configuration is invalid, path must be a string`);
            if (typeof (entry.authentication) !== "string")
                throw new Error(`Index ${index} of the file configuration is invalid, authentication must be a string`);
            if (typeof (entry.subdomain) !== "string")
                throw new Error(`Index ${index} of the file configuration is invalid, subdomain must be a string`);
            if (typeof (entry.domain) !== "string")
                throw new Error(`Index ${index} of the file configuration is invalid, domain must be a string`);
            if (typeof (entry.fileRoot) !== "string")
                throw new Error(`Index ${index} of the file configuration is invalid, fileRoot must be a string`);
            if (!(entry.authentication in AuthenticationMode))
                throw new Error(`Index ${index} of the file configuration is invalid, authentication must be a valid value`);
            if (config_1.has('environment') && config_1.get('environment') === 'dev') {
                _.warn('Acting in a DEV environment so overwriting all domains to "ryan.ax370"');
                entry.domain = "ryan.ax370";
            }
            _.info(`Registered file server on (sub+)domain "${entry.subdomain}.${entry.domain}" serving on "${entry.path}" which will return files from "${entry.fileRoot}" which required an authentication state of ${entry.authentication}`);
            // Convert this record to an entry
            this.records.push(new EndpointRecord(entry.path, AuthenticationMode[entry.authentication], entry.subdomain, entry.domain, entry.fileRoot));
        }
    }
    handle(req, res, next) {
        // Check that this request is coming for the base path of the static files
        if (!req.originalUrl.startsWith(this.basePath)) {
            _.trace(`Failing file server request because "${req.originalUrl}" doesn't start with "${this.basePath}"`);
            return next();
        }
        const newPath = req.originalUrl.substr(this.basePath.length);
        // Now we work through all the file records
        for (const record of this.records) {
            _.trace(`Trying request against record: ${record.subdomain}.${record.domain}/${record.path} (@${record.authentication}`);
            // Start by checking the domain
            // Number of elements in the domain of the file server
            const numberOfSegments = record.domain.split('.').length;
            // Number of elements in domain that this request is coming from
            const fqdnSegments = req.hostname.split('.');
            // The elements of this request that make up the subdomain, if this is the right domain
            const subdomainSegments = fqdnSegments.slice(0, fqdnSegments.length - numberOfSegments);
            // The elements of this request that make up the domain, if this is the right domain
            const domainSegments = fqdnSegments.slice(fqdnSegments.length - numberOfSegments);
            // Reform both into strings
            const subdomain = subdomainSegments.join('.');
            const domain = domainSegments.join('.');
            // Check for wildcard domains
            if (record.domain !== '*') {
                // If the domain doesn't match continue
                if (domain.toLowerCase() !== record.domain.toLowerCase()) {
                    _.trace(`Skipping entry because domain ${domain} doesn't match ${record.domain}`);
                    continue;
                }
            }
            // At this point the domain matches which means that the subdomain must be valid now
            // If it is @ it matches the root domain
            if (record.subdomain === '@' && subdomain !== '') {
                _.trace(`Skipping entry because subdomain ${subdomain} didn't match the @ record`);
                continue;
            }
            // If it is * it will match any domain so only check the subdomains if it is not *
            if (record.subdomain !== '*') {
                if (record.subdomain.toLowerCase() !== subdomain.toLowerCase()) {
                    _.trace(`Skipping entry because subdomain ${subdomain} didn't match ${record.subdomain} and was not "*"`);
                    continue;
                }
            }
            // Now we know this request is hitting the right domain so now we check if the path matches
            // If it doesn't start with the path in the configuration, then we can ignore the request
            if (!newPath.startsWith(record.path)) {
                _.trace(`Skipping entry because ${newPath} doesn't start with ${record.path}`);
                continue;
            }
            const target = newPath.substr(record.path.length);
            const session = requestToSession(req);
            // Now if it does, we just want to check the authentication
            // If this endpoint requires authentication but the user is not authenticated, reject
            if (record.authentication === AuthenticationMode.AUTHENTICATED && !this.composite.authenticationProcesses.isAuthenticated(session)) {
                _.trace(`Reject as UNAUTHORIZED for resource because the user is not logged in`);
                res.sendStatus(ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
                return;
            }
            if (record.authentication === AuthenticationMode.UNAUTHENTICATED && this.composite.authenticationProcesses.isAuthenticated(session)) {
                _.trace(`Reject as BAD_REQUEST for resource because the user is logged in but not allowed to be`);
                res.sendStatus(ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
                return;
            }
            const oldURL = req.url;
            const oldOriginal = req.originalUrl;
            req.url = target;
            req.originalUrl = target;
            _.trace(`Trying to pass off the request to express resolving to ${path_1.join(__dirname, '..', '..', '..', record.fileRoot)}/${req.originalUrl}`);
            // Then finally hand it off to express!
            this._express.static(path_1.join(__dirname, '..', '..', '..', record.fileRoot), { fallthrough: true })(req, res, (err) => {
                _.trace(`The request has fallen through`);
                req.url = oldURL;
                req.originalUrl = oldOriginal;
                next(err);
            });
            return;
        }
        return next();
    }
}
exports.FileServer = FileServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRmlsZVNlcnZlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jYXN2aWkvZmlsZS9GaWxlU2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtRUFBcUQ7QUFDckQsZ0RBQStDO0FBQy9DLDBDQUE0QjtBQUc1Qiw0REFBNkQ7QUFDN0QsK0JBQTRCO0FBQzVCLHdEQUF3RDtBQUN4RCxtQ0FBb0M7QUFDcEMsSUFBTyxnQkFBZ0IsR0FBRywrQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQztBQUU1RCxJQUFLLGtCQUlKO0FBSkQsV0FBSyxrQkFBa0I7SUFDbkIsNkVBQWEsQ0FBQTtJQUNiLGlGQUFlLENBQUE7SUFDZix5REFBRyxDQUFBO0FBQ1AsQ0FBQyxFQUpJLGtCQUFrQixLQUFsQixrQkFBa0IsUUFJdEI7QUFFRCxNQUFNLGNBQWM7SUFFaEIsWUFBNkIsS0FBYSxFQUNiLGVBQW1DLEVBQ25DLFVBQWtCLEVBQ2xCLE9BQWUsRUFDZixTQUFpQjtRQUpqQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2Isb0JBQWUsR0FBZixlQUFlLENBQW9CO1FBQ25DLGVBQVUsR0FBVixVQUFVLENBQVE7UUFDbEIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLGNBQVMsR0FBVCxTQUFTLENBQVE7SUFDOUMsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBSSxRQUFRO1FBQ1IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7Q0FDSjtBQUVELE1BQWEsVUFBVTtJQUluQixZQUFvQixRQUFnQixFQUNQLFNBQXVCLEVBQ2hDLFdBQVcsT0FBTztRQUZsQixhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQ1AsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUNoQyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xDLCtCQUErQjtRQUMvQixhQUFhLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXBDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQ3BDLE1BQU0sSUFBSSxLQUFLLENBQUMsbUhBQW1ILENBQUMsQ0FBQztTQUN4STtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBRWxCLE1BQU0sT0FBTyxHQUE2QixhQUFhLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzVFLEtBQUssTUFBTSxLQUFLLElBQUksT0FBTyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQztnQkFBRSxTQUFTO1lBQzdDLE1BQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7Z0JBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssb0RBQW9ELENBQUMsQ0FBQztZQUN2SCxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyw4REFBOEQsQ0FBQyxDQUFDO1lBQzNJLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyx5REFBeUQsQ0FBQyxDQUFDO1lBQ2pJLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxzREFBc0QsQ0FBQyxDQUFDO1lBQzNILElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyx3REFBd0QsQ0FBQyxDQUFDO1lBQy9ILElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxRQUFRO2dCQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLDhEQUE4RCxDQUFDLENBQUM7WUFDcEksSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxLQUFLLFFBQVE7Z0JBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssd0VBQXdFLENBQUMsQ0FBQztZQUN4SixJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssUUFBUTtnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxtRUFBbUUsQ0FBQyxDQUFDO1lBQzlJLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxRQUFRO2dCQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLGdFQUFnRSxDQUFDLENBQUM7WUFDeEksSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLFFBQVE7Z0JBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssa0VBQWtFLENBQUMsQ0FBQztZQUM1SSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxJQUFJLGtCQUFrQixDQUFDO2dCQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLDZFQUE2RSxDQUFDLENBQUM7WUFFaEssSUFBSSxZQUFHLENBQUMsYUFBYSxDQUFDLElBQUksWUFBRyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssRUFBRTtnQkFDcEQsQ0FBQyxDQUFDLElBQUksQ0FBQyx3RUFBd0UsQ0FBQyxDQUFDO2dCQUNqRixLQUFLLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQzthQUMvQjtZQUVELENBQUMsQ0FBQyxJQUFJLENBQUMsMkNBQTJDLEtBQUssQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLE1BQU0saUJBQWlCLEtBQUssQ0FBQyxJQUFJLG1DQUFtQyxLQUFLLENBQUMsUUFBUSwrQ0FBK0MsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7WUFDcE8sa0NBQWtDO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUNoQyxLQUFLLENBQUMsSUFBSSxFQUNWLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsRUFDeEMsS0FBSyxDQUFDLFNBQVMsRUFDZixLQUFLLENBQUMsTUFBTSxFQUNaLEtBQUssQ0FBQyxRQUFRLENBQ2pCLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCO1FBQ3pELDBFQUEwRTtRQUMxRSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzVDLENBQUMsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLEdBQUcsQ0FBQyxXQUFXLHlCQUF5QixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztZQUMxRyxPQUFPLElBQUksRUFBRSxDQUFDO1NBQ2pCO1FBRUQsTUFBTSxPQUFPLEdBQUcsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU3RCwyQ0FBMkM7UUFDM0MsS0FBSyxNQUFNLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQy9CLENBQUMsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLE1BQU0sQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxNQUFNLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1lBQ3pILCtCQUErQjtZQUUvQixzREFBc0Q7WUFDdEQsTUFBTSxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDekQsZ0VBQWdFO1lBQ2hFLE1BQU0sWUFBWSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTdDLHVGQUF1RjtZQUN2RixNQUFNLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQztZQUN4RixvRkFBb0Y7WUFDcEYsTUFBTSxjQUFjLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLENBQUM7WUFFbEYsMkJBQTJCO1lBQzNCLE1BQU0sU0FBUyxHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM5QyxNQUFNLE1BQU0sR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRXhDLDZCQUE2QjtZQUM3QixJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO2dCQUN2Qix1Q0FBdUM7Z0JBQ3ZDLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEVBQUU7b0JBQ3RELENBQUMsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLE1BQU0sa0JBQWtCLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO29CQUNsRixTQUFTO2lCQUNaO2FBQ0o7WUFFRCxvRkFBb0Y7WUFFcEYsd0NBQXdDO1lBQ3hDLElBQUksTUFBTSxDQUFDLFNBQVMsS0FBSyxHQUFHLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTtnQkFDOUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsU0FBUyw0QkFBNEIsQ0FBQyxDQUFDO2dCQUNuRixTQUFTO2FBQ1o7WUFFRCxrRkFBa0Y7WUFDbEYsSUFBSSxNQUFNLENBQUMsU0FBUyxLQUFLLEdBQUcsRUFBRTtnQkFDMUIsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtvQkFDNUQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsU0FBUyxpQkFBaUIsTUFBTSxDQUFDLFNBQVMsa0JBQWtCLENBQUMsQ0FBQztvQkFDMUcsU0FBUztpQkFDWjthQUNKO1lBRUQsMkZBQTJGO1lBQzNGLHlGQUF5RjtZQUN6RixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2xDLENBQUMsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLE9BQU8sdUJBQXVCLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUMvRSxTQUFTO2FBQ1o7WUFFRCxNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEQsTUFBTSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFdEMsMkRBQTJEO1lBQzNELHFGQUFxRjtZQUNyRixJQUFJLE1BQU0sQ0FBQyxjQUFjLEtBQUssa0JBQWtCLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ2hJLENBQUMsQ0FBQyxLQUFLLENBQUMsdUVBQXVFLENBQUMsQ0FBQztnQkFDakYsR0FBRyxDQUFDLFVBQVUsQ0FBQyw0QkFBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUM1QyxPQUFPO2FBQ1Y7WUFFRCxJQUFJLE1BQU0sQ0FBQyxjQUFjLEtBQUssa0JBQWtCLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNqSSxDQUFDLENBQUMsS0FBSyxDQUFDLHdGQUF3RixDQUFDLENBQUM7Z0JBQ2xHLEdBQUcsQ0FBQyxVQUFVLENBQUMsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDM0MsT0FBTzthQUNWO1lBRUQsTUFBTSxNQUFNLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUN2QixNQUFNLFdBQVcsR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDO1lBRXBDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDO1lBQ2pCLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1lBRXpCLENBQUMsQ0FBQyxLQUFLLENBQUMsMERBQTBELFdBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBRTNJLHVDQUF1QztZQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFTLEVBQUUsRUFBRTtnQkFDcEgsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO2dCQUMxQyxHQUFHLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQztnQkFDakIsR0FBRyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNkLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTztTQUNWO1FBRUQsT0FBTyxJQUFJLEVBQUUsQ0FBQztJQUNsQixDQUFDO0NBQ0o7QUFsSkQsZ0NBa0pDIn0=