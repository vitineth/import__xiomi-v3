import { NextFunction, Request, Response } from "express";
import { CASComposite } from "../integration/CASComposite";
import express = require('express');
export declare class FileServer {
    private basePath;
    private readonly composite;
    private _express;
    private records;
    constructor(basePath: string, composite: CASComposite, _express?: typeof express);
    handle(req: Request, res: Response, next: NextFunction): void;
}
