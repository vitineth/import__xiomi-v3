import { PermissionDAO } from "../database/dao/PermissionDAO";
export declare function convertPermissionsFile(permissionDAO: PermissionDAO): Promise<void>;
