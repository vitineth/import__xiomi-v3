"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertPermissionsFile = void 0;
const path_1 = require("path");
const _ = __importStar(require("logger"));
const crypto = require("crypto");
function hash(input) {
    return crypto.createHash('sha256').update(input).digest('hex');
}
class PermissionNode {
    constructor(name, description) {
        this.name = name;
        this.description = description;
        this.hash = hash(name);
    }
}
class GroupNode {
    constructor(name, description) {
        this.name = name;
        this.description = description;
        this.inherits = [];
        this.permissions = [];
        this.hash = hash(name);
    }
}
class PermissionToGroupRelationship {
    constructor(fromPermission, toGroup) {
        this.fromPermission = fromPermission;
        this.toGroup = toGroup;
    }
}
class GroupToGroupRelationship {
    constructor(fromGroup, toGroup) {
        this.fromGroup = fromGroup;
        this.toGroup = toGroup;
    }
}
function isCyclicInternal(mapping, extractor, index, visited, recursionStack) {
    visited[index] = true;
    if (!mapping.hasOwnProperty(index)) {
        return false;
    }
    recursionStack[index] = true;
    const relationships = mapping[index];
    const neighbours = relationships.map(extractor);
    for (const to of neighbours) {
        if (!visited[to]) {
            if (isCyclicInternal(mapping, extractor, to, visited, recursionStack)) {
                return true;
            }
        }
        else if (recursionStack[to]) {
            return true;
        }
    }
    recursionStack[index] = false;
    return false;
}
function isCyclic(mapping, extractor, startNodes) {
    const visited = {};
    const recStack = {};
    for (const node of startNodes) {
        if (!visited[node]) {
            if (isCyclicInternal(mapping, extractor, node, visited, recStack)) {
                return true;
            }
        }
    }
    return false;
}
async function convertPermissionsFile(permissionDAO) {
    const configuration = require(path_1.join(__dirname, '../../../cas-data/permissions/permissions.js'));
    _.trace(`loaded configuration`);
    const dataPermissions = configuration.permissions;
    const dataGroups = configuration.groups;
    const permissionNodes = {};
    const groupNodes = {};
    const groupToGroupRelationships = {};
    const permissionToGroupRelationships = {};
    // Convert to nodes
    for (const permission of Object.keys(dataPermissions)) {
        const description = dataPermissions[permission];
        const pn = new PermissionNode(permission, description);
        if (permissionNodes.hasOwnProperty(pn.hash)) {
            throw new Error(`Permission ${pn.hash} conflicts with permission ${permissionNodes[pn.hash]}, check these
             are not the same or find a different name that doesn't hash the same`);
        }
        permissionNodes[pn.hash] = pn;
    }
    _.trace(`loaded ${Object.keys(permissionNodes).length} permission nodes`);
    for (const group of Object.keys(dataGroups)) {
        const realNode = dataGroups[group];
        const description = dataGroups[group].description;
        const groupNode = new GroupNode(group, description);
        if (groupNodes.hasOwnProperty(groupNode.hash)) {
            throw new Error(`Group ${groupNode.hash} conflicts with group ${groupNodes[groupNode.hash]}, check these
             are not the same or find a different name that doesn't hash the same`);
        }
        groupNodes[groupNode.hash] = groupNode;
        // Then we want to push in the relationships
        // First we start with permissions to groups because we can identify if it references any permissions that
        // don't exist, we will throw an exception and print an error
        for (const gPermission of realNode.permissions) {
            const gpHash = hash(gPermission);
            // Check that it exists
            if (!permissionNodes.hasOwnProperty(gpHash)) {
                throw new Error(`Group ${group} references permission ${gPermission} but could not find this in
                 our node mapping. Check the permission exists and that the capitalisation is the same`);
            }
            // And then submit the relationship
            if (permissionToGroupRelationships.hasOwnProperty(groupNode.hash)) {
                permissionToGroupRelationships[groupNode.hash].push(new PermissionToGroupRelationship(gpHash, groupNode.hash));
            }
            else {
                permissionToGroupRelationships[groupNode.hash] = [new PermissionToGroupRelationship(gpHash, groupNode.hash)];
            }
        }
        _.trace(`loaded ${Object.keys(groupNodes).length} group nodes`);
        // Then we will move on to inheritance groups
        // We can't verify the groups that exist yet because we don't know how far through the array we are so we will
        // create all the relationships and then check them for all groups existing after they are all complete.
        for (const gGroup of realNode.inherits) {
            const ggHash = hash(gGroup);
            // Submit the relationship
            if (groupToGroupRelationships.hasOwnProperty(groupNode.hash)) {
                groupToGroupRelationships[groupNode.hash].push(new GroupToGroupRelationship(groupNode.hash, ggHash));
            }
            else {
                groupToGroupRelationships[groupNode.hash] = [new GroupToGroupRelationship(groupNode.hash, ggHash)];
            }
        }
    }
    _.trace(`loaded ${Object.keys(groupToGroupRelationships).length} group to group relationships`);
    // Then we want to check all the group to group relationships because all groups are indexed now.
    for (const relationship of [].concat.apply([], Object.values(groupToGroupRelationships))) {
        const fromNodeExists = groupNodes.hasOwnProperty(relationship.fromGroup);
        const toNodeExists = groupNodes.hasOwnProperty(relationship.toGroup);
        if (!fromNodeExists && !toNodeExists) {
            throw new Error(`There is a relationship defined from hash [${relationship.fromGroup}] to
             [${relationship.toGroup}] but neither group is found in our mapping, this can't really happen? You broke
              everything?`);
        }
        if (!fromNodeExists && toNodeExists) {
            throw new Error(`A relationship is defined from an unknown group (hash [${relationship.fromGroup}]) to a
             known group ${groupNodes[relationship.toGroup].name}. Check that all your relationships are well defined`);
        }
        if (fromNodeExists && !toNodeExists) {
            throw new Error(`A relationship is defined from a known group ${groupNodes[relationship.fromGroup].name} to
             an unknown group (hash [${relationship.toGroup}]). Check that all your relationships are well defined`);
        }
    }
    _.trace(`total result [permission node: ${Object.keys(permissionNodes).length}], [group node: ${Object.keys(groupNodes)}], [group to group: ${Object.keys(groupToGroupRelationships).length}], [permission to group: ${Object.keys(groupToGroupRelationships).length}]`);
    // At this point we know all nodes and groups and relationships are valid connection wise. Now we need to try and
    // detect cyclical structures in their permissions.
    if (isCyclic(groupToGroupRelationships, e => e.toGroup, Object.keys(groupNodes))) {
        throw new Error(`Detected a cyclical structure in your Group Inheritance Graph. Please trace your graph
         inheritance and mend any errors then rerun.`);
    }
    _.trace(`permissions confirmed to be non-cyclic`);
    // Now that we know there are no cyclic relationships in the graph, I can start to compare this against the database
    // Lets start with permissions, we fetch all from the database and form two lists, one to delete and one to add
    const dbPermissions = await permissionDAO.fetchAllPermissions();
    const dbPermissionHashes = dbPermissions.map(e => e.hash);
    const permissionChanges = { toAdd: [], toRemove: [] };
    for (const hash of Object.keys(permissionNodes)) {
        if (!dbPermissionHashes.includes(hash)) {
            // This means this has is not included in the database, so that needs to be added
            permissionChanges.toAdd.push(permissionNodes[hash]);
        }
    }
    for (const entry of dbPermissions) {
        if (!permissionNodes.hasOwnProperty(entry.hash)) {
            // This means this database hash is not included in the file data, so that means it needs to be deleted
            permissionChanges.toRemove.push(entry);
        }
    }
    // Then we do the same with groups
    const dbGroups = await permissionDAO.fetchAllPermissionGroups();
    const dbGroupHashes = dbGroups.map(e => e.hash);
    const groupChanges = { toAdd: [], toRemove: [] };
    for (const hash of Object.keys(groupNodes)) {
        if (!dbGroupHashes.includes(hash)) {
            // This means this has is not included in the database, so that needs to be added
            groupChanges.toAdd.push(groupNodes[hash]);
        }
    }
    for (const entry of dbGroups) {
        if (!groupNodes.hasOwnProperty(entry.hash)) {
            // This means this database hash is not included in the file data, so that means it needs to be deleted
            groupChanges.toRemove.push(entry);
        }
    }
    // Then we do it with group inheritance. Group inheritances have an ID based on both their group keys
    const dbInheritances = await permissionDAO.fetchAllPermissionGroupInheritance();
    // This is a little bit hacky but it makes esdoc run
    const convertInheritanceToString = (id) => {
        const find = dbGroups.find(y => y.id === id);
        return find === undefined ? 'undefined' : find.hash;
    };
    const dbInheritanceHashes = dbInheritances.map(e => `${convertInheritanceToString(e.inheritsFromID)}.${convertInheritanceToString(e.targetGroupID)}`);
    const inheritanceChanges = {
        toAdd: [],
        toRemove: [],
    };
    const localInheritances = ([].concat.apply([], Object.values(groupToGroupRelationships)));
    const localInheritanceHashes = localInheritances.map(e => `${e.fromGroup}.${e.toGroup}`);
    for (const relationship of localInheritances) {
        if (!dbInheritanceHashes.includes(`${relationship.fromGroup}.${relationship.toGroup}`)) {
            inheritanceChanges.toAdd.push(relationship);
        }
    }
    for (const entry of dbInheritances) {
        if (!localInheritanceHashes.includes(`${convertInheritanceToString(entry.inheritsFromID)}.${convertInheritanceToString(entry.targetGroupID)}`)) {
            inheritanceChanges.toRemove.push(entry);
        }
    }
    // Then finally with permission to group mappings
    const dbMapping = await permissionDAO.fetchAllPermissionGroupMappings();
    // Convert it into a permission indexed map
    const permissionIndexedMappings = {};
    for (const entry of dbMapping) {
        if (permissionIndexedMappings.hasOwnProperty(entry.permission.hash))
            permissionIndexedMappings[entry.permission.hash].push(entry.permissionGroup.hash);
        else
            permissionIndexedMappings[entry.permission.hash] = [entry.permissionGroup.hash];
    }
    // Convert the local mappings into a nice array format
    const localMappings = [].concat.apply([], Object.values(permissionToGroupRelationships));
    const localPermissionIndexedMappings = {};
    for (const entry of localMappings) {
        if (localPermissionIndexedMappings.hasOwnProperty(entry.fromPermission))
            localPermissionIndexedMappings[entry.fromPermission].push(entry.toGroup);
        else
            localPermissionIndexedMappings[entry.fromPermission] = [entry.toGroup];
    }
    // And somewhere to save the changes
    const mappingChanges = {
        toAdd: [],
        toRemove: [],
    };
    for (const mapping of localMappings) {
        if (!permissionIndexedMappings.hasOwnProperty(mapping.fromPermission)) {
            mappingChanges.toAdd.push(mapping);
        }
        else {
            // Permission exists in the map so see if the group does
            if (!permissionIndexedMappings[mapping.fromPermission].includes(mapping.toGroup)) {
                mappingChanges.toAdd.push(mapping);
            }
        }
    }
    // Then do it for the database against the local mapping
    for (const entry of dbMapping) {
        if (!localPermissionIndexedMappings.hasOwnProperty(entry.permission.hash)) {
            mappingChanges.toRemove.push(entry);
        }
        else {
            if (!localPermissionIndexedMappings[entry.permission.hash].includes(entry.permissionGroup.hash)) {
                mappingChanges.toRemove.push(entry);
            }
        }
    }
    _.trace(`permissions to add: ${permissionChanges.toAdd.length}`);
    _.trace(`permissions to remove: ${permissionChanges.toRemove.length}`);
    _.trace(`groups to add: ${groupChanges.toAdd.length}`);
    _.trace(`groups to remove: ${groupChanges.toRemove.length}`);
    _.trace(`inheritances to add: ${inheritanceChanges.toAdd.length}`);
    _.trace(`inheritances to remove: ${inheritanceChanges.toRemove.length}`);
    _.trace(`mappings to add: ${mappingChanges.toAdd.length}`);
    _.trace(`mappings to remove: ${mappingChanges.toRemove.length}`);
    // Then we want to turn this into database actions
    // Begin with permissions
    await Promise.all(permissionChanges.toRemove.map(e => permissionDAO.removePermission(e)));
    await Promise.all(permissionChanges.toAdd.map(e => permissionDAO.createPermission(e.name, e.description, e.hash)));
    _.trace(`permission changes applied`);
    // Then the groups
    await Promise.all(groupChanges.toRemove.map(e => permissionDAO.removePermissionGroup(e)));
    await Promise.all(groupChanges.toAdd.map(e => permissionDAO.createPermissionGroup(e.name, e.description, e.hash)));
    _.trace(`group changes applied`);
    // Then the inheritance
    await Promise.all(inheritanceChanges.toRemove.map(e => permissionDAO.removeGroupInheritance(e)));
    await Promise.all(inheritanceChanges.toAdd.map(e => permissionDAO.createGroupInheritance(dbGroups.filter(f => f.hash === e.toGroup)[0], dbGroups.filter(f => f.hash === e.fromGroup)[0])));
    _.trace(`inheritance changes applied`);
    // And finally the mappings
    await Promise.all(mappingChanges.toRemove.map(e => permissionDAO.removeGroupMapping(e)));
    await Promise.all(mappingChanges.toAdd.map(e => permissionDAO.createGroupMapping(dbPermissions.filter(f => f.hash === e.fromPermission)[0], dbGroups.filter(f => f.hash === e.toGroup)[0])));
    _.trace(`mapping changes applied`);
}
exports.convertPermissionsFile = convertPermissionsFile;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVybWlzc2lvbi1hdXRvbWF0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS9hdXRvbWF0aW9uL3Blcm1pc3Npb24tYXV0b21hdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0JBQTRCO0FBTTVCLDBDQUE0QjtBQUM1QixpQ0FBa0M7QUFFbEMsU0FBUyxJQUFJLENBQUMsS0FBYTtJQUN2QixPQUFPLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNuRSxDQUFDO0FBRUQsTUFBTSxjQUFjO0lBSWhCLFlBQW1CLElBQVksRUFDWixXQUFtQjtRQURuQixTQUFJLEdBQUosSUFBSSxDQUFRO1FBQ1osZ0JBQVcsR0FBWCxXQUFXLENBQVE7UUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUVKO0FBRUQsTUFBTSxTQUFTO0lBTVgsWUFBbUIsSUFBWSxFQUNaLFdBQW1CO1FBRG5CLFNBQUksR0FBSixJQUFJLENBQVE7UUFDWixnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUovQixhQUFRLEdBQWdCLEVBQUUsQ0FBQztRQUMzQixnQkFBVyxHQUFxQixFQUFFLENBQUM7UUFJdEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUVKO0FBRUQsTUFBTSw2QkFBNkI7SUFFL0IsWUFBbUIsY0FBc0IsRUFDdEIsT0FBZTtRQURmLG1CQUFjLEdBQWQsY0FBYyxDQUFRO1FBQ3RCLFlBQU8sR0FBUCxPQUFPLENBQVE7SUFDbEMsQ0FBQztDQUVKO0FBRUQsTUFBTSx3QkFBd0I7SUFFMUIsWUFBbUIsU0FBaUIsRUFDakIsT0FBZTtRQURmLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFDakIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtJQUNsQyxDQUFDO0NBRUo7QUFFRCxTQUFTLGdCQUFnQixDQUFDLE9BQWlDLEVBQUUsU0FBNkIsRUFBRSxLQUFhLEVBQUUsT0FBbUMsRUFBRSxjQUEwQztJQUN0TCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFO1FBQ2hDLE9BQU8sS0FBSyxDQUFDO0tBQ2hCO0lBRUQsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQztJQUU3QixNQUFNLGFBQWEsR0FBK0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pFLE1BQU0sVUFBVSxHQUFhLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDMUQsS0FBSyxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUU7UUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNkLElBQUksZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLGNBQWMsQ0FBQyxFQUFFO2dCQUNuRSxPQUFPLElBQUksQ0FBQzthQUNmO1NBQ0o7YUFBTSxJQUFJLGNBQWMsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUMzQixPQUFPLElBQUksQ0FBQztTQUNmO0tBQ0o7SUFFRCxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQzlCLE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUM7QUFFRCxTQUFTLFFBQVEsQ0FBQyxPQUFpQyxFQUFFLFNBQTZCLEVBQUUsVUFBb0I7SUFDcEcsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO0lBQ25CLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNwQixLQUFLLE1BQU0sSUFBSSxJQUFJLFVBQVUsRUFBRTtRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hCLElBQUksZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxFQUFFO2dCQUMvRCxPQUFPLElBQUksQ0FBQzthQUNmO1NBQ0o7S0FDSjtJQUVELE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUM7QUFFTSxLQUFLLFVBQVUsc0JBQXNCLENBQUMsYUFBNEI7SUFDckUsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLFdBQUksQ0FBQyxTQUFTLEVBQUUsOENBQThDLENBQUMsQ0FBQyxDQUFDO0lBRS9GLENBQUMsQ0FBQyxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUVoQyxNQUFNLGVBQWUsR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO0lBQ2xELE1BQU0sVUFBVSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFFeEMsTUFBTSxlQUFlLEdBQXNDLEVBQUUsQ0FBQztJQUM5RCxNQUFNLFVBQVUsR0FBaUMsRUFBRSxDQUFDO0lBQ3BELE1BQU0seUJBQXlCLEdBQWtELEVBQUUsQ0FBQztJQUNwRixNQUFNLDhCQUE4QixHQUF1RCxFQUFFLENBQUM7SUFFOUYsbUJBQW1CO0lBQ25CLEtBQUssTUFBTSxVQUFVLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRTtRQUNuRCxNQUFNLFdBQVcsR0FBRyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFaEQsTUFBTSxFQUFFLEdBQUcsSUFBSSxjQUFjLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBRXZELElBQUksZUFBZSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekMsTUFBTSxJQUFJLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLDhCQUE4QixlQUFlLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrRkFDckIsQ0FBQyxDQUFDO1NBQzNFO1FBRUQsZUFBZSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDakM7SUFFRCxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxNQUFNLG1CQUFtQixDQUFDLENBQUM7SUFFMUUsS0FBSyxNQUFNLEtBQUssSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1FBQ3pDLE1BQU0sUUFBUSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQyxNQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDO1FBRWxELE1BQU0sU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztRQUVwRCxJQUFJLFVBQVUsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxTQUFTLENBQUMsSUFBSSx5QkFBeUIsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7a0ZBQ3BCLENBQUMsQ0FBQztTQUMzRTtRQUVELFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsU0FBUyxDQUFDO1FBRXZDLDRDQUE0QztRQUU1QywwR0FBMEc7UUFDMUcsNkRBQTZEO1FBQzdELEtBQUssTUFBTSxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRTtZQUM1QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFakMsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUN6QyxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSywwQkFBMEIsV0FBVzt1R0FDb0IsQ0FBQyxDQUFDO2FBQzVGO1lBRUQsbUNBQW1DO1lBQ25DLElBQUksOEJBQThCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDL0QsOEJBQThCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLDZCQUE2QixDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUNsSDtpQkFBTTtnQkFDSCw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLDZCQUE2QixDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUNoSDtTQUNKO1FBRUQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxjQUFjLENBQUMsQ0FBQztRQUVoRSw2Q0FBNkM7UUFDN0MsOEdBQThHO1FBQzlHLHdHQUF3RztRQUN4RyxLQUFLLE1BQU0sTUFBTSxJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7WUFDcEMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRTVCLDBCQUEwQjtZQUMxQixJQUFJLHlCQUF5QixDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFELHlCQUF5QixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDeEc7aUJBQU07Z0JBQ0gseUJBQXlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDdEc7U0FDSjtLQUNKO0lBRUQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxNQUFNLCtCQUErQixDQUFDLENBQUM7SUFFaEcsaUdBQWlHO0lBQ2pHLEtBQUssTUFBTSxZQUFZLElBQUssRUFBaUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRTtRQUN0SCxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN6RSxNQUFNLFlBQVksR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVyRSxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ2xDLE1BQU0sSUFBSSxLQUFLLENBQUMsOENBQThDLFlBQVksQ0FBQyxTQUFTO2dCQUNoRixZQUFZLENBQUMsT0FBTzswQkFDVixDQUFDLENBQUM7U0FDbkI7UUFFRCxJQUFJLENBQUMsY0FBYyxJQUFJLFlBQVksRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLDBEQUEwRCxZQUFZLENBQUMsU0FBUzsyQkFDakYsVUFBVSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLHNEQUFzRCxDQUFDLENBQUM7U0FDL0c7UUFFRCxJQUFJLGNBQWMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLGdEQUFnRCxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUk7dUNBQzVFLFlBQVksQ0FBQyxPQUFPLHdEQUF3RCxDQUFDLENBQUM7U0FDNUc7S0FDSjtJQUVELENBQUMsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxtQkFBbUIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLE1BQU0sQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxNQUFNLDRCQUE0QixNQUFNLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUV6USxpSEFBaUg7SUFDakgsbURBQW1EO0lBQ25ELElBQUksUUFBUSxDQUFDLHlCQUF5QixFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUU7UUFDOUUsTUFBTSxJQUFJLEtBQUssQ0FBQztxREFDNkIsQ0FBQyxDQUFDO0tBQ2xEO0lBRUQsQ0FBQyxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO0lBRWxELG9IQUFvSDtJQUNwSCwrR0FBK0c7SUFDL0csTUFBTSxhQUFhLEdBQUcsTUFBTSxhQUFhLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUNoRSxNQUFNLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUQsTUFBTSxpQkFBaUIsR0FBRyxFQUFFLEtBQUssRUFBRyxFQUF1QixFQUFFLFFBQVEsRUFBRyxFQUFtQixFQUFFLENBQUM7SUFDOUYsS0FBSyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFO1FBQzdDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDcEMsaUZBQWlGO1lBQ2pGLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDdkQ7S0FDSjtJQUNELEtBQUssTUFBTSxLQUFLLElBQUksYUFBYSxFQUFFO1FBQy9CLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM3Qyx1R0FBdUc7WUFDdkcsaUJBQWlCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQztLQUNKO0lBRUQsa0NBQWtDO0lBQ2xDLE1BQU0sUUFBUSxHQUFHLE1BQU0sYUFBYSxDQUFDLHdCQUF3QixFQUFFLENBQUM7SUFDaEUsTUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoRCxNQUFNLFlBQVksR0FBRyxFQUFFLEtBQUssRUFBRyxFQUFrQixFQUFFLFFBQVEsRUFBRyxFQUF3QixFQUFFLENBQUM7SUFDekYsS0FBSyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1FBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9CLGlGQUFpRjtZQUNqRixZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUM3QztLQUNKO0lBQ0QsS0FBSyxNQUFNLEtBQUssSUFBSSxRQUFRLEVBQUU7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3hDLHVHQUF1RztZQUN2RyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNyQztLQUNKO0lBRUQscUdBQXFHO0lBQ3JHLE1BQU0sY0FBYyxHQUFHLE1BQU0sYUFBYSxDQUFDLGtDQUFrQyxFQUFFLENBQUM7SUFDaEYsb0RBQW9EO0lBQ3BELE1BQU0sMEJBQTBCLEdBQUcsQ0FBQyxFQUFVLEVBQUUsRUFBRTtRQUM5QyxNQUFNLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUM3QyxPQUFPLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN4RCxDQUFDLENBQUM7SUFDRixNQUFNLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSwwQkFBMEIsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3RKLE1BQU0sa0JBQWtCLEdBQUc7UUFDdkIsS0FBSyxFQUFHLEVBQWlDO1FBQ3pDLFFBQVEsRUFBRyxFQUFtQztLQUNqRCxDQUFDO0lBQ0YsTUFBTSxpQkFBaUIsR0FBRyxDQUFFLEVBQWlDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxSCxNQUFNLHNCQUFzQixHQUFHLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUN6RixLQUFLLE1BQU0sWUFBWSxJQUFJLGlCQUFpQixFQUFFO1FBQzFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxZQUFZLENBQUMsU0FBUyxJQUFJLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFO1lBQ3BGLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDL0M7S0FDSjtJQUNELEtBQUssTUFBTSxLQUFLLElBQUksY0FBYyxFQUFFO1FBQ2hDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsR0FBRywwQkFBMEIsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksMEJBQTBCLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUM1SSxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNDO0tBQ0o7SUFFRCxpREFBaUQ7SUFDakQsTUFBTSxTQUFTLEdBQUcsTUFBTSxhQUFhLENBQUMsK0JBQStCLEVBQUUsQ0FBQztJQUV4RSwyQ0FBMkM7SUFDM0MsTUFBTSx5QkFBeUIsR0FBZ0MsRUFBRSxDQUFDO0lBQ2xFLEtBQUssTUFBTSxLQUFLLElBQUksU0FBUyxFQUFFO1FBQzNCLElBQUkseUJBQXlCLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQUUseUJBQXlCLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7WUFDbEoseUJBQXlCLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDeEY7SUFFRCxzREFBc0Q7SUFDdEQsTUFBTSxhQUFhLEdBQUksRUFBc0MsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQztJQUM5SCxNQUFNLDhCQUE4QixHQUFnQyxFQUFFLENBQUM7SUFDdkUsS0FBSyxNQUFNLEtBQUssSUFBSSxhQUFhLEVBQUU7UUFDL0IsSUFBSSw4QkFBOEIsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQztZQUFFLDhCQUE4QixDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDOztZQUM3SSw4QkFBOEIsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDL0U7SUFFRCxvQ0FBb0M7SUFDcEMsTUFBTSxjQUFjLEdBQUc7UUFDbkIsS0FBSyxFQUFHLEVBQXNDO1FBQzlDLFFBQVEsRUFBRyxFQUErQjtLQUM3QyxDQUFDO0lBQ0YsS0FBSyxNQUFNLE9BQU8sSUFBSSxhQUFhLEVBQUU7UUFDakMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDbkUsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNILHdEQUF3RDtZQUN4RCxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzlFLGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3RDO1NBQ0o7S0FDSjtJQUNELHdEQUF3RDtJQUN4RCxLQUFLLE1BQU0sS0FBSyxJQUFJLFNBQVMsRUFBRTtRQUMzQixJQUFJLENBQUMsOEJBQThCLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdkUsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdkM7YUFBTTtZQUNILElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUM3RixjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2QztTQUNKO0tBQ0o7SUFFRCxDQUFDLENBQUMsS0FBSyxDQUFDLHVCQUF1QixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUNqRSxDQUFDLENBQUMsS0FBSyxDQUFDLDBCQUEwQixpQkFBaUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUN2RSxDQUFDLENBQUMsS0FBSyxDQUFDLGtCQUFrQixZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUMsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ25FLENBQUMsQ0FBQyxLQUFLLENBQUMsMkJBQTJCLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ3pFLENBQUMsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLGNBQWMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUMzRCxDQUFDLENBQUMsS0FBSyxDQUFDLHVCQUF1QixjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFakUsa0RBQWtEO0lBQ2xELHlCQUF5QjtJQUN6QixNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDMUYsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkgsQ0FBQyxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO0lBRXRDLGtCQUFrQjtJQUNsQixNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFGLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuSCxDQUFDLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFFakMsdUJBQXVCO0lBQ3ZCLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqRyxNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNMLENBQUMsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQztJQUV2QywyQkFBMkI7SUFDM0IsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6RixNQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3TCxDQUFDLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7QUFDdkMsQ0FBQztBQXhQRCx3REF3UEMifQ==