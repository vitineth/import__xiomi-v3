import { LoggingDAO } from "../database/dao/LoggingDAO";
import { SecurityQuestionDAO } from "../database/dao/SecurityQuestionDAO";
import { TokenDAO } from "../database/dao/TokenDAO";
import { TwoFactorDAO } from "../database/dao/TwoFactorDAO";
import { UserDAO } from "../database/dao/UserDAO";
import { User } from "../database/models/User.model";
import { Login } from "../database/models/logs/Login.model";
import { SecurityQuestion } from "../database/models/security/SecurityQuestion.model";
import { SecurityQuestionMapping } from "../database/models/security/SecurityQuestionMapping.model";
import { ResetDataRequest } from "../database/models/ResetDataRequest.model";
import { RegistrationToken } from "../database/models/security/RegistrationToken.model";
import { TwoFactorConfiguration } from "../database/models/security/TwoFactorConfiguration.model";
import { LogAuth } from "../database/models/logs/LogAuth.model";
import { PermissionDAO } from "../database/dao/PermissionDAO";
import { Permission } from "../database/models/permissions/Permission.model";
import { PermissionGroup } from "../database/models/permissions/PermissionGroup.model";
import { PermissionGroupInheritance } from "../database/models/permissions/PermissionGroupInheritance.model";
import { PermissionGroupMapping } from "../database/models/permissions/PermissionGroupMapping.model";
import { PermissionGroupUserMapping } from "../database/models/permissions/PermissionGroupUserMapping";
import { SelfGrantAuthorizationModel } from "../database/models/permissions/SelfGrantAuthorization.model";
declare type InMemoryPermissionDatastore = {
    permissions: Permission[];
    groups: PermissionGroup[];
    inheritance: PermissionGroupInheritance[];
    groupMapping: PermissionGroupMapping[];
    users: PermissionGroupUserMapping[];
    grants: SelfGrantAuthorizationModel[];
};
export declare class InMemoryPermissionDAO implements PermissionDAO {
    private _data;
    constructor(_data: InMemoryPermissionDatastore);
    get data(): InMemoryPermissionDatastore;
    set data(value: InMemoryPermissionDatastore);
    findPermissionByGroupID(id: number): Promise<Permission[]>;
    findSelfAuthorizationGrantByID(id: number): Promise<SelfGrantAuthorizationModel>;
    findGroupByName(group: string): Promise<PermissionGroup>;
    findPermissionByName(permission: string): Promise<Permission>;
    findUserGroupMapping(group: PermissionGroup, user: User): Promise<PermissionGroupUserMapping>;
    fetchAllPermissionGroupInheritance(): Promise<PermissionGroupInheritance[]>;
    fetchAllPermissionGroupMappings(): Promise<PermissionGroupMapping[]>;
    fetchAllPermissionGroups(): Promise<PermissionGroup[]>;
    fetchAllPermissions(): Promise<Permission[]>;
    createGroupInheritance(target: PermissionGroup, from: PermissionGroup): Promise<PermissionGroupInheritance>;
    createGroupMapping(permission: Permission, group: PermissionGroup): Promise<PermissionGroupMapping>;
    createPermission(name: string, description: string, hash: string): Promise<Permission>;
    createPermissionAutohash(name: string, description: string): Promise<Permission>;
    createPermissionGroup(name: string, description: string, hash: string): Promise<PermissionGroup>;
    createPermissionGroupAutohash(name: string, description: string): Promise<PermissionGroup>;
    createUserGroupMapping(group: PermissionGroup, user: User): Promise<PermissionGroupUserMapping>;
    removeGroupInheritance(permission: PermissionGroupInheritance): Promise<void>;
    removeGroupMapping(permission: PermissionGroupMapping): Promise<void>;
    removePermission(permission: Permission): Promise<void>;
    removePermissionGroup(permission: PermissionGroup): Promise<void>;
    removeUserGroupMapping(mapping: PermissionGroupUserMapping): Promise<void>;
    resolvePermissions(user: User): Promise<Permission[]>;
}
export declare class InMemoryLoggingDAO implements LoggingDAO {
    private _data;
    constructor(_data: {
        login: Login[];
        auth: LogAuth[];
    });
    get data(): {
        login: Login[];
        auth: LogAuth[];
    };
    set data(value: {
        login: Login[];
        auth: LogAuth[];
    });
    fetchLoginsByUser(user: User): Promise<Login[]>;
    logLogin(ipAddress: string, user: User): Promise<Login>;
    saveAuthenticationMessage(source: string, detail: string): Promise<LogAuth>;
}
export declare class InMemorySecurityQuestionDAO implements SecurityQuestionDAO {
    private _data;
    constructor(_data: {
        questions: SecurityQuestion[];
        mapping: SecurityQuestionMapping[];
    });
    get data(): {
        questions: SecurityQuestion[];
        mapping: SecurityQuestionMapping[];
    };
    set data(value: {
        questions: SecurityQuestion[];
        mapping: SecurityQuestionMapping[];
    });
    findAllQuestions(): Promise<SecurityQuestion[]>;
    findQuestionByIDs(user: User, id: number): Promise<SecurityQuestionMapping>;
    findQuestionsByUserID(userID: number): Promise<SecurityQuestion[]>;
    saveSecurityAnswer(user: User, db: SecurityQuestion, answer: string): Promise<SecurityQuestionMapping>;
}
export declare class InMemoryTokenDAO implements TokenDAO {
    private _data;
    constructor(_data: {
        reset: ResetDataRequest[];
        registration: RegistrationToken[];
    });
    get data(): {
        reset: ResetDataRequest[];
        registration: RegistrationToken[];
    };
    set data(value: {
        reset: ResetDataRequest[];
        registration: RegistrationToken[];
    });
    findPasswordChangeRequest(userToken: string, token: string): Promise<ResetDataRequest>;
    findRegistrationToken(userToken: string, registrationToken: string): Promise<RegistrationToken>;
    markChangeComplete(request: ResetDataRequest): Promise<ResetDataRequest>;
    markRegistrationComplete(request: RegistrationToken): Promise<RegistrationToken>;
    requestPasswordChange(user: User): Promise<ResetDataRequest>;
    requestRegistrationToken(user: User): Promise<RegistrationToken>;
}
export declare class InMemoryTwoFactorDAO implements TwoFactorDAO {
    private _data;
    constructor(_data: TwoFactorConfiguration[]);
    get data(): TwoFactorConfiguration[];
    set data(value: TwoFactorConfiguration[]);
    findConfigurationByUser(user: User): Promise<TwoFactorConfiguration>;
}
export declare class InMemoryUserDAO implements UserDAO {
    private _data;
    constructor(_data: User[]);
    get data(): User[];
    set data(value: User[]);
    changeUserPassword(user: User, hash: string, hashVersion: number): Promise<User>;
    changeUserPasswordAutohash(user: User, password: string): Promise<User>;
    createUser(username: string, email: string, name: string, hash: string, hashVersion: number, pin: string, pinVersion: number, status: string): Promise<User>;
    createUserAutohash(username: string, email: string, name: string, password: string, pin: string, status: string): Promise<User>;
    findUserByID(id: number): Promise<User>;
    findUserByUsername(username: string): Promise<User>;
    isEmailAvailable(email: string): Promise<boolean>;
    isUsernameAvailable(username: string): Promise<boolean>;
    makeUserAccessible(user: User): Promise<User>;
}
export {};
