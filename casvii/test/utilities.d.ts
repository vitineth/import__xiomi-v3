import { UserDAO } from "../database/dao/UserDAO";
import { User } from "../database/models/User.model";
import { ObjectSession } from "../utilities/session/impl/ObjectSession";
import * as TestingDAO from "./in-memory-daos";
import { InMemoryLoggingDAO, InMemoryPermissionDAO, InMemorySecurityQuestionDAO, InMemoryTokenDAO, InMemoryTwoFactorDAO, InMemoryUserDAO } from "./in-memory-daos";
import { ResetDataRequest } from "../database/models/ResetDataRequest.model";
export declare namespace TestingUtilities {
    /**
     * Contains demonstration hashes produced using argon2 on the commmand line using argon2id as described by VERSION 0.
     * Password = "Test1ngPa55w0rd!"
     * Pin = "0101"
     */
    const testingHash: {
        password: {
            version: number;
            hash: string;
        };
        pin: {
            version: number;
            hash: string;
        };
    };
    /**
     * Constructs a user with basic properties and the hashes as described in {@link testingHash}.
     * @param dao the user dao through which this user should be created
     */
    const buildUser: (dao: UserDAO) => Promise<User>;
    /**
     * Builds a session using the given user (seeing the ID and provisional status to ACTIVE) but all properties can be
     * overwritten by the optionals variable
     * @param user the user that this session should represent
     * @param optionals overwrite keys for the session
     */
    const buildSession: (user: User, optionals?: {
        [key: string]: any;
    }) => ObjectSession;
    /**
     * Constructs and adds a change request to the token dao with optional overrides for the given user.
     * @param user the user to have their password reset
     * @param tokenDAO the token dao through which this token should be created
     * @param optionals the optional override data for this element
     */
    const buildChangeRequest: (user: User, tokenDAO: TestingDAO.InMemoryTokenDAO, optionals?: {
        [key: string]: any;
    }) => Promise<ResetDataRequest>;
    /**
     * A single type continaing a logging, security question, token, two factor and user in memory DAO
     */
    type DaoSet = {
        loggingDAO: InMemoryLoggingDAO;
        securityQuestionDAO: InMemorySecurityQuestionDAO;
        tokenDAO: InMemoryTokenDAO;
        twoFactorDAO: InMemoryTwoFactorDAO;
        userDAO: InMemoryUserDAO;
        permissionDAO: InMemoryPermissionDAO;
    };
    /**
     * Generates all in memory DAOs as a single object
     * @returns All DAOs in an object
     */
    const generateDAOs: () => DaoSet;
    /**
     * Resets the backing data stores of all the daos passed in to the function
     * @param daos the dao set to reset
     */
    const resetDAOs: (daos: DaoSet) => void;
}
