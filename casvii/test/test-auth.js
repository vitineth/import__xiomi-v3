"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const TestingDAO = __importStar(require("./in-memory-daos"));
const ObjectSession_1 = require("../utilities/session/impl/ObjectSession");
const chai_1 = require("chai");
const _ = __importStar(require("logger"));
const CASEmailProvider_1 = require("../mail/CASEmailProvider");
const PasswordProcessImpl_1 = require("../actions/process/PasswordProcessImpl");
const AuthenticationProcessImpl_1 = require("../actions/process/AuthenticationProcessImpl");
const Constants_1 = require("../utilities/Constants");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
let authenticator;
let password;
let loggingDAO;
let securityQuestionDAO;
let tokenDAO;
let twoFactorDAO;
let userDAO;
// Setup the mocking for node mailer
before(async () => {
    _.setMinimumLevel(_.LogLevel.TRACE);
    const transport = {
        sendMail: async (data) => {
            return {};
        },
    };
    loggingDAO = new TestingDAO.InMemoryLoggingDAO({ auth: [], login: [] });
    securityQuestionDAO = new TestingDAO.InMemorySecurityQuestionDAO({ questions: [], mapping: [] });
    tokenDAO = new TestingDAO.InMemoryTokenDAO({ reset: [], registration: [] });
    twoFactorDAO = new TestingDAO.InMemoryTwoFactorDAO([]);
    userDAO = new TestingDAO.InMemoryUserDAO([]);
    // @ts-ignore - this is valid because we are mocking it, I know it only calls sendMail
    const mailer = new CASEmailProvider_1.CASEmailProvider(transport);
    authenticator = AuthenticationProcessImpl_1.AuthenticationProcessImpl.construct(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
    password = PasswordProcessImpl_1.PasswordProcessImpl.construct(mailer, loggingDAO, userDAO, tokenDAO);
});
/**
 * Before each test, reset the backing data store to each test happens in isolation
 */
beforeEach(async () => {
    loggingDAO.data = { auth: [], login: [] };
    securityQuestionDAO.data = { mapping: [], questions: [] };
    tokenDAO.data = { registration: [], reset: [] };
    twoFactorDAO.data = [];
    userDAO.data = [];
});
// Hashed externally as "Test1ngPa55w0rd!"
const testingHash = {
    password: {
        version: 0,
        hash: '$argon2id$v=19$m=4096,t=3,p=1$BY8W0h15/Xe2XYMobZufew$3b+/Wg7WHd3rpUFA/ZsFks8/J0rjRBHPpNMx2nZSvmk',
    },
    pin: {
        version: 0,
        hash: '$argon2id$v=19$m=4096,t=3,p=1$vVMpAHnn7P8A6UW3XQ/zOA$jNjlgFXKhBCqNGXWivCq0Bvtl8J7PddHdDL9Xz52sM4',
    },
};
const buildUser = (dao) => dao.createUser("username", "email", "name", testingHash.password.hash, testingHash.password.version, testingHash.pin.hash, testingHash.pin.version, 'accessible');
const buildSession = (user) => {
    const session = new ObjectSession_1.ObjectSession();
    session.set('login.userID', user.id);
    session.set('login.provisional', AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
    return session;
};
describe('actions', () => {
    describe('password', () => {
        describe('change when authenticated', () => {
            it('should return a 400 response when provided no user authentication information', async () => {
                const response = await password.changePasswordAuthenticated({}, new ObjectSession_1.ObjectSession());
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when a user is provided but has no data', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({}, session);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when a user is provided but is missing data', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    pin: '',
                }, session);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when the users password does not match', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'olld password',
                    new1: '*ab12!abcDefGH',
                    new2: '*ab12!abcDefGH',
                    pin: '',
                }, session);
                // console.log(response.additionalData);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when the users pin does not match', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'Test1ngPa55w0rd!',
                    new1: '*ab12!abcDefGH',
                    new2: '*ab12!abcDefGH',
                    pin: '0000',
                }, session);
                console.log(response.additionalData);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when the users pin is an invalid format', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'Test1ngPa55w0rd!',
                    new1: '*ab12!abcDefGH',
                    new2: '*ab12!abcDefGH',
                    pin: 'abcde',
                }, session);
                // console.log(response.additionalData);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when the supplied passwords do not match', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'Test1ngPa55w0rd!',
                    new1: 'passwordone',
                    new2: 'passwordtwo',
                    pin: '0000',
                }, session);
                // Verify the response
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.PASSWORD_MATCH);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should reject with a bad request (400) when the user provides a password that is too weak', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'Test1ngPa55w0rd!',
                    new1: 'passwordone',
                    new2: 'passwordone',
                    pin: '0000',
                }, session);
                // The feedback is variable so extract the text
                let r = PUBLIC_RESPONSES.WEAK_PASSWORD('');
                r = r.substr(0, r.length - 4);
                // Verify the response
                chai_1.assert.isTrue(response.externalMessage.includes(r), `${response.externalMessage} did not contain ${r}`);
                chai_1.assert.equal(response.statusCode, 400);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            });
            it('should give a positive response when given valid information', async () => {
                // Add a user to the backing data store and then register this user in the session
                const user = await buildUser(userDAO);
                const session = buildSession(user);
                const original = user.lastRotation;
                // Then run the request
                const response = await password.changePasswordAuthenticated({
                    old: 'Test1ngPa55w0rd!',
                    new1: '*ab12!abcDefGH',
                    new2: '*ab12!abcDefGH',
                    pin: '0101',
                }, session);
                chai_1.assert.isTrue(response.externalMessage.startsWith(PUBLIC_RESPONSES.SUCCESSFUL('')), `'${response.externalMessage}' did not start with '${PUBLIC_RESPONSES.SUCCESSFUL('')}'`);
                chai_1.assert.equal(response.statusCode, 200);
                chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
                const u = await userDAO.findUserByID(user.id);
                chai_1.assert.notEqual(u.hash, testingHash.password.hash);
                chai_1.assert.notEqual(u.lastRotation, original);
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1hdXRoLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS90ZXN0L3Rlc3QtYXV0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFFQSw2REFBK0M7QUFFL0MsMkVBQXdFO0FBQ3hFLCtCQUE4QjtBQUc5QiwwQ0FBNEI7QUFDNUIsK0RBQTREO0FBQzVELGdGQUE2RTtBQUM3RSw0RkFHc0Q7QUFDdEQsc0RBQW1EO0FBQ25ELElBQU8sZ0JBQWdCLEdBQUcscUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztBQUVyRCxJQUFJLGFBQXVDLENBQUM7QUFDNUMsSUFBSSxRQUE0QixDQUFDO0FBRWpDLElBQUksVUFBeUMsQ0FBQztBQUM5QyxJQUFJLG1CQUEyRCxDQUFDO0FBQ2hFLElBQUksUUFBcUMsQ0FBQztBQUMxQyxJQUFJLFlBQTZDLENBQUM7QUFDbEQsSUFBSSxPQUFtQyxDQUFDO0FBQ3hDLG9DQUFvQztBQUVwQyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7SUFDZCxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFcEMsTUFBTSxTQUFTLEdBQUc7UUFDZCxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBdUMsRUFBRTtZQUMxRCxPQUFPLEVBQUUsQ0FBQztRQUNkLENBQUM7S0FDSixDQUFDO0lBRUYsVUFBVSxHQUFHLElBQUksVUFBVSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN4RSxtQkFBbUIsR0FBRyxJQUFJLFVBQVUsQ0FBQywyQkFBMkIsQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDakcsUUFBUSxHQUFHLElBQUksVUFBVSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM1RSxZQUFZLEdBQUcsSUFBSSxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkQsT0FBTyxHQUFHLElBQUksVUFBVSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqRCxzRkFBc0Y7SUFDbEYsTUFBTSxNQUFNLEdBQUcsSUFBSSxtQ0FBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUUvQyxhQUFhLEdBQUcscURBQXlCLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BILFFBQVEsR0FBRyx5Q0FBbUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDcEYsQ0FBQyxDQUFDLENBQ0Q7QUFFRDs7R0FFRztBQUNILFVBQVUsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUNsQixVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUM7SUFDMUMsbUJBQW1CLENBQUMsSUFBSSxHQUFHLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLENBQUM7SUFDMUQsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDO0lBQ2hELFlBQVksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0FBQ3RCLENBQUMsQ0FBQyxDQUFDO0FBRUgsMENBQTBDO0FBQzFDLE1BQU0sV0FBVyxHQUFHO0lBQ2hCLFFBQVEsRUFBRTtRQUNOLE9BQU8sRUFBRSxDQUFDO1FBQ1YsSUFBSSxFQUFFLGtHQUFrRztLQUMzRztJQUNELEdBQUcsRUFBRTtRQUNELE9BQU8sRUFBRSxDQUFDO1FBQ1YsSUFBSSxFQUFFLGtHQUFrRztLQUMzRztDQUNKLENBQUM7QUFFRixNQUFNLFNBQVMsR0FBRyxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztBQUN0TSxNQUFNLFlBQVksR0FBRyxDQUFDLElBQVUsRUFBRSxFQUFFO0lBQ2hDLE1BQU0sT0FBTyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO0lBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLDJEQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pFLE9BQU8sT0FBTyxDQUFDO0FBQ25CLENBQUMsQ0FBQztBQUVGLFFBQVEsQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFO0lBQ3JCLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxFQUFFO1FBQ3RCLFFBQVEsQ0FBQywyQkFBMkIsRUFBRSxHQUFHLEVBQUU7WUFDdkMsRUFBRSxDQUFDLCtFQUErRSxFQUFFLEtBQUssSUFBSSxFQUFFO2dCQUMzRixNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxFQUFFLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztnQkFFckYsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsRUFBRSxDQUFDLGdGQUFnRixFQUFFLEtBQUssSUFBSSxFQUFFO2dCQUM1RixrRkFBa0Y7Z0JBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5DLHVCQUF1QjtnQkFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUV6RSxzQkFBc0I7Z0JBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUNqRixDQUFDLENBQUMsQ0FBQztZQUNILEVBQUUsQ0FBQyxvRkFBb0YsRUFBRSxLQUFLLElBQUksRUFBRTtnQkFDaEcsa0ZBQWtGO2dCQUNsRixNQUFNLElBQUksR0FBRyxNQUFNLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVuQyx1QkFBdUI7Z0JBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDO29CQUN4RCxHQUFHLEVBQUUsRUFBRTtpQkFDVixFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUVaLHNCQUFzQjtnQkFDdEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsRUFBRSxDQUFDLCtFQUErRSxFQUFFLEtBQUssSUFBSSxFQUFFO2dCQUMzRixrRkFBa0Y7Z0JBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5DLHVCQUF1QjtnQkFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7b0JBQ3hELEdBQUcsRUFBRSxlQUFlO29CQUNwQixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixHQUFHLEVBQUUsRUFBRTtpQkFDVixFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUVaLHdDQUF3QztnQkFDeEMsc0JBQXNCO2dCQUN0QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDOUUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUNqRixDQUFDLENBQUMsQ0FBQztZQUNILEVBQUUsQ0FBQywwRUFBMEUsRUFBRSxLQUFLLElBQUksRUFBRTtnQkFDdEYsa0ZBQWtGO2dCQUNsRixNQUFNLElBQUksR0FBRyxNQUFNLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVuQyx1QkFBdUI7Z0JBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDO29CQUN4RCxHQUFHLEVBQUUsa0JBQWtCO29CQUN2QixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixHQUFHLEVBQUUsTUFBTTtpQkFDZCxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUVaLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNyQyxzQkFBc0I7Z0JBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUM5RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsRUFBRSxDQUFDLGdGQUFnRixFQUFFLEtBQUssSUFBSSxFQUFFO2dCQUM1RixrRkFBa0Y7Z0JBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5DLHVCQUF1QjtnQkFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7b0JBQ3hELEdBQUcsRUFBRSxrQkFBa0I7b0JBQ3ZCLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLEdBQUcsRUFBRSxPQUFPO2lCQUNmLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBRVosd0NBQXdDO2dCQUN4QyxzQkFBc0I7Z0JBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUM5RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsRUFBRSxDQUFDLGlGQUFpRixFQUFFLEtBQUssSUFBSSxFQUFFO2dCQUM3RixrRkFBa0Y7Z0JBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN0QyxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRW5DLHVCQUF1QjtnQkFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7b0JBQ3hELEdBQUcsRUFBRSxrQkFBa0I7b0JBQ3ZCLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsYUFBYTtvQkFDbkIsR0FBRyxFQUFFLE1BQU07aUJBQ2QsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFFWixzQkFBc0I7Z0JBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDeEUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUNqRixDQUFDLENBQUMsQ0FBQztZQUNILEVBQUUsQ0FBQywyRkFBMkYsRUFBRSxLQUFLLElBQUksRUFBRTtnQkFDdkcsa0ZBQWtGO2dCQUNsRixNQUFNLElBQUksR0FBRyxNQUFNLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVuQyx1QkFBdUI7Z0JBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDO29CQUN4RCxHQUFHLEVBQUUsa0JBQWtCO29CQUN2QixJQUFJLEVBQUUsYUFBYTtvQkFDbkIsSUFBSSxFQUFFLGFBQWE7b0JBQ25CLEdBQUcsRUFBRSxNQUFNO2lCQUNkLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBRVosK0NBQStDO2dCQUMvQyxJQUFJLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzNDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUU5QixzQkFBc0I7Z0JBQ3RCLGFBQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsZUFBZSxvQkFBb0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEcsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUNqRixDQUFDLENBQUMsQ0FBQztZQUNILEVBQUUsQ0FBQyw4REFBOEQsRUFBRSxLQUFLLElBQUksRUFBRTtnQkFDMUUsa0ZBQWtGO2dCQUNsRixNQUFNLElBQUksR0FBRyxNQUFNLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVuQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUVuQyx1QkFBdUI7Z0JBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDO29CQUN4RCxHQUFHLEVBQUUsa0JBQWtCO29CQUN2QixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixJQUFJLEVBQUUsZ0JBQWdCO29CQUN0QixHQUFHLEVBQUUsTUFBTTtpQkFDZCxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUVaLGFBQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxRQUFRLENBQUMsZUFBZSx5QkFBeUIsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDN0ssYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztnQkFFN0UsTUFBTSxDQUFDLEdBQUcsTUFBTSxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUMsYUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25ELGFBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsQ0FBQztZQUM5QyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQyJ9