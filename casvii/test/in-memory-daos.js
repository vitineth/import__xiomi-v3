"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InMemoryUserDAO = exports.InMemoryTwoFactorDAO = exports.InMemoryTokenDAO = exports.InMemorySecurityQuestionDAO = exports.InMemoryLoggingDAO = exports.InMemoryPermissionDAO = void 0;
const AuthenticationUtilities = __importStar(require("../utilities/AuthenticationUtilities"));
const AuthenticationUtilities_1 = require("../utilities/AuthenticationUtilities");
const uuid_1 = require("uuid");
const crypto = __importStar(require("crypto"));
function nextID(data) {
    if (data === undefined || data.length === 0)
        return 0;
    return Math.max.apply(null, data.map(d => d.id)) + 1;
}
class InMemoryPermissionDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async findPermissionByGroupID(id) {
        return this._data.groupMapping.filter(e => e.permissionGroupID === id).map(e => e.permission);
    }
    async findSelfAuthorizationGrantByID(id) {
        const results = this._data.grants.find(e => e.id === id);
        if (results === undefined)
            throw new Error(`Not found`);
        return results;
    }
    async findGroupByName(group) {
        const results = this._data.groups.find(e => e.name === group);
        if (results === undefined)
            throw new Error(`Not found`);
        return results;
    }
    async findPermissionByName(permission) {
        const results = this._data.permissions.find(e => e.name === permission);
        if (results === undefined)
            throw new Error(`Not found`);
        return results;
    }
    async findUserGroupMapping(group, user) {
        const results = this._data.users.find(e => e.permissionGroup.id === group.id && e.grantedByUser.id === user.id);
        if (results === undefined)
            throw new Error(`Not found`);
        return results;
    }
    async fetchAllPermissionGroupInheritance() {
        return this._data.inheritance;
    }
    async fetchAllPermissionGroupMappings() {
        return this._data.groupMapping;
    }
    async fetchAllPermissionGroups() {
        return this._data.groups;
    }
    async fetchAllPermissions() {
        return this._data.permissions;
    }
    async createGroupInheritance(target, from) {
        const obj = {
            targetGroup: target,
            targetGroupID: target.id,
            inheritsFrom: from,
            inheritsFromID: from.id,
            id: nextID(this._data.inheritance),
        };
        // @ts-ignore
        this._data.inheritance.push(obj);
        console.log(`Created inheritance group: ${JSON.stringify(obj)}`);
        // @ts-ignore
        return obj;
    }
    async createGroupMapping(permission, group) {
        const obj = {
            permission,
            permissionGroup: group,
            permissionID: permission.id,
            permissionGroupID: group.id,
            id: nextID(this._data.groupMapping),
        };
        // @ts-ignore
        this._data.groupMapping.push(obj);
        console.log(`Created group mapping: ${JSON.stringify(obj)}`);
        // @ts-ignore
        return obj;
    }
    async createPermission(name, description, hash) {
        const obj = {
            name,
            description,
            hash,
            id: nextID(this._data.permissions),
        };
        // @ts-ignore
        this._data.permissions.push(obj);
        console.log(`Created permission: ${JSON.stringify(obj)}`);
        // @ts-ignore
        return obj;
    }
    createPermissionAutohash(name, description) {
        return this.createPermission(name, description, crypto.createHash('sha256').update(name).digest('hex'));
    }
    async createPermissionGroup(name, description, hash) {
        const obj = {
            name,
            description,
            hash,
            id: nextID(this._data.groups),
        };
        // @ts-ignore
        this._data.groups.push(obj);
        console.log(`Created permission group: ${JSON.stringify(obj)}`);
        // @ts-ignore
        return obj;
    }
    createPermissionGroupAutohash(name, description) {
        return this.createPermissionGroup(name, description, crypto.createHash('sha256').update(name).digest('hex'));
    }
    async createUserGroupMapping(group, user) {
        const obj = {
            id: nextID(this._data.users),
            permissionGroup: group,
            permissionGroupID: group.id,
            grantedByUser: user,
            grantedByUserID: user.id,
        };
        // @ts-ignore
        this._data.users.push(obj);
        console.log(`Created user group mapping: ${JSON.stringify(obj)}`);
        // @ts-ignore
        return obj;
    }
    async removeGroupInheritance(permission) {
        const l = this._data.inheritance.length;
        this._data.inheritance = this._data.inheritance.filter(e => e.id !== permission.id);
        console.log(`Removed group inheritance: ${JSON.stringify(permission)} going from length ${l} to ${this._data.inheritance.length}`);
    }
    async removeGroupMapping(permission) {
        const l = this._data.groupMapping.length;
        this._data.groupMapping = this._data.groupMapping.filter(e => e.id !== permission.id);
        console.log(`Removed group mapping: ${JSON.stringify(permission)} going from length ${l} to ${this._data.groupMapping.length}`);
    }
    async removePermission(permission) {
        const l = this._data.permissions.length;
        this._data.permissions = this._data.permissions.filter(e => e.id !== permission.id);
        console.log(`Removed permission: ${JSON.stringify(permission)} going from length ${l} to ${this._data.permissions.length}`);
    }
    async removePermissionGroup(permission) {
        const l = this._data.groups.length;
        this._data.groups = this._data.groups.filter(e => e.id !== permission.id);
        console.log(`Removed group: ${JSON.stringify(permission)} going from length ${l} to ${this._data.groups.length}`);
    }
    async removeUserGroupMapping(mapping) {
        const l = this._data.users.length;
        this._data.users = this._data.users.filter(e => e.id !== mapping.id);
        console.log(`Removed group user mapping: ${JSON.stringify(mapping)} going from length ${l} to ${this._data.users.length}`);
    }
    async resolvePermissions(user) {
        // Get every mapping and inheritance group
        const mappings = await this.fetchAllPermissionGroupMappings();
        const inheritance = await this.fetchAllPermissionGroupInheritance();
        // Then find all the groups that this user directly has
        const directInheritance = this._data.users.filter(e => e.grantedByUserID === user.id);
        // Then we want to recursively process these
        async function recurseOnInheritance(group) {
            const inheritanceGroups = inheritance.filter(e => e.targetGroup.id === group.id);
            const permissions = await Promise.all(inheritanceGroups.map(e => recurseOnInheritance(e.inheritsFrom)));
            const directPermissions = (mappings.filter(e => e.permissionGroupID === group.id).map(e => e.permission));
            return [].concat.apply([], permissions).concat(directPermissions);
        }
        // Then finally apply this to all the direct inheritances and join them
        const allResults = await Promise.all(directInheritance.map(e => recurseOnInheritance(e.permissionGroup)));
        return [].concat.apply([], allResults);
    }
}
exports.InMemoryPermissionDAO = InMemoryPermissionDAO;
class InMemoryLoggingDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async fetchLoginsByUser(user) {
        const result = [];
        for (const entry of this._data.login) {
            if (entry.userID === user.id)
                result.push(entry);
        }
        return result;
    }
    async logLogin(ipAddress, user) {
        const entry = {
            user,
            ipAddress,
            id: nextID(this._data.login),
            userID: user.id,
            logged: new Date(),
        };
        // @ts-ignore
        this._data.login.push(entry);
        // @ts-ignore
        return entry;
    }
    async saveAuthenticationMessage(source, detail) {
        const entry = {
            source,
            detail,
            id: nextID(this._data.auth),
            logged: new Date(),
        };
        // @ts-ignore
        this._data.auth.push(entry);
        // @ts-ignore
        return entry;
    }
}
exports.InMemoryLoggingDAO = InMemoryLoggingDAO;
class InMemorySecurityQuestionDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async findAllQuestions() {
        // @ts-ignore
        return this._data.questions;
    }
    async findQuestionByIDs(user, id) {
        for (const entry of this._data.mapping) {
            if (entry.userID === user.id && entry.id === id)
                return entry;
        }
        throw new Error('');
    }
    async findQuestionsByUserID(userID) {
        return this._data.mapping.filter(e => e.userID === userID).map(e => e.securityQuestion);
    }
    async saveSecurityAnswer(user, db, answer) {
        const hashResult = await AuthenticationUtilities_1.hashPassword(answer);
        const hash = `${hashResult[0]}:${hashResult[1]}`;
        // @ts-ignore
        const element = {
            user,
            id: nextID(this._data.mapping),
            securityQuestion: db,
            securityQuestionID: db.id,
            answerHash: hash,
            userID: user.id,
        };
        this._data.mapping.push(element);
        return element;
    }
}
exports.InMemorySecurityQuestionDAO = InMemorySecurityQuestionDAO;
class InMemoryTokenDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async findPasswordChangeRequest(userToken, token) {
        for (const entry of this._data.reset) {
            if (entry.type === 'password' && entry.userToken === userToken && entry.resetToken === token)
                return entry;
        }
        throw new Error('');
    }
    async findRegistrationToken(userToken, registrationToken) {
        for (const entry of this._data.registration) {
            if (entry.userToken === userToken && entry.resetToken === registrationToken)
                return entry;
        }
        throw new Error('');
    }
    async markChangeComplete(request) {
        for (const entry of this._data.reset) {
            if (entry.id === request.id) {
                entry.completed = true;
                return entry;
            }
        }
        throw new Error('');
    }
    async markRegistrationComplete(request) {
        for (const entry of this._data.registration) {
            if (entry.id === request.id) {
                entry.completed = true;
                return entry;
            }
        }
        throw new Error('');
    }
    async requestPasswordChange(user) {
        const entry = {
            user,
            id: nextID(this._data.reset),
            requested: new Date(),
            userID: user.id,
            userToken: uuid_1.v4().toString(),
            resetToken: uuid_1.v4().toString(),
            type: 'password',
            completed: false,
            additionalData: '',
        };
        // @ts-ignore
        this._data.reset.push(entry);
        // @ts-ignore
        return entry;
    }
    requestRegistrationToken(user) {
        const entry = {
            user,
            id: nextID(this._data.reset),
            requested: new Date(),
            userID: user.id,
            userToken: uuid_1.v4().toString(),
            resetToken: uuid_1.v4().toString(),
            completed: false,
        };
        // @ts-ignore
        this._data.registration.push(entry);
        // @ts-ignore
        return entry;
    }
}
exports.InMemoryTokenDAO = InMemoryTokenDAO;
class InMemoryTwoFactorDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async findConfigurationByUser(user) {
        for (const entry of this._data) {
            if (entry.userID === user.id)
                return entry;
        }
        throw new Error('');
    }
}
exports.InMemoryTwoFactorDAO = InMemoryTwoFactorDAO;
class InMemoryUserDAO {
    constructor(_data) {
        this._data = _data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
    async changeUserPassword(user, hash, hashVersion) {
        for (const entry of this._data) {
            if (entry.id === user.id) {
                entry.hash = hash;
                entry.hashVersion = hashVersion;
                entry.lastRotation = new Date();
                return entry;
            }
        }
        throw new Error('');
    }
    async changeUserPasswordAutohash(user, password) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        return this.changeUserPassword(user, hash, hashVersion);
    }
    async createUser(username, email, name, hash, hashVersion, pin, pinVersion, status) {
        const entry = {
            hash,
            hashVersion,
            id: nextID(this.data),
            pinHash: pin,
            username: username.toLowerCase(),
            email: email.toLowerCase(),
            full_name: name,
            pinHashVersion: pinVersion,
            lockedStatus: status,
            registered: true,
        };
        // @ts-ignore
        this._data.push(entry);
        // @ts-ignore
        return entry;
    }
    async createUserAutohash(username, email, name, password, pin, status) {
        const [hashVersion, hash] = await AuthenticationUtilities.hashPassword(password);
        const [pinVersion, pinHash] = await AuthenticationUtilities.hashPin(pin);
        return this.createUser(username, email, name, hash, hashVersion, pinHash, pinVersion, status);
    }
    async findUserByID(id) {
        for (const entry of this._data) {
            if (entry.id === id)
                return entry;
        }
        throw new Error('');
    }
    async findUserByUsername(username) {
        for (const entry of this._data) {
            if (entry.username === username.toLowerCase())
                return entry;
        }
        throw new Error('');
    }
    async isEmailAvailable(email) {
        for (const entry of this._data) {
            if (entry.email === email.toLowerCase())
                return false;
        }
        return true;
    }
    async isUsernameAvailable(username) {
        for (const entry of this._data) {
            if (entry.username === username.toLowerCase())
                return false;
        }
        return true;
    }
    async makeUserAccessible(user) {
        for (const entry of this._data) {
            if (entry.id === user.id) {
                entry.lockedStatus = 'accessible';
                return entry;
            }
        }
        throw new Error('');
    }
}
exports.InMemoryUserDAO = InMemoryUserDAO;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW4tbWVtb3J5LWRhb3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY2FzdmlpL3Rlc3QvaW4tbWVtb3J5LWRhb3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVlBLDhGQUFnRjtBQUNoRixrRkFBb0U7QUFRcEUsK0JBQTBCO0FBQzFCLCtDQUFpQztBQUdqQyxTQUFTLE1BQU0sQ0FBQyxJQUFzQjtJQUNsQyxJQUFJLElBQUksS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDO1FBQUUsT0FBTyxDQUFDLENBQUM7SUFDdEQsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUN6RCxDQUFDO0FBV0QsTUFBYSxxQkFBcUI7SUFFOUIsWUFBb0IsS0FBa0M7UUFBbEMsVUFBSyxHQUFMLEtBQUssQ0FBNkI7SUFDdEQsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBa0M7UUFDdkMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxFQUFVO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixLQUFLLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNsRyxDQUFDO0lBRUQsS0FBSyxDQUFDLDhCQUE4QixDQUFDLEVBQVU7UUFDM0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUV6RCxJQUFJLE9BQU8sS0FBSyxTQUFTO1lBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUV4RCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBRUQsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFhO1FBQy9CLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUM7UUFFOUQsSUFBSSxPQUFPLEtBQUssU0FBUztZQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFeEQsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxVQUFrQjtRQUN6QyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDO1FBRXhFLElBQUksT0FBTyxLQUFLLFNBQVM7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXhELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRCxLQUFLLENBQUMsb0JBQW9CLENBQUMsS0FBc0IsRUFBRSxJQUFVO1FBQ3pELE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWhILElBQUksT0FBTyxLQUFLLFNBQVM7WUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXhELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRCxLQUFLLENBQUMsa0NBQWtDO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7SUFDbEMsQ0FBQztJQUVELEtBQUssQ0FBQywrQkFBK0I7UUFDakMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztJQUNuQyxDQUFDO0lBRUQsS0FBSyxDQUFDLHdCQUF3QjtRQUMxQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzdCLENBQUM7SUFFRCxLQUFLLENBQUMsbUJBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7SUFDbEMsQ0FBQztJQUVELEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxNQUF1QixFQUFFLElBQXFCO1FBQ3ZFLE1BQU0sR0FBRyxHQUFHO1lBQ1IsV0FBVyxFQUFFLE1BQU07WUFDbkIsYUFBYSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ3hCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUN2QixFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1NBQ3JDLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWpFLGFBQWE7UUFDYixPQUFPLEdBQUcsQ0FBQztJQUVmLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsVUFBc0IsRUFBRSxLQUFzQjtRQUNuRSxNQUFNLEdBQUcsR0FBRztZQUNSLFVBQVU7WUFDVixlQUFlLEVBQUUsS0FBSztZQUN0QixZQUFZLEVBQUUsVUFBVSxDQUFDLEVBQUU7WUFDM0IsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDM0IsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztTQUN0QyxDQUFDO1FBRUYsYUFBYTtRQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVsQyxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU3RCxhQUFhO1FBQ2IsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRUQsS0FBSyxDQUFDLGdCQUFnQixDQUFDLElBQVksRUFBRSxXQUFtQixFQUFFLElBQVk7UUFDbEUsTUFBTSxHQUFHLEdBQUc7WUFDUixJQUFJO1lBQ0osV0FBVztZQUNYLElBQUk7WUFDSixFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1NBQ3JDLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWpDLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRTFELGFBQWE7UUFDYixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxJQUFZLEVBQUUsV0FBbUI7UUFDdEQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUM1RyxDQUFDO0lBRUQsS0FBSyxDQUFDLHFCQUFxQixDQUFDLElBQVksRUFBRSxXQUFtQixFQUFFLElBQVk7UUFDdkUsTUFBTSxHQUFHLEdBQUc7WUFDUixJQUFJO1lBQ0osV0FBVztZQUNYLElBQUk7WUFDSixFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1NBQ2hDLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWhFLGFBQWE7UUFDYixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCw2QkFBNkIsQ0FBQyxJQUFZLEVBQUUsV0FBbUI7UUFDM0QsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNqSCxDQUFDO0lBRUQsS0FBSyxDQUFDLHNCQUFzQixDQUFDLEtBQXNCLEVBQUUsSUFBVTtRQUMzRCxNQUFNLEdBQUcsR0FBRztZQUNSLEVBQUUsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDNUIsZUFBZSxFQUFFLEtBQUs7WUFDdEIsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDM0IsYUFBYSxFQUFFLElBQUk7WUFDbkIsZUFBZSxFQUFFLElBQUksQ0FBQyxFQUFFO1NBQzNCLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWxFLGFBQWE7UUFDYixPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCxLQUFLLENBQUMsc0JBQXNCLENBQUMsVUFBc0M7UUFDL0QsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3BGLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUN2SSxDQUFDO0lBRUQsS0FBSyxDQUFDLGtCQUFrQixDQUFDLFVBQWtDO1FBQ3ZELE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQztRQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0RixPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDcEksQ0FBQztJQUVELEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFzQjtRQUN6QyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ2hJLENBQUM7SUFFRCxLQUFLLENBQUMscUJBQXFCLENBQUMsVUFBMkI7UUFDbkQsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUN0SCxDQUFDO0lBRUQsS0FBSyxDQUFDLHNCQUFzQixDQUFDLE9BQW1DO1FBQzVELE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFDL0gsQ0FBQztJQUVELEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFVO1FBQy9CLDBDQUEwQztRQUMxQyxNQUFNLFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1FBQzlELE1BQU0sV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLGtDQUFrQyxFQUFFLENBQUM7UUFFcEUsdURBQXVEO1FBQ3ZELE1BQU0saUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFdEYsNENBQTRDO1FBQzVDLEtBQUssVUFBVSxvQkFBb0IsQ0FBQyxLQUFzQjtZQUN0RCxNQUFNLGlCQUFpQixHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFFakYsTUFBTSxXQUFXLEdBQUcsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEcsTUFBTSxpQkFBaUIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBRTFHLE9BQVEsRUFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN4RixDQUFDO1FBRUQsdUVBQXVFO1FBQ3ZFLE1BQU0sVUFBVSxHQUFHLE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFHLE9BQVEsRUFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM3RCxDQUFDO0NBRUo7QUF2TkQsc0RBdU5DO0FBRUQsTUFBYSxrQkFBa0I7SUFFM0IsWUFBb0IsS0FBMEM7UUFBMUMsVUFBSyxHQUFMLEtBQUssQ0FBcUM7SUFDOUQsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBMEM7UUFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFVO1FBQzlCLE1BQU0sTUFBTSxHQUFZLEVBQUUsQ0FBQztRQUUzQixLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ2xDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVELEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBaUIsRUFBRSxJQUFVO1FBQ3hDLE1BQU0sS0FBSyxHQUFHO1lBQ1YsSUFBSTtZQUNKLFNBQVM7WUFDVCxFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzVCLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNmLE1BQU0sRUFBRSxJQUFJLElBQUksRUFBRTtTQUNyQixDQUFDO1FBRUYsYUFBYTtRQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixhQUFhO1FBQ2IsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxNQUFjLEVBQUUsTUFBYztRQUMxRCxNQUFNLEtBQUssR0FBRztZQUNWLE1BQU07WUFDTixNQUFNO1lBQ04sRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUMzQixNQUFNLEVBQUUsSUFBSSxJQUFJLEVBQUU7U0FDckIsQ0FBQztRQUNGLGFBQWE7UUFDYixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUIsYUFBYTtRQUNiLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Q0FFSjtBQW5ERCxnREFtREM7QUFFRCxNQUFhLDJCQUEyQjtJQUVwQyxZQUFvQixLQUE0RTtRQUE1RSxVQUFLLEdBQUwsS0FBSyxDQUF1RTtJQUNoRyxDQUFDO0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFJLElBQUksQ0FBQyxLQUE0RTtRQUNqRixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQsS0FBSyxDQUFDLGdCQUFnQjtRQUNsQixhQUFhO1FBQ2IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsS0FBSyxDQUFDLGlCQUFpQixDQUFDLElBQVUsRUFBRSxFQUFVO1FBQzFDLEtBQUssTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDcEMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxFQUFFLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFO2dCQUFFLE9BQU8sS0FBSyxDQUFDO1NBQ2pFO1FBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsS0FBSyxDQUFDLHFCQUFxQixDQUFDLE1BQWM7UUFDdEMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsSUFBVSxFQUFFLEVBQW9CLEVBQUUsTUFBYztRQUNyRSxNQUFNLFVBQVUsR0FBRyxNQUFNLHNDQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsTUFBTSxJQUFJLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFakQsYUFBYTtRQUNiLE1BQU0sT0FBTyxHQUE0QjtZQUNyQyxJQUFJO1lBQ0osRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUM5QixnQkFBZ0IsRUFBRSxFQUFFO1lBQ3BCLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3pCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtTQUNsQixDQUFDO1FBRUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Q0FFSjtBQWhERCxrRUFnREM7QUFFRCxNQUFhLGdCQUFnQjtJQUV6QixZQUFvQixLQUF1RTtRQUF2RSxVQUFLLEdBQUwsS0FBSyxDQUFrRTtJQUMzRixDQUFDO0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFJLElBQUksQ0FBQyxLQUF1RTtRQUM1RSxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQsS0FBSyxDQUFDLHlCQUF5QixDQUFDLFNBQWlCLEVBQUUsS0FBYTtRQUM1RCxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ2xDLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxVQUFVLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxTQUFTLElBQUksS0FBSyxDQUFDLFVBQVUsS0FBSyxLQUFLO2dCQUFFLE9BQU8sS0FBSyxDQUFDO1NBQzlHO1FBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsS0FBSyxDQUFDLHFCQUFxQixDQUFDLFNBQWlCLEVBQUUsaUJBQXlCO1FBQ3BFLEtBQUssTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7WUFDekMsSUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLFNBQVMsSUFBSSxLQUFLLENBQUMsVUFBVSxLQUFLLGlCQUFpQjtnQkFBRSxPQUFPLEtBQUssQ0FBQztTQUM3RjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUF5QjtRQUM5QyxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ2xDLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsRUFBRSxFQUFFO2dCQUN6QixLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdkIsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxPQUEwQjtRQUNyRCxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO1lBQ3pDLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsRUFBRSxFQUFFO2dCQUN6QixLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdkIsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxJQUFVO1FBQ2xDLE1BQU0sS0FBSyxHQUFHO1lBQ1YsSUFBSTtZQUNKLEVBQUUsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDNUIsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFO1lBQ3JCLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNmLFNBQVMsRUFBRSxTQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDMUIsVUFBVSxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMzQixJQUFJLEVBQUUsVUFBVTtZQUNoQixTQUFTLEVBQUUsS0FBSztZQUNoQixjQUFjLEVBQUUsRUFBRTtTQUNyQixDQUFDO1FBRUYsYUFBYTtRQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU3QixhQUFhO1FBQ2IsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELHdCQUF3QixDQUFDLElBQVU7UUFDL0IsTUFBTSxLQUFLLEdBQUc7WUFDVixJQUFJO1lBQ0osRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUM1QixTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ2YsU0FBUyxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMxQixVQUFVLEVBQUUsU0FBRSxFQUFFLENBQUMsUUFBUSxFQUFFO1lBQzNCLFNBQVMsRUFBRSxLQUFLO1NBQ25CLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBDLGFBQWE7UUFDYixPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0NBRUo7QUFyRkQsNENBcUZDO0FBRUQsTUFBYSxvQkFBb0I7SUFFN0IsWUFBb0IsS0FBK0I7UUFBL0IsVUFBSyxHQUFMLEtBQUssQ0FBMEI7SUFDbkQsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBK0I7UUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxJQUFVO1FBQ3BDLEtBQUssTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUM1QixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQUUsT0FBTyxLQUFLLENBQUM7U0FDOUM7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7Q0FDSjtBQW5CRCxvREFtQkM7QUFFRCxNQUFhLGVBQWU7SUFFeEIsWUFBb0IsS0FBYTtRQUFiLFVBQUssR0FBTCxLQUFLLENBQVE7SUFDakMsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxJQUFJLENBQUMsS0FBYTtRQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQsS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQVUsRUFBRSxJQUFZLEVBQUUsV0FBbUI7UUFDbEUsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzVCLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUN0QixLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDbEIsS0FBSyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7Z0JBQ2hDLEtBQUssQ0FBQyxZQUFZLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztnQkFDaEMsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxJQUFVLEVBQUUsUUFBZ0I7UUFDekQsTUFBTSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsR0FBRyxNQUFNLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxLQUFLLENBQUMsVUFBVSxDQUFDLFFBQWdCLEVBQUUsS0FBYSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsV0FBbUIsRUFBRSxHQUFXLEVBQUUsVUFBa0IsRUFBRSxNQUFjO1FBQzlJLE1BQU0sS0FBSyxHQUFHO1lBQ1YsSUFBSTtZQUNKLFdBQVc7WUFDWCxFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDckIsT0FBTyxFQUFFLEdBQUc7WUFDWixRQUFRLEVBQUUsUUFBUSxDQUFDLFdBQVcsRUFBRTtZQUNoQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsRUFBRTtZQUMxQixTQUFTLEVBQUUsSUFBSTtZQUNmLGNBQWMsRUFBRSxVQUFVO1lBQzFCLFlBQVksRUFBRSxNQUFNO1lBQ3BCLFVBQVUsRUFBRSxJQUFJO1NBQ25CLENBQUM7UUFFRixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdkIsYUFBYTtRQUNiLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBZ0IsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLFFBQWdCLEVBQUUsR0FBVyxFQUFFLE1BQWM7UUFDakgsTUFBTSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsR0FBRyxNQUFNLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRixNQUFNLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxHQUFHLE1BQU0sdUJBQXVCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEcsQ0FBQztJQUVELEtBQUssQ0FBQyxZQUFZLENBQUMsRUFBVTtRQUN6QixLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDNUIsSUFBSSxLQUFLLENBQUMsRUFBRSxLQUFLLEVBQUU7Z0JBQUUsT0FBTyxLQUFLLENBQUM7U0FDckM7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBZ0I7UUFDckMsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzVCLElBQUksS0FBSyxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsV0FBVyxFQUFFO2dCQUFFLE9BQU8sS0FBSyxDQUFDO1NBQy9EO1FBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsS0FBSyxDQUFDLGdCQUFnQixDQUFDLEtBQWE7UUFDaEMsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzVCLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsV0FBVyxFQUFFO2dCQUFFLE9BQU8sS0FBSyxDQUFDO1NBQ3pEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxRQUFnQjtRQUN0QyxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDNUIsSUFBSSxLQUFLLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxXQUFXLEVBQUU7Z0JBQUUsT0FBTyxLQUFLLENBQUM7U0FDL0Q7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQVU7UUFDL0IsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzVCLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUN0QixLQUFLLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztnQkFDbEMsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQztDQUVKO0FBL0ZELDBDQStGQyJ9