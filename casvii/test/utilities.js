"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestingUtilities = void 0;
const ObjectSession_1 = require("../utilities/session/impl/ObjectSession");
const AuthenticationProcessImpl_1 = require("../actions/process/AuthenticationProcessImpl");
const in_memory_daos_1 = require("./in-memory-daos");
var TestingUtilities;
(function (TestingUtilities) {
    /**
     * Contains demonstration hashes produced using argon2 on the commmand line using argon2id as described by VERSION 0.
     * Password = "Test1ngPa55w0rd!"
     * Pin = "0101"
     */
    TestingUtilities.testingHash = {
        password: {
            version: 0,
            hash: '$argon2id$v=19$m=4096,t=3,p=1$BY8W0h15/Xe2XYMobZufew$3b+/Wg7WHd3rpUFA/ZsFks8/J0rjRBHPpNMx2nZSvmk',
        },
        pin: {
            version: 0,
            hash: '$argon2id$v=19$m=4096,t=3,p=1$vVMpAHnn7P8A6UW3XQ/zOA$jNjlgFXKhBCqNGXWivCq0Bvtl8J7PddHdDL9Xz52sM4',
        },
    };
    /**
     * Constructs a user with basic properties and the hashes as described in {@link testingHash}.
     * @param dao the user dao through which this user should be created
     */
    TestingUtilities.buildUser = (dao) => dao.createUser("username", "email@example.org", "name", TestingUtilities.testingHash.password.hash, TestingUtilities.testingHash.password.version, TestingUtilities.testingHash.pin.hash, TestingUtilities.testingHash.pin.version, 'accessible');
    /**
     * Builds a session using the given user (seeing the ID and provisional status to ACTIVE) but all properties can be
     * overwritten by the optionals variable
     * @param user the user that this session should represent
     * @param optionals overwrite keys for the session
     */
    TestingUtilities.buildSession = (user, optionals = {}) => {
        const session = new ObjectSession_1.ObjectSession();
        session.set('login.userID', user.id);
        session.set('login.provisional', AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
        for (const key of Object.keys(optionals))
            session.set(key, optionals[key]);
        return session;
    };
    /**
     * Constructs and adds a change request to the token dao with optional overrides for the given user.
     * @param user the user to have their password reset
     * @param tokenDAO the token dao through which this token should be created
     * @param optionals the optional override data for this element
     */
    TestingUtilities.buildChangeRequest = async (user, tokenDAO, optionals = {}) => {
        const element = await tokenDAO.requestPasswordChange(user);
        const index = tokenDAO.data.reset.indexOf(element);
        for (const key of Object.keys(optionals)) {
            element[key] = optionals[key];
        }
        // Update it with the changed data just in case
        tokenDAO.data.reset[index] = element;
        return element;
    };
    /**
     * Generates all in memory DAOs as a single object
     * @returns All DAOs in an object
     */
    TestingUtilities.generateDAOs = () => {
        return {
            loggingDAO: new in_memory_daos_1.InMemoryLoggingDAO({ auth: [], login: [] }),
            securityQuestionDAO: new in_memory_daos_1.InMemorySecurityQuestionDAO({ questions: [], mapping: [] }),
            tokenDAO: new in_memory_daos_1.InMemoryTokenDAO({ reset: [], registration: [] }),
            twoFactorDAO: new in_memory_daos_1.InMemoryTwoFactorDAO([]),
            userDAO: new in_memory_daos_1.InMemoryUserDAO([]),
            permissionDAO: new in_memory_daos_1.InMemoryPermissionDAO({
                inheritance: [],
                groupMapping: [],
                groups: [],
                permissions: [],
                users: [],
                grants: [],
            }),
        };
    };
    /**
     * Resets the backing data stores of all the daos passed in to the function
     * @param daos the dao set to reset
     */
    TestingUtilities.resetDAOs = (daos) => {
        daos.loggingDAO.data = { auth: [], login: [] };
        daos.securityQuestionDAO.data = { mapping: [], questions: [] };
        daos.tokenDAO.data = { registration: [], reset: [] };
        daos.twoFactorDAO.data = [];
        daos.userDAO.data = [];
        daos.permissionDAO.data = {
            inheritance: [],
            groupMapping: [],
            groups: [],
            permissions: [],
            users: [],
            grants: [],
        };
    };
})(TestingUtilities = exports.TestingUtilities || (exports.TestingUtilities = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbGl0aWVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2Nhc3ZpaS90ZXN0L3V0aWxpdGllcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFFQSwyRUFBd0U7QUFDeEUsNEZBQStGO0FBRS9GLHFEQU8wQjtBQUcxQixJQUFpQixnQkFBZ0IsQ0FpSGhDO0FBakhELFdBQWlCLGdCQUFnQjtJQUU3Qjs7OztPQUlHO0lBQ1UsNEJBQVcsR0FBRztRQUN2QixRQUFRLEVBQUU7WUFDTixPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxrR0FBa0c7U0FDM0c7UUFDRCxHQUFHLEVBQUU7WUFDRCxPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxrR0FBa0c7U0FDM0c7S0FDSixDQUFDO0lBRUY7OztPQUdHO0lBQ1UsMEJBQVMsR0FBRyxDQUFDLEdBQVksRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxFQUFFLGlCQUFBLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGlCQUFBLFdBQVcsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLGlCQUFBLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGlCQUFBLFdBQVcsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFDO0lBRXpOOzs7OztPQUtHO0lBQ1UsNkJBQVksR0FBRyxDQUFDLElBQVUsRUFBRSxZQUFvQyxFQUFFLEVBQUUsRUFBRTtRQUMvRSxNQUFNLE9BQU8sR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztRQUVwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV6RSxLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFM0UsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQyxDQUFDO0lBRUY7Ozs7O09BS0c7SUFDVSxtQ0FBa0IsR0FBRyxLQUFLLEVBQUUsSUFBVSxFQUFFLFFBQXFDLEVBQUUsWUFBb0MsRUFBRSxFQUE2QixFQUFFO1FBQzdKLE1BQU0sT0FBTyxHQUFHLE1BQU0sUUFBUSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNELE1BQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVuRCxLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNqQztRQUNELCtDQUErQztRQUMvQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxPQUFPLENBQUM7UUFFckMsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQyxDQUFDO0lBY0Y7OztPQUdHO0lBQ1UsNkJBQVksR0FBRyxHQUFXLEVBQUU7UUFDckMsT0FBTztZQUNILFVBQVUsRUFBRSxJQUFJLG1DQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUM7WUFDM0QsbUJBQW1CLEVBQUUsSUFBSSw0Q0FBMkIsQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQ3BGLFFBQVEsRUFBRSxJQUFJLGlDQUFnQixDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUM7WUFDL0QsWUFBWSxFQUFFLElBQUkscUNBQW9CLENBQUMsRUFBRSxDQUFDO1lBQzFDLE9BQU8sRUFBRSxJQUFJLGdDQUFlLENBQUMsRUFBRSxDQUFDO1lBQ2hDLGFBQWEsRUFBRSxJQUFJLHNDQUFxQixDQUFDO2dCQUNyQyxXQUFXLEVBQUUsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLEVBQUU7YUFDYixDQUFDO1NBQ0wsQ0FBQztJQUNOLENBQUMsQ0FBQztJQUVGOzs7T0FHRztJQUNVLDBCQUFTLEdBQUcsQ0FBQyxJQUFZLEVBQUUsRUFBRTtRQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQy9DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEdBQUc7WUFDdEIsV0FBVyxFQUFFLEVBQUU7WUFDZixZQUFZLEVBQUUsRUFBRTtZQUNoQixNQUFNLEVBQUUsRUFBRTtZQUNWLFdBQVcsRUFBRSxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtTQUNiLENBQUM7SUFDTixDQUFDLENBQUM7QUFDTixDQUFDLEVBakhnQixnQkFBZ0IsR0FBaEIsd0JBQWdCLEtBQWhCLHdCQUFnQixRQWlIaEMifQ==