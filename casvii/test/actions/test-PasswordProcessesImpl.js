"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const TestingDAO = __importStar(require("../in-memory-daos"));
const ObjectSession_1 = require("../../utilities/session/impl/ObjectSession");
const chai_1 = require("chai");
const _ = __importStar(require("logger"));
const CASEmailProvider_1 = require("../../mail/CASEmailProvider");
const PasswordProcessImpl_1 = require("../../actions/process/PasswordProcessImpl");
const AuthenticationProcessImpl_1 = require("../../actions/process/AuthenticationProcessImpl");
const Constants_1 = require("../../utilities/Constants");
const sinon_1 = require("sinon");
const ProcessImpl_1 = require("../../actions/ProcessImpl");
const utilities_1 = require("../utilities");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
let password;
let loggingDAO;
let securityQuestionDAO;
let tokenDAO;
let twoFactorDAO;
let userDAO;
let sendMailFake;
before(async () => {
    _.setMinimumLevel(_.LogLevel.TRACE);
    sendMailFake = sinon_1.fake.resolves({});
    const transport = {
        sendMail: sendMailFake,
    };
    loggingDAO = new TestingDAO.InMemoryLoggingDAO({ auth: [], login: [] });
    securityQuestionDAO = new TestingDAO.InMemorySecurityQuestionDAO({ questions: [], mapping: [] });
    tokenDAO = new TestingDAO.InMemoryTokenDAO({ reset: [], registration: [] });
    twoFactorDAO = new TestingDAO.InMemoryTwoFactorDAO([]);
    userDAO = new TestingDAO.InMemoryUserDAO([]);
    // @ts-ignore - this is valid because we are mocking it, I know it only calls sendMail
    const mailer = new CASEmailProvider_1.CASEmailProvider(transport);
    password = PasswordProcessImpl_1.PasswordProcessImpl.construct(mailer, loggingDAO, userDAO, tokenDAO);
});
/**
 * Before each test, reset the backing data store to each test happens in isolation
 */
beforeEach(async () => {
    sendMailFake.resetHistory();
    loggingDAO.data = { auth: [], login: [] };
    securityQuestionDAO.data = { mapping: [], questions: [] };
    tokenDAO.data = { registration: [], reset: [] };
    twoFactorDAO.data = [];
    userDAO.data = [];
});
describe('casvii/actions/processes/PasswordProcessImpl.ts', () => {
    describe('changePasswordUnauthenticatedSubmit', () => {
        // changePasswordUnauthenticatedSubmit
        // - No data => 400
        it('400 and fail on no data', async () => {
            const response = await password.changePasswordUnauthenticatedSubmit({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Mismatched password => 400
        it('400 and fail on mismatched password', async () => {
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: '',
                rt: '',
                pin: '',
                new1: 'one password',
                new2: 'not the same password',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.PASSWORD_MATCH);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Wrong pin => 400 + email
        it('400 and email (with email) on wrong pin', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const reset = await utilities_1.TestingUtilities.buildChangeRequest(user, tokenDAO);
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: reset.userToken,
                rt: reset.resetToken,
                pin: '0000',
                new1: 'A*(*DS235d',
                new2: 'A*(*DS235d',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.calledOnce);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Invalid tokens => 400
        it('400 and fail on invalid tokens', async () => {
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: 'invalid token',
                rt: 'invalid token',
                pin: '0000',
                new1: 'one password',
                new2: 'one password',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Missing data => 400
        it('400 and fail on missing data', async () => {
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: '',
                new2: 'one password',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Expired token => 400
        it('400 and fail on expired tokens', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const reset = await utilities_1.TestingUtilities.buildChangeRequest(user, tokenDAO, {
                requested: new Date(new Date().getTime() - 1.48e+7),
            });
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: reset.userToken,
                rt: reset.resetToken,
                pin: '0002',
                new1: 'A*(*DS235d',
                new2: 'A*(*DS235d',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.EXPIRED_TOKEN);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Expired token => 400
        it('400 and fail on a used token', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const reset = await utilities_1.TestingUtilities.buildChangeRequest(user, tokenDAO, {
                completed: true,
            });
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: reset.userToken,
                rt: reset.resetToken,
                pin: '0003',
                new1: 'A*(*DS235d',
                new2: 'A*(*DS235d',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.EXPIRED_TOKEN);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Weak password => 400
        it('400 and fail on a weak password', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const reset = await utilities_1.TestingUtilities.buildChangeRequest(user, tokenDAO);
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: reset.userToken,
                rt: reset.resetToken,
                pin: '0101',
                new1: 'one password',
                new2: 'one password',
            }, new ObjectSession_1.ObjectSession());
            // The feedback is variable so extract the text
            let r = PUBLIC_RESPONSES.WEAK_PASSWORD('');
            r = r.substr(0, r.length - 4);
            chai_1.assert.isTrue(response.externalMessage.startsWith(r), `Expect "${response.externalMessage}" to start with "${r}"`);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        // - Successful => 200 + database + email
        it('200 and success (with email) on valid data', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const reset = await utilities_1.TestingUtilities.buildChangeRequest(user, tokenDAO);
            const original = user.hash;
            const originalTime = user.lastRotation;
            const response = await password.changePasswordUnauthenticatedSubmit({
                ut: reset.userToken,
                rt: reset.resetToken,
                pin: '0101',
                new1: 'R3plac3m3ntPa55w0rd!*!*',
                new2: 'R3plac3m3ntPa55w0rd!*!*',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.isTrue(response.externalMessage.startsWith(PUBLIC_RESPONSES.SUCCESSFUL('')), 'Expecting success message');
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.calledOnce);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.notEqual(user.hash, original, 'Expected hashed not to match');
            chai_1.assert.notEqual(user.lastRotation, originalTime, 'Expected last rotation time not to match');
        });
    });
    describe('changePasswordUnauthenticatedRequest', () => {
        // changePasswordUnauthenticatedRequest
        // - No data => 400
        // - Mismatched username email => 400
        // - Wrong username => 400
        // - Write data => 200 + database change + email
        it('400 and fail on authenticated user', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = await utilities_1.TestingUtilities.buildSession(user);
            const response = await password.changePasswordUnauthenticatedRequest({
                username: 'invalid',
                email: 'invalid',
            }, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.AUTHORIZED);
            chai_1.assert.equal(tokenDAO.data.reset.length, 0, 'No reset tokens');
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('400 and fail on no data', async () => {
            const response = await password.changePasswordUnauthenticatedRequest({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(tokenDAO.data.reset.length, 0, 'No reset tokens');
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('200 and fail on a mismatched username and email', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            const response = await password.changePasswordUnauthenticatedRequest({
                username: 'username',
                email: 'not-the-right-email@example.org',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
            chai_1.assert.equal(tokenDAO.data.reset.length, 0, 'No reset tokens');
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('200 and fail on a wrong username', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            const response = await password.changePasswordUnauthenticatedRequest({
                username: 'no-the-username',
                email: 'not-the-right-email@example.org',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
            chai_1.assert.equal(tokenDAO.data.reset.length, 0, 'No reset tokens');
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('200 and success on right data', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const response = await password.changePasswordUnauthenticatedRequest({
                username: user.username,
                email: user.email,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.SUCCESS_UNAUTHENTICATED_CHANGE_REQUEST));
            chai_1.assert.equal(tokenDAO.data.reset.length, 1, 'No reset tokens');
            chai_1.assert.isTrue(sendMailFake.calledOnce);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
    });
    describe('changePasswordAuthenticated', () => {
        it('500 and fail on invalid user', async () => {
            // @ts-ignore - this is safe because the function only takes the user ID
            const session = utilities_1.TestingUtilities.buildSession({ id: -1 });
            console.log(session);
            const response = await password.changePasswordAuthenticated({
                old: '',
                new1: '',
                new2: '',
                pin: '',
            }, session);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            chai_1.assert.equal(response.statusCode, 500);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('302 and fail on non-active user', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = await utilities_1.TestingUtilities.buildSession(user, { 'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION });
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: '*ab12!abcDefGH',
                new2: '*ab12!abcDefGH',
                pin: '0101',
            }, session);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP);
            chai_1.assert.equal(response.statusCode, 302);
            chai_1.assert.isObject(response.additionalData);
            chai_1.assert.hasAllKeys(response.additionalData, ['Location']);
            // @ts-ignore - this is verified by assert.isObject
            chai_1.assert.equal(response.additionalData['Location'], '/casvii/login/flow/security');
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
        });
        it('400 and fail on no user authentication information', async () => {
            const response = await password.changePasswordAuthenticated({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
        });
        it('400 and fail when a user is provided but has no body', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({}, session);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
        });
        it('400 and fail when a user is provided but is missing body keys', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                pin: '',
            }, session);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
        });
        it('400 and fail when the users current password does not match', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'olld password',
                new1: '*ab12!abcDefGH',
                new2: '*ab12!abcDefGH',
                pin: '',
            }, session);
            // console.log(response.additionalData);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.called);
        });
        it('400 and fail when the users pin does not match', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: '*ab12!abcDefGH',
                new2: '*ab12!abcDefGH',
                pin: '0000',
            }, session);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.called);
        });
        it('400 and fail when the users pin is an invalid format', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: '*ab12!abcDefGH',
                new2: '*ab12!abcDefGH',
                pin: 'abcde',
            }, session);
            // console.log(response.additionalData);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PASSWORD_PIN);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.called);
        });
        it('400 and fail when the two supplied passwords do not match', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: 'passwordone',
                new2: 'passwordtwo',
                pin: '0000',
            }, session);
            // Verify the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.PASSWORD_MATCH);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            chai_1.assert.isTrue(sendMailFake.notCalled);
        });
        it('400 and fail when the user provides a password that is too weak', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: 'passwordone',
                new2: 'passwordone',
                pin: '0000',
            }, session);
            // The feedback is variable so extract the text
            let r = PUBLIC_RESPONSES.WEAK_PASSWORD('');
            r = r.substr(0, r.length - 4);
            // Verify the response
            chai_1.assert.isTrue(response.externalMessage.includes(r), `${response.externalMessage} did not contain ${r}`);
            chai_1.assert.equal(response.statusCode, 400);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
        });
        it('200 and success when given valid information', async () => {
            // Add a user to the backing data store and then register this user in the session
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            const original = user.lastRotation;
            // Then run the request
            const response = await password.changePasswordAuthenticated({
                old: 'Test1ngPa55w0rd!',
                new1: '*ab12!abcDefGH',
                new2: '*ab12!abcDefGH',
                pin: '0101',
            }, session);
            chai_1.assert.isTrue(response.externalMessage.startsWith(PUBLIC_RESPONSES.SUCCESSFUL('')), `'${response.externalMessage}' did not start with '${PUBLIC_RESPONSES.SUCCESSFUL('')}'`);
            chai_1.assert.equal(response.statusCode, 200);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'Expected one logging message');
            const u = await userDAO.findUserByID(user.id);
            chai_1.assert.notEqual(u.hash, utilities_1.TestingUtilities.testingHash.password.hash);
            chai_1.assert.notEqual(u.lastRotation, original);
            chai_1.assert.isTrue(sendMailFake.called);
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1QYXNzd29yZFByb2Nlc3Nlc0ltcGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY2FzdmlpL3Rlc3QvYWN0aW9ucy90ZXN0LVBhc3N3b3JkUHJvY2Vzc2VzSW1wbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw4REFBZ0Q7QUFFaEQsOEVBQTJFO0FBQzNFLCtCQUE4QjtBQUM5QiwwQ0FBNEI7QUFDNUIsa0VBQStEO0FBQy9ELG1GQUFnRjtBQUNoRiwrRkFBa0c7QUFDbEcseURBQXNEO0FBQ3RELGlDQUE2QjtBQUM3QiwyREFBd0U7QUFDeEUsNENBQWdEO0FBQ2hELElBQU8sZ0JBQWdCLEdBQUcscUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztBQUNyRCxJQUFPLG9CQUFvQixHQUFHLHFCQUFTLENBQUMsb0JBQW9CLENBQUM7QUFFN0QsSUFBSSxRQUE0QixDQUFDO0FBQ2pDLElBQUksVUFBeUMsQ0FBQztBQUM5QyxJQUFJLG1CQUEyRCxDQUFDO0FBQ2hFLElBQUksUUFBcUMsQ0FBQztBQUMxQyxJQUFJLFlBQTZDLENBQUM7QUFDbEQsSUFBSSxPQUFtQyxDQUFDO0FBQ3hDLElBQUksWUFBNEIsQ0FBQztBQUVqQyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7SUFDZCxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFcEMsWUFBWSxHQUFHLFlBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7SUFFakMsTUFBTSxTQUFTLEdBQUc7UUFDZCxRQUFRLEVBQUUsWUFBWTtLQUN6QixDQUFDO0lBRUYsVUFBVSxHQUFHLElBQUksVUFBVSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN4RSxtQkFBbUIsR0FBRyxJQUFJLFVBQVUsQ0FBQywyQkFBMkIsQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDakcsUUFBUSxHQUFHLElBQUksVUFBVSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM1RSxZQUFZLEdBQUcsSUFBSSxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkQsT0FBTyxHQUFHLElBQUksVUFBVSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqRCxzRkFBc0Y7SUFDbEYsTUFBTSxNQUFNLEdBQUcsSUFBSSxtQ0FBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUUvQyxRQUFRLEdBQUcseUNBQW1CLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0FBQ3BGLENBQUMsQ0FBQyxDQUNEO0FBRUQ7O0dBRUc7QUFDSCxVQUFVLENBQUMsS0FBSyxJQUFJLEVBQUU7SUFDbEIsWUFBWSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVCLFVBQVUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQztJQUMxQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsQ0FBQztJQUMxRCxRQUFRLENBQUMsSUFBSSxHQUFHLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUM7SUFDaEQsWUFBWSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7SUFDdkIsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7QUFDdEIsQ0FBQyxDQUFDLENBQUM7QUFFSCxRQUFRLENBQUMsaURBQWlELEVBQUUsR0FBRyxFQUFFO0lBQzdELFFBQVEsQ0FBQyxxQ0FBcUMsRUFBRSxHQUFHLEVBQUU7UUFDakQsc0NBQXNDO1FBRXRDLG1CQUFtQjtRQUNuQixFQUFFLENBQUMseUJBQXlCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDckMsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUMsRUFBRSxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFN0YsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3pFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsK0JBQStCO1FBQy9CLEVBQUUsQ0FBQyxxQ0FBcUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRCxNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQyxtQ0FBbUMsQ0FBQztnQkFDaEUsRUFBRSxFQUFFLEVBQUU7Z0JBQ04sRUFBRSxFQUFFLEVBQUU7Z0JBQ04sR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLElBQUksRUFBRSx1QkFBdUI7YUFDaEMsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILDZCQUE2QjtRQUM3QixFQUFFLENBQUMseUNBQXlDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDckQsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxLQUFLLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDeEUsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUM7Z0JBQ2hFLEVBQUUsRUFBRSxLQUFLLENBQUMsU0FBUztnQkFDbkIsRUFBRSxFQUFFLEtBQUssQ0FBQyxVQUFVO2dCQUNwQixHQUFHLEVBQUUsTUFBTTtnQkFDWCxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLFlBQVk7YUFDckIsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzlFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsMEJBQTBCO1FBQzFCLEVBQUUsQ0FBQyxnQ0FBZ0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM1QyxNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQyxtQ0FBbUMsQ0FBQztnQkFDaEUsRUFBRSxFQUFFLGVBQWU7Z0JBQ25CLEVBQUUsRUFBRSxlQUFlO2dCQUNuQixHQUFHLEVBQUUsTUFBTTtnQkFDWCxJQUFJLEVBQUUsY0FBYztnQkFDcEIsSUFBSSxFQUFFLGNBQWM7YUFDdkIsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILHdCQUF3QjtRQUN4QixFQUFFLENBQUMsOEJBQThCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDMUMsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUM7Z0JBQ2hFLEVBQUUsRUFBRSxFQUFFO2dCQUNOLElBQUksRUFBRSxjQUFjO2FBQ3ZCLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCx5QkFBeUI7UUFDekIsRUFBRSxDQUFDLGdDQUFnQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzVDLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sS0FBSyxHQUFHLE1BQU0sNEJBQWdCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRTtnQkFDcEUsU0FBUyxFQUFFLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsT0FBTyxDQUFDO2FBQ3RELENBQUMsQ0FBQztZQUVILE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLG1DQUFtQyxDQUFDO2dCQUNoRSxFQUFFLEVBQUUsS0FBSyxDQUFDLFNBQVM7Z0JBQ25CLEVBQUUsRUFBRSxLQUFLLENBQUMsVUFBVTtnQkFDcEIsR0FBRyxFQUFFLE1BQU07Z0JBQ1gsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLElBQUksRUFBRSxZQUFZO2FBQ3JCLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDdkUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCx5QkFBeUI7UUFDekIsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzFDLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sS0FBSyxHQUFHLE1BQU0sNEJBQWdCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRTtnQkFDcEUsU0FBUyxFQUFFLElBQUk7YUFDbEIsQ0FBQyxDQUFDO1lBRUgsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUM7Z0JBQ2hFLEVBQUUsRUFBRSxLQUFLLENBQUMsU0FBUztnQkFDbkIsRUFBRSxFQUFFLEtBQUssQ0FBQyxVQUFVO2dCQUNwQixHQUFHLEVBQUUsTUFBTTtnQkFDWCxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLFlBQVk7YUFDckIsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILHlCQUF5QjtRQUN6QixFQUFFLENBQUMsaUNBQWlDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDN0MsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxLQUFLLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFFeEUsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUM7Z0JBQ2hFLEVBQUUsRUFBRSxLQUFLLENBQUMsU0FBUztnQkFDbkIsRUFBRSxFQUFFLEtBQUssQ0FBQyxVQUFVO2dCQUNwQixHQUFHLEVBQUUsTUFBTTtnQkFDWCxJQUFJLEVBQUUsY0FBYztnQkFDcEIsSUFBSSxFQUFFLGNBQWM7YUFDdkIsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLCtDQUErQztZQUMvQyxJQUFJLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFFOUIsYUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLFFBQVEsQ0FBQyxlQUFlLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25ILGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgseUNBQXlDO1FBQ3pDLEVBQUUsQ0FBQyw0Q0FBNEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN4RCxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLEtBQUssR0FBRyxNQUFNLDRCQUFnQixDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN4RSxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzNCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFdkMsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsbUNBQW1DLENBQUM7Z0JBQ2hFLEVBQUUsRUFBRSxLQUFLLENBQUMsU0FBUztnQkFDbkIsRUFBRSxFQUFFLEtBQUssQ0FBQyxVQUFVO2dCQUNwQixHQUFHLEVBQUUsTUFBTTtnQkFDWCxJQUFJLEVBQUUseUJBQXlCO2dCQUMvQixJQUFJLEVBQUUseUJBQXlCO2FBQ2xDLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDakgsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDMUUsYUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQ3JFLGFBQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxZQUFZLEVBQUUsMENBQTBDLENBQUMsQ0FBQztRQUNqRyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUgsUUFBUSxDQUFDLHNDQUFzQyxFQUFFLEdBQUcsRUFBRTtRQUNsRCx1Q0FBdUM7UUFDdkMsbUJBQW1CO1FBQ25CLHFDQUFxQztRQUNyQywwQkFBMEI7UUFDMUIsZ0RBQWdEO1FBRWhELEVBQUUsQ0FBQyxvQ0FBb0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNoRCxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyxNQUFNLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUUxRCxNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQyxvQ0FBb0MsQ0FBQztnQkFDakUsUUFBUSxFQUFFLFNBQVM7Z0JBQ25CLEtBQUssRUFBRSxTQUFTO2FBQ25CLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFWixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDL0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMseUJBQXlCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDckMsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsb0NBQW9DLENBQUMsRUFBRSxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFOUYsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3pFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQy9ELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLGlEQUFpRCxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzdELE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFDLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLG9DQUFvQyxDQUFDO2dCQUNqRSxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsS0FBSyxFQUFFLGlDQUFpQzthQUMzQyxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUM7WUFDakksYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDL0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsa0NBQWtDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDOUMsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUMsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsb0NBQW9DLENBQUM7Z0JBQ2pFLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLEtBQUssRUFBRSxpQ0FBaUM7YUFDM0MsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxDQUFDO1lBQ2pJLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQy9ELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLCtCQUErQixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzNDLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLG9DQUFvQyxDQUFDO2dCQUNqRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNwQixFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUM7WUFDakksYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDL0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLEVBQUU7UUFDekMsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzFDLHdFQUF3RTtZQUN4RSxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFckIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7Z0JBQ3hELEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLEdBQUcsRUFBRSxFQUFFO2FBQ1YsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVaLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3QyxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyxNQUFNLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQywwQkFBMEIsRUFBRSxDQUFDLENBQUM7WUFDL0ksTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7Z0JBQ3hELEdBQUcsRUFBRSxrQkFBa0I7Z0JBQ3ZCLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLEdBQUcsRUFBRSxNQUFNO2FBQ2QsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVaLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ2pGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN6QyxhQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3pELG1EQUFtRDtZQUNuRCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQUUsNkJBQTZCLENBQUMsQ0FBQztZQUNqRixhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxvREFBb0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNoRSxNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxFQUFFLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUVyRixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHNEQUFzRCxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2xFLGtGQUFrRjtZQUNsRixNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFcEQsdUJBQXVCO1lBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUV6RSxzQkFBc0I7WUFDdEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3pFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQywrREFBK0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMzRSxrRkFBa0Y7WUFDbEYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXBELHVCQUF1QjtZQUN2QixNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQztnQkFDeEQsR0FBRyxFQUFFLEVBQUU7YUFDVixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDhCQUE4QixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsNkRBQTZELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDekUsa0ZBQWtGO1lBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVwRCx1QkFBdUI7WUFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7Z0JBQ3hELEdBQUcsRUFBRSxlQUFlO2dCQUNwQixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixHQUFHLEVBQUUsRUFBRTthQUNWLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFWix3Q0FBd0M7WUFDeEMsc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzlFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxnREFBZ0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM1RCxrRkFBa0Y7WUFDbEYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXBELHVCQUF1QjtZQUN2QixNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQztnQkFDeEQsR0FBRyxFQUFFLGtCQUFrQjtnQkFDdkIsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsR0FBRyxFQUFFLE1BQU07YUFDZCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzlFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxzREFBc0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNsRSxrRkFBa0Y7WUFDbEYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXBELHVCQUF1QjtZQUN2QixNQUFNLFFBQVEsR0FBRyxNQUFNLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQztnQkFDeEQsR0FBRyxFQUFFLGtCQUFrQjtnQkFDdkIsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsR0FBRyxFQUFFLE9BQU87YUFDZixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosd0NBQXdDO1lBQ3hDLHNCQUFzQjtZQUN0QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUM5RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDhCQUE4QixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMkRBQTJELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDdkUsa0ZBQWtGO1lBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVwRCx1QkFBdUI7WUFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7Z0JBQ3hELEdBQUcsRUFBRSxrQkFBa0I7Z0JBQ3ZCLElBQUksRUFBRSxhQUFhO2dCQUNuQixJQUFJLEVBQUUsYUFBYTtnQkFDbkIsR0FBRyxFQUFFLE1BQU07YUFDZCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDhCQUE4QixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsaUVBQWlFLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDN0Usa0ZBQWtGO1lBQ2xGLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVwRCx1QkFBdUI7WUFDdkIsTUFBTSxRQUFRLEdBQUcsTUFBTSxRQUFRLENBQUMsMkJBQTJCLENBQUM7Z0JBQ3hELEdBQUcsRUFBRSxrQkFBa0I7Z0JBQ3ZCLElBQUksRUFBRSxhQUFhO2dCQUNuQixJQUFJLEVBQUUsYUFBYTtnQkFDbkIsR0FBRyxFQUFFLE1BQU07YUFDZCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosK0NBQStDO1lBQy9DLElBQUksQ0FBQyxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMzQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUU5QixzQkFBc0I7WUFDdEIsYUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxlQUFlLG9CQUFvQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3hHLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsOEJBQThCLENBQUMsQ0FBQztRQUNqRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyw4Q0FBOEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMxRCxrRkFBa0Y7WUFDbEYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXBELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFFbkMsdUJBQXVCO1lBQ3ZCLE1BQU0sUUFBUSxHQUFHLE1BQU0sUUFBUSxDQUFDLDJCQUEyQixDQUFDO2dCQUN4RCxHQUFHLEVBQUUsa0JBQWtCO2dCQUN2QixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixHQUFHLEVBQUUsTUFBTTthQUNkLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFWixhQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLElBQUksUUFBUSxDQUFDLGVBQWUseUJBQXlCLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0ssYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSw4QkFBOEIsQ0FBQyxDQUFDO1lBRTdFLE1BQU0sQ0FBQyxHQUFHLE1BQU0sT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDOUMsYUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLDRCQUFnQixDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEUsYUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQyJ9