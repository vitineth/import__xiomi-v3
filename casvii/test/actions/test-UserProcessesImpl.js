"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = require("sinon");
const CASEmailProvider_1 = require("../../mail/CASEmailProvider");
const _ = __importStar(require("logger"));
const UserProcessesImpl_1 = require("../../actions/process/UserProcessesImpl");
const utilities_1 = require("../utilities");
const ObjectSession_1 = require("../../utilities/session/impl/ObjectSession");
const chai_1 = require("chai");
const Constants_1 = require("../../utilities/Constants");
const ProcessImpl_1 = require("../../actions/ProcessImpl");
const uuid_1 = require("uuid");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
let userProcess;
let sendMailFake;
const daos = utilities_1.TestingUtilities.generateDAOs();
before(async () => {
    _.setMinimumLevel(_.LogLevel.TRACE);
    sendMailFake = sinon_1.fake.resolves({});
    // @ts-ignore - this is valid because we are mocking it, I know it only calls sendMail
    const mailer = new CASEmailProvider_1.CASEmailProvider({
        sendMail: sendMailFake,
    });
    userProcess = UserProcessesImpl_1.UserProcessesImpl.construct(daos.loggingDAO, daos.userDAO, daos.tokenDAO, daos.securityQuestionDAO, daos.permissionDAO, mailer);
});
/**
 * Before each test, reset the backing data store to each test happens in isolation
 */
beforeEach(async () => {
    sendMailFake.resetHistory();
    utilities_1.TestingUtilities.resetDAOs(daos);
    // Setup security questions
    // @ts-ignore
    daos.securityQuestionDAO.data.questions.push({
        id: 0,
        question: 'q1',
    });
    // @ts-ignore
    daos.securityQuestionDAO.data.questions.push({
        id: 1,
        question: 'q2',
    });
    // @ts-ignore
    daos.securityQuestionDAO.data.questions.push({
        id: 2,
        question: 'q3',
    });
    await daos.permissionDAO.createPermissionGroupAutohash(`default`, `default`);
});
describe('casvii/actions/processes/UserProcessesImpl.ts', () => {
    describe('registerUser', () => {
        // Register user takes the following bits of data:
        // * 'username'
        // * 'email'
        // * 'name'
        // * 'password'
        // * 'confirmation'
        // * 'pin'
        // * 'sc1'
        // * 'sc2'
        // * 'sc3'
        // * 'sc1a'
        // * 'sc2a'
        // * 'sc3a'
        // No data
        // Logged in user
        // Username is taken
        // Email is taken
        // Username of different case is taken (username vs USername)
        // Email of different case is taken (email vs EMail)
        // Passwords don't match
        // Password aren't secure enough
        // Pin doesn't match format
        // Pin is too weak?
        // Security question are the same
        // Successful submission
        it('BAD_REQUEST on no data provided in request', async () => {
            const response = await userProcess.registerUser({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('AUTHORIZED on a logged in user registering', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(daos.userDAO));
            const response = await userProcess.registerUser({
                username: 'usernamedss',
                email: 'new@example',
                password: 'something',
                name: 'person',
                confirmation: 'something',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, session);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.AUTHORIZED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on email not being valid', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'usernamedss',
                email: 'new@example',
                password: 'something',
                name: 'person',
                confirmation: 'something',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_EMAIL);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on username taken', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'username',
                email: 'new@example.org',
                password: 'S)me23thing!',
                name: 'person',
                confirmation: 'S)me23thing!',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.USERNAME_EMAIL_IN_USE);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on email taken', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'email@example.org',
                password: 'S)me23thing!',
                name: 'person',
                confirmation: 'S)me23thing!',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.USERNAME_EMAIL_IN_USE);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on different case username already in use', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'UsERname',
                email: 'new@example.org',
                password: 'S)me23thing!',
                name: 'person',
                confirmation: 'S)me23thing!',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.USERNAME_EMAIL_IN_USE);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on different case email already in use', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'Email@exAMple.org',
                password: 'S)me23thing!',
                name: 'person',
                confirmation: 'S)me23thing!',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.USERNAME_EMAIL_IN_USE);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on passwords not matching', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'something',
                name: 'person',
                confirmation: 'something-else',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.PASSWORD_MATCH);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on insecure passwords', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'something',
                name: 'person',
                confirmation: 'something',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            // The feedback is variable so extract the text
            let r = PUBLIC_RESPONSES.WEAK_PASSWORD('');
            r = r.substr(0, r.length - 4);
            chai_1.assert.isTrue(response.externalMessage.startsWith(r), `Expect "${response.externalMessage}" to start with "${r}"`);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on pin not matching format', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'Test1ngPa55w0rd!',
                name: 'person',
                confirmation: 'Test1ngPa55w0rd!',
                pin: 'abd245',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_PIN_FORMAT);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on security questions being the same', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'Test1ngPa55w0rd!',
                name: 'person',
                confirmation: 'Test1ngPa55w0rd!',
                pin: '000000',
                sc1: 0,
                sc2: 0,
                sc3: 0,
                sc1a: '',
                sc2a: '',
                sc3a: '',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SECURITY_QUESTIONS_NOT_FOUND);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on security questions that don\'t exist', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'Test1ngPa55w0rd!',
                name: 'person',
                confirmation: 'Test1ngPa55w0rd!',
                pin: '000000',
                sc1: 0,
                sc2: 1,
                sc3: 7,
                sc1a: 'a',
                sc2a: 'b',
                sc3a: 'c',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SECURITY_QUESTIONS_NOT_FOUND);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('SUCCESS on successful submission', async () => {
            await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            const response = await userProcess.registerUser({
                username: 'not-taken',
                email: 'new-not-taken@example.org',
                password: 'Test1ngPa55w0rd!',
                name: 'person',
                confirmation: 'Test1ngPa55w0rd!',
                pin: '000000',
                sc1: 0,
                sc2: 1,
                sc3: 2,
                sc1a: 'a',
                sc2a: 'b',
                sc3a: 'c',
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.REGISTERED));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.calledOnce);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
    });
    describe('registerConfirmation', () => {
        // Takes data
        // * ut
        // * rt
        // no data
        // logged in user (?)
        // invalid tokens x2
        // used token
        // expired token
        // correct request
        it('BAD_REQUEST on no data provided in request', async () => {
            const response = await userProcess.registerConfirmation({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('AUTHORIZED on a logged in user registering', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(daos.userDAO));
            const response = await userProcess.registerConfirmation({
                ut: 'something invalid',
                rt: 'something invalid',
            }, session);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.AUTHORIZED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on an invalid token set', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            // @ts-ignore
            daos.tokenDAO.data.registration.push({
                user,
                id: 0,
                requested: new Date(),
                userID: user.id,
                userToken: uuid_1.v4().toString(),
                resetToken: uuid_1.v4().toString(),
                completed: false,
            });
            const response = await userProcess.registerConfirmation({
                ut: 'something invalid',
                rt: daos.tokenDAO.data.registration[0].resetToken,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on a used token', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            // @ts-ignore
            daos.tokenDAO.data.registration.push({
                user,
                id: 0,
                requested: new Date(),
                userID: user.id,
                userToken: uuid_1.v4().toString(),
                resetToken: uuid_1.v4().toString(),
                completed: true,
            });
            const response = await userProcess.registerConfirmation({
                ut: daos.tokenDAO.data.registration[0].userToken,
                rt: daos.tokenDAO.data.registration[0].resetToken,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.EXPIRED_TOKEN);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST on a used token', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            // @ts-ignore
            daos.tokenDAO.data.registration.push({
                user,
                id: 0,
                requested: new Date(new Date().getTime() - 1.48e+7),
                userID: user.id,
                userToken: uuid_1.v4().toString(),
                resetToken: uuid_1.v4().toString(),
                completed: false,
            });
            const response = await userProcess.registerConfirmation({
                ut: daos.tokenDAO.data.registration[0].userToken,
                rt: daos.tokenDAO.data.registration[0].resetToken,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.EXPIRED_TOKEN);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('SUCCESS on a valid request', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(daos.userDAO);
            // @ts-ignore
            daos.tokenDAO.data.registration.push({
                user,
                id: 0,
                requested: new Date(),
                userID: user.id,
                userToken: uuid_1.v4().toString(),
                resetToken: uuid_1.v4().toString(),
                completed: false,
            });
            const response = await userProcess.registerConfirmation({
                ut: daos.tokenDAO.data.registration[0].userToken,
                rt: daos.tokenDAO.data.registration[0].resetToken,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.REGISTRATION_COMPLETE));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.calledOnce);
            chai_1.assert.isTrue(daos.userDAO.data[0].registered);
            chai_1.assert.equal(daos.userDAO.data[0].lockedStatus, 'accessible');
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1Vc2VyUHJvY2Vzc2VzSW1wbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jYXN2aWkvdGVzdC9hY3Rpb25zL3Rlc3QtVXNlclByb2Nlc3Nlc0ltcGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsaUNBQTZCO0FBQzdCLGtFQUErRDtBQUMvRCwwQ0FBNEI7QUFDNUIsK0VBQTRFO0FBQzVFLDRDQUFnRDtBQUNoRCw4RUFBMkU7QUFDM0UsK0JBQThCO0FBQzlCLHlEQUFzRDtBQUN0RCwyREFBMkQ7QUFDM0QsK0JBQTBCO0FBQzFCLElBQU8sZ0JBQWdCLEdBQUcscUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztBQUNyRCxJQUFPLG9CQUFvQixHQUFHLHFCQUFTLENBQUMsb0JBQW9CLENBQUM7QUFFN0QsSUFBSSxXQUEyQixDQUFDO0FBQ2hDLElBQUksWUFBNEIsQ0FBQztBQUVqQyxNQUFNLElBQUksR0FBNEIsNEJBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7QUFFdEUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO0lBQ2QsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXBDLFlBQVksR0FBRyxZQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRWpDLHNGQUFzRjtJQUN0RixNQUFNLE1BQU0sR0FBRyxJQUFJLG1DQUFnQixDQUFDO1FBQ2hDLFFBQVEsRUFBRSxZQUFZO0tBQ3pCLENBQUMsQ0FBQztJQUVILFdBQVcsR0FBRyxxQ0FBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbEosQ0FBQyxDQUFDLENBQUM7QUFFSDs7R0FFRztBQUNILFVBQVUsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUNsQixZQUFZLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDNUIsNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRWpDLDJCQUEyQjtJQUMzQixhQUFhO0lBQ2IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQ3pDLEVBQUUsRUFBRSxDQUFDO1FBQ0wsUUFBUSxFQUFFLElBQUk7S0FDakIsQ0FBQyxDQUFDO0lBQ0gsYUFBYTtJQUNiLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUN6QyxFQUFFLEVBQUUsQ0FBQztRQUNMLFFBQVEsRUFBRSxJQUFJO0tBQ2pCLENBQUMsQ0FBQztJQUNILGFBQWE7SUFDYixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDekMsRUFBRSxFQUFFLENBQUM7UUFDTCxRQUFRLEVBQUUsSUFBSTtLQUNqQixDQUFDLENBQUM7SUFFSCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsNkJBQTZCLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ2pGLENBQUMsQ0FBQyxDQUFDO0FBRUgsUUFBUSxDQUFDLCtDQUErQyxFQUFFLEdBQUcsRUFBRTtJQUMzRCxRQUFRLENBQUMsY0FBYyxFQUFFLEdBQUcsRUFBRTtRQUMxQixrREFBa0Q7UUFDbEQsZUFBZTtRQUNmLFlBQVk7UUFDWixXQUFXO1FBQ1gsZUFBZTtRQUNmLG1CQUFtQjtRQUNuQixVQUFVO1FBQ1YsVUFBVTtRQUNWLFVBQVU7UUFDVixVQUFVO1FBQ1YsV0FBVztRQUNYLFdBQVc7UUFDWCxXQUFXO1FBRVgsVUFBVTtRQUNWLGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsaUJBQWlCO1FBQ2pCLDZEQUE2RDtRQUM3RCxvREFBb0Q7UUFDcEQsd0JBQXdCO1FBQ3hCLGdDQUFnQztRQUNoQywyQkFBMkI7UUFDM0IsbUJBQW1CO1FBQ25CLGlDQUFpQztRQUNqQyx3QkFBd0I7UUFFeEIsRUFBRSxDQUFDLDRDQUE0QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3hELE1BQU0sUUFBUSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDRDQUE0QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3hELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUM5RixNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxhQUFhO2dCQUN2QixLQUFLLEVBQUUsYUFBYTtnQkFDcEIsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLElBQUksRUFBRSxRQUFRO2dCQUNkLFlBQVksRUFBRSxXQUFXO2dCQUN6QixHQUFHLEVBQUUsUUFBUTtnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTthQUNYLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFWixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHNDQUFzQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2xELE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQyxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxhQUFhO2dCQUN2QixLQUFLLEVBQUUsYUFBYTtnQkFDcEIsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLElBQUksRUFBRSxRQUFRO2dCQUNkLFlBQVksRUFBRSxXQUFXO2dCQUN6QixHQUFHLEVBQUUsUUFBUTtnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTthQUNYLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDdkUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLCtCQUErQixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzNDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQyxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxVQUFVO2dCQUNwQixLQUFLLEVBQUUsaUJBQWlCO2dCQUN4QixRQUFRLEVBQUUsY0FBYztnQkFDeEIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsWUFBWSxFQUFFLGNBQWM7Z0JBQzVCLEdBQUcsRUFBRSxRQUFRO2dCQUNiLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2FBQ1gsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyw0QkFBNEIsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN4QyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0MsTUFBTSxRQUFRLEdBQUcsTUFBTSxXQUFXLENBQUMsWUFBWSxDQUFDO2dCQUM1QyxRQUFRLEVBQUUsV0FBVztnQkFDckIsS0FBSyxFQUFFLG1CQUFtQjtnQkFDMUIsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLElBQUksRUFBRSxRQUFRO2dCQUNkLFlBQVksRUFBRSxjQUFjO2dCQUM1QixHQUFHLEVBQUUsUUFBUTtnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTthQUNYLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsdURBQXVELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbkUsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLE1BQU0sUUFBUSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQztnQkFDNUMsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLEtBQUssRUFBRSxpQkFBaUI7Z0JBQ3hCLFFBQVEsRUFBRSxjQUFjO2dCQUN4QixJQUFJLEVBQUUsUUFBUTtnQkFDZCxZQUFZLEVBQUUsY0FBYztnQkFDNUIsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7YUFDWCxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLG9EQUFvRCxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2hFLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQyxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixLQUFLLEVBQUUsbUJBQW1CO2dCQUMxQixRQUFRLEVBQUUsY0FBYztnQkFDeEIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsWUFBWSxFQUFFLGNBQWM7Z0JBQzVCLEdBQUcsRUFBRSxRQUFRO2dCQUNiLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2FBQ1gsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyx1Q0FBdUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNuRCxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0MsTUFBTSxRQUFRLEdBQUcsTUFBTSxXQUFXLENBQUMsWUFBWSxDQUFDO2dCQUM1QyxRQUFRLEVBQUUsV0FBVztnQkFDckIsS0FBSyxFQUFFLDJCQUEyQjtnQkFDbEMsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLElBQUksRUFBRSxRQUFRO2dCQUNkLFlBQVksRUFBRSxnQkFBZ0I7Z0JBQzlCLEdBQUcsRUFBRSxRQUFRO2dCQUNiLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2FBQ1gsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsbUNBQW1DLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDL0MsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLE1BQU0sUUFBUSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQztnQkFDNUMsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLEtBQUssRUFBRSwyQkFBMkI7Z0JBQ2xDLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixJQUFJLEVBQUUsUUFBUTtnQkFDZCxZQUFZLEVBQUUsV0FBVztnQkFDekIsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7YUFDWCxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsK0NBQStDO1lBQy9DLElBQUksQ0FBQyxHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMzQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUU5QixhQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQVcsUUFBUSxDQUFDLGVBQWUsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkgsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHdDQUF3QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3BELE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQyxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixLQUFLLEVBQUUsMkJBQTJCO2dCQUNsQyxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixJQUFJLEVBQUUsUUFBUTtnQkFDZCxZQUFZLEVBQUUsa0JBQWtCO2dCQUNoQyxHQUFHLEVBQUUsUUFBUTtnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTthQUNYLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUM1RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsa0RBQWtELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDOUQsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLE1BQU0sUUFBUSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQztnQkFDNUMsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLEtBQUssRUFBRSwyQkFBMkI7Z0JBQ2xDLFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLElBQUksRUFBRSxRQUFRO2dCQUNkLFlBQVksRUFBRSxrQkFBa0I7Z0JBQ2hDLEdBQUcsRUFBRSxRQUFRO2dCQUNiLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLEdBQUcsRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2FBQ1gsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1lBQ3RGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxxREFBcUQsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRSxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0MsTUFBTSxRQUFRLEdBQUcsTUFBTSxXQUFXLENBQUMsWUFBWSxDQUFDO2dCQUM1QyxRQUFRLEVBQUUsV0FBVztnQkFDckIsS0FBSyxFQUFFLDJCQUEyQjtnQkFDbEMsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsWUFBWSxFQUFFLGtCQUFrQjtnQkFDaEMsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sR0FBRyxFQUFFLENBQUM7Z0JBQ04sSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsSUFBSSxFQUFFLEdBQUc7YUFDWixFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDdEYsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLGtDQUFrQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzlDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQyxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixLQUFLLEVBQUUsMkJBQTJCO2dCQUNsQyxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixJQUFJLEVBQUUsUUFBUTtnQkFDZCxZQUFZLEVBQUUsa0JBQWtCO2dCQUNoQyxHQUFHLEVBQUUsUUFBUTtnQkFDYixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixHQUFHLEVBQUUsQ0FBQztnQkFDTixJQUFJLEVBQUUsR0FBRztnQkFDVCxJQUFJLEVBQUUsR0FBRztnQkFDVCxJQUFJLEVBQUUsR0FBRzthQUNaLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDckcsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxFQUFFO1FBQ2xDLGFBQWE7UUFDYixPQUFPO1FBQ1AsT0FBTztRQUVQLFVBQVU7UUFDVixxQkFBcUI7UUFDckIsb0JBQW9CO1FBQ3BCLGFBQWE7UUFDYixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBRWxCLEVBQUUsQ0FBQyw0Q0FBNEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN4RCxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUVqRixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDRDQUE0QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3hELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUM5RixNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEQsRUFBRSxFQUFFLG1CQUFtQjtnQkFDdkIsRUFBRSxFQUFFLG1CQUFtQjthQUMxQixFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxxQ0FBcUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRCxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUQsYUFBYTtZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7Z0JBQ2pDLElBQUk7Z0JBQ0osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFO2dCQUNyQixNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsU0FBUyxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDMUIsVUFBVSxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDM0IsU0FBUyxFQUFFLEtBQUs7YUFDbkIsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxRQUFRLEdBQUcsTUFBTSxXQUFXLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3BELEVBQUUsRUFBRSxtQkFBbUI7Z0JBQ3ZCLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTthQUNwRCxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3pFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUNuRixDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyw2QkFBNkIsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN6QyxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUQsYUFBYTtZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7Z0JBQ2pDLElBQUk7Z0JBQ0osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFO2dCQUNyQixNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsU0FBUyxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDMUIsVUFBVSxFQUFFLFNBQUUsRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDM0IsU0FBUyxFQUFFLElBQUk7YUFDbEIsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxRQUFRLEdBQUcsTUFBTSxXQUFXLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3BELEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUztnQkFDaEQsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO2FBQ3BELEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDdkUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDZCQUE2QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3pDLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM1RCxhQUFhO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDakMsSUFBSTtnQkFDSixFQUFFLEVBQUUsQ0FBQztnQkFDTCxTQUFTLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxPQUFPLENBQUM7Z0JBQ25ELE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDZixTQUFTLEVBQUUsU0FBRSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUMxQixVQUFVLEVBQUUsU0FBRSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUMzQixTQUFTLEVBQUUsS0FBSzthQUNuQixDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEQsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTO2dCQUNoRCxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVU7YUFDcEQsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsNEJBQTRCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDeEMsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVELGFBQWE7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO2dCQUNqQyxJQUFJO2dCQUNKLEVBQUUsRUFBRSxDQUFDO2dCQUNMLFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtnQkFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO2dCQUNmLFNBQVMsRUFBRSxTQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLFVBQVUsRUFBRSxTQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUU7Z0JBQzNCLFNBQVMsRUFBRSxLQUFLO2FBQ25CLENBQUMsQ0FBQztZQUVILE1BQU0sUUFBUSxHQUFHLE1BQU0sV0FBVyxDQUFDLG9CQUFvQixDQUFDO2dCQUNwRCxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7Z0JBQ2hELEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTthQUNwRCxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7WUFDaEgsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkMsYUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMvQyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDbkYsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDLENBQUMsQ0FBQztBQUVQLENBQUMsQ0FBQyxDQUFDIn0=