"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const utilities_1 = require("../utilities");
const PermissionProcessesImpl_1 = require("../../actions/process/PermissionProcessesImpl");
const CASEmailProvider_1 = require("../../mail/CASEmailProvider");
const sinon_1 = require("sinon");
const ObjectSession_1 = require("../../utilities/session/impl/ObjectSession");
const chai_1 = require("chai");
const _ = __importStar(require("logger"));
const Constants_1 = require("../../utilities/Constants");
const ProcessImpl_1 = require("../../actions/ProcessImpl");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
let permissionProcess;
let sendMailFake;
const daos = utilities_1.TestingUtilities.generateDAOs();
before(async () => {
    _.setMinimumLevel(_.LogLevel.TRACE);
    sendMailFake = sinon_1.fake.resolves({});
    // @ts-ignore - this is valid because we are mocking it, I know it only calls sendMail
    const mailer = new CASEmailProvider_1.CASEmailProvider({
        sendMail: sendMailFake,
    });
    permissionProcess = PermissionProcessesImpl_1.PermissionProcessImpl.construct(mailer, daos.loggingDAO, daos.userDAO, daos.permissionDAO);
});
/**
 * Before each test, reset the backing data store to each test happens in isolation
 */
beforeEach(async () => {
    sendMailFake.resetHistory();
    utilities_1.TestingUtilities.resetDAOs(daos);
    await utilities_1.TestingUtilities.buildUser(daos.userDAO);
    await utilities_1.TestingUtilities.buildUser(daos.userDAO);
    // Create four permissions
    const p1 = await daos.permissionDAO.createPermissionAutohash(`permission1`, `permission 1 description`);
    const p2 = await daos.permissionDAO.createPermissionAutohash(`permission2`, `permission 1 description`);
    const p3 = await daos.permissionDAO.createPermissionAutohash(`permission3`, `permission 1 description`);
    const p4 = await daos.permissionDAO.createPermissionAutohash(`permission4`, `permission 1 description`);
    const grantSelf = await daos.permissionDAO.createPermissionAutohash(`casvii.permission.grant.self`, `permission 1 description`);
    const grantOther = await daos.permissionDAO.createPermissionAutohash(`casvii.permission.grant.other`, `permission 1 description`);
    const ungrantSelf = await daos.permissionDAO.createPermissionAutohash(`casvii.permission.ungrant.self`, `permission 1 description`);
    const ungrantOther = await daos.permissionDAO.createPermissionAutohash(`casvii.permission.ungrant.other`, `permission 1 description`);
    const selfAuthorize = await daos.permissionDAO.createPermissionAutohash(`casvii.permission.self-grant`, `permission 1 description`);
    // And three groups
    const g1 = await daos.permissionDAO.createPermissionGroupAutohash(`group-direct`, `direct inheritance of permissions`);
    const g2 = await daos.permissionDAO.createPermissionGroupAutohash(`group-indirect1`, `indirect inheritance of permissions #1`);
    const g3 = await daos.permissionDAO.createPermissionGroupAutohash(`group-indirect2`, `indirect inheritance of permissions #2`);
    const selfGroup = await daos.permissionDAO.createPermissionGroupAutohash(`self-modify`, `permissions to grant and ungrant permissions to self`);
    const otherGroup = await daos.permissionDAO.createPermissionGroupAutohash(`other-modify`, `permissions to grant and ungrant permissions to others`);
    const selfAuthorizeGroup = await daos.permissionDAO.createPermissionGroupAutohash(`self-authorize`, ``);
    // Map permissions directly to the groups forming
    //   g1: p1, p2
    //   g2:
    //   g3: p3, p4
    await daos.permissionDAO.createGroupMapping(p1, g1);
    await daos.permissionDAO.createGroupMapping(p2, g1);
    await daos.permissionDAO.createGroupMapping(p3, g3);
    await daos.permissionDAO.createGroupMapping(p4, g3);
    await daos.permissionDAO.createGroupMapping(selfAuthorize, selfAuthorizeGroup);
    await daos.permissionDAO.createGroupMapping(grantSelf, selfGroup);
    await daos.permissionDAO.createGroupMapping(ungrantSelf, selfGroup);
    await daos.permissionDAO.createGroupMapping(grantOther, otherGroup);
    await daos.permissionDAO.createGroupMapping(ungrantOther, otherGroup);
    // And then add an inheritance system so we get
    //   group-direct:    permission1, permission2
    //   group-indirect1: [permission3], [permission4]
    //   group-indirect2: permission3, permission4
    // Where [x] represents the group inherits permission `x` from another group.
    await daos.permissionDAO.createGroupInheritance(g2, g3);
    // @ts-ignore
    daos.permissionDAO.data.grants.push({
        id: 0,
        groupID: g1.id,
        group: g1,
        serviceName: 'Service',
        serviceDescription: 'Service Description',
    });
});
describe('casvii/actions/processes/PermissionProcessesImpl.ts', () => {
    describe('grantGroupToUser', () => {
        // Test with no data
        it('BAD_REQUEST on no data', async () => {
            const response = await permissionProcess.grantGroupToUser({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
        });
        // Test from unauthenticated user
        it('UNAUTHORIZED on unauthenticated user', async () => {
            const response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: 0,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.UNAUTHORIZED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 0, `resolved permissions`);
        });
        // Test from authenticated usr without permission
        it('FORBIDDEN on authenticated user without permission', async () => {
            // Acting for the SELF user
            let response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.MISSING_PERMISSIONS);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.FORBIDDEN);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 0, `resolved permissions`);
            // Acting for the OTHER user
            response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.MISSING_PERMISSIONS);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.FORBIDDEN);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test from authenticated user with permission for user without the group
        it('SUCCESS on request for user without group', async () => {
            // Allow them to modify the self for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `self-modify`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            let response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.GRANTED));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 4, `resolved permissions`);
            // Allow them to modify the other for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `other-modify`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.GRANTED));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 4, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 2, `resolved permissions`);
        });
        // Test from authenticated user with permission for user with the group
        it('BAD_REQUEST on request for user with group', async () => {
            // Allow them to modify the self for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `self-modify`)[0], daos.userDAO.data[0]);
            // And add the group we are trying to add
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `group-direct`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            let response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.ALREADY_GRANTED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 4, `resolved permissions`);
            // Allow them to modify the other for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `other-modify`)[0], daos.userDAO.data[0]);
            // And add the group we are trying to add
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `group-direct`)[0], daos.userDAO.data[1]);
            // Then run them trying to add the group-direct group to this user
            response = await permissionProcess.grantGroupToUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.ALREADY_GRANTED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 4, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 2, `resolved permissions`);
        });
    });
    describe('ungrantGroupFromUser', () => {
        // Test with no data
        it('BAD_REQUEST on no data', async () => {
            const response = await permissionProcess.ungrantGroupFromUser({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
        });
        // Test from unauthenticated user
        it('UNAUTHORIZED on unauthenticated user', async () => {
            const response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: 0,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.UNAUTHORIZED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 0, `resolved permissions`);
        });
        // Test from authenticated usr without permission
        it('FORBIDDEN on authenticated user without permission', async () => {
            // Acting for the SELF user
            let response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.MISSING_PERMISSIONS);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.FORBIDDEN);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 0, `resolved permissions`);
            // Acting for the OTHER user
            response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.MISSING_PERMISSIONS);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.FORBIDDEN);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test from authenticated user with permission for user without the group
        it('BAD_REQUEST on request for user without group', async () => {
            // Allow them to modify the self for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `self-modify`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            let response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.NOT_GRANTED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 1, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 2, `resolved permissions`);
            // Allow them to modify the other for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `other-modify`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.NOT_GRANTED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test from authenticated user with permission for user with the group
        it('SUCCESS on request for user with group', async () => {
            // Allow them to modify the self for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `self-modify`)[0], daos.userDAO.data[0]);
            // And add the group we are trying to add
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `group-direct`)[0], daos.userDAO.data[0]);
            // Then run them trying to add the group-direct group to this user
            let response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[0].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.UNGRANTED));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 1, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 2, `resolved permissions`);
            // Allow them to modify the other for this test
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `other-modify`)[0], daos.userDAO.data[0]);
            // And add the group we are trying to add
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === `group-direct`)[0], daos.userDAO.data[1]);
            // Then run them trying to add the group-direct group to this user
            response = await permissionProcess.ungrantGroupFromUser({
                group: `group-direct`,
                user: daos.userDAO.data[1].id,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            // Assert the response
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.UNGRANTED));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            // Carry over from the last test
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 2, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
    });
    describe('userHasPermission', () => {
        // And then add an inheritance system so we get
        //   group-direct:    permission1, permission2
        //   group-indirect1: [permission3], [permission4]
        //   group-indirect2: permission3, permission4
        // Where [x] represents the group inherits permission `x` from another group.
        // Test with permission directly
        it('TRUE on having "direct" permission', async () => {
            // Giving permission group directly
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-direct')[0], daos.userDAO.data[0]);
            // And then check for `permission1`
            chai_1.assert.isTrue(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission1`));
            chai_1.assert.isFalse(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission4`));
        });
        // Test with permission inherited
        it('TRUE on having inherited permission', async () => {
            // Giving permission group indirectly
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-indirect1')[0], daos.userDAO.data[0]);
            // And then check for `permission1`
            chai_1.assert.isTrue(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission4`));
            chai_1.assert.isFalse(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission1`));
        });
        // Test without permission
        it('FALSE on not having permission', async () => {
            chai_1.assert.isFalse(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission4`));
            chai_1.assert.isFalse(await permissionProcess.userHasPermission(daos.userDAO.data[0], `permission1`));
        });
    });
    describe('sessionHasPermission', () => {
        // Test with permission directly
        it('TRUE on having "direct" permission', async () => {
            // Giving permission group directly
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-direct')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            // And then check for `permission1`
            chai_1.assert.isTrue(await permissionProcess.sessionHasPermission(session, `permission1`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(session, `permission4`));
        });
        // Test with permission inherited
        it('TRUE on having inherited permission', async () => {
            // Giving permission group indirectly
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-indirect1')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            // And then check for `permission1`
            chai_1.assert.isTrue(await permissionProcess.sessionHasPermission(session, `permission4`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(session, `permission1`));
        });
        // Test without permission
        it('FALSE on not having permission', async () => {
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            // And then check for `permission1`
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(session, `permission4`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(session, `permission1`));
        });
        // Test with null session
        it('FALSE on unauthenticated session', async () => {
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(new ObjectSession_1.ObjectSession(), `permission1`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(new ObjectSession_1.ObjectSession(), `permission2`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(new ObjectSession_1.ObjectSession(), `permission3`));
            chai_1.assert.isFalse(await permissionProcess.sessionHasPermission(new ObjectSession_1.ObjectSession(), `permission4`));
        });
        // Test that verifications is updated
        it('verification session count modified on call', async () => {
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-indirect1')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            chai_1.assert.isTrue(await permissionProcess.sessionHasPermission(session, `permission4`));
            chai_1.assert.isTrue(session.has(`user.permissions`));
            chai_1.assert.isTrue(session.has(`user.permission-verifications`));
        });
        // Test that 'database' is hit on the 10th call
        it('repeated calls hit the database', async () => {
            // Now for some hacky shit to replace the function in the DAO
            const spiedResolveFunction = sinon_1.spy(daos.permissionDAO.resolvePermissions);
            // @ts-ignore
            daos.permissionDAO._resolvePermissions = daos.permissionDAO.resolvePermissions;
            daos.permissionDAO.resolvePermissions = spiedResolveFunction;
            // Then move on to testing
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-indirect2')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            for (let i = 0; i < 15; i++) {
                chai_1.assert.isTrue(await permissionProcess.sessionHasPermission(session, `permission4`));
            }
            // Check that we hit the database twice
            chai_1.assert.equal(spiedResolveFunction.callCount, 2);
        });
    });
    describe('resolveAllUserPermissions', () => {
        // Test with just basic
        it('valid result on one level of inheritance', async () => {
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-direct')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            const result = await permissionProcess.resolveAllUserPermissions(session, daos.userDAO.data[0]);
            chai_1.assert.isTrue(result);
            chai_1.assert.isTrue(session.has(`user.permissions`));
            const permissions = session.get(`user.permissions`);
            const permissionNames = permissions.map(e => e.name);
            chai_1.assert.includeMembers(permissionNames, [`permission1`, `permission2`]);
        });
        // Test with inheritances
        it('valid result on multiple level of inheritance', async () => {
            await daos.permissionDAO.createUserGroupMapping(daos.permissionDAO.data.groups.filter(e => e.name === 'group-indirect1')[0], daos.userDAO.data[0]);
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            const result = await permissionProcess.resolveAllUserPermissions(session, daos.userDAO.data[0]);
            chai_1.assert.isTrue(result);
            chai_1.assert.isTrue(session.has(`user.permissions`));
            const permissions = session.get(`user.permissions`);
            const permissionNames = permissions.map(e => e.name);
            chai_1.assert.includeMembers(permissionNames, [`permission3`, `permission4`]);
        });
        // Test user with no permissions
        it('valid result on no permissions', async () => {
            const session = utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]);
            const result = await permissionProcess.resolveAllUserPermissions(session, daos.userDAO.data[0]);
            chai_1.assert.isTrue(result);
            chai_1.assert.isTrue(session.has(`user.permissions`));
            chai_1.assert.equal(session.get(`user.permissions`).length, 0);
        });
    });
    describe('activateSelfPermissionGrant', () => {
        // Test with no data
        it('BAD_REQUEST on no data provided', async () => {
            const response = await permissionProcess.activateSelfPermissionGrant({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test with unauthenticated user
        it('UNAUTHORIZED on user not logged in', async () => {
            const response = await permissionProcess.activateSelfPermissionGrant({
                authorizationID: 1,
            }, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.UNAUTHORIZED);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test with authenticated user without permission
        it('FORBIDDEN on user not having permissions', async () => {
            const response = await permissionProcess.activateSelfPermissionGrant({
                authorizationID: 1,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.MISSING_PERMISSIONS);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.FORBIDDEN);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 0, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[1])).length, 0, `resolved permissions`);
        });
        // Test with invalid self grant
        it('BAD_REQUEST on invalid self grant', async () => {
            await daos.permissionDAO.createUserGroupMapping(await daos.permissionDAO.findGroupByName(`self-authorize`), daos.userDAO.data[0]);
            const response = await permissionProcess.activateSelfPermissionGrant({
                authorizationID: 1,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_AUTHORIZATION);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 1, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 1, `resolved permissions`);
        });
        // Test with already having grant
        it('BAD_REQUEST on user already having the grant', async () => {
            await daos.permissionDAO.createUserGroupMapping(await daos.permissionDAO.findGroupByName(`self-authorize`), daos.userDAO.data[0]);
            await daos.permissionDAO.createUserGroupMapping(await daos.permissionDAO.findGroupByName(`group-direct`), daos.userDAO.data[0]);
            const response = await permissionProcess.activateSelfPermissionGrant({
                authorizationID: 0,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.ALREADY_USED_GRANT);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailFake.notCalled, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 3, `resolved permissions`);
        });
        // Test with successful request
        it('SUCCESSFUL on valid request', async () => {
            await daos.permissionDAO.createUserGroupMapping(await daos.permissionDAO.findGroupByName(`self-authorize`), daos.userDAO.data[0]);
            const response = await permissionProcess.activateSelfPermissionGrant({
                authorizationID: 0,
            }, utilities_1.TestingUtilities.buildSession(daos.userDAO.data[0]));
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN_WITH_CASVII));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailFake.called, `email sending`);
            chai_1.assert.equal(daos.loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(daos.permissionDAO.data.users.length, 2, `group user mappings`);
            chai_1.assert.equal((await daos.permissionDAO.resolvePermissions(daos.userDAO.data[0])).length, 3, `resolved permissions`);
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1QZXJtaXNzaW9uUHJvY2Vzc2VzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS90ZXN0L2FjdGlvbnMvdGVzdC1QZXJtaXNzaW9uUHJvY2Vzc2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLDRDQUFnRDtBQUNoRCwyRkFBc0Y7QUFDdEYsa0VBQStEO0FBQy9ELGlDQUFrQztBQUNsQyw4RUFBMkU7QUFDM0UsK0JBQThCO0FBQzlCLDBDQUE0QjtBQUM1Qix5REFBc0Q7QUFDdEQsMkRBQXdFO0FBRXhFLElBQU8sZ0JBQWdCLEdBQUcscUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztBQUNyRCxJQUFPLG9CQUFvQixHQUFHLHFCQUFTLENBQUMsb0JBQW9CLENBQUM7QUFFN0QsSUFBSSxpQkFBdUMsQ0FBQztBQUM1QyxJQUFJLFlBQTRCLENBQUM7QUFFakMsTUFBTSxJQUFJLEdBQTRCLDRCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDO0FBRXRFLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtJQUNkLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUVwQyxZQUFZLEdBQUcsWUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUVqQyxzRkFBc0Y7SUFDdEYsTUFBTSxNQUFNLEdBQUcsSUFBSSxtQ0FBZ0IsQ0FBQztRQUNoQyxRQUFRLEVBQUUsWUFBWTtLQUN6QixDQUFDLENBQUM7SUFFSCxpQkFBaUIsR0FBRywrQ0FBcUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDbkgsQ0FBQyxDQUFDLENBQUM7QUFFSDs7R0FFRztBQUNILFVBQVUsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUNsQixZQUFZLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDNUIsNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRWpDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFL0MsMEJBQTBCO0lBQzFCLE1BQU0sRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxhQUFhLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUN4RyxNQUFNLEVBQUUsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsYUFBYSxFQUFFLDBCQUEwQixDQUFDLENBQUM7SUFDeEcsTUFBTSxFQUFFLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLGFBQWEsRUFBRSwwQkFBMEIsQ0FBQyxDQUFDO0lBQ3hHLE1BQU0sRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxhQUFhLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUN4RyxNQUFNLFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsOEJBQThCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUNoSSxNQUFNLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsK0JBQStCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUNsSSxNQUFNLFdBQVcsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsZ0NBQWdDLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUNwSSxNQUFNLFlBQVksR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsaUNBQWlDLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUN0SSxNQUFNLGFBQWEsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsOEJBQThCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUVwSSxtQkFBbUI7SUFDbkIsTUFBTSxFQUFFLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLDZCQUE2QixDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQyxDQUFDO0lBQ3ZILE1BQU0sRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxpQkFBaUIsRUFBRSx3Q0FBd0MsQ0FBQyxDQUFDO0lBQy9ILE1BQU0sRUFBRSxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxpQkFBaUIsRUFBRSx3Q0FBd0MsQ0FBQyxDQUFDO0lBQy9ILE1BQU0sU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxhQUFhLEVBQUUsc0RBQXNELENBQUMsQ0FBQztJQUNoSixNQUFNLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsNkJBQTZCLENBQUMsY0FBYyxFQUFFLHdEQUF3RCxDQUFDLENBQUM7SUFDcEosTUFBTSxrQkFBa0IsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFeEcsaURBQWlEO0lBQ2pELGVBQWU7SUFDZixRQUFRO0lBQ1IsZUFBZTtJQUNmLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDcEQsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNwRCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDcEQsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBQy9FLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEUsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNwRSxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BFLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFFdEUsK0NBQStDO0lBQy9DLDhDQUE4QztJQUM5QyxrREFBa0Q7SUFDbEQsOENBQThDO0lBQzlDLDZFQUE2RTtJQUM3RSxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRXhELGFBQWE7SUFDYixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hDLEVBQUUsRUFBRSxDQUFDO1FBQ0wsT0FBTyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1FBQ2QsS0FBSyxFQUFFLEVBQUU7UUFDVCxXQUFXLEVBQUUsU0FBUztRQUN0QixrQkFBa0IsRUFBRSxxQkFBcUI7S0FDNUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUM7QUFFSCxRQUFRLENBQUMscURBQXFELEVBQUUsR0FBRyxFQUFFO0lBQ2pFLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRSxHQUFHLEVBQUU7UUFDOUIsb0JBQW9CO1FBQ3BCLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNwQyxNQUFNLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEVBQUUsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRW5GLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztRQUNqRixDQUFDLENBQUMsQ0FBQztRQUVILGlDQUFpQztRQUNqQyxFQUFFLENBQUMsc0NBQXNDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbEQsTUFBTSxRQUFRLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDdEQsS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLElBQUksRUFBRSxDQUFDO2FBQ1YsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN0RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMvRCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDeEgsQ0FBQyxDQUFDLENBQUM7UUFFSCxpREFBaUQ7UUFDakQsRUFBRSxDQUFDLG9EQUFvRCxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2hFLDJCQUEyQjtZQUMzQixJQUFJLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDO2dCQUNwRCxLQUFLLEVBQUUsY0FBYztnQkFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDaEMsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztZQUVwSCw0QkFBNEI7WUFDNUIsUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2hELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGdDQUFnQztZQUNoQyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN4SCxDQUFDLENBQUMsQ0FBQztRQUVILDBFQUEwRTtRQUMxRSxFQUFFLENBQUMsMkNBQTJDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDdkQsOENBQThDO1lBQzlDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRS9JLGtFQUFrRTtZQUNsRSxJQUFJLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDO2dCQUNwRCxLQUFLLEVBQUUsY0FBYztnQkFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDaEMsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELHNCQUFzQjtZQUN0QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDbEcsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1lBRXBILCtDQUErQztZQUMvQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVoSixrRUFBa0U7WUFDbEUsUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2hELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNsRyxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsZ0NBQWdDO1lBQ2hDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hILENBQUMsQ0FBQyxDQUFDO1FBRUgsdUVBQXVFO1FBQ3ZFLEVBQUUsQ0FBQyw0Q0FBNEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN4RCw4Q0FBOEM7WUFDOUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0kseUNBQXlDO1lBQ3pDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWhKLGtFQUFrRTtZQUNsRSxJQUFJLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDO2dCQUNwRCxLQUFLLEVBQUUsY0FBYztnQkFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDaEMsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELHNCQUFzQjtZQUN0QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1lBRXBILCtDQUErQztZQUMvQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoSix5Q0FBeUM7WUFDekMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEosa0VBQWtFO1lBQ2xFLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDO2dCQUNoRCxLQUFLLEVBQUUsY0FBYztnQkFDckIsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDaEMsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELHNCQUFzQjtZQUN0QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGdDQUFnQztZQUNoQyxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN4SCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUgsUUFBUSxDQUFDLHNCQUFzQixFQUFFLEdBQUcsRUFBRTtRQUNsQyxvQkFBb0I7UUFDcEIsRUFBRSxDQUFDLHdCQUF3QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3BDLE1BQU0sUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFdkYsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3pFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1FBQ2pGLENBQUMsQ0FBQyxDQUFDO1FBRUgsaUNBQWlDO1FBQ2pDLEVBQUUsQ0FBQyxzQ0FBc0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNsRCxNQUFNLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDO2dCQUMxRCxLQUFLLEVBQUUsY0FBYztnQkFDckIsSUFBSSxFQUFFLENBQUM7YUFDVixFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFeEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3RFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQy9ELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN4SCxDQUFDLENBQUMsQ0FBQztRQUVILGlEQUFpRDtRQUNqRCxFQUFFLENBQUMsb0RBQW9ELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDaEUsMkJBQTJCO1lBQzNCLElBQUksUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3hELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1lBRXBILDRCQUE0QjtZQUM1QixRQUFRLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEQsS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQ2hDLEVBQUUsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV4RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsZ0NBQWdDO1lBQ2hDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hILENBQUMsQ0FBQyxDQUFDO1FBRUgsMEVBQTBFO1FBQzFFLEVBQUUsQ0FBQywrQ0FBK0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMzRCw4Q0FBOEM7WUFDOUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFL0ksa0VBQWtFO1lBQ2xFLElBQUksUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3hELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNyRSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7WUFFcEgsK0NBQStDO1lBQy9DLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWhKLGtFQUFrRTtZQUNsRSxRQUFRLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEQsS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQ2hDLEVBQUUsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV4RCxzQkFBc0I7WUFDdEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3JFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxnQ0FBZ0M7WUFDaEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDeEgsQ0FBQyxDQUFDLENBQUM7UUFFSCx1RUFBdUU7UUFDdkUsRUFBRSxDQUFDLHdDQUF3QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3BELDhDQUE4QztZQUM5QyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvSSx5Q0FBeUM7WUFDekMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEosa0VBQWtFO1lBQ2xFLElBQUksUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3hELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNwRyxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7WUFFcEgsK0NBQStDO1lBQy9DLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hKLHlDQUF5QztZQUN6QyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVoSixrRUFBa0U7WUFDbEUsUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3BELEtBQUssRUFBRSxjQUFjO2dCQUNyQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNoQyxFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsc0JBQXNCO1lBQ3RCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNwRyxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsZ0NBQWdDO1lBQ2hDLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsbUJBQW1CLEVBQUUsR0FBRyxFQUFFO1FBQy9CLCtDQUErQztRQUMvQyw4Q0FBOEM7UUFDOUMsa0RBQWtEO1FBQ2xELDhDQUE4QztRQUM5Qyw2RUFBNkU7UUFFN0UsZ0NBQWdDO1FBQ2hDLEVBQUUsQ0FBQyxvQ0FBb0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNoRCxtQ0FBbUM7WUFDbkMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEosbUNBQW1DO1lBQ25DLGFBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzlGLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ25HLENBQUMsQ0FBQyxDQUFDO1FBRUgsaUNBQWlDO1FBQ2pDLEVBQUUsQ0FBQyxxQ0FBcUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRCxxQ0FBcUM7WUFDckMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVuSixtQ0FBbUM7WUFDbkMsYUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDOUYsYUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDbkcsQ0FBQyxDQUFDLENBQUM7UUFFSCwwQkFBMEI7UUFDMUIsRUFBRSxDQUFDLGdDQUFnQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzVDLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQy9GLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ25HLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsc0JBQXNCLEVBQUUsR0FBRyxFQUFFO1FBQ2xDLGdDQUFnQztRQUNoQyxFQUFFLENBQUMsb0NBQW9DLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDaEQsbUNBQW1DO1lBQ25DLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hKLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBFLG1DQUFtQztZQUNuQyxhQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDcEYsYUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ3pGLENBQUMsQ0FBQyxDQUFDO1FBRUgsaUNBQWlDO1FBQ2pDLEVBQUUsQ0FBQyxxQ0FBcUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRCxxQ0FBcUM7WUFDckMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuSixNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVwRSxtQ0FBbUM7WUFDbkMsYUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUN6RixDQUFDLENBQUMsQ0FBQztRQUVILDBCQUEwQjtRQUMxQixFQUFFLENBQUMsZ0NBQWdDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDNUMsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFcEUsbUNBQW1DO1lBQ25DLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUNyRixhQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0saUJBQWlCLENBQUMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDekYsQ0FBQyxDQUFDLENBQUM7UUFFSCx5QkFBeUI7UUFDekIsRUFBRSxDQUFDLGtDQUFrQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzlDLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLDZCQUFhLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2pHLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLDZCQUFhLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2pHLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLDZCQUFhLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2pHLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLDZCQUFhLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ3JHLENBQUMsQ0FBQyxDQUFDO1FBRUgscUNBQXFDO1FBQ3JDLEVBQUUsQ0FBQyw2Q0FBNkMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN6RCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25KLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBFLGFBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUNwRixhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBQy9DLGFBQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7UUFDaEUsQ0FBQyxDQUFDLENBQUM7UUFFSCwrQ0FBK0M7UUFDL0MsRUFBRSxDQUFDLGlDQUFpQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzdDLDZEQUE2RDtZQUM3RCxNQUFNLG9CQUFvQixHQUFHLFdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDeEUsYUFBYTtZQUNiLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQztZQUMvRSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixHQUFHLG9CQUFvQixDQUFDO1lBRTdELDBCQUEwQjtZQUMxQixNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25KLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pCLGFBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQzthQUN2RjtZQUVELHVDQUF1QztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUgsUUFBUSxDQUFDLDJCQUEyQixFQUFFLEdBQUcsRUFBRTtRQUN2Qyx1QkFBdUI7UUFDdkIsRUFBRSxDQUFDLDBDQUEwQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3RELE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hKLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBFLE1BQU0sTUFBTSxHQUFHLE1BQU0saUJBQWlCLENBQUMseUJBQXlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEcsYUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QixhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBRS9DLE1BQU0sV0FBVyxHQUFpQixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDbEUsTUFBTSxlQUFlLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVyRCxhQUFNLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQzNFLENBQUMsQ0FBQyxDQUFDO1FBRUgseUJBQXlCO1FBQ3pCLEVBQUUsQ0FBQywrQ0FBK0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMzRCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25KLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXBFLE1BQU0sTUFBTSxHQUFHLE1BQU0saUJBQWlCLENBQUMseUJBQXlCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFaEcsYUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QixhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBRS9DLE1BQU0sV0FBVyxHQUFpQixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDbEUsTUFBTSxlQUFlLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVyRCxhQUFNLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQzNFLENBQUMsQ0FBQyxDQUFDO1FBRUgsZ0NBQWdDO1FBQ2hDLEVBQUUsQ0FBQyxnQ0FBZ0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM1QyxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVwRSxNQUFNLE1BQU0sR0FBRyxNQUFNLGlCQUFpQixDQUFDLHlCQUF5QixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWhHLGFBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEIsYUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztZQUUvQyxhQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDNUQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLEVBQUU7UUFDekMsb0JBQW9CO1FBQ3BCLEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3QyxNQUFNLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLEVBQUUsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRTlGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDeEgsQ0FBQyxDQUFDLENBQUM7UUFFSCxpQ0FBaUM7UUFDakMsRUFBRSxDQUFDLG9DQUFvQyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2hELE1BQU0sUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsMkJBQTJCLENBQUM7Z0JBQ2pFLGVBQWUsRUFBRSxDQUFDO2FBQ3JCLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUV4QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDL0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hILENBQUMsQ0FBQyxDQUFDO1FBRUgsa0RBQWtEO1FBQ2xELEVBQUUsQ0FBQywwQ0FBMEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN0RCxNQUFNLFFBQVEsR0FBRyxNQUFNLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDO2dCQUNqRSxlQUFlLEVBQUUsQ0FBQzthQUNyQixFQUFFLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3hILENBQUMsQ0FBQyxDQUFDO1FBRUgsK0JBQStCO1FBQy9CLEVBQUUsQ0FBQyxtQ0FBbUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMvQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEksTUFBTSxRQUFRLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQztnQkFDakUsZUFBZSxFQUFFLENBQUM7YUFDckIsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN4SCxDQUFDLENBQUMsQ0FBQztRQUVILGlDQUFpQztRQUNqQyxFQUFFLENBQUMsOENBQThDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDMUQsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xJLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEksTUFBTSxRQUFRLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQztnQkFDakUsZUFBZSxFQUFFLENBQUM7YUFDckIsRUFBRSw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXhELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDL0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdFLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN4SCxDQUFDLENBQUMsQ0FBQztRQUVILCtCQUErQjtRQUMvQixFQUFFLENBQUMsNkJBQTZCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDekMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWxJLE1BQU0sUUFBUSxHQUFHLE1BQU0saUJBQWlCLENBQUMsMkJBQTJCLENBQUM7Z0JBQ2pFLGVBQWUsRUFBRSxDQUFDO2FBQ3JCLEVBQUUsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV4RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztZQUNoSCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxRCxhQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDcEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDeEgsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUMsQ0FBQyxDQUFDIn0=