"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const CASEmailProvider_1 = require("../../mail/CASEmailProvider");
const _ = __importStar(require("logger"));
const TestingDAO = __importStar(require("../in-memory-daos"));
const sinon_1 = require("sinon");
const AuthenticationProcessImpl_1 = require("../../actions/process/AuthenticationProcessImpl");
const chai_1 = require("chai");
const ObjectSession_1 = require("../../utilities/session/impl/ObjectSession");
const utilities_1 = require("../utilities");
const Constants_1 = require("../../utilities/Constants");
const ProcessImpl_1 = require("../../actions/ProcessImpl");
const otplib_1 = require("otplib");
var PUBLIC_RESPONSES = Constants_1.Constants.PUBLIC_RESPONSES;
var PUBLIC_SUBSTITUTIONS = Constants_1.Constants.PUBLIC_SUBSTITUTIONS;
let authentication;
let mailer;
let twoFactorDAO;
let securityQuestionDAO;
let userDAO;
let loggingDAO;
let sendMailSpy;
/**
 * Before all the tests, set up the spies, DAOs and finally initialise the authentication process
 */
before(async () => {
    _.setMinimumLevel(_.LogLevel.TRACE);
    sendMailSpy = sinon_1.fake.resolves({});
    const transport = {
        sendMail: sendMailSpy,
    };
    // @ts-ignore - see test-PasswordProcessImpl.ts
    mailer = new CASEmailProvider_1.CASEmailProvider(transport);
    twoFactorDAO = new TestingDAO.InMemoryTwoFactorDAO([]);
    securityQuestionDAO = new TestingDAO.InMemorySecurityQuestionDAO({ questions: [], mapping: [] });
    userDAO = new TestingDAO.InMemoryUserDAO([]);
    loggingDAO = new TestingDAO.InMemoryLoggingDAO({ login: [], auth: [] });
    authentication = AuthenticationProcessImpl_1.AuthenticationProcessImpl.construct(mailer, twoFactorDAO, securityQuestionDAO, userDAO, loggingDAO);
});
/**
 * Before each test, reset the history on the sendMail spy and reset the data in each of the DAOs
 */
beforeEach(async () => {
    sendMailSpy.resetHistory();
    twoFactorDAO.data = [];
    securityQuestionDAO.data = { questions: [], mapping: [] };
    userDAO.data = [];
    loggingDAO.data = { auth: [], login: [] };
});
/**
 * Contains demonstration hashes produced using argon2 on the commmand line using argon2id as described by VERSION 0.
 * Password = "Test1ngPa55w0rd!"
 * Pin = "0101"
 */
const testingHash = {
    password: {
        version: 0,
        hash: '$argon2id$v=19$m=4096,t=3,p=1$BY8W0h15/Xe2XYMobZufew$3b+/Wg7WHd3rpUFA/ZsFks8/J0rjRBHPpNMx2nZSvmk',
    },
    pin: {
        version: 0,
        hash: '$argon2id$v=19$m=4096,t=3,p=1$vVMpAHnn7P8A6UW3XQ/zOA$jNjlgFXKhBCqNGXWivCq0Bvtl8J7PddHdDL9Xz52sM4',
    },
};
describe('casvii/actions/processes/AuthenticationProcessImpl.ts', () => {
    describe('getUser', () => {
        /**
         * getUser(session)
         *  - session is null
         *      => null
         *  - session does not contain user
         *      => null
         *  - session contains invalid user id
         *      => null
         *  - session contains valid ID
         *      => User
         */
        it('returns null on null session', async () => {
            // @ts-ignore
            chai_1.assert.equal(await authentication.getUser(null), null);
            // @ts-ignore
            chai_1.assert.equal(await authentication.getUser(undefined), null);
        });
        it('returns null on empty user session', async () => {
            chai_1.assert.equal(await authentication.getUser(new ObjectSession_1.ObjectSession()), null);
        });
        it('returns null on invalid user id', async () => {
            const session = new ObjectSession_1.ObjectSession();
            session.set(`login.userID`, 10);
            chai_1.assert.equal(await authentication.getUser(session), null);
        });
        it('returns user instance on valid ID', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user);
            chai_1.assert.equal(await authentication.getUser(session), user);
        });
    });
    describe('getUserID', () => {
        /*
         * getUserID(session)
         *  - session is null
         *      => undefined
         *  - session does not contain user
         *      => undefined
         *  - session contains ID
         *      => int
         */
        it('returns null on null session', async () => {
            // @ts-ignore
            chai_1.assert.equal(authentication.getUserID(null), null);
            // @ts-ignore
            chai_1.assert.equal(authentication.getUserID(undefined), null);
        });
        it('returns null on empty user session', async () => {
            chai_1.assert.equal(authentication.getUserID(new ObjectSession_1.ObjectSession()), null);
        });
        it('returns int on valid session', async () => {
            const session = new ObjectSession_1.ObjectSession();
            session.set(`login.userID`, 3);
            chai_1.assert.equal(authentication.getUserID(session), 3);
        });
    });
    describe('getAuthenticationStatus', () => {
        /*
         * getAuthenticationStatus(session)
         *  - session is null
         *      => null
         *  - session does not contain user
         *      => null
         *  - session contains valid status
         *      => status
         */
        it('returns null on null session', async () => {
            // @ts-ignore
            chai_1.assert.equal(authentication.getAuthenticationStatus(null), null);
            // @ts-ignore
            chai_1.assert.equal(authentication.getAuthenticationStatus(undefined), null);
        });
        it('returns null on empty user session', async () => {
            chai_1.assert.equal(authentication.getAuthenticationStatus(new ObjectSession_1.ObjectSession()), null);
        });
        it('returns valid status on valid session', async () => {
            const session = new ObjectSession_1.ObjectSession();
            session.set(`login.provisional`, AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
            chai_1.assert.equal(authentication.getAuthenticationStatus(session), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
        });
    });
    describe('isAuthenticated', () => {
        /*
         * isAuthenticated(session)
         *  - session is null
         *      => false
         *  - session does not contain property
         *      => false
         *  - session is valid
         *      => true
         */
        it('returns false on null session', async () => {
            // @ts-ignore
            chai_1.assert.isFalse(authentication.isAuthenticated(null));
            // @ts-ignore
            chai_1.assert.isFalse(authentication.isAuthenticated(undefined));
        });
        it('returns false on empty user session', async () => {
            chai_1.assert.isFalse(authentication.isAuthenticated(new ObjectSession_1.ObjectSession()));
        });
        it('returns true on valid session', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO));
            chai_1.assert.isTrue(authentication.isAuthenticated(session));
        });
    });
    describe('logout', () => {
        /*
         * logout(session)
         *  - session is null
         *      => 200 + log message
         *  - session is invalid
         *      => 200 + changed session + log message
         *  - session is valid
         *      => 200 + changed session + log message
         */
        it('200 + log message on null session', async () => {
            // @ts-ignore
            const response = await authentication.logout(null);
            chai_1.assert.equal(response.statusCode, 200);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_OUT));
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('200 + changed session + log message on invalid session', async () => {
            // @ts-ignore
            const session = new ObjectSession_1.ObjectSession();
            session.set('login.provisional', 0);
            session.set('login.set', 0);
            session.set('something.else', 0);
            session.set('loginx.one', 1);
            const response = await authentication.logout(session);
            chai_1.assert.equal(response.statusCode, 200);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_OUT));
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.isTrue(session.has('loginx.one'));
            chai_1.assert.isTrue(session.has('something.else'));
            chai_1.assert.isTrue(session.has('login.set'));
            chai_1.assert.isFalse(session.has('login.provisional'));
        });
        it('200 + changed session + log message on valid session', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO));
            chai_1.assert.isTrue(authentication.isAuthenticated(session));
            const response = await authentication.logout(session);
            chai_1.assert.isFalse(authentication.isAuthenticated(session));
            chai_1.assert.equal(response.statusCode, 200);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_OUT));
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
    });
    describe('verifyLoginWithTwoFactor', () => {
        /*
         * twoFactorSubmission(body, session)
         *  - session is null
         *      => 401 + INVALID_REQUEST
         *  - session does not contain user details (provision, userID)
         *      => 401 + INVALID_REQUEST
         *  - session does not require two factor
         *      => 401 + INVALID_REQUEST + log
         *  - body missing code
         *      => 401 + INVALID_REQUEST
         *  - user does not have two factor configuration
         *      => 302 + confirmed session + log
         *  - code is invalid
         *      => 401 + INVALID CODE + log + email
         *  - valid
         *      => 200 + SUCCESS
         */
        it('UNAUTHORIZED + reject on null session', async () => {
            // @ts-ignore
            const response = await authentication.verifyLoginWithTwoFactor({}, null);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            // @ts-ignore
            const response2 = await authentication.verifyLoginWithTwoFactor({}, undefined);
            chai_1.assert.equal(response2.statusCode, ProcessImpl_1.HttpStatusCode.UNAUTHORIZED);
            chai_1.assert.equal(response2.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            // Because we've already logged once
            chai_1.assert.equal(loggingDAO.data.auth.length, 2, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid session', async () => {
            const response = await authentication.verifyLoginWithTwoFactor({}, new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on not requiring two factor', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO), {
                // Use something else, can be removed because it will default to ACTIVE
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            const response = await authentication.verifyLoginWithTwoFactor({}, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on missing code', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO), {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR,
            });
            const response = await authentication.verifyLoginWithTwoFactor({}, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
        });
        it('REDIRECT + reject on user without two factor configuration', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR,
            });
            const response = await authentication.verifyLoginWithTwoFactor({
                code: '123457',
            }, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.REDIRECT);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.NO_NEED_TWO_FACTOR);
            chai_1.assert.isNotNull(response.additionalData);
            chai_1.assert.isTrue(response.additionalData !== undefined);
            // @ts-ignore - checked above
            chai_1.assert.isTrue(response.additionalData.hasOwnProperty(`location`));
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
        });
        it('BAD_REQUEST + reject on invalid code (with email on configuration)', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR,
            });
            // @ts-ignore - ignore wrong type, it matches the model without requiring sequelize
            twoFactorDAO.data.push({
                user,
                id: 0,
                userID: user.id,
                secret: 'secret',
                savedDevices: '',
                emailOnFail: true,
            });
            const response = await authentication.verifyLoginWithTwoFactor({
                code: '123456',
            }, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_TWO_FACTOR);
            chai_1.assert.isTrue(sendMailSpy.calledOnce);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
        });
        it('BAD_REQUEST + reject on invalid code (without email on configuration)', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR,
            });
            // @ts-ignore - ignore wrong type, it matches the model without requiring sequelize
            twoFactorDAO.data.push({
                user,
                id: 0,
                userID: user.id,
                secret: 'secret',
                savedDevices: '',
                emailOnFail: false,
            });
            const response = await authentication.verifyLoginWithTwoFactor({
                code: '123457',
            }, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_TWO_FACTOR);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
        });
        it('200 + pass on valid request', async () => {
            // We have to generate the code and then submit it to make sure it passes
            const secret = otplib_1.authenticator.generateSecret();
            const code = otplib_1.authenticator.generate(secret);
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR,
            });
            // @ts-ignore - ignore wrong type, it matches the model without requiring sequelize
            twoFactorDAO.data.push({
                user,
                secret,
                id: 0,
                userID: user.id,
                savedDevices: '',
                emailOnFail: false,
            });
            const response = await authentication.verifyLoginWithTwoFactor({
                code,
            }, session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN));
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
        });
    });
    describe('login', () => {
        /*
         * login(body, ip, session)
         *  - session contains user
         *      => 401 + LOGGED IN
         *  - missing username
         *      => 401 + BAD REQUEST
         *  - missing password
         *      => 401 + BAD REQUEST
         *  - username is invalid
         *      => 401 + INVALID USERNAME PASSWORD
         *  - password is wrong
         *      => 401 + INVALID USERNAME PASSWORD
         *  - user has not completed registration
         *      => 401 + NOT REGISTERED
         *  - user account is locked
         *      => 401 + LOCKED
         *  - user is from a new ip
         *      => 302 + REDIRECT + SESSION QUESTION + email
         *  - user is from an old ip, needs 2fa
         *      => 302 + REDIRECT + SESSION UPDATE
         *  - valid
         *      => 200 + SESSION UPDATE + log added
         */
        it('BAD_REQUEST + reject on logged in user', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO));
            chai_1.assert.isTrue(authentication.isAuthenticated(session));
            chai_1.assert.equal(authentication.getAuthenticationStatus(session), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
            const response = await authentication.login({}, "ip", session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.AUTHORIZED);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on missing username', async () => {
            const response = await authentication.login({}, "ip", new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on missing password', async () => {
            const response = await authentication.login({
                username: "username",
            }, "ip", new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid username', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            const response = await authentication.login({
                username: "not-the-right-username",
                password: "some password",
            }, "ip", new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid password', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            const response = await authentication.login({
                username: "username",
                password: "some password",
            }, "ip", new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_USERNAME_PASSWORD);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on incomplete registration', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            // Mark this entry as not registered
            userDAO.data[0].registered = false;
            const response = await authentication.login({
                username: "username",
                password: "Test1ngPa55w0rd!",
            }, "ip", new ObjectSession_1.ObjectSession());
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.REGISTRATION_DISABLED);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on locked account', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            await utilities_1.TestingUtilities.buildUser(userDAO);
            await utilities_1.TestingUtilities.buildUser(userDAO);
            // Mark this entry as not registered
            userDAO.data[0].lockedStatus = 'auth';
            userDAO.data[1].username = 'username1';
            userDAO.data[1].lockedStatus = 'admin';
            userDAO.data[2].username = 'username2';
            userDAO.data[2].lockedStatus = 'two_factor_setup';
            const responses = [
                await authentication.login({
                    username: "username",
                    password: "Test1ngPa55w0rd!",
                }, "ip", new ObjectSession_1.ObjectSession()),
                await authentication.login({
                    username: "username1",
                    password: "Test1ngPa55w0rd!",
                }, "ip", new ObjectSession_1.ObjectSession()),
                await authentication.login({
                    username: "username2",
                    password: "Test1ngPa55w0rd!",
                }, "ip", new ObjectSession_1.ObjectSession()),
            ];
            for (const response of responses) {
                chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
                chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.LOCK_STATUS);
            }
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 3, 'expecting logging message');
        });
        it('REDIRECT + redirect with security question on new ip', async () => {
            await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = new ObjectSession_1.ObjectSession();
            const response = await authentication.login({
                username: "username",
                password: "Test1ngPa55w0rd!",
            }, "127.0.0.1", session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.REDIRECT);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.REQUEST_SECURITY_QUESTION);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.isTrue(response.additionalData !== undefined);
            chai_1.assert.isTrue(session.has('login.provisional'));
            chai_1.assert.isTrue(session.has('login.userID'));
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION);
            // @ts-ignore - safe due to the assertion above
            chai_1.assert.isTrue(response.additionalData.hasOwnProperty('redirect'));
            // Note: Security question ID is not set here, that happens on the question page
        });
        it('REDIRECT + redirect with two factor on user with 2fa configuration', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = new ObjectSession_1.ObjectSession();
            // @ts-ignore
            loggingDAO.data.login.push({
                id: 0,
                userID: user.id,
                ipAddress: "127.0.0.1",
                logged: new Date(),
            });
            // @ts-ignore
            twoFactorDAO.data.push({
                user,
                id: 0,
                userID: user.id,
                secret: 'secret',
                savedDevices: '',
                emailOnFail: false,
            });
            const response = await authentication.login({
                username: "username",
                password: "Test1ngPa55w0rd!",
            }, "127.0.0.1", session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.REDIRECT);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.isTrue(response.additionalData !== undefined);
            chai_1.assert.isTrue(session.has('login.provisional'));
            chai_1.assert.isTrue(session.has('login.userID'));
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_TWO_FACTOR);
            // @ts-ignore - safe due to the assertion above
            chai_1.assert.isTrue(response.additionalData.hasOwnProperty('redirect'));
        });
        it('SUCCESS + pass on valid request', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = new ObjectSession_1.ObjectSession();
            // @ts-ignore
            loggingDAO.data.login.push({
                id: 0,
                userID: user.id,
                ipAddress: "127.0.0.1",
                logged: new Date(),
            });
            const response = await authentication.login({
                username: "username",
                password: "Test1ngPa55w0rd!",
            }, "127.0.0.1", session);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN));
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.isTrue(session.has('login.provisional'));
            chai_1.assert.isTrue(session.has('login.userID'));
            chai_1.assert.equal(session.get('login.provisional'), AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.ACTIVE);
        });
    });
    describe('verifyLoginWithSecurityQuestion', () => {
        /*
         * verifyLoginWithSecurityQuestions(body, session)
         *  - session missing user
         *      => 401 + INVALID
         *  - session has invalid status
         *      => 401 + INVALID
         *  - session doesnt contain user
         *      => 401 + INVLAID
         *  - body is missing answer
         *      => 401 INVALID
         *  - session is missing question
         *      => 401 INVALID
         *  - answer is invalid
         *      => 401 INVALID ANSWER
         *  - valid, but needs two factor as well
         *      => 200 REDIRECT + SESSION UPDATE
         *  - valid
         *      => 200 SESSION UPDATE
         */
        it('BAD_REQUEST + reject on missing session user', async () => {
            const response = await authentication.verifyLoginWithSecurityQuestion({}, new ObjectSession_1.ObjectSession(), 'ip');
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid session', async () => {
            const session = new ObjectSession_1.ObjectSession();
            const response = await authentication.verifyLoginWithSecurityQuestion({}, session, 'ip');
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('REDIRECT + reject on not requiring security question', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO));
            session.set('login.securityQuestion', 0);
            const response = await authentication.verifyLoginWithSecurityQuestion({}, session, 'ip');
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.NO_NEED_TWO_FACTOR);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.REDIRECT);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on missing answer', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO), {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            session.set('login.securityQuestion', 0);
            const response = await authentication.verifyLoginWithSecurityQuestion({}, session, 'ip');
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INVALID_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid question code', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO), {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            session.set('login.securityQuestion', 1);
            const response = await authentication.verifyLoginWithSecurityQuestion({
                answer: '',
            }, session, 'ip');
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('INTERNAL_SERVER_ERROR + reject on missing question', async () => {
            const session = utilities_1.TestingUtilities.buildSession(await utilities_1.TestingUtilities.buildUser(userDAO), {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            const response = await authentication.verifyLoginWithSecurityQuestion({}, session, 'ip');
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.INTERNAL_SERVER_ERROR);
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.INTERNAL_SERVER_ERROR);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('BAD_REQUEST + reject on invalid answer', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            session.set('login.securityQuestion', 0);
            // @ts-ignore
            const question = {
                id: 0,
                question: 'Question',
            };
            securityQuestionDAO.data.questions.push(question);
            // @ts-ignore
            securityQuestionDAO.data.mapping.push({
                user,
                id: 0,
                userID: user.id,
                securityQuestionID: 0,
                securityQuestion: question,
                additionalData: '',
                answerHash: `${utilities_1.TestingUtilities.testingHash.password.version}:${utilities_1.TestingUtilities.testingHash.password.hash}`,
            });
            const response = await authentication.verifyLoginWithSecurityQuestion({
                answer: 'something',
            }, session, 'ip');
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.CHECK_YOUR_RESPONSE);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.BAD_REQUEST);
            chai_1.assert.isTrue(sendMailSpy.calledOnce);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
        });
        it('REDIRECT + redirect on user with two factor and valid answer', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            session.set('login.securityQuestion', 0);
            // @ts-ignore
            twoFactorDAO.data.push({
                user,
                id: 0,
                userID: user.id,
                secret: 'something',
                savedDevices: '',
                emailOnFail: false,
            });
            // @ts-ignore
            const question = {
                id: 0,
                question: 'Question',
            };
            securityQuestionDAO.data.questions.push(question);
            // @ts-ignore
            securityQuestionDAO.data.mapping.push({
                user,
                id: 0,
                userID: user.id,
                securityQuestionID: 0,
                securityQuestion: question,
                additionalData: '',
                answerHash: `${utilities_1.TestingUtilities.testingHash.password.version}:${utilities_1.TestingUtilities.testingHash.password.hash}`,
            });
            const response = await authentication.verifyLoginWithSecurityQuestion({
                answer: 'Test1ngPa55w0rd!',
            }, session, 'ip');
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.REQUIRE_ADDITIONAL_STEP);
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.REDIRECT);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(loggingDAO.data.login.length, 0);
        });
        it('SUCCESS + pass on valid request', async () => {
            const user = await utilities_1.TestingUtilities.buildUser(userDAO);
            const session = utilities_1.TestingUtilities.buildSession(user, {
                'login.provisional': AuthenticationProcessImpl_1.ProvisionalAuthenticationStatus.REQUIRES_SECURITY_QUESTION,
            });
            session.set('login.securityQuestion', 0);
            // @ts-ignore
            const question = {
                id: 0,
                question: 'Question',
            };
            securityQuestionDAO.data.questions.push(question);
            // @ts-ignore
            securityQuestionDAO.data.mapping.push({
                user,
                id: 0,
                userID: user.id,
                securityQuestionID: 0,
                securityQuestion: question,
                additionalData: '',
                answerHash: `${utilities_1.TestingUtilities.testingHash.password.version}:${utilities_1.TestingUtilities.testingHash.password.hash}`,
            });
            const response = await authentication.verifyLoginWithSecurityQuestion({
                answer: 'Test1ngPa55w0rd!',
            }, session, 'ip');
            chai_1.assert.equal(response.externalMessage, PUBLIC_RESPONSES.SUCCESSFUL(PUBLIC_SUBSTITUTIONS.LOGGED_IN));
            chai_1.assert.equal(response.statusCode, ProcessImpl_1.HttpStatusCode.SUCCESS);
            chai_1.assert.isTrue(sendMailSpy.notCalled);
            chai_1.assert.equal(loggingDAO.data.auth.length, 1, 'expecting logging message');
            chai_1.assert.equal(loggingDAO.data.login.length, 1);
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1BdXRoZW50aWNhdGlvblByb2Nlc3NJbXBsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2Nhc3ZpaS90ZXN0L2FjdGlvbnMvdGVzdC1BdXRoZW50aWNhdGlvblByb2Nlc3NJbXBsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGtFQUErRDtBQUMvRCwwQ0FBNEI7QUFDNUIsOERBQWdEO0FBQ2hELGlDQUE2QjtBQUM3QiwrRkFHeUQ7QUFDekQsK0JBQThCO0FBQzlCLDhFQUEyRTtBQUMzRSw0Q0FBZ0Q7QUFDaEQseURBQXNEO0FBQ3RELDJEQUF3RTtBQUN4RSxtQ0FBdUM7QUFFdkMsSUFBTyxnQkFBZ0IsR0FBRyxxQkFBUyxDQUFDLGdCQUFnQixDQUFDO0FBQ3JELElBQU8sb0JBQW9CLEdBQUcscUJBQVMsQ0FBQyxvQkFBb0IsQ0FBQztBQUU3RCxJQUFJLGNBQXdDLENBQUM7QUFFN0MsSUFBSSxNQUF3QixDQUFDO0FBQzdCLElBQUksWUFBNkMsQ0FBQztBQUNsRCxJQUFJLG1CQUEyRCxDQUFDO0FBQ2hFLElBQUksT0FBbUMsQ0FBQztBQUN4QyxJQUFJLFVBQXlDLENBQUM7QUFFOUMsSUFBSSxXQUEyQixDQUFDO0FBRWhDOztHQUVHO0FBQ0gsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO0lBQ2QsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXBDLFdBQVcsR0FBRyxZQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRWhDLE1BQU0sU0FBUyxHQUFHO1FBQ2QsUUFBUSxFQUFFLFdBQVc7S0FDeEIsQ0FBQztJQUVGLCtDQUErQztJQUMvQyxNQUFNLEdBQUcsSUFBSSxtQ0FBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUV6QyxZQUFZLEdBQUcsSUFBSSxVQUFVLENBQUMsb0JBQW9CLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkQsbUJBQW1CLEdBQUcsSUFBSSxVQUFVLENBQUMsMkJBQTJCLENBQUMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2pHLE9BQU8sR0FBRyxJQUFJLFVBQVUsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDN0MsVUFBVSxHQUFHLElBQUksVUFBVSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUV4RSxjQUFjLEdBQUcscURBQXlCLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0FBQ3pILENBQUMsQ0FBQyxDQUFDO0FBRUg7O0dBRUc7QUFDSCxVQUFVLENBQUMsS0FBSyxJQUFJLEVBQUU7SUFDbEIsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBRTNCLFlBQVksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLG1CQUFtQixDQUFDLElBQUksR0FBRyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDO0lBQzFELE9BQU8sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ2xCLFVBQVUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQztBQUM5QyxDQUFDLENBQUMsQ0FBQztBQUVIOzs7O0dBSUc7QUFDSCxNQUFNLFdBQVcsR0FBRztJQUNoQixRQUFRLEVBQUU7UUFDTixPQUFPLEVBQUUsQ0FBQztRQUNWLElBQUksRUFBRSxrR0FBa0c7S0FDM0c7SUFDRCxHQUFHLEVBQUU7UUFDRCxPQUFPLEVBQUUsQ0FBQztRQUNWLElBQUksRUFBRSxrR0FBa0c7S0FDM0c7Q0FDSixDQUFDO0FBRUYsUUFBUSxDQUFDLHVEQUF1RCxFQUFFLEdBQUcsRUFBRTtJQUNuRSxRQUFRLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRTtRQUNyQjs7Ozs7Ozs7OztXQVVHO1FBRUgsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzFDLGFBQWE7WUFDYixhQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN2RCxhQUFhO1lBQ2IsYUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLGNBQWMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDaEUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsb0NBQW9DLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDaEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSw2QkFBYSxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3QyxNQUFNLE9BQU8sR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUVoQyxhQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxtQ0FBbUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMvQyxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFcEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFO1FBQ3ZCOzs7Ozs7OztXQVFHO1FBRUgsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzFDLGFBQWE7WUFDYixhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDbkQsYUFBYTtZQUNiLGFBQU0sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM1RCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxvQ0FBb0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNoRCxhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSw2QkFBYSxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyw4QkFBOEIsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMxQyxNQUFNLE9BQU8sR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUUvQixhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyx5QkFBeUIsRUFBRSxHQUFHLEVBQUU7UUFDckM7Ozs7Ozs7O1dBUUc7UUFFSCxFQUFFLENBQUMsOEJBQThCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDMUMsYUFBYTtZQUNiLGFBQU0sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2pFLGFBQWE7WUFDYixhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxvQ0FBb0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNoRCxhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BGLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHVDQUF1QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ25ELE1BQU0sT0FBTyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO1lBRXBDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLEVBQUUsMkRBQStCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUcsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7UUFDN0I7Ozs7Ozs7O1dBUUc7UUFFSCxFQUFFLENBQUMsK0JBQStCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDM0MsYUFBYTtZQUNiLGFBQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3JELGFBQWE7WUFDYixhQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxxQ0FBcUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNqRCxhQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLCtCQUErQixFQUFFLEtBQUssSUFBSSxFQUFFO1lBQzNDLE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBRXpGLGFBQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtRQUNwQjs7Ozs7Ozs7V0FRRztRQUVILEVBQUUsQ0FBQyxtQ0FBbUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMvQyxhQUFhO1lBQ2IsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRW5ELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDckcsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsd0RBQXdELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDcEUsYUFBYTtZQUNiLE1BQU0sT0FBTyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO1lBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUU3QixNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFdEQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNyRyxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMxRSxhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUN6QyxhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQzdDLGFBQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLGFBQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7UUFDckQsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsc0RBQXNELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbEUsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFekYsYUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFdkQsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXRELGFBQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN2QyxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDckcsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQywwQkFBMEIsRUFBRSxHQUFHLEVBQUU7UUFDdEM7Ozs7Ozs7Ozs7Ozs7Ozs7V0FnQkc7UUFFSCxFQUFFLENBQUMsdUNBQXVDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbkQsYUFBYTtZQUNiLE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUV6RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMvRCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFFMUUsYUFBYTtZQUNiLE1BQU0sU0FBUyxHQUFHLE1BQU0sY0FBYyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUUvRSxhQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNoRSxhQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDMUUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsb0NBQW9DO1lBQ3BDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHlDQUF5QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3JELE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRXhGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxrREFBa0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM5RCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3JGLHVFQUF1RTtnQkFDdkUsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsMEJBQTBCO2FBQ2xGLENBQUMsQ0FBQztZQUNILE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUU1RSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsc0NBQXNDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbEQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNyRixtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQyxtQkFBbUI7YUFDM0UsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsd0JBQXdCLENBQUMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRTVFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMxRSxhQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsRUFBRSwyREFBK0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3hHLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDREQUE0RCxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3hFLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2hELG1CQUFtQixFQUFFLDJEQUErQixDQUFDLG1CQUFtQjthQUMzRSxDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDM0QsSUFBSSxFQUFFLFFBQVE7YUFDakIsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVaLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVFLGFBQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzFDLGFBQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsQ0FBQztZQUNyRCw2QkFBNkI7WUFDN0IsYUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2xFLGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDeEcsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsb0VBQW9FLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDaEYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtnQkFDaEQsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsbUJBQW1CO2FBQzNFLENBQUMsQ0FBQztZQUVILG1GQUFtRjtZQUNuRixZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsSUFBSTtnQkFDSixFQUFFLEVBQUUsQ0FBQztnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixXQUFXLEVBQUUsSUFBSTthQUNwQixDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDM0QsSUFBSSxFQUFFLFFBQVE7YUFDakIsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVaLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVFLGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDeEcsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsdUVBQXVFLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbkYsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtnQkFDaEQsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsbUJBQW1CO2FBQzNFLENBQUMsQ0FBQztZQUVILG1GQUFtRjtZQUNuRixZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsSUFBSTtnQkFDSixFQUFFLEVBQUUsQ0FBQztnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixXQUFXLEVBQUUsS0FBSzthQUNyQixDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDM0QsSUFBSSxFQUFFLFFBQVE7YUFDakIsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVaLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVFLGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDeEcsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsNkJBQTZCLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDekMseUVBQXlFO1lBQ3pFLE1BQU0sTUFBTSxHQUFHLHNCQUFhLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDOUMsTUFBTSxJQUFJLEdBQUcsc0JBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFNUMsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtnQkFDaEQsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsbUJBQW1CO2FBQzNFLENBQUMsQ0FBQztZQUVILG1GQUFtRjtZQUNuRixZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsSUFBSTtnQkFDSixNQUFNO2dCQUNOLEVBQUUsRUFBRSxDQUFDO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDZixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsV0FBVyxFQUFFLEtBQUs7YUFDckIsQ0FBQyxDQUFDO1lBRUgsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsd0JBQXdCLENBQUM7Z0JBQzNELElBQUk7YUFDUCxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRVosYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3BHLGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRTtRQUNuQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztXQXNCRztRQUVILEVBQUUsQ0FBQyx3Q0FBd0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNwRCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUV6RixhQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN2RCxhQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsRUFBRSwyREFBK0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUV0RyxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztZQUUvRCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMENBQTBDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDdEQsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUUzRSxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDekUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMENBQTBDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDdEQsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsS0FBSyxDQUFDO2dCQUN4QyxRQUFRLEVBQUUsVUFBVTthQUN2QixFQUFFLElBQUksRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxDQUFDO1lBRTlCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQywwQ0FBMEMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUN0RCxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hDLFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLFFBQVEsRUFBRSxlQUFlO2FBQzVCLEVBQUUsSUFBSSxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDLENBQUM7WUFFOUIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbkYsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMENBQTBDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDdEQsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUMsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsS0FBSyxDQUFDO2dCQUN4QyxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsUUFBUSxFQUFFLGVBQWU7YUFDNUIsRUFBRSxJQUFJLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUU5QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNuRixhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpREFBaUQsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3RCxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxvQ0FBb0M7WUFDcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBRW5DLE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLEtBQUssQ0FBQztnQkFDeEMsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFFBQVEsRUFBRSxrQkFBa0I7YUFDL0IsRUFBRSxJQUFJLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUMsQ0FBQztZQUU5QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5RCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvRSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyx3Q0FBd0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNwRCxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxvQ0FBb0M7WUFDcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO1lBRXRDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztZQUN2QyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUM7WUFFdkMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLGtCQUFrQixDQUFDO1lBRWxELE1BQU0sU0FBUyxHQUFHO2dCQUNkLE1BQU0sY0FBYyxDQUFDLEtBQUssQ0FBQztvQkFDdkIsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRSxrQkFBa0I7aUJBQy9CLEVBQUUsSUFBSSxFQUFFLElBQUksNkJBQWEsRUFBRSxDQUFDO2dCQUM3QixNQUFNLGNBQWMsQ0FBQyxLQUFLLENBQUM7b0JBQ3ZCLFFBQVEsRUFBRSxXQUFXO29CQUNyQixRQUFRLEVBQUUsa0JBQWtCO2lCQUMvQixFQUFFLElBQUksRUFBRSxJQUFJLDZCQUFhLEVBQUUsQ0FBQztnQkFDN0IsTUFBTSxjQUFjLENBQUMsS0FBSyxDQUFDO29CQUN2QixRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLGtCQUFrQjtpQkFDL0IsRUFBRSxJQUFJLEVBQUUsSUFBSSw2QkFBYSxFQUFFLENBQUM7YUFDaEMsQ0FBQztZQUVGLEtBQUssTUFBTSxRQUFRLElBQUksU0FBUyxFQUFFO2dCQUM5QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDOUQsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3hFO1lBQ0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsc0RBQXNELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDbEUsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUMsTUFBTSxPQUFPLEdBQUcsSUFBSSw2QkFBYSxFQUFFLENBQUM7WUFDcEMsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsS0FBSyxDQUFDO2dCQUN4QyxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsUUFBUSxFQUFFLGtCQUFrQjthQUMvQixFQUFFLFdBQVcsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUV6QixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzRCxhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNuRixhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMxRSxhQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLENBQUM7WUFDckQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUNoRCxhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMzQyxhQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsRUFBRSwyREFBK0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1lBQzNHLCtDQUErQztZQUMvQyxhQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDbEUsZ0ZBQWdGO1FBQ3BGLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLG9FQUFvRSxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ2hGLE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLElBQUksNkJBQWEsRUFBRSxDQUFDO1lBRXBDLGFBQWE7WUFDYixVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLEVBQUUsRUFBRSxDQUFDO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDZixTQUFTLEVBQUUsV0FBVztnQkFDdEIsTUFBTSxFQUFFLElBQUksSUFBSSxFQUFFO2FBQ3JCLENBQUMsQ0FBQztZQUNILGFBQWE7WUFDYixZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbkIsSUFBSTtnQkFDSixFQUFFLEVBQUUsQ0FBQztnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLFlBQVksRUFBRSxFQUFFO2dCQUNoQixXQUFXLEVBQUUsS0FBSzthQUNyQixDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hDLFFBQVEsRUFBRSxVQUFVO2dCQUNwQixRQUFRLEVBQUUsa0JBQWtCO2FBQy9CLEVBQUUsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRXpCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzNELGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ2pGLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsQ0FBQztZQUNyRCxhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO1lBQ2hELGFBQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzNDLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDcEcsK0NBQStDO1lBQy9DLGFBQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUN0RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3QyxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztZQUVwQyxhQUFhO1lBQ2IsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUN2QixFQUFFLEVBQUUsQ0FBQztnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQ2YsU0FBUyxFQUFFLFdBQVc7Z0JBQ3RCLE1BQU0sRUFBRSxJQUFJLElBQUksRUFBRTthQUNyQixDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hDLFFBQVEsRUFBRSxVQUFVO2dCQUNwQixRQUFRLEVBQUUsa0JBQWtCO2FBQy9CLEVBQUUsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRXpCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFELGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNwRyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMxRSxhQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO1lBQ2hELGFBQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzNDLGFBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLDJEQUErQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsaUNBQWlDLEVBQUUsR0FBRyxFQUFFO1FBQzdDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7V0FrQkc7UUFFSCxFQUFFLENBQUMsOENBQThDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDMUQsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsK0JBQStCLENBQUMsRUFBRSxFQUFFLElBQUksNkJBQWEsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRXJHLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyx5Q0FBeUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNyRCxNQUFNLE9BQU8sR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztZQUVwQyxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQywrQkFBK0IsQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRXpGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxzREFBc0QsRUFBRSxLQUFLLElBQUksRUFBRTtZQUNsRSxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN6RixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRXpDLE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLCtCQUErQixDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFekYsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDNUUsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0QsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsd0NBQXdDLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDcEQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNyRixtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQywwQkFBMEI7YUFDbEYsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUV6QyxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQywrQkFBK0IsQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRXpGLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzlELGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RSxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQywrQ0FBK0MsRUFBRSxLQUFLLElBQUksRUFBRTtZQUMzRCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3JGLG1CQUFtQixFQUFFLDJEQUErQixDQUFDLDBCQUEwQjthQUNsRixDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRXpDLE1BQU0sUUFBUSxHQUFHLE1BQU0sY0FBYyxDQUFDLCtCQUErQixDQUFDO2dCQUNsRSxNQUFNLEVBQUUsRUFBRTthQUNiLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRWxCLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDeEUsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsb0RBQW9ELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDaEUsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNyRixtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQywwQkFBMEI7YUFDbEYsQ0FBQyxDQUFDO1lBRUgsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsK0JBQStCLENBQUMsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUV6RixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3hFLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQy9FLGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlFLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHdDQUF3QyxFQUFFLEtBQUssSUFBSSxFQUFFO1lBQ3BELE1BQU0sSUFBSSxHQUFHLE1BQU0sNEJBQWdCLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sT0FBTyxHQUFHLDRCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2hELG1CQUFtQixFQUFFLDJEQUErQixDQUFDLDBCQUEwQjthQUNsRixDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLGFBQWE7WUFDYixNQUFNLFFBQVEsR0FBcUI7Z0JBQy9CLEVBQUUsRUFBRSxDQUFDO2dCQUNMLFFBQVEsRUFBRSxVQUFVO2FBQ3ZCLENBQUM7WUFDRixtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsRCxhQUFhO1lBQ2IsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2xDLElBQUk7Z0JBQ0osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO2dCQUNmLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3JCLGdCQUFnQixFQUFFLFFBQVE7Z0JBQzFCLGNBQWMsRUFBRSxFQUFFO2dCQUNsQixVQUFVLEVBQUUsR0FBRyw0QkFBZ0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSw0QkFBZ0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRTthQUMvRyxDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQywrQkFBK0IsQ0FBQztnQkFDbEUsTUFBTSxFQUFFLFdBQVc7YUFDdEIsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFbEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDN0UsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLDRCQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUQsYUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdEMsYUFBTSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsOERBQThELEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDMUUsTUFBTSxJQUFJLEdBQUcsTUFBTSw0QkFBZ0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkQsTUFBTSxPQUFPLEdBQUcsNEJBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRTtnQkFDaEQsbUJBQW1CLEVBQUUsMkRBQStCLENBQUMsMEJBQTBCO2FBQ2xGLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekMsYUFBYTtZQUNiLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuQixJQUFJO2dCQUNKLEVBQUUsRUFBRSxDQUFDO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDZixNQUFNLEVBQUUsV0FBVztnQkFDbkIsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFdBQVcsRUFBRSxLQUFLO2FBQ3JCLENBQUMsQ0FBQztZQUNILGFBQWE7WUFDYixNQUFNLFFBQVEsR0FBcUI7Z0JBQy9CLEVBQUUsRUFBRSxDQUFDO2dCQUNMLFFBQVEsRUFBRSxVQUFVO2FBQ3ZCLENBQUM7WUFDRixtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsRCxhQUFhO1lBQ2IsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2xDLElBQUk7Z0JBQ0osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFO2dCQUNmLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3JCLGdCQUFnQixFQUFFLFFBQVE7Z0JBQzFCLGNBQWMsRUFBRSxFQUFFO2dCQUNsQixVQUFVLEVBQUUsR0FBRyw0QkFBZ0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSw0QkFBZ0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRTthQUMvRyxDQUFDLENBQUM7WUFFSCxNQUFNLFFBQVEsR0FBRyxNQUFNLGNBQWMsQ0FBQywrQkFBK0IsQ0FBQztnQkFDbEUsTUFBTSxFQUFFLGtCQUFrQjthQUM3QixFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUVsQixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUNqRixhQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsNEJBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzRCxhQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNyQyxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztZQUMxRSxhQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpQ0FBaUMsRUFBRSxLQUFLLElBQUksRUFBRTtZQUM3QyxNQUFNLElBQUksR0FBRyxNQUFNLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxNQUFNLE9BQU8sR0FBRyw0QkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFO2dCQUNoRCxtQkFBbUIsRUFBRSwyREFBK0IsQ0FBQywwQkFBMEI7YUFDbEYsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN6QyxhQUFhO1lBQ2IsTUFBTSxRQUFRLEdBQXFCO2dCQUMvQixFQUFFLEVBQUUsQ0FBQztnQkFDTCxRQUFRLEVBQUUsVUFBVTthQUN2QixDQUFDO1lBQ0YsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEQsYUFBYTtZQUNiLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNsQyxJQUFJO2dCQUNKLEVBQUUsRUFBRSxDQUFDO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTtnQkFDZixrQkFBa0IsRUFBRSxDQUFDO2dCQUNyQixnQkFBZ0IsRUFBRSxRQUFRO2dCQUMxQixjQUFjLEVBQUUsRUFBRTtnQkFDbEIsVUFBVSxFQUFFLEdBQUcsNEJBQWdCLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksNEJBQWdCLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUU7YUFDL0csQ0FBQyxDQUFDO1lBRUgsTUFBTSxRQUFRLEdBQUcsTUFBTSxjQUFjLENBQUMsK0JBQStCLENBQUM7Z0JBQ2xFLE1BQU0sRUFBRSxrQkFBa0I7YUFDN0IsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFbEIsYUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3BHLGFBQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSw0QkFBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFELGFBQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3JDLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzFFLGFBQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQyJ9